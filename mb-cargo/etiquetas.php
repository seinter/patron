<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require_once("pdf/fpdf.php");

include('classes/phpqrcode.php');
//include("barcode/barcode.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$pdf = new FPDF('P','in','Letter');
$pdf->AddPage();

$str = "select bitacora.id, bitacora.codigo, id_venta, id_dest, local1.localidad origen, local2.localidad destino, clase, IF(productos.id, producto, detalle_venta.descripcion) producto from ventas inner join detalle_venta on ventas.id = detalle_venta.id_venta inner join clasificacion on detalle_venta.id_clase = clasificacion.id inner join bitacora on detalle_venta.id = bitacora.id_detalle inner join localidades local1 on ventas.origen = local1.id inner join localidades local2 on ventas.destino = local2.id left join productos on detalle_venta.id_producto = productos.id where md5(bitacora.id) = '$id';";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$$key = $value;


$str = "select nombre, telefono, direccion, id_pais, id_depto, zip, paises.pais from destinatarios inner join paises on destinatarios.id_pais = paises.id where destinatarios.id = $id_dest;";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$dest[$key] = $value;

switch($dest["id_pais"]){
case 225:
	$str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$dest[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0].", ".$row[1]." ".$dest["zip"];
break;
case 136:
	$str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$dest[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0].", ".$row[1];
break;
default:
	$str = "select departamento from departamentos where id = {$dest[id_depto]};";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0];
break;
}

for($ox=0;$ox<8.5;$ox+=4.25){
	for($oy=0;$oy<11;$oy+=5.5){
		$x = $ox;
		$y = $oy;
		$pdf->Image('images/print.jpg', $x, $y, 2.867, 1.3);
		$pdf->SetFont('Helvetica','B',15);
		
		
		$x += .25;
		$y += .3;
		$pdf->SetXY($x,$y);
		$pdf->Cell(3.7,.25,formatCode($id_venta)." / ".formatCode($id),0,1,'R',0);

		$pdf->SetFont('Helvetica','B',10);
        $pdf->SetX($x);
        $pdf->Cell(3.7,.25, $codigo . ' ',0,0,'R',0);

		$pdf->SetFont('Helvetica','B',10);
		$y += 1.2;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, 'DESTINO / DESTINATION:');
		
		$pdf->SetFont('Helvetica','',9);
		$y += .2;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, strtoupper($origen." - ".$destino));
		
		
		$pdf->SetFont('Helvetica','B',10);
		$y += .3;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, 'DESTINATARIO / ADDRESSEE:');
		
		$pdf->SetFont('Helvetica','',9);
		$y += .2;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, utf8_decode($dest["nombre"]));
		

		/*
		$pdf->SetFont('Helvetica','B',10);
		$y += .3;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, 'TELEFONO / PHONE:');
		
		$pdf->SetFont('Helvetica','',9);
		$y += .2;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, $dest["telefono"]);
		
		
		$pdf->SetFont('Helvetica','B',10);
		$y += .3;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, 'DIRECCION / ADDRESS:');
		
		$pdf->SetFont('Helvetica','',9);
		$y += .2;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, utf8_decode($dest["direccion"]));
		$y += .175;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, utf8_decode($dest["direccion2"]));
		$y += .175;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, $dest["pais"]);
		*/

		$pdf->SetFont('Helvetica','B',10);
		$y += .3;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, 'PRODUCTO / PRODUCT:');

		$pdf->SetFont('Helvetica','',9);
		$y += .2;
		$pdf->SetXY($x,$y);
		$pdf->Write(.25, strtoupper($clase." - ".$producto));

		// $y = $oy + 4.1;
        $y = $oy + 4;


        /*
         * OLD CODE39 BARCODE
		$barcode = formatBarCode($id);
		create_barcode($barcode);
		$pdf->Image('tmp/'.$barcode.'.png', $x, $y, 3.75,.694);
        */

        QRCode::png($codigo, 'tmp/'.$codigo.'.png', QR_ECLEVEL_H, 10, 2);
        $pdf->Image('tmp/'.$codigo.'.png', $x + 2.7, $y, 1,1);


	}
}

$pdf->Output();
unlink('tmp/'.$codigo.'.png');

?>