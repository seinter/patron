<?php
session_start();
require("config.php");
include("classes/system.inc.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();
$ida = $user->id_central ?: $user->id_agencia;

/*
error_reporting(-1);
ini_set('display_errors', true);
*/

/*
// SEND ERROR
http_response_code(500);
exit();
*/

$query = "SELECT clientes.id, clientes.casillero box, CONCAT(clientes.nombre, ' ', clientes.apellido) name, clientes.telefono phone, COUNT(entradas.id) cnt FROM clientes LEFT JOIN entradas ON clientes.id = entradas.id_cliente AND (entradas.cantidad > JSON_LENGTH(IF(entradas.empaque IS NULL, JSON_ARRAY(), entradas.empaque))) AND entradas.status WHERE clientes.id_agencia = {$ida} AND clientes.casillero AND clientes.status GROUP BY clientes.id";

switch($_REQUEST['act']){
    case 1:
        // LOAD WAREHOUSE DIAGRAM
        $str = "SELECT casilleros boxes, columnas width, diagrama diagram FROM agencias WHERE id = {$ida}";
        $res = mysql_query($str, $link);
        extract(mysql_fetch_assoc($res));
        $diagram = json_decode($diagram);

        $grid = false;
        if($boxes){
            $grid = $diagram;
            foreach($grid as $key => $value)
                $grid[$key]->id = 0;
            $res = mysql_query($query, $link);
            while ($row = mysql_fetch_assoc($res)) {
                $row['name'] = htmlspecialchars($row['name']);
                $grid[$row['box'] - 1] = (object)array_merge((array)$grid[$row['box'] - 1], $row);
            }

        }

        $data = ['width' => $width, 'grid' => $grid];

        header('Content-type: application/json');
        print json_encode($data, JSON_NUMERIC_CHECK);
        exit();

    break;
    case 2:

        $str = 'UPDATE agencias SET casilleros = ' . $_POST['casilleros'] . ', columnas = ' . $_POST['columnas'] . ', diagrama = \'' . $_POST['diagrama'] . '\', id_autoriza = ' . $user->id . ' WHERE id = ' . $ida;
        mysql_query($str, $link);

        header('Location: ?');
        exit();

    break;
    case 3:
        // ADD / EDIT WAREHOUSE DIAGRAM
        $diagram = json_decode($_REQUEST['diagrama']);
        $boxes = count($diagram);
        $width = +$_REQUEST['columnas'];

        $max_box = 0;
        $res = mysql_query("SELECT MAX(casillero) FROM clientes WHERE id_agencia = {$ida} AND status", $link);
        $max_box = +mysql_result($res, 0);

        if($max_box)
            while(+$_REQUEST['casilleros'] < $max_box)
                $_REQUEST['casilleros'] += 10;

        if(+$_REQUEST['casilleros'] > $boxes){
            // ADD
            $grid = $diagram;
            $new = array_fill(0, (+$_REQUEST['casilleros'] - $boxes), 0);

            if($new){
                $max_row = 0;
                foreach($grid as $value)
                    $max_row = ($item = +$value->row + ($value->size_y - 1)) > $max_row ? $item : $max_row;

                $new = array_chunk($new, $width);
                // DEFAULTS
                for ($i = 0; $i < count($new); $i++)
                    for ($j = 0; $j < count($new[$i]); $j++)
                        $grid[] = (object) ['col' => ($j + 1), 'row' => ($i + $max_row + 1), 'size_x' => 1, 'size_y' => 1];
            }

        } else {
            // REMOVE
            $grid = array_slice($diagram, 0, +$_REQUEST['casilleros']);

        }

        if($grid) {
            foreach($grid as $key => $value)
                $grid[$key]->id = 0;

            $res = mysql_query($query, $link);
            while ($row = mysql_fetch_assoc($res)) {
                $row['name'] = htmlspecialchars($row['name']);
                $grid[$row['box'] - 1] = (object)array_merge((array)$grid[$row['box'] - 1], $row);
            }

        } else
            $grid = false;

        $data = ['width' => $width, 'grid' => $grid];

        header('Content-type: application/json');
        print json_encode($data, JSON_NUMERIC_CHECK);
        exit();

    break;
    case 4:
        // ADD / EDIT WAREHOUSE DIAGRAM
        $_REQUEST['diagrama'] = json_decode($_REQUEST['diagrama']);
        $boxes = count($_REQUEST['diagrama']);
        $width = +$_REQUEST['columnas'];

        $max_box = 0;
        $res = mysql_query("SELECT MAX(casillero) FROM clientes WHERE id_agencia = {$ida} AND status", $link);
        $max_box = +mysql_result($res, 0);

        if($max_box)
            while(+$_REQUEST['casilleros'] < $max_box)
                $_REQUEST['casilleros'] += 10;

        $grid = false;
        if($boxes || +$_REQUEST['casilleros']) {

            $grid = $_REQUEST['diagrama'];

            // RELOCATE
            $relocate = [];
            foreach($grid as $key => $value)
                if(($item = +$value->col + ($value->size_x - 1)) > $width)
                    $relocate[] = &$grid[$key];

            // ADD
            if($new = array_fill(0, (+$_REQUEST['casilleros'] - $boxes), 0))
                foreach($new as $value) {
                    $grid[] = (object) [];
                    $relocate[] = &$grid[count($grid) - 1];
                }

            if($relocate) {

                $max_row = 0;
                $diff = array_filter($grid, function($item) use ($relocate) {
                    return !in_array($item, $relocate);
                });
                foreach($diff as $value)
                    $max_row = ($item = +$value->row + ($value->size_y - 1)) > $max_row ? $item : $max_row;

                $relocate = array_chunk($relocate, $width);

                // APPLY DEFAULTS
                for ($i = 0; $i < count($relocate); $i++)
                    for ($j = 0; $j < count($relocate[$i]); $j++)
                        $relocate[$i][$j] = (object) ['col' => ($j + 1), 'row' => ($i + $max_row + 1), 'size_x' => 1, 'size_y' => 1];


            }

            foreach($grid as $key => $value)
                $grid[$key]->id = 0;

            $res = mysql_query($query, $link);
            while ($row = mysql_fetch_assoc($res)){
                $row['name'] = htmlspecialchars($row['name']);
                $grid[$row['box'] - 1] = (object) array_merge((array) $grid[$row['box'] - 1], $row);
            }

        } else
            $width = $_REQUEST['columnas'];

        $data = ['width' => $width, 'grid' => $grid];

        header('Content-type: application/json');
        print json_encode($data, JSON_NUMERIC_CHECK);
        exit();

    break;

    case 5:

        // MOVE CLIENT
        $res = mysql_query("SELECT COUNT(*) FROM clientes WHERE id != " . $_POST['idc'] . " AND id_agencia = {$ida} AND casillero = " . $_POST['box'] . " AND status", $link);
        if (!mysql_result($res, 0)) {
            mysql_query($str = 'UPDATE clientes SET casillero = ' . $_POST['box'] . ' WHERE id = ' . $_POST['idc'], $link);
            $data = ['status' => true];
        } else
            $data = ['status' => false];

        header('Content-type: application/json');
        print json_encode($data);
        exit();

    break;

}


?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col">
            <div class="row">
                <div class="col-6 col-sm-8">
                    <div style="padding:5px 0">
                    <?php if(+$user->id_nivel === 2): ?>
                        <button type="button" id="edit-btn" class="btn btn-danger" style="margin:3px; margin-left:0;"><i class="fa fa-th-large"></i>EDITAR DIAGRAMA</button>
                        <div style="display:inline; margin:3px; margin-left:0;">
                            <input type="checkbox" id="edit-chk" data-off="<i class='fa fa-arrows'></i> MOVER CLIENTES" data-offstyle="danger" data-on="<i class='fa fa-pause'></i> MOVER CLIENTES" >
                        </div>
                    <?php endif ?>
                        <button type="button" id="inbox-btn" class="btn btn-danger" style="margin:3px; margin-left:0;"><i class="fa fa-arrow-circle-down"></i>NUEVA RECEPCION</button>
                    </div>
                </div>
                <div class="col-6 col-sm-4 text-right text-truncate caption">CASILLEROS</div>
            </div>
        </div>
    </div>
    <div class="row main-content">
        <div class="col" style="padding:10px 10px 0">
            <table id="caption" class="data-table" style="margin-bottom:3px">
                <caption>DISTRIBUCION</caption>
            </table>
        <?php if(+$user->id_nivel === 2): ?>
            <form id="edit-form" action="?" method="post" style="display:none">
                <input type="hidden" name="act" value="1">
                <input type="hidden" name="diagrama">
                <table class="data-table form-responsive" style="margin-bottom:3px">
                    <caption>EDITAR DIAGRAMA</caption>
                    <tr>
                        <th>Casilleros:</th>
                        <td width="30%" class="text-right">
                            <select name="casilleros" id="casilleros" data-action="3" style="width:100%">
                            <?php for($i = 0; $i <= 100; $i += 10): ?>
                                <option class="text-right" value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php endfor ?>
                            </select>
                        </td>
                        <th>Columnas:</th>
                        <td class="text-right">
                            <select name="columnas" id="columnas" data-action="4" style="width:100%">
                            <?php for($i = 1; $i <= 10; $i++): ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php endfor ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </form>
        <?php endif ?>

        </div>
    </div>

    <div class="row main-content">
        <div class="col table-responsive" style="padding:0 10px;">
            <div class="gridster" style="padding:2px"></div>
        </div>
    </div>

<?php if(+$user->id_nivel === 2): ?>
    <div class="row main-content">
        <div class="col" style="padding:0 10px 10px">
            <table id="edit-form-footer" class="data-form" style="display:none;">
                <tr>
                    <td colspan="4" class="text-center">
                        <div class="row d-sm-none">
                            <div class="col-12">
                                <input type="button" class="btn btn-danger w-100 cancel-btn" value="&lt;&lt;&lt; Cancelar" />
                            </div>
                            <div class="col-12" style="margin-top:5px">
                                <input type="button" class="btn btn-primary w-100 submit-btn" value="Guardar &gt;&gt;&gt;" />
                            </div>
                        </div>
                        <div class="d-none d-sm-block">
                            <input type="button" class="btn btn-danger cancel cancel-btn" value="&lt;&lt;&lt; Cancelar" />
                            <input type="button" class="btn btn-primary save submit-btn" value="Guardar &gt;&gt;&gt;" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php endif ?>

    <link rel="stylesheet" href="assets/js/fancybox/jquery.fancybox.min.css" />
    <script src="assets/js/fancybox/jquery.fancybox.min.js"></script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>

    <link rel="stylesheet" href="assets/js/gridster/jquery.gridster.min.css">
    <script type="text/javascript" src="assets/js/gridster/jquery.gridster.min.js"></script>

    <link rel="stylesheet" href="assets/js/tipsy/tipsy.css">
    <script type="text/javascript" src="assets/js/tipsy/jquery.tipsy.js"></script>

    <link rel="stylesheet" href="assets/js/toggle/bootstrap4-toggle.min.css">
    <script type="text/javascript" src="assets/js/toggle/bootstrap4-toggle.min.js"></script>

    <script type="text/javascript" src="assets/js/po_boxes.js"></script>

</div>
<?php include 'footer.php' ?>