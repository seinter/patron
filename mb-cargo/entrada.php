<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("classes/image.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$ida = $user->id_central ?: $user->id_agencia;

$link = DB::connect();

/*
error_reporting(-1);
ini_set('display_errors', true);
*/

switch(+$_REQUEST['act']) {
    case 1: // INBOX

        $res = mysql_query('SELECT casillero FROM clientes WHERE id = ' . $_GET['idc'], $link);
        if (!+mysql_result($res, 0)) {

            $res = mysql_query("SELECT * FROM clientes WHERE id_agencia = {$ida} AND casillero = " . +$_POST['box'] . " AND status", $link);
            if (+$_POST['box'] && !mysql_num_rows($res))
                mysql_query('UPDATE clientes SET casillero = ' . $_POST['box'] . ' WHERE id = ' . $_GET['idc'], $link);
            else {
                $res = mysql_query('SELECT id id_tienda, tienda FROM tiendas WHERE id = ' . $_POST['id_tienda'], $link);
                extract(mysql_fetch_assoc($res));
                $err = 'El casillero NO SE ENCUENTRA DISPONIBLE!';
                break;
            }

        }

        $_POST['peso'] = $_POST['peso'] ?: 0;

        // INSERT / UPDATE
        if ($id = $_POST['id']) {
            $str = 'UPDATE entradas SET id_tienda = ' . $_POST['id_tienda'] . ', cantidad = ' . $_POST['cantidad'] . ', descripcion = \'' . mysql_real_escape_string($_POST['descripcion']) . '\', peso = ' . $_POST['peso'] . ', piezas = ' . $_POST['piezas'] . ', entrega = \'' . mysql_real_escape_string($_POST['entrega']) . '\', observaciones = \'' . mysql_real_escape_string($_POST['observaciones']) . '\' WHERE id = ' . $_POST['id'];
            mysql_query($str, $link);
        } else {
            $str = 'INSERT INTO entradas(id_usuario, id_cliente, id_tienda, cantidad, descripcion, peso, piezas, entrega, observaciones, codigo) VALUES(' . $user->id . ', ' . $_GET['idc'] . ', ' . $_POST['id_tienda'] . ', ' . $_POST['cantidad'] . ', \'' . mysql_real_escape_string($_POST['descripcion']) . '\', ' . $_POST['peso'] . ', ' . $_POST['piezas'] . ', \'' . mysql_real_escape_string($_POST['entrega']) . '\',  \'' . mysql_real_escape_string($_POST['observaciones']) . '\', ' . getUniqueCode('entradas') . ')';
            mysql_query($str, $link);
            $id = @mysql_insert_id();
        }

        if ($id)
            if ($_FILES["foto"]["name"]) {
                $imagen = Image::resampImage(800, 600, $_FILES["foto"]["tmp_name"], 0);
                $imagen = addslashes($imagen);

                $str = "UPDATE entradas SET tipo = '" . $_FILES["foto"]["type"] . "', foto = '$imagen' WHERE id = $id";
                mysql_query($str, $link);
            }

        header('Location: ?idc=' . $_GET['idc'] . '&idt=' . $id);
        exit();

        break;
    case 2:
        // FETCH FOR EDITING
        $res = mysql_query('SELECT entradas.id, id_tienda, tienda, entradas.cantidad, entradas.descripcion, entradas.peso, entradas.piezas, entradas.entrega, entradas.observaciones FROM entradas INNER JOIN tiendas ON entradas.id_tienda = tiendas.id WHERE entradas.id = ' . $_POST['id'], $link);
        $row = mysql_fetch_assoc($res);
        extract($row);

        break;
    case 3:
        // DELETE
        mysql_query('UPDATE entradas SET id_autoriza = ' . $_GET['idu'] . ', status = 0 WHERE id = ' . $_GET['id'], $link);

        header('Location: ?idc=' . $_GET['idc']);
        exit();

        break;
    case 4:
        // RELEASE INBOX
        $res = mysql_query('SELECT COUNT(*) FROM contenidos WHERE id_cliente = ' . $_GET['idc'] . ' AND NOT contenidos.id_bitacora AND contenidos.status', $link);
        if (!mysql_result($res, 0)) {
            mysql_query('UPDATE clientes SET casillero = 0 WHERE id = ' . $_GET['idc'], $link);

            header('Location: recepcion.php');
        } else
            header('Location: ?idc=' . $_GET['idc']);

        exit();

        break;
    case 5: // PACK
        // INSERT / UPDATE
        if ($idb = $_POST['idb']) {
            $str = "UPDATE detalle_venta SET id_clase = " . $_POST['id_clase'] . ", id_producto = " . $_POST['id_producto'] . ", descripcion = '" . $_POST['producto'] . "' WHERE id = " . $_POST['idd'];
            mysql_query($str, $link);
        } else {
            $str = "INSERT INTO detalle_venta(id_cliente, id_clase, id_producto, descripcion, cantidad, guias) VALUES(" . $_GET['idc'] . ", " . $_POST['id_clase'] . ", " . $_POST['id_producto'] . ", '" . $_POST['producto'] . "', 1, 1)";

            mysql_query($str, $link);
            $idd = mysql_insert_id();
            mysql_query("INSERT INTO bitacora(id_detalle, codigo) VALUE({$idd}, " . getUniqueCode('bitacora') . ")", $link);
            $idb = mysql_insert_id();
        }

        // CLEAN UP ENTRIES
        $str = "SELECT entradas.id, entradas.empaque, contenidos.empaque cont_empaque FROM entradas INNER JOIN contenidos ON entradas.id = contenidos.id_entrada WHERE contenidos.id_bitacora = {$idb} AND contenidos.status";
        $res = mysql_query($str, $link);
        while ($row = mysql_fetch_object($res)) {
            $clean = array_diff(($row->empaque ? json_decode($row->empaque) : []), ($row->cont_empaque ? json_decode($row->cont_empaque) : []));
            mysql_query("UPDATE entradas SET empaque = JSON_ARRAY(" . implode(', ', $clean) . ") WHERE id = {$row->id}", $link);
        }
        // CLEAN UP CONTENTS
        mysql_query("UPDATE contenidos SET status = 0 WHERE id_bitacora = {$idb}", $link);


        foreach ($_POST['id'] as $key => $value) {

            // INSERT INTO CONTENTS
            $new = json_decode($_POST['empaque'][$key]);
            if (+$value)
                $str = "UPDATE contenidos INNER JOIN entradas ON contenidos.id_entrada = entradas.id SET contenidos.id_tienda = entradas.id_tienda, contenidos.cantidad = " . count($new) . ", contenidos.empaque = JSON_ARRAY(" . implode(', ', $new) . "), contenidos.descripcion = entradas.descripcion, contenidos.peso = (entradas.peso / entradas.cantidad) * " . count($new) . ", contenidos.piezas = (entradas.piezas / entradas.cantidad) * " . count($new) . ", contenidos.status = 1 WHERE contenidos.id = {$value}";
            else
                $str = "INSERT INTO contenidos(id_usuario, id_bitacora, id_entrada, id_tienda, cantidad, empaque, descripcion, peso, piezas) SELECT {$user->id}, {$idb}, entradas.id, entradas.id_tienda, " . count($new) . ", JSON_ARRAY(" . implode(', ', $new) . "), entradas.descripcion, (entradas.peso / entradas.cantidad) * " . count($new) . ", (entradas.piezas / entradas.cantidad) * " . count($new) . " FROM entradas WHERE entradas.id = " . $_POST['ide'][$key];

            mysql_query($str, $link);

        }

        // UPDATE ENTRIES WITH PACKED
        $str = "UPDATE entradas INNER JOIN contenidos ON entradas.id = contenidos.id_entrada SET entradas.empaque = JSON_MERGE_PRESERVE(IFNULL(entradas.empaque, JSON_ARRAY()), contenidos.empaque) WHERE contenidos.id_bitacora = {$idb} AND contenidos.status";
        mysql_query($str, $link);

        // ASSIGN TO CONTAINER
        if ($id_prog = $id_prog ?: 0) {
            $str = "insert into historial(id_bitacora, id_accion, id_usuario, id_prog, fecha, hora) values($idb, 3, {$user->id}, {$id_prog}, curdate(), curtime())";
            mysql_query($str, $link);
        }

        header('Location: ?idc=' . $_GET['idc']);
        exit();

    break;
    case 6:
        // FETCH PACK DATA FOR EDITING
        $res = mysql_query('SELECT detalle_venta.id idd, bitacora.id idb, clasificacion.id id_clase, clasificacion.otros, detalle_venta.id_producto, detalle_venta.descripcion producto FROM detalle_venta INNER JOIN clasificacion ON detalle_venta.id_clase = clasificacion.id INNER JOIN bitacora ON detalle_venta.id = bitacora.id_detalle WHERE detalle_venta.id = ' . $_POST['idd'], $link);
        $row = mysql_fetch_assoc($res);
        extract($row);

        $str = "SELECT contenidos.id, entradas.id ide, entradas.codigo code, contenidos.creado date, tiendas.tienda store, entradas.descripcion description, entradas.peso / entradas.cantidad weight, entradas.piezas / entradas.cantidad pieces, entradas.observaciones comments, contenidos.empaque items FROM contenidos INNER JOIN entradas ON contenidos.id_entrada = entradas.id INNER JOIN tiendas ON entradas.id_tienda = tiendas.id WHERE contenidos.id_bitacora = {$idb} AND contenidos.status";
        $res = mysql_query($str, $link);
        $contents = [];
        while($row = mysql_fetch_object($res)){
            $row->items = json_decode($row->items);
            $row->date = date('Y-m-d', strtotime($row->date)) . ' <span class="d-none d-md-inline-block" style="color:#acbbc8; margin-left:1px;">' . date('h:i', strtotime($row->date)) . '</span>';
            $row->comments = nl2br($row->comments);
            $contents[] = $row;
        }

    break;
    case 7:
        // DELETE
        // CLEAN UP ENTRIES
        $str = 'SELECT entradas.id, entradas.empaque, contenidos.empaque cont_empaque FROM entradas INNER JOIN contenidos ON entradas.id = contenidos.id_entrada INNER JOIN bitacora ON contenidos.id_bitacora = bitacora.id WHERE bitacora.id_detalle = ' . $_GET['id'] . ' AND contenidos.status';
        $res = mysql_query($str, $link);
        while($row = mysql_fetch_object($res)){
            $clean = array_diff(($row->empaque ? json_decode($row->empaque) : []), ($row->cont_empaque ? json_decode($row->cont_empaque) : []));
            mysql_query("UPDATE entradas SET empaque = JSON_ARRAY(" . implode(', ', $clean) . ") WHERE id = {$row->id}", $link);
        }
        // CLEAN UP TABLES
        $str = 'UPDATE detalle_venta INNER JOIN bitacora ON detalle_venta.id = bitacora.id_detalle LEFT JOIN contenidos ON bitacora.id = contenidos.id_bitacora SET detalle_venta.status = 0, bitacora.status = 0, contenidos.status = 0 WHERE detalle_venta.id = ' . $_GET['id'];
        mysql_query($str, $link);

        header('Location: ?idc=' . $_GET['idc']);
        exit();

    break;
}

//CLIENTE
$str = 'select clientes.*, paises.pais from clientes inner join paises on clientes.id_pais = paises.id where clientes.id = ' . $_GET['idc'];
$res = mysql_query($str, $link);
if($row = @mysql_fetch_assoc($res)){
    foreach($row as $key => $value)
        $customer[$key] = $value;
} else {
    header('Location: recepcion.php');
    exit();
}


$str = 'SELECT entradas.id, entradas.creado fecha, tiendas.tienda, entradas.cantidad, entradas.descripcion, entradas.peso, entradas.piezas, IF(entradas.foto IS NULL, 0, 1) foto, JSON_LENGTH(IF(entradas.empaque IS NULL, JSON_ARRAY(), entradas.empaque)) cnt_empaque FROM entradas INNER JOIN tiendas ON entradas.id_tienda = tiendas.id WHERE entradas.id_cliente = ' . $_GET['idc'] . ' AND entradas.status HAVING entradas.cantidad > cnt_empaque ORDER BY entradas.creado';
$res = mysql_query($str, $link);
$inbox = [];
while($row = mysql_fetch_object($res))
    $inbox[] = $row;

$str = "SELECT detalle_venta.id idd, clasificacion.icono, clasificacion.clase, IF(productos.id, productos.producto, detalle_venta.descripcion) producto, bitacora.id idb, contenidos.id, contenidos.creado fecha, tiendas.tienda, contenidos.cantidad, contenidos.descripcion, contenidos.peso, contenidos.piezas, entradas.id ide, IF(entradas.foto IS NULL, 0, 1) foto FROM contenidos INNER JOIN entradas ON contenidos.id_entrada = entradas.id INNER JOIN tiendas ON contenidos.id_tienda = tiendas.id INNER JOIN bitacora ON contenidos.id_bitacora = bitacora.id INNER JOIN detalle_venta ON bitacora.id_detalle = detalle_venta.id INNER JOIN clasificacion ON detalle_venta.id_clase = clasificacion.id LEFT JOIN productos ON detalle_venta.id_producto = productos.id LEFT JOIN historial ON bitacora.id = historial.id_bitacora AND historial.status WHERE detalle_venta.id_cliente = " . $_GET['idc'] . " AND historial.id IS NULL AND detalle_venta.status AND bitacora.status AND contenidos.id_entrada AND contenidos.status ORDER BY detalle_venta.creado";
$res = mysql_query($str, $link);
$pack = [];
while($row = mysql_fetch_assoc($res)){
    $idx = array_shift($row);
    $pack[$idx]['product'] = '<i class="fa fa-' . array_shift($row) . '" rel="tipsy" title="<h6>' . array_shift($row) . '</h6>"></i> ' . array_shift($row);
    $pack[$idx]['items'][] = (object) $row;
}

$str = "SELECT detalle_venta.id idd, ventas.id idv, clasificacion.icono, clasificacion.clase, IF(productos.id, productos.producto, detalle_venta.descripcion) producto, bitacora.id idb, contenidos.id, IF(historial.id_accion = 4, CONCAT(historial.fecha, ' ', historial.hora), historial.creado) fecha, tiendas.tienda, contenidos.cantidad, contenidos.descripcion, contenidos.peso, contenidos.piezas, entradas.id ide, IF(entradas.foto IS NULL, 0, 1) foto, historial.id_accion, paises.pais, agencias.agencia, programacion.id idp, tipo_salidas.tipo tipo_salida, transportistas.transportista salida, carriers.id idc, carriers.carrier, carriers.website, entregas.id entrega, entregas.guia, entregas.nombre, entregas.documento, IF(entregas.foto IS NOT NULL, 1, 0) efoto FROM contenidos INNER JOIN entradas ON contenidos.id_entrada = entradas.id  INNER JOIN tiendas ON contenidos.id_tienda = tiendas.id INNER JOIN bitacora ON contenidos.id_bitacora = bitacora.id INNER JOIN detalle_venta ON bitacora.id_detalle = detalle_venta.id INNER JOIN historial ON bitacora.id = historial.id_bitacora INNER JOIN usuarios ON historial.id_usuario = usuarios.id INNER JOIN agencias ON usuarios.id_agencia = agencias.id INNER JOIN paises ON agencias.id_pais = paises.id INNER JOIN clasificacion ON detalle_venta.id_clase = clasificacion.id LEFT JOIN ventas ON detalle_venta.id_venta = ventas.id LEFT JOIN productos ON detalle_venta.id_producto = productos.id LEFT JOIN programacion ON historial.id_prog = programacion.id LEFT JOIN tipo_salidas ON programacion.id_tipo = tipo_salidas.id LEFT JOIN transportistas ON programacion.id_transportista = transportistas.id LEFT JOIN entregas ON bitacora.id = entregas.id_bitacora AND entregas.status LEFT JOIN carriers ON entregas.id_carrier = carriers.id WHERE detalle_venta.id_cliente = " . $_GET['idc'] . " AND detalle_venta.status AND ((detalle_venta.id_venta AND ventas.status) OR NOT detalle_venta.id_venta) AND bitacora.status AND historial.activo AND historial.id_accion IN (2, 3, 4) AND contenidos.id_entrada AND contenidos.status AND historial.status ORDER BY detalle_venta.creado";
$res = mysql_query($str, $link);
$outbox = [];
while($row = mysql_fetch_assoc($res)){
    $idx = array_shift($row);
    $outbox[$idx]['sale'] = array_shift($row);
    $outbox[$idx]['product'] = '<i class="fa fa-' . array_shift($row) . '" rel="tipsy" title="<h6>' . array_shift($row) . '</h6>"></i> ' . array_shift($row);
    $outbox[$idx]['items'][] = (object) $row;
}

$str = 'SELECT clasificacion.id, clasificacion.clase, clasificacion.icono, clasificacion.otros FROM clasificacion LEFT JOIN productos ON clasificacion.id = productos.id_clase AND productos.status WHERE clasificacion.status GROUP BY clasificacion.id HAVING COUNT(productos.id) OR clasificacion.otros ORDER BY clasificacion.clase';
$res = mysql_query($str, $link);
$categories = [];
while($row = mysql_fetch_object($res)){
    $categories[] = $row;
}

$products = [];
if($id_clase) {
    $res = mysql_query("SELECT id, producto FROM productos WHERE id_clase = {$id_clase} AND status", $link);
    while ($row = mysql_fetch_object($res))
        $products[] = $row;

    if($otros)
        $products[] = (object) ['id' => 0, 'producto' => 'OTRO'];
}


if(!+$customer['casillero']){
    $res = mysql_query("SELECT agencias.casilleros max, GROUP_CONCAT(clientes.casillero) busy FROM agencias LEFT JOIN clientes ON agencias.id = clientes.id_agencia WHERE agencias.id = {$ida} AND clientes.casillero AND clientes.status", $link);
    extract(mysql_fetch_assoc($res));
    $busy = explode(',', $busy);
    $boxes = array_filter(range(1, $max), function($item) use ($busy) {
       return !in_array($item, $busy);
    });
}

?>
<?php include 'header.php' ?>

<style>
    @media (min-width: 576px) {
        .text-cell {
            line-height: 30px;
        }
    }

    #inbox th {
        background-color: #4c9e5f;
    }
    #pack th {
        background-color: #4F7DB0;
    }
    #outbox th {
        background-color: #818d99;
    }
</style>

<div class="container-fluid">

    <div class="row main-title">
        <div class="col">
            <div class="row">
                <div class="col-4">
                    <button type="button" class="btn btn-danger" onclick="window.location = 'recepcion.php'">
                        <i class="fa fa-arrow-left"></i>
                        REGRESAR
                    </button>
                </div>
                <div class="col-8 text-right text-truncate caption"><?php echo +$customer['casillero'] ? 'CASILLERO [ <i class="fa fa-inbox"></i> #' . $customer['casillero'] . ' ]' : '- - - -&nbsp;' ?></div>
           </div>
        </div>
    </div>
    <div class="row main-content">
        <div class="col">
            <div class="row">
                <table width="100%" border="0" cellspacing="10" cellpadding="0" style="margin-bottom:-10px">
                    <tr id="customer" <?php echo $id || $idd ? 'style="display:none"' : '' ?> >
                        <td style="padding:0">
                            <table class="data-form">
                                <caption><i class="fa fa-user" style="margin-right:8px"></i>CLIENTE</caption>
                                <tr class="font-weight-bold">
                                    <th style="line-height:20px">Cliente:</th>
                                    <td class="text-cell" width="30%"><?php echo formatClientCode($customer["id"]) ?> - <?php echo $customer["nombre"]." ".$customer["apellido"]; ?></td>
                                    <th style="line-height:20px">Teléfono(s):</th>
                                    <td class="text-cell"><?php echo $customer["telefono"].($customer["telefono2"]?" / ".$customer["telefono2"]:"") ?></td>
                                </tr>
                                <tr>
                                    <td colspan="4">

                                        <div class="row" style="min-height:38px">

                                            <div class="col-12 d-sm-none">
                                            <?php if(+$customer['casillero'] && !count($inbox)){ ?>
                                                <button type="button" class="btn btn-danger w-100 release-btn"  style="margin-bottom:5px"><i class="fa fa-times-circle" style="margin-right:3px"></i> LIBERAR CASILLERO</button>
                                            <?php } ?>
                                            </div>
                                            <div class="col-6 d-none d-sm-block">
                                            <?php if(+$customer['casillero'] && !count($inbox)){ ?>
                                                <button type="button" class="btn btn-danger release-btn"><i class="fa fa-times-circle" style="margin-right:3px"></i> LIBERAR CASILLERO</button>
                                            <?php } ?>
                                            </div>

                                            <div class="col-12 d-sm-none">
                                                <button type="button" class="btn btn-success w-100 add-btn" style="display:none">AGREGAR ENTRADA <?php echo $id; ?> <i class="fa fa-plus-circle" style="margin-left:3px"></i></button>
                                                <button type="button" class="btn btn-primary w-100 add-pack-btn" style="display:none">NUEVO EMPAQUE <i class="fa fa-plus-circle" style="margin-left:3px"></i></button>
                                            </div>
                                            <div class="col-6 d-none d-sm-block">
                                                <button type="button" class="btn btn-success float-right add-btn" style="display:none">AGREGAR ENTRADA <i class="fa fa-plus-circle" style="margin-left:3px"></i></button>
                                                <button type="button" class="btn btn-primary float-right add-pack-btn" style="display:none">NUEVO EMPAQUE <i class="fa fa-plus-circle" style="margin-left:3px"></i></button>
                                            </div>

                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr id="inbox-form" <?php echo !$id ? 'style="display:none"' : '' ?> >
                        <td style="padding:0">

                            <form action="?idc=<?php echo $_GET['idc'] ?>" method="post" enctype="multipart/form-data" onsubmit="return validate(this)" >
                                <input type="hidden" name="act" value="1">
                                <input type="hidden" name="id" value="<?php echo $id ?: 0 ?>">

                                <table class="data-form">
                                    <caption><i class="fa fa-arrow-circle-down" style="margin-right:8px"></i>AGREGAR ENTRADA</caption>

                                <?php if(!+$customer['casillero']): ?>
                                    <tr>
                                        <th style="background-color:#b00; line-height:22px;"><i class="fa fa-inbox" style="font-size:1.2em; margin-right:3px;"></i> Casillero:</th>
                                        <td class="text-right">
                                            <div class="med-combo">
                                                <select name="box" id="box" style="width:100%">
                                                <?php foreach($boxes as $item): ?>
                                                    <option value="<?php echo $item ?>">#<?php echo $item ?></option>
                                                <?php endforeach ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endif ?>

                                    <tr class="font-weight-bold">
                                        <th style="line-height:20px">Cliente:</th>
                                        <td class="text-cell" width="30%"><?php echo formatClientCode($customer["id"]) ?> - <?php echo $customer["nombre"]." ".$customer["apellido"]; ?></td>
                                        <th style="line-height:20px">Teléfono(s):</th>
                                        <td class="text-cell"><?php echo $customer["telefono"].($customer["telefono2"]?" / ".$customer["telefono2"]:"") ?></td>
                                    </tr>
                                    <tr>
                                        <th style="line-height:22px">Tienda:</th>
                                        <td colspan="3">
                                            <div class="med-combo">
                                                <select name="id_tienda" id="id_tienda" style="width:100%">
                                                    <option></option>
                                                    <?php if($id_tienda): ?>
                                                        <option value="<?php echo $id_tienda ?>" selected><?php echo $tienda ?></option>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Tipo de Paquete:</th>
                                        <td>
                                            <input type="text" name="descripcion" id="descripcion" value="<?php echo $descripcion ?>">
                                        </td>
                                        <th>Cantidad:</th>
                                        <td>
                                            <input type="text" name="cantidad" id="cantidad" class="text-right" onchange="this.value = numFormatInt(this.value, 1)" value="<?php echo $cantidad ?: 1 ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Peso Kg.:</th>
                                        <td>
                                            <input type="text" name="peso" id="peso" class="text-right optional-input" onchange="this.value = isNaN(this.value) ? '' : (Number(this.value) ? numFormat(this.value) : '')" value="<?php echo $peso ? numFormat($peso) : '' ?>">
                                        </td>
                                        <th>Piezas:</th>
                                        <td>
                                            <input type="text" name="piezas" id="piezas" class="text-right" onchange="this.value = numFormatInt(this.value, 1)" value="<?php echo $piezas ?: 0 ?>">
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th>Observaciones:</th>
                                        <td colspan="3">
                                            <textarea name="observaciones" id="observaciones" class="optional-input" style="height:68px; min-height:30px"><?php echo $observaciones ?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Foto Digital:</th>
                                        <td>
                                            <input type="file" name="foto" id="foto" class="optional-input" accept="image/*" capture >
                                        </td>
                                        <th>Persona quien Entrega:</th>
                                        <td>
                                            <input type="text" name="entrega" id="entrega" class="optional-input" value="<?php echo $entrega ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="4">
                                            <div align="center">
                                            <?php if($id): ?>
                                                <input type="button" class="btn btn-danger cancel" value="&lt;&lt;&lt; Cancelar" onclick="window.location = '?idc=<?php echo $_GET['idc'] ?>'" />
                                            <?php else: ?>
                                                <input type="button" class="btn btn-danger cancel cancel-btn" value="&lt;&lt;&lt; Cancelar" />
                                            <?php endif ?>
                                                <input type="submit" class="btn btn-primary save" value="Guardar &gt;&gt;&gt;" />
                                            </div>
                                        </th>
                                    </tr>
                                </table>

                            </form>

                        </td>
                    </tr>


                    <tr id="pack-form" <?php echo !$idd ? 'style="display:none"' : '' ?> >
                        <td style="padding:0">

                            <form action="?idc=<?php echo $_GET['idc'] ?>" method="post" onsubmit="return validatePack(this)" >
                                <input type="hidden" name="act" value="5">
                                <input type="hidden" name="idd" value="<?php echo $idd ?: 0 ?>">
                                <input type="hidden" name="idb" value="<?php echo $idb ?: 0 ?>">

                                <table class="data-table form-responsive">
                                    <caption><i class="fa fa-cube" style="margin-right:8px"></i>NUEVO EMPAQUE</caption>
                                    <tr class="font-weight-bold">
                                        <th style="line-height:20px">Cliente:</th>
                                        <td class="text-cell" width="30%"><?php echo formatClientCode($customer["id"]) ?> - <?php echo $customer["nombre"]." ".$customer["apellido"]; ?></td>
                                        <th style="line-height:20px">Teléfono(s):</th>
                                        <td class="text-cell"><?php echo $customer["telefono"].($customer["telefono2"]?" / ".$customer["telefono2"]:"") ?></td>
                                    </tr>
                                    <tr>
                                        <th style="line-height:22px">Categoría:</th>
                                        <td colspan="3">
                                            <div class="med-combo">
                                                <select name="id_clase" id="id_clase" style="width:100%">
                                                    <option></option>
                                                    <?php foreach($categories as $item): ?>
                                                        <option value="<?php echo $item->id ?>" data-icon="<?php echo $item->icono ?>" <?php echo +$item->id === +$id_clase ? 'selected' : '' ?> ><?php echo $item->clase ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Producto:</th>
                                        <td>
                                            <select name="id_producto" id="id_producto" style="width:100%">
                                            <?php foreach($products as $item): ?>
                                                <option value="<?php echo $item->id ?>" <?php echo +$item->id === +$id_producto ? 'selected' : '' ?> ><?php echo $item->producto ?></option>
                                            <?php endforeach ?>
                                            </select>
                                        </td>
                                        <th class="measures" <?php echo $id_producto !== 0 ? 'style="display:none"' : '' ?> >Medidas:</th>
                                        <td class="measures" <?php echo $id_producto !== 0 ? 'style="display:none"' : '' ?> >
                                            <input type="text" name="producto" id="producto" value="<?php echo $producto ?>">
                                        </td>
                                    </tr>
                                </table>

                                <div style="margin:5px 3px 3px; padding:12px 10px; border:1px solid #999; border-radius:5px;">

                                    <table id="contents-table" class="table table-sm table-hover" style="margin:0">
                                        <caption style="padding:0">
                                            <div class="row">
                                                <div class="col-12 col-sm-6"><h4 class="ml-sm-2 mt-sm-2"><i class="fa fa-dropbox"></i> CONTENIDO</h4></div>
                                                <div class="col-12 col-sm-6 mb-2">
                                                    <div class="input-group">
                                                        <input type="text" id="search-input" class="form-control optional-input" placeholder="Barcode...">
                                                        <div class="input-group-append">
                                                            <button type="button" id="search-btn" class="btn btn-outline-secondary" data-loading="<i class='fa fa-spinner fa-spin'></i> Buscando..." style="width:150px"><i class="fa fa-search"></i> Buscar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </caption>
                                        <thead>
                                            <tr class="bg-light">
                                                <th class="d-none d-sm-table-cell">Codigo</th>
                                                <th>Fecha</th>
                                                <th class="d-none d-md-table-cell">Tienda</th>
                                                <th>Tipo</th>
                                                <th>Cantidad</th>
                                                <th>Peso</th>
                                                <th>Piezas</th>
                                                <th width="35"></th>
                                                <th width="60"></th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                    <?php
                                    if($idd):
                                        foreach($contents as $row):
                                    ?>
                                            <tr>
                                                <td class="d-none d-sm-table-cell">
                                                    <input type="hidden" name="id[]" value="<?php echo $row->id ?>">
                                                    <input type="hidden" name="ide[]" value="<?php echo $row->ide ?>" >
                                                    <input type="hidden" name="empaque[]" value="<?php echo json_encode($row->items) ?>" >
                                                    <?php echo $row->code ?>
                                                </td>
                                                <td><?php echo $row->date ?></td>
                                                <td class="d-none d-md-table-cell"><?php echo $row->store ?></td>
                                                <td><?php echo $row->description ?></td>
                                                <td class="text-right font-weight-bold" style="padding-right:5px"><?php echo numFormatInt(count($row->items)) ?></td>
                                                <td class="text-right" style="padding-right:5px"><?php echo numFormat($row->weight * count($row->items)) ?></td>
                                                <td class="text-right" style="padding-right:5px"><?php echo numFormatInt($row->pieces * count($row->items)) ?></td>
                                                <td class="text-center" style="line-height:25px">
                                                    <?php if($row->comments): ?>
                                                        <i class="fa fa-commenting" rel="tipsy" title="<?php echo $row->comments ?>" style="font-size:1.3em; cursor:pointer;"></i>
                                                    <?php else: ?>
                                                        <i class="fa fa-commenting" style="color:lightgray; font-size:1.3em;"></i>
                                                    <?php endif ?>
                                                </td>
                                                <td style="padding:2px">
                                                    <button type="button" class="btn btn-sm btn-outline-danger w-100 delete-row" data-id="<?php echo $row->ide ?>" data-description="<?php echo $row->code . ': (' . count($row->items) . ') ' . $row->description ?>">- X -</button>
                                                </td>
                                            </tr>
                                    <?php
                                        endforeach;
                                    endif;
                                    ?>
                                        </tbody>
                                    </table>
                                </div>

                                <table class="data-table form-fields">
                                    <tr>
                                        <th style="background-color:orange; width:200px;">ASIGNA A SALIDA:</th>
                                        <td>
                                            <select name="id_prog" id="id_prog" style="width:100%; font-size:12px;" class="optional-input">
                                                <option></option>
                                                <?php
                                                $str = "select tipo_salidas.id, tipo from tipo_salidas inner join programacion on tipo_salidas.id = programacion.id_tipo where programacion.fecha >= curdate() and programacion.status group by tipo_salidas.id order by tipo;";
                                                $res = mysql_query($str, $link);
                                                while($row = mysql_fetch_row($res)){
                                                    ?>
                                                    <optgroup style="background-color:#4179AB; color:#FFFFFF" label="<?php echo $row[1] ?>">
                                                        <?php
                                                        $str = "select programacion.*, local1.localidad origen, local2.localidad destino, transportistas.transportista from programacion inner join localidades local1 on programacion.origen = local1.id inner join localidades local2 on programacion.destino = local2.id LEFT JOIN transportistas ON programacion.id_transportista = transportistas.id AND transportistas.status where id_tipo = {$row[0]} and programacion.fecha >= curdate() and programacion.status order by fecha;";
                                                        $res2 = mysql_query($str, $link);
                                                        $bg = "";
                                                        while($row2 = mysql_fetch_assoc($res2)){
                                                            $bg = $bg=="#76A3CB"?"#568CBE":"#76A3CB";
                                                            ?>
                                                            <option style="background-color:<?php echo $bg ?>; color:#F1F1F1" value="<?php echo $row2["id"] ?>"><?php echo formatCode($row2["id"])." - ".$row2["fecha"].": ".strtoupper($row2["origen"]." - ".$row2["destino"]) . ($row2['transportista'] ? ' (' . $row2['transportista'] . ')' : '') ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </optgroup>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                </table>

                                <table class="data-form" >
                                    <tr>
                                        <th colspan="4">
                                            <div align="center">
                                                <?php if($idd): ?>
                                                    <input type="button" class="btn btn-danger cancel" value="&lt;&lt;&lt; Cancelar" onclick="window.location = '?idc=<?php echo $_GET['idc'] ?>'" />
                                                <?php else: ?>
                                                    <input type="button" class="btn btn-danger cancel cancel-btn" value="&lt;&lt;&lt; Cancelar" />
                                                <?php endif ?>
                                                <input type="submit" class="btn btn-primary save" value="Guardar &gt;&gt;&gt;" />
                                            </div>
                                        </th>
                                    </tr>
                                </table>

                            </form>

                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>



    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">
                <table width="100%" border="0" cellspacing="10" cellpadding="0""> 
                    <tr>
                        <td style="padding:0">
                            <table class="data-grid">
                                <caption style="padding:5px">
                                    <ul class="nav nav-pills d-block d-sm-flex">
                                        <li class="nav-item"><a class="nav-link text-white" data-toggle="tab" href="#inbox" style="padding:5px 15px"><i class="fa fa-arrow-circle-down" style="margin-right:5px"></i>ENTRADAS</a></li>
                                        <li class="nav-item"><a class="nav-link text-white" data-toggle="tab" href="#pack" style="padding:5px 15px"><i class="fa fa-cube" style="margin-right:5px"></i>EMPAQUE</a></li>
                                        <li class="nav-item"><a class="nav-link text-white" data-toggle="tab" href="#outbox" style="padding:5px 15px"><i class="fa fa-arrow-circle-up" style="margin-right:5px"></i>SALIDAS</a></li>
                                        <li class="nav-item"><a class="nav-link text-white" data-toggle="tab" href="#balance" style="padding:5px 15px"><i class="fa fa-balance-scale" style="margin-right:5px"></i>BALANCE</a></li>
                                    </ul>
                                </caption>
                            </table>

                            <div class="tab-content">


                                <div id="inbox" class="tab-pane fade">
                                    <table class="data-grid">
                                        <tr>
                                            <th width="35"></th>
                                            <th>Fecha</th>
                                            <th class="d-none d-md-table-cell">Tienda</th>
                                            <th class="d-none d-sm-table-cell">Tipo</th>
                                            <th>
                                                <div class="d-sm-none">Cnt.</div>
                                                <div class="d-none d-sm-block">Cantidad</div>
                                            </th>
                                            <th>
                                                <div class="d-sm-none">Kg.</div>
                                                <div class="d-none d-sm-block">Peso Kg.</div>
                                            </th>
                                            <th>
                                                <div class="d-sm-none">Pzs.</div>
                                                <div class="d-none d-sm-block">Piezas</div>
                                            </th>
                                            <th width="50"></th>
                                            <th width="75"></th>
                                            <th width="75"></th>
                                        </tr>
                                    <?php foreach($inbox as $item): ?>
                                        <tr <?php echo +$item->id === +$id ? 'class="table-primary"' : '' ?> >
                                            <td class="text-center">
                                            <?php if($item->foto): ?>
                                                <i class="fa fa-camera photo" style="font-size:1.3em; cursor:pointer;" data-id="<?php echo md5($item->id) ?>"></i>
                                            <?php else: ?>
                                                <i class="fa fa-camera" style="color:lightgray; font-size:1.3em;"></i>
                                            <?php endif ?>
                                            </td>
                                            <td><?php echo date('Y-m-d', strtotime($item->fecha)) ?> <span class="d-none d-md-inline-block" style="color:#acbbc8; margin-left:1px;"><?php echo date('h:i', strtotime($item->fecha)) ?></span></td>
                                            <td class="d-none d-md-table-cell"><?php echo $item->tienda ?></td>
                                            <td class="d-none d-sm-table-cell"><?php echo $item->descripcion ?></td>
                                            <td class="text-right" style="padding-right:5px"><?php echo $item->cnt_empaque ? ($item->cantidad - $item->cnt_empaque) . '<div class="text-muted" style="display:inline-block; white-space:nowrap;">&nbsp;/ ' . $item->cantidad . '</div>': $item->cantidad ?></td>
                                            <td class="text-right" style="padding-right:5px"><?php
                                                if($item->peso) {
                                                    echo $item->cnt_empaque ? numFormat($item->peso - (($item->peso / $item->cantidad) * $item->cnt_empaque)) . '<div class="text-muted" style="display:inline-block; white-space:nowrap;">&nbsp;/ ' . numFormat($item->peso) . '</div>' : numFormat($item->peso);
                                                }
                                                ?></td>
                                            <td class="text-right" style="padding-right:5px"><?php echo $item->cnt_empaque ? numFormatInt($item->piezas - (($item->piezas / $item->cantidad) * $item->cnt_empaque)) . '<div class="text-muted" style="display:inline-block; white-space:nowrap;">&nbsp;/ ' . $item->piezas . '</div>': $item->piezas ?></td>
                                            <td>
                                                <button class="btn btn-sm btn-success print" title="<h6>Imprimir Etiquetas</h6>" data-id="<?php echo md5($item->id) ?>">- I -</button>
                                            </td>
                                            <td>
                                                <form action="?idc=<?php echo $_GET['idc'] ?>" method="post">
                                                    <input type="hidden" name="act" value="2">
                                                    <input type="hidden" name="id" value="<?php echo $item->id ?>">
                                                    <button class="btn btn-sm btn-primary edit" <?php echo $item->cnt_empaque ? 'disabled' : '' ?> >- E -</button>
                                                </form>
                                            </td>
                                            <td><button class="btn btn-sm btn-danger delete" onclick="confirmDelete(<?php echo $item->id ?>, '(<?php echo $item->cantidad . ') ' . $item->descripcion . ' - ' . numFormatInt($item->peso) . 'Kg.' ?>')" <?php echo $item->cnt_empaque ? 'disabled' : '' ?> >- X -</button></td>
                                        </tr>
                                    <?php endforeach ?>
                                    </table>
                                </div>


                                <div id="pack" class="tab-pane fade">
                                    <table class="data-grid form-footer">
                                        <tr>
                                            <th>Producto</th>
                                            <th width="35"></th>
                                            <th>Fecha</th>
                                            <th class="d-none d-md-table-cell">Tienda</th>
                                            <th class="d-none d-sm-table-cell">Tipo</th>
                                            <th>
                                                <div class="d-sm-none">Cnt.</div>
                                                <div class="d-none d-sm-block">Cantidad</div>
                                            </th>
                                            <th>
                                                <div class="d-sm-none">Kg.</div>
                                                <div class="d-none d-sm-block">Peso Kg.</div>
                                            </th>
                                            <th>
                                                <div class="d-sm-none">Pzs.</div>
                                                <div class="d-none d-sm-block">Piezas</div>
                                            </th>
                                            <th width="75"></th>
                                            <th width="75"></th>
                                        </tr>
                                    <?php
                                        foreach($pack as $idx => $detail):
                                            foreach($detail['items'] as $key => $item):
                                    ?>
                                        <tr <?php echo +$idx === +$idd ? 'class="table-primary"' : '' ?> >
                                        <?php if(!$key): ?>
                                            <td rowspan="<?php echo count($detail['items']) ?>" class="text-center font-weight-bold table-primary"><?php echo $detail['product'] ?></td>
                                        <?php endif ?>
                                            <td class="text-center" style="line-height:30px">
                                                <?php if($item->foto): ?>
                                                    <i class="fa fa-camera photo" style="font-size:1.3em; cursor:pointer;" data-id="<?php echo md5($item->ide) ?>"></i>
                                                <?php else: ?>
                                                    <i class="fa fa-camera" style="color:lightgray; font-size:1.3em;"></i>
                                                <?php endif ?>
                                            </td>
                                            <td><?php echo date('Y-m-d', strtotime($item->fecha)) ?> <span class="d-none d-md-inline-block" style="color:#acbbc8; margin-left:1px;"><?php echo date('h:i', strtotime($item->fecha)) ?></span></td>
                                            <td class="d-none d-md-table-cell"><?php echo $item->tienda ?></td>
                                            <td class="d-none d-sm-table-cell"><?php echo $item->descripcion ?></td>
                                            <td class="text-right" style="padding-right:5px"><?php echo $item->cantidad ?></td>
                                            <td class="text-right" style="padding-right:5px"><?php echo $item->peso ?numFormat($item->peso) : '' ?></td>
                                            <td class="text-right" style="padding-right:5px"><?php echo $item->piezas ?></td>
                                        <?php if(!$key): ?>
                                            <td rowspan="<?php echo count($detail['items']) ?>" valign="top">
                                                <form action="?idc=<?php echo $_GET['idc'] ?>" method="post">
                                                    <input type="hidden" name="act" value="6">
                                                    <input type="hidden" name="idd" value="<?php echo $idx ?>">
                                                    <button class="btn btn-sm btn-primary edit">- E -</button>
                                                </form>
                                            </td>
                                            <td rowspan="<?php echo count($detail['items']) ?>" valign="top">
                                                <button class="btn btn-sm btn-danger delete" onclick="confirmDeletePack(<?php echo $idx ?>, '<?php echo trim(strip_tags($detail['product'])) ?>')">- X -</button>
                                            </td>
                                        <?php endif ?>
                                        </tr>
                                    <?php
                                            endforeach;
                                        endforeach;
                                    ?>
                                        <tr <?php echo !count($pack) ? 'class="d-none"' : '' ?>>
                                            <td colspan="10" class="text-center">
                                                <form action="venta.php" method="post">
                                                    <input type="hidden" name="idc" value="<?php echo $_GET['idc'] ?>" />
                                                    <button type="submit" class="btn btn-info" style="width:196px">FACTURACION <i class="fa fa-caret-right" style="margin-left:5px"></i> <i class="fa fa-caret-right"></i> <i class="fa fa-caret-right"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>


                                <div id="outbox" class="tab-pane fade">
                                    <table class="data-grid form-footer">
                                        <tr>
                                            <th>Producto</th>
                                            <th>Guía</th>
                                            <th width="35"></th>
                                            <th>Fecha</th>
                                            <th class="d-none d-md-table-cell">Tienda</th>
                                            <th class="d-none d-sm-table-cell">Tipo</th>
                                            <th>
                                                <div class="d-sm-none">Cnt.</div>
                                                <div class="d-none d-sm-block">Cantidad</div>
                                            </th>
                                            <th>
                                                <div class="d-sm-none">Kg.</div>
                                                <div class="d-none d-sm-block">Peso Kg.</div>
                                            </th>
                                            <th>
                                                <div class="d-sm-none">Pzs.</div>
                                                <div class="d-none d-sm-block">Piezas</div>
                                            </th>
                                            <th class="bg-primary text-center">Estado</th>
                                            <th class="d-none d-md-table-cell">Detalle</th>
                                        </tr>
                                        <?php
                                        foreach($outbox as $detail):
                                            foreach($detail['items'] as $key => $item):
                                                ?>
                                                <tr>
                                                    <?php if(!$key): ?>
                                                        <td rowspan="<?php echo count($detail['items']) ?>" class="text-center font-weight-bold <?php echo $detail['sale'] ? 'table-primary' : 'table-info'; ?>"><?php echo $detail['product'] ?></td>
                                                    <?php endif ?>
                                                    <td><?php echo formatCode($item->idb) ?></td>
                                                    <td class="text-center" style="line-height:30px">
                                                        <?php if($item->foto): ?>
                                                            <i class="fa fa-camera photo" style="font-size:1.3em; cursor:pointer;" data-id="<?php echo md5($item->ide) ?>"></i>
                                                        <?php else: ?>
                                                            <i class="fa fa-camera" style="color:lightgray; font-size:1.3em;"></i>
                                                        <?php endif ?>
                                                    </td>
                                                    <td><?php echo date('Y-m-d', strtotime($item->fecha)) ?> <span class="d-none d-md-inline-block" style="color:#acbbc8; margin-left:1px;"><?php echo date('h:i', strtotime($item->fecha)) ?></span></td>
                                                    <td class="d-none d-md-table-cell"><?php echo $item->tienda ?></td>
                                                    <td class="d-none d-sm-table-cell"><?php echo $item->descripcion ?></td>
                                                    <td class="text-right font-weight-bold" style="padding-right:5px"><?php echo $item->cantidad ?></td>
                                                    <td class="text-right" style="padding-right:5px"><?php echo $item->peso ? numFormat($item->peso) : '' ?></td>
                                                    <td class="text-right" style="padding-right:5px"><?php echo $item->piezas ?></td>
                                                    <td class="text-center"><?php
                                                        switch(+$item->id_accion){
                                                            case 3:
                                                                $status = 'TRANSITO';
                                                                // $desc = strtoupper($item->tipo_salida) . ' ' . formatCode($item->idp) . ': ' . $item->salida;
                                                                $desc = formatCode($item->idp) . ': ' . $item->salida;
                                                            break;
                                                            case 4:
                                                                $status = 'ENTREGADO';
                                                                $desc = $item->idc ? '<a class="btn btn-sm btn-link" href="' . $item->website. '" target="_blank"><i class="fa fa-external-link" style="margin-right:5px"></i>' . $item->carrier . ': ' . $item->guia . '</a>' : '<a class="btn btn-sm btn-link" rel="tipsy" title="<h6>' . $item->nombre . '</h6>' . $item->documento . '"><i class="fa fa-user" style="margin-right:5px"></i> Destinatario</a>';
                                                                $desc .=  $item->efoto ? '<div class="pull-right"><button class="btn btn-sm btn-primary" onclick="showPhoto(2, \'' . md5($item->entrega) . '\')" rel="tipsy" title="Detalle"><i class="fa fa-camera" style="margin: 0 5px"></i></button></div>' : '';
                                                            break;
                                                            default:
                                                                $status = 'BODEGA';
                                                                $desc = $item->pais . ' / ' . $item->agencia;
                                                            break;
                                                        }

                                                        print $status;

                                                    ?></td>
                                                    <td class="d-none d-md-table-cell"><?php echo $desc ?></td>
                                                </tr>
                                            <?php
                                            endforeach;
                                        endforeach;
                                        ?>

                                        <tr <?php echo !count($outbox) ? 'class="d-none"' : '' ?>>
                                            <td colspan="11" class="text-center">
                                                <form action="venta.php" method="post">
                                                    <input type="hidden" name="idc" value="<?php echo $_GET['idc'] ?>" />
                                                    <button type="submit" class="btn btn-info" style="width:196px">FACTURACION <i class="fa fa-caret-right" style="margin-left:5px"></i> <i class="fa fa-caret-right"></i> <i class="fa fa-caret-right"></i></button>
                                                </form>
                                            </td>
                                        </tr>

                                    </table>
                                </div>


                                <div id="balance" class="tab-pane fade" data-url="estado_de_cta.php?id=<?php echo md5($_GET['idc']) ?>" style="padding:2px"></div>


                            </div>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>


    <link rel="stylesheet" href="assets/js/fancybox/jquery.fancybox.min.css" />
    <script src="assets/js/fancybox/jquery.fancybox.min.js"></script>
	
    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>

    <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>

    <link rel="stylesheet" href="assets/js/tipsy/tipsy.css">
    <script type="text/javascript" src="assets/js/tipsy/jquery.tipsy.js"></script>

    <script type="text/javascript" src="assets/js/js.cookie.js"></script>

    <script type="text/javascript">

        var idc = <?php echo $_GET['idc'] ?>

        var pref_cookie = 'user_' + '<?php echo md5($user->id) ?>' + '_mode'
        var pack = []

        JSON.parse('<?php echo $contents ? addslashes(json_encode($contents)) : '[]' ?>').map(function(data){
            var idx = +data.ide

            pack[idx] = pack[idx] ? pack[idx] : data
            pack[idx]['items'] = pack[idx]['items'] ? pack[idx]['items'] : []

        })


        function confirmDelete(id, name){
            if(confirm("Esta a punto de eliminar le entrada \n'"+name+"', desea continuar?"))
                authorize(6, function(idu) {
                    window.location = 'entrada.php?act=3&idc=' + idc + '&id=' + id + '&idu=' + idu
                })
        }

        function confirmDeletePack(id, name){
            if(confirm("Esta a punto de deshacer el empaque \n'"+name+"', desea continuar?"))
                window.location = 'entrada.php?act=7&idc=' + idc + '&id=' + id
        }

        function validatePack(form){

            if(validate(form)){

                if($('#id_producto').val() === '0'){
                    var prd = $('#producto')
                    var i = 0

                    prd.val().split('x').map(function(item){
                        if(+item > 0)
                            i++
                    })

                    if(i!==3) {
                        alert('Debe ingresar medidas validas!')
                        stackError(prd.get(0))
                        return false
                    }

                }

                if($('input[name^=ide]').length)
                    return true
                else
                    alert('No se ha agregado ningun contenido!')

            }

            return false
        }

        $(function(){
            <?php if ($idt): ?>
                var color = ['light', 'warning'].indexOf('success') !== -1 ? 'text-dark' : 'text-light'
                var alert = $('<div class="big-message-highlight"></div><div class="alert bg-success ' + color + ' text-center big-message-success"><h5 style="margin:5px 0">LA ENTRADA SE HA CREADO, AHORA IMPRIME LA ETIQUETA</h5><button class="btn btn-sm btn-light big-print" title="<h6>Imprimir Etiquetas</h6>" data-id="<?php echo md5($idt) ?>">- IMPRIMIR ETIQUETA -</button></div>')
                $('body').append(alert)
            <?php endif; ?>

            var inbox = window.location.hash.substring(1) === 'inbox'

<?php if($err): ?>
            inbox = true
            flashAlert('<?php echo $err ?>')
<?php endif ?>

            if(inbox || Cookies.get(pref_cookie) === undefined)
                Cookies.set(pref_cookie, '#inbox')

            setMode(Cookies.get(pref_cookie))

            $('.release-btn').click(function(){
                if(confirm("Esta a punto de liberar este casillero,\ndesea continuar?"))
                    window.location = '?act=4&idc=' + idc
            })

            $('.add-btn').click(function() {
                $('#customer, #inbox-form').each(function(){
                    $(this).toggle(!$(this).is(':visible'))
                })
            })

            inbox && $('.add-btn:visible').trigger('click')

            $('.add-pack-btn').click(function() {
                $('#customer, #pack-form').each(function(){
                    $(this).toggle(!$(this).is(':visible'))
                })

                $('#search-input').focus()
            })


            $('.cancel-btn').click(function() {

                this.form.reset()

                var selector = $(this.form).closest('tr').attr('id')

                $('#box, #id_tienda, #id_clase, #id_producto').trigger('change')
                resetErrors(this.form)

                $('#customer, #' +  selector).each(function(){
                    $(this).toggle(!$(this).is(':visible'))
                })
            })

            $('#box').length && $('#box').select2({
                templateResult: function(item){
                    return $('<div class="text-right">' + item.text + '</div>')
                }
            })

            $('#id_tienda').select2({
                minimumInputLength: 2,
                placeholder: 'Buscar...',
                ajax: {
                    url: 'json/tiendas.php',
                    dataType: "json",
                    data: function(params) {
                        return {
                            term: params.term
                        }
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id
                                }
                            })
                        }
                    }
                }
            })

            $('#id_clase').select2({
                placeholder: 'Seleccionar...',
                templateResult: formatCategory,
                templateSelection: formatCategory
            }).on('change', function(){
                $.ajax("xml/productos.php", {
                    data: { id: this.value }
                }).done(function(data){

                    var sel = $('#id_producto')

                    sel.select2('destroy')
                    sel.empty()

                    $(data).find('prod').each(function(){
                        sel.append($('<option value="' + $(this).find('id').text() + '">' + $(this).find('desc').text() + '</option>'))
                    })

                    sel.select2({
                        placeholder: 'Seleccionar...',
                        templateSelection: formatProduct,
                        templateResult: formatProduct
                    }).trigger('change')

                })

            })


            $('#id_producto').select2({
                placeholder: 'Seleccionar...',
                templateSelection: formatProduct,
                templateResult: formatProduct
            }).on('change', function(){
                $('#producto').val('').toggleClass('optional-input', this.value !== '0')
                $('.measures').toggle(this.value === '0')
            }).trigger('change')

            $('#id_prog').select2({
                allowClear: true,
                placeholder: "Seleccionar..."
            })

            function formatCategory (item) {
                if(+item.id)
                    return $('<span><i class="fa fa-' + $(item.element).data('icon') + '" style="margin-right:3px"></i> ' + item.text + '</span>')
                else
                    return item.text
            }

            function formatProduct (item) {
                if(+item.id || item.id === '')
                    return item.text
                else
                    return $('<span><i class="fa fa-calculator" style="margin-right:3px"></i> ' + item.text + '</span>')
            }

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var mode = $(e.target).attr('href')
                Cookies.set(pref_cookie, mode)

                switch($(e.relatedTarget).attr('href')){
                    case '#inbox':
                        if($('#inbox-form').is(':visible'))
                            $('#inbox-form .cancel').click()
                    break
                    case '#pack':
                        if($('#pack-form').is(':visible'))
                            $('#pack-form .cancel').click()
                    break
                    default:
                        $('.add-btn, .add-pack-btn').hide()
                    break
                }

                setMode(mode)

            })

            $('.photo').click(function(){
                showPhoto(1, $(this).data('id'))
            })

            $('.print').tipsy({
                gravity: 's',
                html: true
            }).click(function(){
                window.open('etiquetas_recepcion.php?id=' + $(this).data('id'))
            });

            $('.big-print').tipsy({
                gravity: 's',
                html: true
            }).click(function(){
                window.open('etiquetas_recepcion.php?id=' + $(this).data('id'))
                $('.big-message-highlight, .big-message-success, .tipsy').remove();
            });

            $('[rel=tipsy]').tipsy({
                gravity: 's',
                html: true
            })

            $('#producto').mask('099x099x099', { placeholder: "___x___x___" })

            $('#search-input').on('keypress', function(e) {
                var code = e.keyCode || e.which

                if(code === 13) {
                    performSearch()
                    e.preventDefault()
                }
            })

            $('#search-btn').click(performSearch)

            function performSearch() {
                var val = $('#search-input').val()

                if(val !== '') {
                    $('#search-input, #search-btn').prop('disabled', true)

                    $('#search-btn').data('original', $('#search-btn').html())
                    $('#search-btn').html($('#search-btn').data('loading'))

                    $.ajax('json/paquetes.php', {
                        data: {
                            id: $('#idb').val(),
                            term: val
                        }
                    }).done(function (data) {
                        if(data){

                            if(+data.idc !== idc) {
                                flashAlert('El código NO PERTENECE AL CLIENTE!')
                                return
                            }

                            if(+data.packed)
                                flashAlert('El código YA FUE EMPACADO!', 'warning')
                            else {
                                flashAlert('Código ENCONTRADO!', 'success')
                                addTableNode(data)
                            }

                        } else
                            flashAlert('El código NO EXISTE!')

                    }).always(function() {
                        $('#search-input, #search-btn').prop('disabled', false)
                        $('#search-btn').html($('#search-btn').data('original'))
                        $('#search-input').val('').focus()
                    })
                }
            }

            function addTableNode(data) {

                var idx = +data.ide

                pack[idx] = pack[idx] ? pack[idx] : data
                pack[idx]['items'] = pack[idx]['items'] ? pack[idx]['items'] : []

                if (pack[idx]['items'].indexOf(+data.current) === -1) {

                    var table = $('#contents-table > tbody')

                    pack[idx]['items'].push(+data.current)

                    table.empty()
                    pack.map(function(row) {

                        var tr = $('<tr>')

                        $('<td class="d-none d-sm-table-cell"><input type="hidden" name="id[]" value="' + (Boolean(row.id) ? row.id : 0) + '"><input type="hidden" name="ide[]" value="' + row.ide + '"><input type="hidden" name="empaque[]" value="' + JSON.stringify(row.items) + '" >' + row.code + '</td>')
                            .appendTo(tr)

                        $('<td>' + row.date + '</td>')
                            .appendTo(tr)

                        $('<td class="d-none d-md-table-cell">' + row.store + '</td>')
                            .appendTo(tr)

                        $('<td>' + row.description + '</td>')
                            .appendTo(tr)

                        $('<td class="text-right font-weight-bold" style="padding-right:5px">' + row.items.length + '</td>')
                            .appendTo(tr)

                        $('<td class="text-right" style="padding-right:5px">' + numFormat(row.weight * row.items.length) + '</td>')
                            .appendTo(tr)

                        $('<td class="text-right" style="padding-right:5px">' + numFormatInt(row.pieces * row.items.length) + '</td>')
                            .appendTo(tr)

                        $('<td class="text-center" style="line-height:25px">' + (Boolean(row.comments) ? '<i class="fa fa-commenting" rel="tipsy" title="' + row.comments + '" style="font-size:1.3em; cursor:pointer;" ></i>' : '<i class="fa fa-commenting" style="color:lightgray; font-size:1.3em;"></i>') + '</td>')
                            .appendTo(tr)

                        $('<td style="padding:2px"><button type="button" class="btn btn-sm btn-outline-danger w-100 delete-row" data-id="' + row.ide + '" data-description="' + row.code + ': (' + row.items.length + ') ' + row.description + '">- X -</button></td>')
                            .appendTo(tr)

                        tr.appendTo(table)

                        $('[rel=tipsy]').tipsy({
                            gravity: 's',
                            html: true
                        })

                    })

                } else
                    flashAlert('El código YA ESTA EN LA LISTA', 'warning')

            }

            $('#contents-table').on('click', '.delete-row', function(){
                if(confirm('Desea quitar el artículo: ' + $(this).data('description') + ' ?')) {
                    delete pack[+$(this).data('id')]
                    $(this).closest('tr').remove()
                }
            })

            function setMode(mode) {

                $('a[href=\\' + mode + ']').tab('show')
                $('.add-btn, .add-pack-btn').hide()

                switch(mode){
                    case '#inbox':
                        $('.add-btn').show()
                    break
                    case '#pack':
                        $('.add-pack-btn').show()
                    break
                }
            }

        })
    </script>

    <script type="text/javascript" src="assets/js/pdfobject.min.js"></script>
    <script>

        $(function(){

            var timer = false
            $(window).resize(function() {
                if(timer)
                    clearTimeout(timer)
                timer = setTimeout(function(){
                    $('#balance').css('height', parseInt($('#balance').width() * .66) + 'px') // THUMBNAILS + LANDSCAPE
                    timer = false
                }, 300)
            }).trigger('resize')

            $('a[href=\\#balance]').click(function(){
                $(window).trigger('resize')
            })

        })

        var options = {
            pdfOpenParams: {
                view: 'FitH',
                pagemode: 'thumbs',
                statusbar: '0',
                messages: '0',
                navpanes: '0'
            },
            forcePDFJS: !PDFObject.supportsPDFs,
            PDFJS_URL: "assets/js/pdfjs/web/viewer.html"
        }
        PDFObject.embed((PDFObject.supportsPDFs ? './' : '../../../../') + $('#balance').data('url'), "#balance", options);

    </script>

</div>
<?php include 'footer.php' ?>