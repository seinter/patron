<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <link rel="stylesheet" href="assets/js/menu/jquerycssmenu.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="assets/js/fancybox/jquery.fancybox.min.css" />
    <script src="assets/js/fancybox/jquery.fancybox.min.js"></script>

    <script type="text/javascript" src="assets/js/menu/jquerycssmenu.js"></script>
    <script type="text/javascript" src="assets/js/functions.js"></script>

</head>

<body>
<?php
/* SANDBOX WATERMARK */
if(SANDBOX && basename($_SERVER['PHP_SELF']) !== 'viewer.php'): ?>
    <script type="text/javascript">

        var watermark = document.createElement("img");
        watermark.setAttribute('src', 'images/watermark.png');
        watermark.setAttribute('class', 'watermark');
        document.body.appendChild(watermark);

    </script>
<?php endif ?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col text-center text-sm-left"><img src="assets/img/logo.jpg" width="150" style="margin:5px;"></div>
                <div class="col d-none d-sm-block text-right">
                    <div style="margin:10px"><span><?php echo $title ?></span></div>
                    <div style="position:absolute; bottom:0; right:15px; margin:10px 0">
                        <span class="badge badge-danger" style="font-size:13px; padding:5px 10px">AREA DEL CLIENTE</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row main-menu">
        <div class="col">
            <div class="row">
                <div class="col d-md-none">
                    <div class="navbar-dark">
                        <a class="btn btn-primary navbar-toggler" data-toggle="collapse" aria-expanded="false" aria-controls="main-menu-collapse" role="button" href="#main-menu-collapse" style="margin:8px 0">
                            <span class="navbar-toggler-icon"></span>
                        </a>
                    </div>
                </div>
                <div class="col d-none d-md-block">
                    <div class="row">
                        <div id="top-menu" class="jquerycssmenu">
                            <?php include 'menu.php' ?>
                        </div>
                    </div>
                </div>
                <div class="col col-md-2 text-right today" style="padding-left:0"><?php echo date("l M j, Y") ?></div>
            </div>
        </div>
    </div>
    <div class="row d-md-none collapse main-menu-collapse" id="main-menu-collapse">
        <?php include 'menu.php' ?>
    </div>
</div>