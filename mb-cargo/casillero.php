<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();


if($_GET['id']){
    $str = "SELECT id, CONCAT(nombre, ' ', apellido) customer, casillero box FROM clientes WHERE id = " . $_GET['id'] . " AND status";
    $res = mysql_query($str, $link);
    if(mysql_num_rows($res))
        extract(mysql_fetch_assoc($res));
    else
        $close = true;
} else
    $close = true;

if($box){

    $items = $_GET['items'] ? json_decode($_GET['items']) : [];
    $str = "SELECT detalle_venta.id idd, clasificacion.id idc, clasificacion.id_tipo, clasificacion.clase, clasificacion.icono, productos.id idp, IF(productos.id, productos.producto, detalle_venta.descripcion) producto, bitacora.id idb, contenidos.id, contenidos.creado fecha, tiendas.tienda, contenidos.cantidad, contenidos.descripcion, contenidos.peso, contenidos.piezas, entradas.id ide, IF(entradas.foto IS NULL, 0, 1) foto FROM contenidos INNER JOIN entradas ON contenidos.id_entrada = entradas.id INNER JOIN tiendas ON contenidos.id_tienda = tiendas.id INNER JOIN bitacora ON contenidos.id_bitacora = bitacora.id INNER JOIN detalle_venta ON bitacora.id_detalle = detalle_venta.id INNER JOIN clasificacion ON detalle_venta.id_clase = clasificacion.id LEFT JOIN productos ON detalle_venta.id_producto = productos.id WHERE detalle_venta.id_cliente = {$id} AND NOT detalle_venta.id_venta " . ($items ? ' AND detalle_venta.id NOT IN (' . implode(', ', $items). ')' : '') . " AND detalle_venta.status AND bitacora.status AND contenidos.id_entrada AND contenidos.status ORDER BY detalle_venta.creado";
    $res = mysql_query($str, $link);
    $pack = [];
    while($row = mysql_fetch_assoc($res)){
        $idx = array_shift($row);
        $pack[$idx]['category'] = (object) ['id' => array_shift($row), 'type' => array_shift($row), 'name' => array_shift($row), 'icon' => array_shift($row)];
        $pack[$idx]['product'] = (object) ['id' => +array_shift($row), 'name' => array_shift($row)];
        $pack[$idx]['items'][] = (object) $row;
    }
}


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>

    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="assets/js/fancybox/jquery.fancybox.min.css" />
    <script src="assets/js/fancybox/jquery.fancybox.min.js"></script>

    <script type="text/javascript" src="assets/js/functions.js"></script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>

    <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>

    <link rel="stylesheet" href="assets/js/tipsy/tipsy.css">
    <script type="text/javascript" src="assets/js/tipsy/jquery.tipsy.js"></script>

    <style type="text/css">
        <!--
        table {
            border-collapse: initial;
        }
        table caption {
            caption-side: initial;
        }
        -->

        #inbox th {
            background-color: #4F7DB0;
        }

    </style>

    <script type="text/javascript">

        var initial = '[]'
        var items = JSON.parse(initial)

        if(window.self === window.top)
            window.location = 'index.php'

        function closeLightBox(){
            window.parent.$.fancybox.close()
        }

    <?php if($close): ?>
        closeLightBox()
    <?php endif ?>

    </script>

    <script type="text/javascript">
        $(function(){

            $('input.item').click(function(e){

                var self = this

                var row = $('tr.item-' + self.value)
                    .toggleClass('bg-primary font-weight-bold text-light', this.checked)
                    .first()

                if(this.checked)
                    items.push({
                        id: self.value,
                        idc: row.find('input.idc').val(),
                        cat: row.find('input.category').val(),
                        icon: row.find('input.icon').val(),
                        type: row.find('input.type').val(),
                        idp: row.find('input.idp').val(),
                        desc: row.find('input.product').val()
                    })
                else
                    items = items.filter(function(item){
                        return +item.id !== +self.value
                    })

                $('#items').val(JSON.stringify(items))

                e.stopPropagation()
            }).parent().click(function(e){
                $(this).find('input.item').trigger('click')
            })

            $('.cancel').click(function(){
                $('#items').val(initial)
                closeLightBox()
            })

            $('.save').click(closeLightBox)

            $('[rel=tipsy]').tipsy({
                html: true,
                gravity: 's'
            })

            $('.photo').tipsy({
                html: true,
                gravity: 'w'
            })
        })
    </script>


</head>

<body class="modal-container">



<div class="container-fluid">

    <div class="row main-title">
        <div class="col">
            <div class="row">
                <div class="col-5 text-truncate caption"><?php echo $customer ?></div>
            <?php if ($box): ?>
                <div class="col-7 text-right text-truncate caption">CASILLERO [ <i class="fa fa-inbox"></i> #<?php echo $box ?> ]</div>
            <?php endif ?>
            </div>
        </div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">


                <table width="100%" border="0" cellspacing="10" cellpadding="0">
                    <tr>
                        <td style="padding:0; height:300px; vertical-align:top;">
                        <?php if ($box): ?>
                            <form id="inbox">

                                <input type="hidden" id="items" name="items" value="[]" >

                                <table class="data-grid form-footer">
                                    <caption>EMPAQUE</caption>
                                    <tr>
                                        <th width="40"></th>
                                        <th>Producto</th>
                                        <th width="35"></th>
                                        <th>Fecha</th>
                                        <th class="d-none d-md-table-cell">Tienda</th>
                                        <th class="d-none d-sm-table-cell">Tipo</th>
                                        <th>
                                            <div class="d-sm-none">Cnt.</div>
                                            <div class="d-none d-sm-block">Cantidad</div>
                                        </th>
                                        <th>
                                            <div class="d-sm-none">Kg.</div>
                                            <div class="d-none d-sm-block">Peso Kg.</div>
                                        </th>
                                        <th>
                                            <div class="d-sm-none">Pzs.</div>
                                            <div class="d-none d-sm-block">Piezas</div>
                                        </th>
                                    </tr>
                            <?php
                            foreach($pack as $idx => $detail):
                                foreach($detail['items'] as $key => $item):
                            ?>
                                    <tr class="item-<?php echo $idx  ?>">
                                    <?php if(!$key): ?>
                                        <td rowspan="<?php echo count($detail['items']) ?>" class="text-center" style="cursor:pointer">
                                            <input type="hidden" class="idc" value="<?php echo $detail['category']->id ?>">
                                            <input type="hidden" class="category" value="<?php echo $detail['category']->name ?>">
                                            <input type="hidden" class="type" value="<?php echo $detail['category']->type ?>">
                                            <input type="hidden" class="icon" value="<?php echo $detail['category']->icon ?>">
                                            <input type="hidden" class="idp" value="<?php echo $detail['product']->id ?>">
                                            <input type="hidden" class="product" value="<?php echo $detail['product']->name ?>">
                                            <input type="checkbox" class="item" value="<?php echo $idx ?>" style="margin:8px; cursor:pointer;">
                                        </td>
                                        <td rowspan="<?php echo count($detail['items']) ?>" class="text-center font-weight-bold table-primary text-dark">
                                            <i class="fa fa-<?php echo $detail['category']->icon ?>" rel="tipsy" title="<h6><?php echo $detail['category']->name ?></h6>"></i>
                                            <?php echo $detail['product']->name ?></td>
                                    <?php endif ?>
                                        <td class="text-center" style="line-height:30px">
                                        <?php if($item->foto): ?>
                                            <i class="fa fa-camera photo" style="font-size:1.3em; cursor:pointer;" title="<img class='img-fluid' src='file.php?dll=1&id=<?php echo md5($item->ide) ?>' style='margin:3px 0' /><h6 class='text-truncate'><?php echo $item->descripcion ?></h6>"></i>
                                        <?php else: ?>
                                            <i class="fa fa-camera" style="color:lightgray; font-size:1.3em;"></i>
                                        <?php endif ?>
                                        </td>
                                        <td><?php echo date('Y-m-d', strtotime($item->fecha)) ?> <span class="d-none d-md-inline-block" style="color:#acbbc8; margin-left:1px;"><?php echo date('h:i', strtotime($item->fecha)) ?></span></td>
                                        <td class="d-none d-md-table-cell"><?php echo $item->tienda ?></td>
                                        <td class="d-none d-sm-table-cell"><?php echo $item->descripcion ?></td>
                                        <td class="text-right" style="padding-right:5px"><?php echo $item->cantidad ?></td>
                                        <td class="text-right" style="padding-right:5px"><?php echo numFormat($item->peso) ?></td>
                                        <td class="text-right" style="padding-right:5px"><?php echo $item->piezas ?></td>
                                    </tr>
                            <?php
                                endforeach;
                            endforeach;
                            ?>

                                    <tr>
                                        <td colspan="9" class="text-center">
                                            <div class="d-none d-sm-block">
                                                <button type="button" class="btn btn-light cancel">&lt;&lt;&lt; Cancelar</button>
                                                <input type="button" class="btn btn-primary save" value="Aceptar &gt;&gt;&gt;" />
                                            </div>
                                            <div class="d-sm-none">
                                                <button type="button" class="btn btn-light w-100 cancel">&lt;&lt;&lt; Cancelar</button>
                                                <input type="button" class="btn btn-primary w-100 save" value="Aceptar &gt;&gt;&gt;" style="margin-top:5px" />
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </form>

                        <?php else: ?>
                            <div class="text-center text-muted" style="padding-top:80px">
                                <h5><i class="fa fa-inbox" style="margin-right:5px"></i> No tiene asignado casillero.</h5>
                                <button type="button" class="btn btn-light save" style="margin-top:20px">Aceptar &gt;&gt;&gt;</button>
                            </div>
                        <?php endif ?>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</div>





<div class="container-fluid" style="position:fixed; bottom:0;">
    <div class="row main-footer">
        <div class="col copyright"><?php echo $copyright ?></div>
    </div>
</div>

</body>
</html>