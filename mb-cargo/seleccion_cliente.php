<?php
session_start();
require("config.php");
include("classes/system.inc.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);


if($_POST['idc']){

    $link = DB::connect();

    $ida = $user->id_central ?: $user->id_agencia;
    $str = "SELECT * FROM clientes WHERE id_agencia = {$ida} AND casillero = " . $_POST['box'] . " AND status";
    $res = mysql_query($str, $link);
    if(!mysql_num_rows($res))
        mysql_query('UPDATE clientes SET casillero = ' . $_POST['box'] . ' WHERE id = ' . $_POST['idc'], $link);

    $close = true;
}

// WHILE ASSIGNING, ONLY CUSTOMERS WITHOUT BOX (ACT 2)
// AUTO ASSIGN WHEN NECESSARY (ACT 4)
$act = +$_GET['box'] === -1 ? 4 : 2;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>

    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="assets/js/fancybox/jquery.fancybox.min.css" />
    <script src="assets/js/fancybox/jquery.fancybox.min.js"></script>

    <script type="text/javascript" src="assets/js/functions.js"></script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>

    <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>

    <style type="text/css">
        <!--
        table {
            border-collapse: initial;
        }
        table caption {
            caption-side: initial;
        }
        -->
    </style>

    <script type="text/javascript">

        if(window.self === window.top)
            window.location = 'index.php'

    <?php if($close): ?>
        window.parent.$.fancybox.close()
    <?php endif ?>

    </script>

</head>

<body class="modal-container">

<?php include 'busqueda_cliente.php' ?>

<div class="container-fluid" style="position:fixed;bottom:0;">
    <div class="row main-footer">
        <div class="col copyright"><?php echo $copyright ?></div>
    </div>
</div>

</body>
</html>