<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$res = mysql_query("SELECT id_tipo, tipo, clase, icono FROM clasificacion INNER JOIN tipo_clases ON clasificacion.id_tipo = tipo_clases.id WHERE clasificacion.id = $id", $link);
extract(mysql_fetch_assoc($res));

if(+$id_tipo === 1){
    $ida = $user->id_central ?: $user->id_agencia;
    $res = mysql_query("SELECT tarifa FROM tarifas_pie WHERE id_agencia = {$ida} AND origen = {$org} AND destino = {$dest} AND status", $link);
    $tarifa = @mysql_result($res, 0) ?: 0;
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>

    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="assets/js/functions.js"></script>

    <script type="text/javascript">

        if(window.self === window.top)
            window.location = 'index.php'

        function closeLightBox(){
            window.parent.$.fancybox.close();
        }

    </script>

    <script type="text/javascript">
        $(function(){
            $('.factor').mask('000')
                .on('blur', function(){
                    this.value = +this.value > 0 ? +this.value : ''

                    if($('#l').val() !== '' && $('#w').val() !== '' && $('#h').val() !== ''){
                        $('#desc').val($('#l').val() + 'x' + $('#w').val() + 'x' + $('#h').val())

                        $('#product').html(+$('#l').val() * +$('#w').val() * +$('#h').val())
                        $('#feets').html(round(+$('#product').html() / +$('#factor').html(), 3))
                        $('#total').val(numFormat(+$('#feets').html() * +$('#rate').html()))
                    } else {
                        $('#product, #feets').html(0)
                        $('#total').val(0)
                    }

                })

            $('.cancel').click(closeLightBox)

            $('.save').click(function(){
                if(validate(this.form)){
                    $('#act').val(1)
                    closeLightBox()
                }
            })
        })
    </script>

    <style type="text/css">
        <!--
        body {
            max-width: 450px;
        }
        table {
            border-collapse: initial;
        }
        table caption {
            caption-side: top;
        }

        @media (min-width: 300px) {
            .footer-sm {
                display: none;
            }
        }
        @media (max-width: 299px) {
            .footer-md {
                display: none;
            }
        }
        .variable {
            display: inline-block;
            font-size: 16px;
        }
        -->
    </style>

</head>

<body>
<div class="container-fluid">
    <div class="row main-title">
        <div class="col text-right text-truncate caption"><i class="fa fa-<?php echo $icono ?>" style="margin-right:3px"></i> <?php echo strtoupper($clase) ?></div>
    </div>
</div>

<table width="100%" border="0" cellpadding="0" cellspacing="10">

    <tr>
        <td>
            <form onsubmit="return false">
                <input type="hidden" id="act" value="0" />

                <table class="data-table form-fields form-footer">
                    <caption><?php echo strtoupper($tipo)  ?> NO LISTADO</caption>

                <?php
                    switch($id_tipo) {
                        case 1:
                ?>
                        <tr>
                            <th>Largo</th>
                            <th>Ancho</th>
                            <th>Alto</th>
                        </tr>
                        <tr>
                            <td><input type="text" id="l" class="text-right factor"></td>
                            <td><input type="text" id="w" class="text-right factor"></td>
                            <td><input type="text" id="h" class="text-right factor"></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-center" style="background-color:#F1F1F1; font-weight:bold; padding:5px">
                                = <div id="product" class="variable">0</div> / <div id="factor" class="variable">129360</div> = <div class="variable" style="white-space:nowrap"><div id="feets" class="variable">0</div>ft</div> * <div id="rate" class="variable"><?php echo numFormat($tarifa) ?></div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">Tarifa USD:</th>
                            <td>
                                <input type="hidden" id="desc">
                                <input type="text" id="total" class="text-right" value="0" readonly>
                            </td>
                        </tr>
                <?php
                        break;
                        default:
                ?>
                            <tr>
                                <th>Descripcion:</th>
                                <td colspan="2"><input type="text" id="desc"></td>
                            </tr>
                            <tr>
                                <th>Tarifa USD:</th>
                                <td><input type="text" id="total" class="text-right" value="0.00" onblur="this.value = numFormat(this.value)"></td>
                                <td width="33%"></td>
                            </tr>
                <?php
                        break;
                    }
                ?>

                    <tr>
                        <td colspan="3" class="text-center">
                            <div class="row footer-md">
                                <div class="col-6" style="padding-right:5px">
                                    <input name="button" type="button" class="btn btn-danger cancel w-100" value="&lt;&lt;&lt; Cancelar" />
                                </div>
                                <div class="col-6" style="padding-left:5px">
                                    <input name="button" type="button" class="btn btn-primary save w-100" value="Aceptar &gt;&gt;&gt;" />
                                </div>
                            </div>
                            <div class="footer-sm">
                                <input name="button" type="button" class="btn btn-danger w-100 cancel" value="&lt;&lt;&lt; Cancelar" />
                                <input name="button" type="button" class="btn btn-primary w-100 save" value="Aceptar &gt;&gt;&gt;" style="margin-top:5px" />
                            </div>
                        </td>
                    </tr>
                </table>

            </form>
        </td>
    </tr>
</table>



<div class="container-fluid" style="position:fixed;bottom:0;">
    <div class="row main-footer">
        <div class="col copyright"><?php echo $copyright ?></div>
    </div>
</div>

</body>
</html>