<?php
session_start();
require("../config.php");
include("../classes/system.inc.php");
include('../functions.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$ida = $user->id_central ?: $user->id_agencia;

header("Content-type: text/xml");
$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

$fare = 0;
$str = "SELECT tarifa FROM tarifas_cliente WHERE id_cliente = {$idc} AND id_producto = $id AND origen = $org AND destino = $dest;";
$res = mysql_query($str, $link);
if (!mysql_num_rows($res)) {
    if($idd = $_GET['idd']){
        $str = "SELECT detalle_venta.*, tarifas.tarifa, tarifas_pie.tarifa tarifa_pie FROM detalle_venta LEFT JOIN productos ON detalle_venta.id_producto = productos.id LEFT JOIN tarifas ON detalle_venta.id_producto = tarifas.id_producto AND tarifas.id_agencia = {$ida} AND tarifas.origen = {$org} AND tarifas.destino = {$dest} AND tarifas.status LEFT JOIN tarifas_pie ON tarifas_pie.id_agencia = {$ida} AND tarifas_pie.origen = {$org} AND tarifas_pie.destino = {$dest} AND tarifas_pie.status WHERE detalle_venta.id = {$idd}";

        $res = mysql_query($str, $link);
        if($row = @mysql_fetch_object($res)) {

            if (+$row->id_producto)
                $fare = $row->tarifa;
            else
                $fare = numFormat((array_reduce(explode('x', $row->descripcion), function ($mix, $item) {
                        return $mix * $item;
                }, 1) / 129360) * $row->tarifa_pie);
        }

    } else {
        $str = "SELECT tarifa FROM tarifas WHERE id_agencia = {$ida} AND id_producto = $id AND origen = $org AND destino = $dest AND status;";
        $res = mysql_query($str, $link);
        $fare = @mysql_result($res, 0) ?: 0;
    }
} else {
    $fare = @mysql_result($res, 0) ?: 0;
}

$xml .= "<records>";
$xml .= "    <record><tar>{$fare}</tar></record>";
$xml .= "</records>";

print $xml;

?>