<?php include("config.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/js/functions.js"></script>

    <script type="text/javascript">

        if(window.self !== window.top)
            window.top.location = 'index.php'

        $(function(){
            $('#name').focus()
        })
    </script>

</head>

<body style="padding-bottom:0">

<?php
/* SANDBOX WATERMARK */
if(SANDBOX && basename($_SERVER['PHP_SELF']) !== 'viewer.php'):
    ?>
    <script type="text/javascript">

        var watermark = document.createElement("img");
        watermark.setAttribute('src', 'images/watermark.png');
        watermark.setAttribute('class', 'watermark');
        document.body.appendChild(watermark);

    </script>
<?php endif ?>

<nav class="navbar navbar-dark navbar-expand-md bg-secondary">
    <div class="container-fluid" style="padding:0">
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navcol">
            <div class="col-md-7 offset-md-5" style="padding:0">

                <hr class="d-md-none" style="margin-bottom:10px">

                <form method="post" action="auth.php" enctype="application/x-www-form-urlencoded" autocomplete="on" onsubmit="return validate(this)">
                    <input type="hidden" name="act" value="2">

                    <div class="form-row align-items-center">
                        <div class="col-3 font-weight-bold text-center text-white text-truncate">ACCESO CLIENTES</div>
                        <div class="col-4">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">@</div>
                                </div>
                                <input type="email" class="form-control" name="email" placeholder="E-mail" style="font-size:12px">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">#</div>
                                </div>
                                <input type="text" class="form-control" name="code" placeholder="Código" style="font-size:12px">
                            </div>
                        </div>

                        <div class="col-2">
                            <button type="submit" class="btn btn-sm btn-info w-100"><i class="fa fa-sign-in" style="font-size:1.2em"></i></button>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>
</nav>

<form action="auth.php" method="post" enctype="application/x-www-form-urlencoded" autocomplete="on" onsubmit="return validate(this)">
    <div class="container">
        <div class="row">
            <div class="col-12 d-none d-md-block" style="height:100px"></div>
            <div class="col-12 d-none d-sm-block" style="height:50px"></div>
        </div>
        <div class="row">
            <div class="col-12 login-form">
                <div class="row login-title">
                    <div class="col text-right text-truncate caption"><?php echo $title ?></div>
                </div>
                <div class="row" style="position:relative; padding:15px 0">
                    <img class="d-none d-md-block" src="assets/img/logo.jpg" width="200" style="border:0; position:absolute; top:0; left:0;">
                    <div class="col-12 d-md-none text-center" style="padding:0">
                        <img src="assets/img/logo.jpg" width="180" style="border:0">
                    </div>
                    <div class="col-12 col-md-8 offset-md-4 text-center text-warning" style="padding:10px 10px 20px">
                        <div class="d-none d-md-block" style="height:50px"></div>
                        <span class="lead">INGRESO AL SISTEMA</span>
                    </div>
                    <div class="col-12 col-md-5 text-center">
                        <?php if($ad): ?>
                        <div class="row">
                            <div class="col-12 col-sm-1 offset-sm-5 col-md-12 offset-md-0">
                                <div class="d-none d-md-block" style="height:25px"></div>
                                <i class="text-danger fa fa-exclamation-circle blink" style="font-size:50px"></i>
                            </div>
                            <div class="col-12 col-sm-5 col-md-12">
                                <div class="d-none d-sm-block d-md-none" style="height:8px"></div>
                                <h6 class="text-danger font-weight-bold blink" style="line-height:2">ACCESO DENEGADO</h6>
                            </div>
                        </div>
                        <?php endif ?>
                        <div class="d-none d-sm-block d-md-none" style="height:20px"></div>
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="row">
                            <div class="col-12 col-sm-4"><label>USUARIO:</label></div>
                            <div class="col-12 col-sm-8"><input name="name" type="text" id="name" style="width:100%" /></div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-4"><label>PASSWORD:</label></div>
                            <div class="col-12 col-sm-8"><input type="password" name="password" id="password" autocomplete="off" style="width:100%"/></div>
                        </div>
                        <div class="row">
                            <div class="col-8 offset-4">
                                <button type="submit" class="btn btn-danger form-control" value="#">
                                    LOG IN
                                    <i class="fa fa-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row login-footer">
                    <div class="col text-truncate copyright" style="width:0"><?php echo str_replace('<br>', '&nbsp;&nbsp;', $copyright) ?></div>
                </div>

            </div>
        </div>
    </div>
</form>
</body>
</html>