<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, -1);

if($user->id_nivel > 1){

    $link = DB::connect();

    $ida = $user->id_central ?: $user->id_agencia;
    $customers = [];
    $res = mysql_query("SELECT id, CONCAT(nombre, ' ', apellido) cliente, casillero FROM clientes WHERE id_agencia = {$ida} AND casillero AND status ORDER BY casillero", $link);
    while($row = mysql_fetch_object($res))
        $customers[] = $row;

}

?>
<?php include 'header.php' ?>
    <form id="rep" name="rep" method="post" action="viewer.php" target="_blank" onsubmit="return validate(this)">
        <input type="hidden" name="dll" value="20" />

        <div class="container-fluid">

            <div class="row main-title">
                <div class="col text-right text-truncate caption">REPORTE DE ENTRADAS</div>
            </div>
            <div class="row main-content">
                <div class="col table-responsive">
                    <div class="row">

                        <table width="100%" border="0" cellspacing="10" cellpadding="0">

                        <?php if($user->id_nivel > 1): ?>
                            <tr>

                                <td style="padding:0">
                                    <div class="alert alert-dark big-box">

                                        <div class="row">
                                            <div class="col-4 text-right" style="line-height:38px">
                                                <h4 class="text-truncate" style="margin:5px 0">CLIENTE</h4>
                                            </div>
                                            <div class="col-8 big-combo">
                                                <select name="idc" id="idc" style="width:100%">
                                                    <option value="0">&nbsp;- - - TODOS - - - </option>
                                                <?php foreach($customers as $row): ?>
                                                    <option value="<?php echo $row->id ?>">[ #<?php echo $row->casillero ?> ] <?php echo $row->cliente ?></option>
                                                <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </td>

                            </tr>
                        <?php endif ?>

                            <tr>
                                <td>

                                    <table cellpadding="0" cellspacing="0" class="data-form">
                                        <caption>
                                            RANGO DE FECHAS
                                        </caption>
                                        <tr>
                                            <th>Fecha Inicial:</th>
                                            <td>
                                                <div class="date-field">
                                                    <input name="f1" type="text" id="f1" value="<?php echo date("Y-m-d"); ?>" readonly="readonly" />
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </td>
                                            <th>Fecha Final:</th>
                                            <td>
                                                <div class="date-field">
                                                    <input name="f2" type="text" id="f2" value="<?php echo date("Y-m-d"); ?>" readonly="readonly" />
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="4" class="text-center">
                                                <div class="row d-sm-none">
                                                    <div class="col-12">
                                                        <button type="button" class="btn btn-success print w-100 excel"><i class="fa fa-file-excel-o" style="margin-right:5px"></i> Generar</button>
                                                    </div>
                                                    <div class="col-12" style="margin-top:5px">
                                                        <button type="button" class="btn btn-danger print w-100 pdf"><i class="fa fa-file-pdf-o" style="margin-right:5px"></i> Generar</button>
                                                    </div>
                                                </div>
                                                <div class="d-none d-sm-block">
                                                    <button type="button" class="btn btn-success print excel"><i class="fa fa-file-excel-o" style="margin-right:5px"></i> Generar</button>
                                                    <button type="button" class="btn btn-danger print pdf"><i class="fa fa-file-pdf-o" style="margin-right:5px"></i> Generar</button>
                                                </div>
                                            </th>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>


            <script language="javascript" type="text/javascript" src="popcalendar.js"></script>
            <script language="javascript" type="text/javascript">
                function afterClose(obj){
                    return;
                }
            </script>

            <link rel="stylesheet" href="assets/js/select2/select2.min.css">
            <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>
            <script type="text/javascript">
                $(function(){
                    $('select').select2()

                    $('.date-field input').click(function() {
                        showCalendar(this, this, 'yyyy-mm-dd','es',1)
                    })

                    $('.excel').click(function(){
                        this.form.action = 'rep_entradas_xls.php'
                        this.form.submit()
                    })

                    $('.pdf').click(function(){
                        this.form.action = 'viewer.php'
                        this.form.submit()
                    })
                })
            </script>

        </div>
    </form>

<?php include 'footer.php' ?>