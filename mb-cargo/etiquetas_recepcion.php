<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

require 'vendor/autoload.php';
use \Picqer\Barcode\BarcodeGeneratorPNG;

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();


$str = "SELECT entradas.id, entradas.codigo, entradas.cantidad, entradas.peso, CONCAT(clientes.nombre, ' ', clientes.apellido) cliente, clientes.casillero, tiendas.tienda, usuarios.nombre agente, entradas.creado FROM entradas INNER JOIN clientes ON entradas.id_cliente = clientes.id INNER JOIN tiendas ON entradas.id_tienda = tiendas.id INNER JOIN usuarios ON entradas.id_usuario = usuarios.id WHERE MD5(entradas.id) = '" . $_GET['id'] . "'";
$res = mysql_query($str, $link);
extract(mysql_fetch_assoc($res));

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $title ?></title>
	<style>
		* {
			margin: 0;
			padding 0;
		}

		body {
			color: black;
			font-family: Arial;
			font-size: 12px;
			line-height: 18px;
		}

		.sticker {
			position: relative;
			margin: 0 auto;
			width: 366px;
			height: 126px;
			border: 1px black dashed;
			margin-bottom: 0px;
			padding: 8px;
		}

		.big {
			position: absolute;
			top:0;
			right: 0;
			text-align: center;
			width: 183px;
			font-size: 80px;
			font-weight: bold;
			line-height: 142px;
		}
	</style>
</head>
<body>
	<div class="sticker">
		<div><b>RECEIPT <?php echo $company ?> # <?php echo formatCode($id) ?></b> <div style="float:right">Date: <b><?php echo $creado ?>&nbsp;</b></div></div>
		<div>Store: <b><?php echo $tienda ?></b></div>
		<div>Weight: <b><?php echo numFormat($peso) ?> Kg</b></div>
		<div>Client: <b><?php echo $cliente ?></b></div>
		<div>Boxes: <b><?php echo $cantidad ?></b></div>
		<div>Received: <b><?php echo $agente ?></b></div>
		<div style="text-align:right; font-style:italic">Receipt Valid for 15 days&nbsp;</div>
	</div>
<?php
	for($i = 1; $i <= $cantidad; $i++):
		$code = formatCode($id) . '_' . $i . '_' . $codigo;

		$generator = new BarcodeGeneratorPNG();
		$barcode = 'data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128));
?>
	<div class="sticker">
		<div class="big"><?php echo $casillero ?></div>
		<div><b>ENTRY # <?php echo formatCode($id) ?></b></div>
		<div>Weight: <b><?php echo numFormat($peso / $cantidad) ?> Kg</b></div>
		<div>Client: <b><?php echo $cliente ?></b></div>
		<div>Box: <b><?php echo $i . ' / ' . $cantidad ?></b></div>
		<div>Date: <b><?php echo $creado ?></b></div>
		<div>Code: <b><?php echo $code ?></b></div>
		<div>Store: <b><?php echo $tienda ?></b></div>
	</div>
	<div class="sticker">
		<div style="text-align:center; padding:8px 0"><img src="<?php echo $barcode ?>" width="260" height="50"></div>
		<div>Code: <b><?php echo $code ?></b> <div style="float:right">Date: <b><?php echo $creado ?>&nbsp;</b></div></div>
		<div>Box: <b><?php echo $i . ' / ' . $cantidad ?></b></div>
		<div>Store: <b><?php echo $tienda ?></b></div>
	</div>
<?php endfor ?>
	
</body>
</html>