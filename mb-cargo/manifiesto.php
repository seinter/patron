<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$str = "SELECT programacion.descripcion, programacion.fecha, local1.localidad origen, local2.localidad destino, tipo FROM programacion INNER JOIN localidades local1 ON programacion.origen = local1.id INNER JOIN localidades local2 ON programacion.destino = local2.id INNER JOIN tipo_salidas ON programacion.id_tipo = tipo_salidas.id WHERE programacion.id = {$id}";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
    $$key = $value;

?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col">
            <div class="row">
                <div class="col-4">
                    <a href="manifiestos.php" class="btn btn-danger">
                        <i class="fa fa-arrow-left"></i>
                        REGRESAR
                    </a>
                </div>
                <div class="col-8 text-right text-truncate caption">MANIFIESTO DE <?php echo strtoupper($tipo) ?></div>
            </div>
        </div>
    </div>

    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">


    <table width="100%" border="0" cellspacing="10" cellpadding="0">
<?php

$str = "SELECT bitacora.id, bitacora.codigo, ventas.id id_venta, ventas.cargos, ventas.declarado, ventas.pc_seguro, ventas.descuento, IF(ventas.id IS NULL, detalle_venta.id_cliente, ventas.id_cliente) id_cliente, local1.localidad origen, local2.localidad destino, CONCAT(clientes.nombre, ' ', clientes.apellido) nombre, clientes.telefono, destinatarios.nombre dest_nombre, destinatarios.telefono dest_telefono, clasificacion.clase, IF(productos.id, productos.producto, detalle_venta.descripcion) producto, SUM(detalle_venta.tarifa * detalle_venta.cantidad) monto, pagos.pagos, pagos.costos FROM historial INNER JOIN bitacora ON historial.id_bitacora = bitacora.id INNER JOIN detalle_venta ON bitacora.id_detalle = detalle_venta.id INNER JOIN clasificacion ON detalle_venta.id_clase = clasificacion.id LEFT JOIN ventas ON detalle_venta.id_venta = ventas.id LEFT JOIN clientes ON ventas.id_cliente = clientes.id OR detalle_venta.id_cliente = clientes.id LEFT JOIN destinatarios ON ventas.id_dest = destinatarios.id LEFT JOIN localidades local1 ON ventas.origen = local1.id LEFT JOIN localidades local2 ON ventas.destino = local2.id LEFT JOIN productos ON detalle_venta.id_producto = productos.id LEFT JOIN (SELECT id_venta, SUM(monto) pagos, SUM(costo) costos FROM pagos WHERE status GROUP BY id_venta) pagos ON ventas.id = pagos.id_venta WHERE detalle_venta.status AND bitacora.status AND historial.id_prog = {$id} AND historial.status ORDER BY historial.id;";
$res = mysql_query($str, $link);
while($row = mysql_fetch_assoc($res))
    $records[$row["destino"] ? $row["destino"] : 0][] = $row;

if(records) {
    foreach ($records as $key => $value) {

?>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" class="data-grid">
                    <caption>DESTINO: <?php echo $key ? $key : '- - - -' ?> | (<?php echo count($value) ?>)</caption>
                    <tr>
                        <th>No. Guia</th>
                        <th>No. Venta</th>
                        <th>Remitente</th>
                        <th>Teléfono</th>
                        <th>Destino</th>
                        <th>Producto</th>
                        <th width="110">Saldo</th>
                        <th width="100"></th>
                    </tr>
                <?php
                    foreach ($value as $row) {
                        $idv = $row['id_venta'] ?: 0;
                        $ensurance = ($row['declarado'] * $row['pc_seguro']) / 100;
                        $balance = $row['monto'] + $row['cargos'] + $ensurance + $row['costos'] - $row['descuento'] - $row['pagos'];
                ?>
                    <tr>
                        <td><?php echo formatCode($row["id"]) ?></td>
                        <td><?php echo $idv ? formatCode($row["id_venta"]) : '- - - -' ?></td>
                        <td><?php echo $row["nombre"] ?></td>
                        <td><?php echo $row["telefono"] ?></td>
                        <td><?php echo $row["destino"] ?></td>
                        <td><?php echo $row["clase"] ?></td>
                        <td class="text-right"><?php echo $idv ? numFormat($balance) : '- - - -' ?></td>
                        <td>
                            <button type="button" class="btn btn-sm btn-primary w-100 sale" data-id="<?php echo $idv ?>" data-idc="<?php echo $row['id_cliente'] ?>"><i class="fa fa-external-link" style="margin-right:5px"></i> Facturación</button>
                        </td>
                    </tr>
                <?php } ?>

                </table>
            </td>
        </tr>
<?php
    }
}
?>
</table>

            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $('.sale').click(function(){
                var form = getNewSubmitForm('venta.php')
                createNewFormElement(form, 'id', $(this).data('id'))
                createNewFormElement(form, 'idc', $(this).data('idc'))
                form.target = '_blank'
                form.submit()
            })
        })
    </script>

</div>
<?php include 'footer.php' ?>