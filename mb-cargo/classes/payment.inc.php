<?php
/**
 * Created by PhpStorm.
 * User: melvin
 * Date: 12/2/17
 * Time: 12:14 AM
 */

class Payment
{

    /**
     * Requires php5-curl (apt-get install php5-curl)
     * @param $user User
     * @return bool|string
     */
    public static function getGatewayToken($user) {

        global $pfix;

        if($user->gateway && $user->gateway->expires_in > time())
            return $user->gateway->access_token;
        else {

            $curl = curl_init(GATEWAY_API_URL . 'oauth/token');
            $data = [
                'grant_type' => 'password',
                'username' => GATEWAY_CEXPRESS_USER,
                'password' => GATEWAY_CEXPRESS_PASS
            ];

            curl_setopt_array($curl, [
                CURLOPT_HTTPHEADER => [
                    'Authorization: Basic ' . GATEWAY_API_KEY
                ],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => http_build_query($data) // passing a URL-encoded string will encode the data as application/x-www-form-urlencoded
            ]);

            $res = curl_exec($curl);
            curl_close($curl);

            $res = json_decode($res);

            if($user->gateway = ($res->error ? false : $res)) {
                $user->gateway->expires_in += time();
            }

            session_write_close();
            session_start();
            $_SESSION[$pfix . 'user'] = serialize($user);
            session_write_close();

            return $user->gateway ? $user->gateway->access_token : false;

        }

    }

    /**
     * @param $user User
     */
    public static function revokeGatewayToken($user){

        if($user->gateway){
            $curl = curl_init(GATEWAY_API_URL . 'oauth/revoke');
            $data = [
                'token' => $user->gateway->access_token,
                'token_type' => 'access_token'
            ];

            curl_setopt_array($curl, [
                CURLOPT_HTTPHEADER => [
                    'Authorization: Basic ' . GATEWAY_API_KEY
                ],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => http_build_query($data) // passing a URL-encoded string will encode the data as application/x-www-form-urlencoded
            ]);

            curl_exec($curl);
        }

    }

    public static function calculateFee($token, $amount) {

        $curl = curl_init(GATEWAY_API_URL . 'services/gateway_PaymentService/calculateFee/?' . http_build_query(['amount' => $amount]));

        curl_setopt_array($curl, [
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer ' . $token
            ],
            CURLOPT_RETURNTRANSFER => true,
        ]);

        return curl_exec($curl);

    }
    
    public static function generateKey(){
        return strtoupper(bin2hex(openssl_random_pseudo_bytes(5)));        
    }


    public static function deleteCard($token, $customer_id, $code) {

        $curl = curl_init(GATEWAY_API_URL . 'services/gateway_PaymentService/deleteCard/?' . http_build_query(['customer_id' => $customer_id, 'code' => $code]));

        curl_setopt_array($curl, [
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer ' . $token
            ],
            CURLOPT_RETURNTRANSFER => true,
        ]);

        return json_decode(curl_exec($curl));

    }

    public static function logTransaction($user, $post) {
        $link = DB::connect();
        $post = (object) $post;

        $card_id = $post->card_id ?: 0;
        $res = mysql_query("SELECT id FROM log_transacciones WHERE referencia = '{$post->ref}'", $link);
        if($row = mysql_fetch_object($res))
            $str = "UPDATE log_transacciones SET status = '{$post->status}', descripcion = '{$post->desc}' WHERE id = {$row->id}";
        else
            $str = "INSERT INTO log_transacciones(id_usuario, id_cartera, referencia, tipo, nombre, numero, monto) VALUES({$user->id}, {$card_id},'{$post->ref}', '{$post->type}', '{$post->name}', '{$post->number}', {$post->amount})";
        mysql_query($str, $link);

        return true;

    }

}