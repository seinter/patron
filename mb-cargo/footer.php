
<div class="container-fluid" style="position:fixed;bottom:0;z-index:999;">
    <div class="row main-footer">
        <div class="col">
            <div class="row">
                <div class="col text-truncate copyright"><?php echo $copyright ?></div>
                <div class="col text-right" style="padding:15px;">
                    <div class="dropup btn-group" data-toogle="dropdown">
                        <button id="user-btn" class="btn btn-danger btn-sm" type="button" style="min-width:150px;"><i class="fa fa-user-circle float-left" style="margin-top:3px;margin-right:10px;margin-left:4px;"></i><?php echo strtoupper($user->nombre) ?></button>
                        <button id="user-menu" class="btn btn-danger btn-sm dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-expanded="false" type="button"></button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu" style="min-width:172px; font-size:14px;">
                            <a class="dropdown-item disabled text-center" role="presentation" href="main.php"><i class="fa fa-user-circle" style="font-size:50px;margin:10px 0;margin-top:15px;"></i></a>
                        <?php if(+$user->id_nivel === 2 || $user->opr): ?>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" role="presentation" href="rep_upagos.php" style="font-size:13px; line-height:24px">Balance Diario</a>
                        <?php endif ?>
                            <a class="dropdown-item" role="presentation" href="password.php" style="font-size:13px; line-height:24px">Cambiar Password</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" role="presentation" href="logout.php"><i class="fa fa-power-off float-left" style="margin-top:4px;margin-right:10px;"></i>Log Out</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>