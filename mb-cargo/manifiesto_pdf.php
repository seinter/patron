<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();
$pdf = new PDF("L");
$pdf->page_footer = $pdf_footer;

$str = "SELECT programacion.descripcion, programacion.fecha, local1.localidad origen, local2.localidad destino, tipo FROM programacion INNER JOIN localidades local1 ON programacion.origen = local1.id INNER JOIN localidades local2 ON programacion.destino = local2.id INNER JOIN tipo_salidas ON programacion.id_tipo = tipo_salidas.id WHERE programacion.id = {$id}";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$$key = $value;

$pdf->title = array($title." - MANIFIESTO DE ".strtoupper($tipo), formatCode($id)." - ".	$fecha.": ".$origen." - ".$destino." | ".$descripcion);
$pdf->AddPage();

$str = "SELECT bitacora.id, bitacora.codigo, ventas.id id_venta, local1.localidad origen, local2.localidad destino, CONCAT(clientes.nombre, ' ', clientes.apellido) nombre, clientes.telefono, destinatarios.nombre dest_nombre, destinatarios.telefono dest_telefono, clasificacion.clase, IF(productos.id, productos.producto, detalle_venta.descripcion) producto FROM historial INNER JOIN bitacora ON historial.id_bitacora = bitacora.id INNER JOIN detalle_venta ON bitacora.id_detalle = detalle_venta.id INNER JOIN clasificacion ON detalle_venta.id_clase = clasificacion.id LEFT JOIN ventas ON detalle_venta.id_venta = ventas.id LEFT JOIN clientes ON ventas.id_cliente = clientes.id OR detalle_venta.id_cliente = clientes.id LEFT JOIN destinatarios ON ventas.id_dest = destinatarios.id LEFT JOIN localidades local1 ON ventas.origen = local1.id LEFT JOIN localidades local2 ON ventas.destino = local2.id LEFT JOIN productos ON detalle_venta.id_producto = productos.id WHERE detalle_venta.status AND bitacora.status AND historial.id_prog = {$id} AND historial.status ORDER BY historial.id;";
$res = mysql_query($str, $link);
while($row = mysql_fetch_assoc($res))
	$records[$row["destino"]][] = $row;

if($records)
	foreach($records as $key => $value){
		unset($data);
		foreach($value as $row)
			$data[] = array(formatCode($row["id"]), formatCode($row["id_venta"]), "| ".$row["origen"], "| ".$row["nombre"], "| ".$row["telefono"], "| ".$row["destino"], "| ".$row["dest_nombre"], "| ".$row["dest_telefono"], "| ".$row["clase"]." - ".$row["producto"]);
		$pdf->SetFont('Verdana','B',10);
		$pdf->Cell(0,20,"DESTINO: ".$key,0,1);
		$header = array("No. GUIA", "No. VENTA", "ORIGEN", "REMITENTE", "TELEFONO", "DESTINO", "DESTINATARIO", "TELEFONO", "PRODUCTO");
		$footer = array('','','','','','','','','');
		$pdf->ImprovedTable($header, $data, $footer);
		$pdf->Ln(20);
	}

$pdf->Output();

?>