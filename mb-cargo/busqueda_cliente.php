<?php

if($act) {
    $link = DB::connect();
    $str = 'SELECT agencia FROM agencias WHERE id = ' . ($user->id_central ?: $user->id_agencia);
    $res = mysql_query($str, $link);
    $agencia = mysql_result($res, 0);
}

?>
<div class="container-fluid">

    <div class="row main-title">
    <?php if($box): ?>
        <div class="col">
            <div class="row">
                <div class="col-4 text-truncate caption"><?php echo +$box !== -1 ? 'ASIGNACION' : 'NUEVA RECEPCION' ; ?></div>
            <?php if(+$box !== -1): ?>
                <div class="col-8 text-right text-truncate caption">CASILLERO [ <i class="fa fa-inbox"></i> #<?php echo $box ?> ]</div>
            <?php endif ?>
            </div>
        </div>
    <?php else: ?>
        <div class="col text-right text-truncate caption"><?php echo $act ? "SELECCION DEL CLIENTE" : "CLIENTES" ?></div>
    <?php endif ?>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">


            <table width="100%" border="0" cellspacing="10" cellpadding="0">
                <tr>
                    <td style="padding:0">

                        <div class="alert <?php echo $act ? 'alert-info' : 'alert-dark' ?> big-box">

                            <div class="row">
                                <div class="col-12 col-sm-3 text-sm-right" style="line-height:38px">
                                    <h4 class="text-truncate" style="margin:5px 0">BUSCAR POR</h4>
                                </div>
                                <div class="col-6 col-sm-4 col-md-3 big-combo" style="padding-right:0">
                                    <select name="qt" id="qt" style="width:100%" onchange="setType(this.value);">
                                        <option value="1">Nombre</option>
                                        <option value="2">Télefono</option>
                                        <option value="3">Ciudad</option>
                                    </select>
                                </div>
                                <div class="col-6 col-sm-5 col-md-3" style="padding-left:5px">
                                    <input type="text" name="q" id="q" class="form-control" style="border-color:#AAA" onKeyUp="loadData();" onKeyDown="specialChar(this.value, event);" />
                                </div>

                                <div class="col-12 col-sm-9 offset-sm-3 d-md-none">
                                    <button class="btn btn-sm <?php echo $act ? 'btn-info' : 'btn-ligth' ?> w-100" style="margin-top:4px" onclick="openClient(0)">CLIENTE NUEVO <i class="fa fa-user-plus" style="margin-left:5px"></i></button>
                                </div>

                                <div class="d-none d-md-block col-3" style="padding-right:19px; padding-left:0;">
                                    <button class="btn btn-sm <?php echo $act ? 'btn-info' : 'btn-ligth' ?> w-100" style="margin-top:4px" onclick="openClient(0)">CLIENTE NUEVO <i class="fa fa-user-plus" style="margin-left:5px"></i></button>
                                </div>

                            </div>

                        </div>

                    </td>
                </tr>
				<tr>
					<td><div id="div_clientes"></div></td>
				</tr>
			</table>

            </div>
        </div>
    </div>

    <script language="javascript" type="text/javascript" src="ajaxlib.js"></script>
    <script language="javascript" type="text/javascript">

        function setType(qt){
            var q = $('#q').val('')

            if(+qt === 2){
                q.mask(masks[0], {
                    onKeyPress: function(cep, e, field, options){
                        if(cep.length > 15)
                            field.mask(masks[2], options)
                        else
                            field.mask(masks[+(cep.length === 15)], options)
                    }
                })
            } else
                q.unmask()

            loadData()

            setTimeout(function(){
                q.focus()
            }, 100)
        }

        function loadData(q){
            q = q == undefined ? document.getElementById("q").value : q;
            var qt = document.getElementById("qt").value;
            loadXMLDoc("xml/clientes.php?act=<?php echo +$act ?: 0 ?>&qt="+qt+"&q="+q, decodeXMLCustomers, false);
        }

        function decodeXMLCustomers(){
            var records = resultXML.getElementsByTagName("record");
            var holder = document.getElementById("div_clientes");
            var holder = document.getElementById("div_clientes");
            var html = "<table class='data-grid'><caption>CLIENTES<?php echo $act ? ' - ' . strtoupper($agencia) : '' ?></caption>";

            html += "<tr><th class='d-none d-sm-table-cell'>ID</th><th>Nombre</th><th class='d-none d-md-table-cell'>Teléfono</th><th class='d-none d-lg-table-cell'>País</th><th width='75'></th><th width='90'></th></tr>";
            for(var idx = 0; idx < records.length; idx++)
                html += "<tr><td class='d-none d-sm-table-cell'>"+getNodeValue(records[idx],"code")+"</td><td>"+getNodeValue(records[idx],"nombre")+"</td><td class='d-none d-md-table-cell'>"+getNodeValue(records[idx],"tel")+"</td><td class='d-none d-lg-table-cell'>"+getNodeValue(records[idx],"pais")+"</td><td width='75'><input type='button' class='btn btn-sm btn-primary edit' value='- E -' onclick=\"openClient("+getNodeValue(records[idx],"id")+")\" /></td>" +
            <?php
                switch(+$act){
                    case 1:
            ?>
                    "<td width='90'><form action='venta.php' method='post'><input type='hidden' name='idc' value='" + getNodeValue(records[idx], "id") + "' /><button type='submit' class='btn btn-sm btn-info' style='width:90px'><i class='fa fa-caret-right'></i> <i class='fa fa-caret-right'></i> <i class='fa fa-caret-right'></i></buttom></form></td>" +
            <?php
                    break;
                    case 2:
            ?>
                    "<td width='90'><form action='seleccion_cliente.php' method='post'><input type='hidden' name='idc' value='" + getNodeValue(records[idx], "id") + "' /><input type='hidden' name='box' value='<?php echo $box ?>' /><button type='submit' class='btn btn-sm btn-info' style='width:90px'><i class='fa fa-caret-right'></i> <i class='fa fa-caret-right'></i> <i class='fa fa-caret-right'></i></buttom></form></td>" +
            <?php
                    break;
                    case 4:
            ?>
                    "<td width='90'><form action='entrada.php#inbox' method='get' target='_top'><input type='hidden' name='idc' value='" + getNodeValue(records[idx], "id") + "' /><button type='submit' class='btn btn-sm btn-success' style='width:90px;'>- <i class='fa fa-arrow-circle-down'></i> -</buttom></form></td>" +
            <?php
                    break;
                    default:
            ?>
                    "<td width='90'><button type='button' class='btn btn-sm btn-secondary' onclick=\"openAccount(" + getNodeValue(records[idx], "id") + ")\" style='width:90px'><i class='fa fa-id-card' style='margin-right:5px'></i>Cuenta</button></td>" +
            <?php
                    break;
                }
            ?>
                    "</tr>\n";

            html += "</table>\n";
            holder.innerHTML = html;

            if(window.self !== window.top)
                window.parent.$.fancybox.getInstance().update()

        }

        function specialChar(q, event){
            var y, keyCode = event.which ? event.which : event.keyCode;
            if(keyCode==8){
                y = q.substr(0,q.length-1);
                loadData(y);
            }
        }

        function openAccount(id){
            window.open("viewer.php?dll=12&id="+id,"wndAccount");
        }

        setFocus("q");

    </script>

    <script type="text/javascript">
        $(function(){
            $('select').select2()
        })

        function openClient(id){
            var name = document.getElementById("q").value
            var url = 'cliente.php'

            if(id)
                url += "?dll=2&id=" + id
            else
                url += "?nn=" + name

            $.fancybox.open({
                src: url,
                type: 'iframe',
                modal: true,
                afterClose: function() {
                    if($('#q').val() !== '')
                        loadData()
                },
                afterShow: function() {
                    $('.fancybox-iframe').contents().find("#nombre").focus();
                }
            })
        }

    </script>

</div>
