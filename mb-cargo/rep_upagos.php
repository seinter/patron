<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right text-truncate caption">BALANCE DIARIO</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
      <tr>
      	<td>

            <form id="rep" name="rep" method="post" action="viewer.php" target="_blank" onsubmit="return validate(this)">
                <input type="hidden" name="dll" value="8" />

            <table cellpadding="0" cellspacing="0" class="data-form">
			<caption>
			SELECCIONAR FECHA
			</caption>
      		<tr>
				<th>FECHA DEL REPORTE:</th>
      			<td>
                    <div class="date-field">
                        <input name="f1" type="text" id="f1" value="<?php echo date("Y-m-d"); ?>" readonly="readonly" />
                        <i class="fa fa-calendar"></i>
                    </div>
                </td>
      			</tr>
                <tr>
                    <td colspan="4">
                        <div align="center">
                            <button type="submit" class="btn btn-danger print">
                                <i class="fa fa-file-pdf-o" style="margin-right:5px"></i>
                                Generar
                            </button>
                        </div>
                    </td>
                </tr>

		</table>
    </form>

            </td>
      	</tr>
</table>

            </div>
        </div>
    </div>


    <script type="text/javascript" language="JavaScript" src="popcalendar.js"></script>
    <script language="JavaScript" type="text/JavaScript">
        function afterClose(ctl){
            return;
        }
    </script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('select').select2()

            $('.date-field input').click(function() {
                showCalendar(this, this, 'yyyy-mm-dd','es',1)
            })
        })
    </script>

</div>
<?php include 'footer.php' ?>