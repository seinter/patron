<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

if($dll){
	$str = "update contenidos set status = 0 where id_bitacora = {$id} AND NOT id_entrada;";
	mysql_query($str, $link);
	foreach($_POST as $key => $value){
		if(substr($key,0,3) == "idc"){
			$i = substr($key,3,strlen($key)-3);
			if($value) {
			    $res = mysql_query("SELECT id_entrada FROM contenidos WHERE id = {$value}", $link);
			    if(mysql_result($res, 0))
			        $str = "UPDATE contenidos SET monto = " . $_POST["mnt" . $i] . ", status = 1 WHERE id = {$value}";
                else
                    $str = "update contenidos set cantidad = " . $_POST["cnt" . $i] . ", descripcion = '" . $_POST["desc" . $i] . "', peso = " . $_POST["peso" . $i] . ", piezas = " . $_POST["pzs" . $i] . ", monto = " . $_POST["mnt" . $i] . ", status = 1 where id = $value;";
            } else
				$str = "insert into contenidos(id_bitacora, cantidad, descripcion, peso, piezas, monto) values($id, ".$_POST["cnt".$i].", '".$_POST["desc".$i]."', ".$_POST["peso".$i].", ".$_POST["pzs".$i].", ".$_POST["mnt".$i].")";
			mysql_query($str, $link);
		}
	}
	$close = true;
}

/*
$res = mysql_query("SELECT codigo FROM bitacora WHERE id = {$id}", $link);
extract(mysql_fetch_assoc($res));
*/

$str = "SELECT contenidos.id, IF(id_entrada, 1, 0) box, cantidad cnt, piezas pzs, IF(id_entrada, CONCAT(tienda, ': ', descripcion), descripcion) `desc`, peso, monto mnt FROM contenidos LEFT JOIN tiendas ON contenidos.id_tienda = tiendas.id WHERE id_bitacora = {$id} AND contenidos.status ORDER BY contenidos.id;";
$res = mysql_query($str, $link);
while($row = mysql_fetch_assoc($res))
	$conts[array_shift($row)] = $row;

if(!$conts)
	$conts[] = array("cnt"=>1);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>

    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <script type="text/javascript">


        function closeLightBox(){
            window.parent.$.fancybox.close();
        }

        <?php if($close){ ?>
        closeLightBox()
        <?php } ?>

        var nodeCount = <?php echo count($conts) ?>;

        function addTableNode(){
            try{

                nodeCount++;

                var myTable = document.getElementById("sTable");
                var newTR = document.createElement("TR");
                newTR.setAttribute("id","tr"+nodeCount);

                //COL0
                newTD = document.createElement("TD");
                newTD.innerHTML = "<div align=\"right\">"+nodeCount+".&nbsp;</div>";
                newTR.appendChild(newTD);

                //COL1
                newTD = document.createElement("TD");
                newTD.innerHTML = "<input type=\"hidden\" name=\"idc"+nodeCount+"\" id=\"idc"+nodeCount+"\" value=\"0\" /><input name=\"cnt"+nodeCount+"\" type=\"text\" class=\"text-right\" id=\"cnt"+nodeCount+"\" onblur=\"this.value = numFormatInt(this.value,1)\" value=\"1\" />";
                newTR.appendChild(newTD);

                //COL2
                newTD = document.createElement("TD");
                newTD.setAttribute("align","center");
                newTD.innerHTML = "<input type=\"text\" name=\"desc"+nodeCount+"\" id=\"desc"+nodeCount+"\" />";
                newTR.appendChild(newTD);

                //COL3
                newTD = document.createElement("TD");
                newTD.innerHTML = "<input type=\"text\" name=\"peso"+nodeCount+"\" id=\"peso"+nodeCount+"\" class=\"text-right\" onblur=\"this.value = numFormat(this.value)\" value=\"0.00\" />";
                newTR.appendChild(newTD);

                //COL4
                newTD = document.createElement("TD");
                newTD.innerHTML = "<input type=\"text\" name=\"pzs"+nodeCount+"\" id=\"pzs"+nodeCount+"\" class=\"text-right\" onblur=\"this.value = numFormatInt(this.value)\" value=\"0\" />";
                newTR.appendChild(newTD);

                //COL5
                newTD = document.createElement("TD");
                newTD.innerHTML = "<input type=\"text\" name=\"mnt"+nodeCount+"\" id=\"mnt"+nodeCount+"\" class=\"text-right\" onblur=\"this.value = numFormat(this.value)\" value=\"0.00\" />";
                newTR.appendChild(newTD);

                //COL6
                newTD = document.createElement("TD");
                newTD.innerHTML = "<input type=\"button\" class=\"btn btn-sm btn-danger delete\" style=\"width:75px\" value=\"- X -\" onClick=\"deleteTableNode("+nodeCount+")\">";
                newTR.appendChild(newTD);

                myTable.appendChild(newTR);

                window.parent.$.fancybox.getInstance().update()

            } catch(e) {
                alert(e.message);
            }
        }

        function deleteTableNode(val){
            try {
                var myTable = document.getElementById("sTable");
                var killNode = document.getElementById("tr"+val);
                myTable.removeChild(killNode);

                window.parent.$.fancybox.getInstance().update()

            } catch(e) {
                alert(e.message);
            }
        }

        function numFormat(number, def){
            var x;
            var y;
            if(def == undefined)
                def = 0;
            if(isNaN(number)||number==0)
                number = def;
            number = round(number, 2);
            number = number.toString();
            y = number.length - 1;
            for(x=0;x<number.length;x++)
                if(number.substr(x,1) == ".")
                    y = x;
            switch((number.length-1) - y){
                case 1:
                    number += "0";
                    break;
                case 0:
                    number += ".00";
                    break;
            }
            return number;
        }

        function round(number,X) {
            X = (!X ? 2 : X);
            return Math.round(number*Math.pow(10,X))/Math.pow(10,X);
        }

        function numFormatInt(number, def){
            var x;
            var y;

            if(def == undefined)
                def = 0;
            if(isNaN(number)||number==0)
                number = def;
            number = parseInt(number);
            return number;
        }

        function validate(form){
            var y = 0;
            for(x = 0;x<form.length;x++)
                if(form.elements[x].value=="" && !form.elements[x].disabled)
                    y++;
            if(y>0){
                alert("Debe llenar todos los campos requeridos !!!");
                return false;
            }
            return true;
        }


    </script>

    <style type="text/css">
        <!--
        table {
            border-collapse: initial;
        }
        table caption {
            caption-side: top;
        }
        -->
    </style>

</head>
<body class="modal-container">
<div class="container-fluid">
    <div class="row main-title">
        <div class="col text-right caption">DETALLE GUIA No. <?php echo formatCode($id) ?></div>
    </div>
</div>


<table width="100%" border="0" cellpadding="0" cellspacing="10">
    <tr>
        <td>
        <form action="?" method="post" onsubmit="return validate(this)">
        <input type="hidden" name="dll" value="1" />
        <input type="hidden" name="id" value="<?php echo $id ?>" />
        <table class="data-grid form-fields">
            <tbody id="sTable">
                <tr>
                    <th width="40">ITEM</th>
                    <th width="60">CANTIDAD</th>
                    <th>DESCRIPCION</th>
                    <th width="60">PESO KG</th>
                    <th width="60">PIEZAS</th>
                    <th width="90">MONTO USD</th>
                    <th width="75">&nbsp;</th>
                </tr>
                <?php
if($conts){
    foreach($conts as $key => $value){
        $x++;
?>
                <tr id="tr<?php echo $x ?>">
                    <td width="40"><div align="right"><?php echo $x ?>.&nbsp;</div></td>
                    <td width="60"><input name="idc<?php echo $x; ?>" type="hidden" id="idc<?php echo $x; ?>" value="<?php echo $key ?>" /><input name="cnt<?php echo $x ?>" type="text" class="text-right" id="cnt<?php echo $x ?>" onblur="this.value = numFormatInt(this.value,1)" value="<?php echo $value["cnt"] ?>" <?php echo $value['box'] ? ' readonly' : '' ?> /></td>
                    <td><input type="text" name="desc<?php echo $x ?>" id="desc<?php echo $x ?>" value="<?php echo $value["desc"] ?>" <?php echo $value['box'] ? ' readonly' : '' ?> /></td>
                    <td width="60"><input name="peso<?php echo $x ?>" type="text" class="forMoney" id="peso<?php echo $x ?>" onblur="this.value = numFormat(this.value)" value="<?php echo numFormat($value["peso"]) ?>" <?php echo $value['box'] ? ' readonly' : '' ?> /></td>
                    <td width="60"><input name="pzs<?php echo $x ?>" type="text" class="forMoney" id="pzs<?php echo $x ?>" onblur="this.value = numFormatInt(this.value)" value="<?php echo $value["pzs"] ?>" <?php echo $value['box'] ? ' readonly' : '' ?> /></td>
                    <td width="90"><input name="mnt<?php echo $x ?>" type="text" class="forMoney" id="mnt<?php echo $x ?>" onblur="this.value = numFormat(this.value)" value="<?php echo numFormat($value["mnt"]) ?>" /></td>
                    <td width="75"><input type="button" class="btn btn-sm btn-danger delete" style="width:75px" value="- X -" onclick="deleteTableNode(<?php echo $x ?>)" <?php echo $value['box'] ? 'disabled' : '' ?> /></td>
                </tr>
<?php
    }
}
?>
            </tbody>
        </table>
        <table class="data-table form-footer">
                <tr>
                    <td><div align="right">
                        <input name="button" type="button" class="btn btn-sm btn-primary edit" id="button" style="width:75px" value="- + -" onclick="addTableNode();" />
                    </div></td>
                </tr>
                <tr>
                    <th><div align="center">
                        <input name="button" type="button" class="btn btn-danger cancel" id="button" value="&lt;&lt;&lt; Cancelar" onclick="closeLightBox()" />
                        <input name="button" type="submit" class="btn btn-primary save" id="button2" value="Guardar &gt;&gt;&gt;" />
                    </div></th>
                </tr>
            </table>
        </form>
        </td>
    </tr>
</table>


<div class="container-fluid" style="position:fixed;bottom:0;">
    <div class="row main-footer">
        <div class="col copyright"><?php echo $copyright ?></div>
    </div>
</div>

</body>
</html>