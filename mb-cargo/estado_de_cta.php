<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, -1);

if($user->id_nivel < 0 && $_REQUEST['id'] !== $user->id){
    http_response_code(403);
    exit();
}


$link = DB::connect();

$pdf = new PDF("L");
$pdf->page_footer = $pdf_footer;

$tipos = array("Normal", "Pago en Entrega", "Credito");
$str = "select ventas.*, CONCAT(local1.localidad, ' - ', local2.localidad) destino, carrier, clientes.id id_cliente, clientes.nombre, clientes.apellido, usuarios.nombre agente, pagos, sum(detalle_venta.tarifa*detalle_venta.cantidad) monto from ventas inner join clientes on ventas.id_cliente = clientes.id inner join usuarios on ventas.id_usuario = usuarios.id inner join detalle_venta on ventas.id = detalle_venta.id_venta INNER JOIN localidades local1 ON ventas.origen = local1.id INNER JOIN localidades local2 ON ventas.destino = local2.id left join (select id_venta, sum(monto) pagos from pagos where status group by id_venta) pagos on ventas.id = pagos.id_venta left join carriers on id_credito = carriers.id where MD5(clientes.id) = '{$id}' and ventas.status and detalle_venta.status group by ventas.id order by fecha, ventas.id;";
$res2 = mysql_query($str, $link);
if(mysql_num_rows($res2)){
	while($row2 = mysql_fetch_assoc($res2)){
		$cliente = $cliente?$cliente:$row2["nombre"]." ".$row2["apellido"];
		$codigo = $codigo ?: formatClientCode($row2['id_cliente']);

		$data[] = array($row2["fecha"], formatCode($row2["id"]), $row2["destino"], $tipos[$row2["id_tipo"]], $row2["carrier"], numFormat($row2["monto"]), numFormat($row2["cargos"]), numFormat($row2["descuento"]),numFormat($seguro = ($row2["declarado"]*$row2["pc_seguro"])/100), numFormat($precio = ($row2["monto"]+$row2["cargos"]+$seguro-$row2["descuento"])), numFormat($row2["pagos"]), numFormat($precio-$row2["pagos"]));
		$total += $precio;
		$pagado += $row2["pagos"];
	}
	
	$pdf->title = array($title . " - ESTADO DE CUENTA" , "CLIENTE: {$codigo} - {$cliente}");
	$pdf->AddPage();

	$header = array("FECHA", "No. VENTA", "DESTINO", "TIPO", "CARRIER", "MONTO", "CARGOS", "DESCUENTO", "SEGURO" ,"TOTAL", "PAGADO", "SALDO");
	$footer = array("","","","","","","","","TOTALES USD:",numFormat($total),numFormat($pagado),numFormat($total-$pagado));
	$pdf->ImprovedTable($header, $data, $footer, $c);
}

$pdf->Output();

?>