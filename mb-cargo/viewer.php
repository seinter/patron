<?php
require_once("config.php");

switch($dll){
	case 1:
		$src = "rutas_pdf.php?id=$id";
	break;
	case 2:
		$src = "recibo.php?id=$id";
	break;
	case 3:
	//REPORTE DE PAGOS
		$src = "rep_pagos_pdf.php?f1=$f1&f2=$f2&ida=$ida&c=$c";
	break;
	case 4:
	//REPORTE DE VENTAS
		$src = "rep_ventas_pdf.php?f1=$f1&f2=$f2&ida=$ida&c=$c";
	break;
	case 8:
		$src = "rep_upagos_pdf.php?id=$id&f1=$f1&c=$c";
	break;
	case 10:
	//REPORTE GOBAL DE PAGOS
		$src = "rep_pagos_global_pdf.php?f1=$f1&f2=$f2&ida=$ida&c=$c";
	break;
	case 11:
	//REPORTE GOBAL DE VENTAS
		$src = "rep_ventas_global_pdf.php?f1=$f1&f2=$f2&ida=$ida&c=$c";
	break;
	case 12:
	//ESTADO DE CUENTA DEL CLIENTE
		$src = 'estado_de_cta.php?id=' . md5($id);
	break;
	case 13:
	//GUIA DE RECEPCION
		$src = "guia_recepcion.php?id=$id";
	break;
	case 14:
	//GUIA DE ENTREGA
		$src = "guia_entrega.php?id=$id";
	break;
	case 15:
	//ETIQUETAS
		$src = "etiquetas.php?id=$id";
	break;
	case 16:
	//MANIFIESTO
		$src = "manifiesto_pdf.php?id=$id";
	break;
	case 17:
	//GUIAS
		$src = "guias.php?idp=$id";
	break;
	case 18:
		$src = "comprobante.php?id=$id";
	break;
	case 19:
		$src = "itinerario.php?f1=$f1";
	break;
    case 20:
        $src = "rep_entradas_pdf.php?f1=$f1&f2=$f2&idc=$idc";
    break;
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">


    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/js/functions.js"></script>

    <style>
        html, body {
            height: 100%;
        }
        .flex-grow {
            flex: 1 0 auto;
        }
    </style>

</head>

<body>
<div class="d-flex flex-column h-100">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col text-center text-sm-left"><img src="assets/img/logo.jpg" width="90" style="margin:5px;"></div>
                    <div class="col d-none d-sm-block text-right">
                        <div style="margin:10px;"><span><?php echo $title ?></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column flex-grow">
        <iframe class="h-100 flex-grow" width="100%" frameborder="0" src="<?php echo $src ?>"></iframe>
    </div>

    <div class="container-fluid" style="position:fixed; bottom:0;">
        <div class="row main-footer" style="height:65px">
            <div class="col copyright"><?php echo $copyright ?></div>
        </div>
    </div>

</div>
</body>
</html>