<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
include("classes/FusionCharts.inc.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 2);

$link = DB::connect();

$lang["months"]	= array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$m = $m?$m:date("n");
$y = $y?$y:date("Y");

$nextyear = ($m != 12) ? $y : $y + 1;
$prevyear = ($m != 1) ? $y : $y - 1;
$prevmonth = ($m == 1) ? 12 : $m - 1;
$nextmonth = ($m == 12) ? 1 : $m + 1;

function monthPullDown($month, $montharray) {
    echo "\n<select name=\"m\" style=\"width:100%\">\n";

    for($i=0;$i < 12; $i++){
        if ($i != ($month - 1))
            echo "	<option value=\"" . ($i + 1) . "\">$montharray[$i]</option>\n";
        else
            echo "	<option value=\"" . ($i + 1) . "\" selected>$montharray[$i]</option>\n";
    }

    echo "</select>\n\n";
}

function yearPullDown($year){
    echo "<select name=\"y\" style=\"width:100%px\">\n";

    $z = 3;
    for($i=1;$i < 8; $i++) {
        if ($z == 0)
            echo "	<option value=\"" . ($year - $z) . "\" selected>" . ($year - $z) . "</option>\n";
        else
            echo "	<option value=\"" . ($year - $z) . "\">" . ($year - $z) . "</option>\n";

        $z--;
    }

    echo "</select>\n\n";
}
?>
<?php include 'header.php' ?>
<form method="post" action="?" onsubmit="return validate(this)">
    <input type="hidden" name="dll" value="1" />

    <div class="container-fluid">

        <div class="row main-title">
            <div class="col text-right text-truncate caption">ESTADISTICAS DEL MES</div>
        </div>
        <div class="row main-content">
            <div class="col table-responsive">
                <div class="row">

                    <table width="100%" border="0" cellspacing="10" cellpadding="0">
                        <tr>

                            <td style="padding:0">
                                <div class="alert alert-dark big-box">

                                    <div class="row">
                                        <div class="col-4 text-right" style="line-height:38px">
                                            <h4 class="text-truncate" style="margin:5px 0">AGENCIA / SUB</h4>
                                        </div>
                                        <div class="col-8 big-combo">
                                            <select name="ida" id="ida" style="width:100%">
                                                <option value="0">&nbsp;- - - TODAS - - - </option>
                                                <option value="<?php echo $user->id_agencia ?>"><?php echo $user->agencia ?></option>
                                                <?php
                                                $str = "select id, agencia from agencias where id_central = {$user->id_agencia} and status order by agencia";
                                                $res = mysql_query($str, $link);
                                                if(mysql_num_rows($res)){
                                                    ?>
                                                    <optgroup style="background-color:#7DAAEE; color:#FFFFFF" label="SUB AGENCIAS">
                                                        <?php while($row = mysql_fetch_row($res)){ ?>
                                                            <option value="<?php echo $row[0] ?>">&nbsp;&bull;&nbsp;<?php echo $row[1] ?></option>
                                                        <?php	} ?>
                                                    </optgroup>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </td>

                        </tr>
                        <tr>

                            <td style="padding:0">

                                <div class="alert alert-success big-box">

                                    <div class="row">
                                        <div class="col-4 text-right" style="line-height:38px">
                                            <h4 class="text-truncate" style="margin:5px 0; text-transform:uppercase;"><?php echo $lang["months"][$m-1] . ' ' . $y ?></h4>
                                        </div>
                                        <div class="col-4 col-md-3 big-combo" style="padding-right:0">
                                            <?php monthPullDown($m, $lang["months"]) ?>
                                        </div>
                                        <div class="col-4 col-md-2 big-combo" style="padding-left:5px">
                                            <?php echo yearPullDown($y) ?>
                                        </div>

                                        <div class="col-8 offset-4 d-md-none">
                                            <input type="submit" class="btn btn-sm btn-info w-100" style="margin-top:4px" value="GENERAR &gt;&gt;&gt;" />
                                        </div>
                                        <div class="d-none d-md-block col-3" style="padding-right:19px; padding-left:0;">
                                            <input type="submit" class="btn btn-sm btn-info w-100" style="margin-top:4px" value="GENERAR &gt;&gt;&gt;" />
                                        </div>

                                    </div>

                                </div>

                            </td>


                        </tr>
                        <?php
                        if($dll){
                            $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

                            if($ida){
                                $str = "select agencia, id_central from agencias where id = $ida;";
                                $res = mysql_query($str, $link);
                                $agencia = (mysql_result($res,0,1)?"SUB ":"")."AGENCIA: ".strtoupper(mysql_result($res,0,0));
                                $str = "select dayofmonth(fecha), sum(monto) from pagos inner join usuarios on pagos.id_usuario = usuarios.id where year(fecha) = $y and month(fecha) = $m and usuarios.id_agencia = $ida and pagos.status group by fecha order by fecha;";
                            } else
                                $str = "select dayofmonth(fecha), sum(monto) from pagos inner join usuarios on pagos.id_usuario = usuarios.id inner join agencias on usuarios.id_agencia = agencias.id where year(fecha) = $y and month(fecha) = $m and (agencias.id = {$user->id_agencia} or id_central = {$user->id_agencia}) and pagos.status group by fecha order by fecha;";

                            $res = mysql_query($str, $link);

                            $xml .= "<chart caption='INGRESOS: ".$months[$m-1]." $y' subcaption='".($ida?$agencia:"TODAS")."' xAxisName='Dia' yAxisName='Cobrado' numberPrefix='US$'>";
                            while($row = mysql_fetch_row($res))
                                $xml .= "<set label='{$row[0]}' value='".numFormat($row[1])."' />";
                            $xml .= "</chart>";
                            ?>
                            <tr>
                                <td style="padding:5px; padding-bottom:2px; background-color: #F1F1F1"><?php echo renderChartHTML("swf/Column3D.swf", "", $xml, "graphPagos", '100%', 400, false); ?></td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td class="text-center" style="padding:5px; background-color: #F1F1F1">
                                    <i class="fa fa-bar-chart" style="line-height:400px; font-size:100px; color:lightgray;"></i>

                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>

        <link rel="stylesheet" href="assets/js/select2/select2.min.css">
        <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>
        <script type="text/javascript">
            $(function(){
                $('select').select2()
            })
        </script>

    </div>
</form>

<?php include 'footer.php' ?>