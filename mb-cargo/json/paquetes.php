<?php
session_start();
require("../config.php");
include("../classes/system.inc.php");
include("../functions.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();


$data = false;
if(count(explode('_', $_GET['term'])) === 3) {

    list($id, $item, $code) = array_map(function($item) {
        return +$item;
    }, explode('_', $_GET['term']));

    $idb = $_GET['idb'] ?: 0;
    $str = "SELECT entradas.id ide, entradas.id_cliente idc, {$item} current, entradas.codigo code, entradas.creado date, tiendas.tienda store, entradas.descripcion description, entradas.peso / entradas.cantidad weight, entradas.piezas / entradas.cantidad pieces, (JSON_CONTAINS(entradas.empaque, '[{$item}]', '$') AND NOT JSON_CONTAINS(contenidos.empaque, '[{$item}]', '$')) packed, entradas.observaciones comments FROM entradas INNER JOIN tiendas ON entradas.id_tienda = tiendas.id LEFT JOIN contenidos ON entradas.id = contenidos.id_entrada AND contenidos.id_bitacora = {$idb} AND contenidos.status WHERE entradas.id = {$id} AND entradas.codigo = {$code} AND entradas.status";

    $res = mysql_query($str, $link);
    if($data = @mysql_fetch_object($res)) {
        $data->date = date('Y-m-d', strtotime($data->date)) . ' <span class="d-none d-md-inline-block" style="color:#acbbc8; margin-left:1px;">' . date('h:i', strtotime($data->date)) . '</span>';
        $data->comments = nl2br($data->comments);
    }
}

header('Content-type: application/json');
print json_encode($data);
exit();