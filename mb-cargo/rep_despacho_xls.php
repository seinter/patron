<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("classes/excel.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 2);

/*
error_reporting(-1);
ini_set('display_errors', true);
*/

$link = DB::connect();

$str = 'SELECT programacion.*, local1.localidad origen, local2.localidad destino, tipo, descripcion FROM programacion INNER JOIN localidades local1 ON programacion.origen = local1.id INNER JOIN localidades local2 ON programacion.destino = local2.id INNER JOIN tipo_salidas ON programacion.id_tipo = tipo_salidas.id WHERE programacion.id = ' . $_POST['id'];

$res = mysql_query($str, $link);
extract(mysql_fetch_assoc($res));

$xls = new Excel("despacho_{$origen}_{$destino}_{$fecha}");

$ida = $user->id_central ?: $user->id_agencia;
$str = "SELECT clientes.id, CONCAT(clientes.nombre, ' ', clientes.apellido) cliente, clientes.telefono, agencias.codigo, productos.id idp, IF(productos.id, productos.producto, detalle_venta.descripcion) producto, SUM(contenidos.peso) peso FROM clientes INNER JOIN agencias ON clientes.id_agencia = agencias.id INNER JOIN ventas ON clientes.id = ventas.id_cliente INNER JOIN detalle_venta ON ventas.id = detalle_venta.id_venta INNER JOIN bitacora ON detalle_venta.id = bitacora.id_detalle INNER JOIN historial ON bitacora.id = historial.id_bitacora LEFT JOIN contenidos ON bitacora.id = contenidos.id_bitacora LEFT JOIN productos ON detalle_venta.id_producto = productos.id WHERE agencias.id = {$ida} AND ventas.status AND detalle_venta.id_clase = 1 AND detalle_venta.status AND bitacora.status AND historial.status AND historial.id_accion = 3 AND historial.id_prog = {$id} GROUP BY bitacora.id";
$res = mysql_query($str, $link);

$grid = [];
while($row = mysql_fetch_object($res)){
    $idx = $row->id;
    $grid[$idx]['codigo'] = formatClientCode($idx);
    $grid[$idx]['cliente'] = $row->cliente;
    $grid[$idx]['telefono'] = $row->telefono;
    $grid[$idx][($row->idp ?: 0)]++;
    $grid[$idx]['peso'] += $row->peso;
    $grid[$idx]['cajas']++;

    if($row->idp)
        $columns[$row->idp] = $row->producto;
    else
        $grid[$idx]['pies'] += array_reduce(explode('x', $row->producto),function($mix, $item) {
            return $mix * $item;
        }, 1) / 129360;

}

asort($columns);

$agencia = "AGENCIA: " . strtoupper($user->agencia);
$xls->title(array($title, "REPORTE DE DESPACHO - " . $agencia));
$xls->title(array(formatCode($id) . ': ' . $origen . " - ".$destino, $descripcion));
$xls->Ln();

$data = []; $totals = [];
foreach($grid as $row){
    $data[] = array_merge([$row['codigo'], $row['cliente'], $row['telefono']], array_map(function($item) use($row, &$totals) {
        $totals[$item] += $row[$item] ?: 0;
        return $row[$item] ?: 0;
    }, array_keys($columns)), [numFormat($row['pies'], 3), $row[0], $row['peso'], $row['cajas']]);

    $totals['pies'] += $row['pies'] ?: 0;
    $totals[0] += $row[0] ?: 0;
    $totals['peso'] += $row['peso'];
    $totals['cajas'] += $row['cajas'];

}

$header = array_merge(['ID', 'CLIENTE', 'TELEFONO'], $columns, ['PIES', 'CAJAS PIES', 'PESO KG', 'CAJAS TOTALES']);
$footer = array_merge(['', '', 'TOTALES: '], array_map(function($item) use($totals) {
        return $totals[$item] ?: 0;
    }, array_keys($columns)), [numFormat($totals['pies'], 3), $totals[0], numFormat($totals['peso']), $totals['cajas']]);

$xls->table($header, $data, $footer);


$xls->xlsEOF();
exit();

?>