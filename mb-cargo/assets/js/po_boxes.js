// Javascript document

var gridster = false, grid = ''

$(function() {

    $('select').select2({
        templateResult: function(item){
            return $('<div class="text-right">' + item.text + '</div>')
        }
    })

    $('#edit-btn').click(function(){

        if($('#edit-form').is(':visible'))
            return

        $(this).add('#edit-chk, #inbox-btn').prop('disabled', true)
        grid = '';
        loadGridData()

        $('#caption, #edit-form, #edit-form-footer').toggle()

    })

    $('#edit-chk').bootstrapToggle()
        .on('change', function(){
            $('#edit-btn, #inbox-btn').prop('disabled', $(this).prop('checked'))
        })

    $('#inbox-btn').click(function(){

        $.fancybox.open({
            src: 'seleccion_cliente.php?box=-1',
            type: 'iframe',
            iframe: {
                css: {
                    minHeight: '90%',
                }
            },
            afterShow: function () {
                $('.fancybox-iframe').contents().find("#q").focus();
            },
            afterClose: loadGridData
        })

    })

    $('.cancel-btn').click(function(){
        grid = ''
        loadGridData()

        $('#caption, #edit-form, #edit-form-footer').toggle()
        $('#edit-btn, #edit-chk, #inbox-btn').prop('disabled', false)
    })

    $('.submit-btn').click(function() {
        $('input[name=act]').val(2)
        $('input[name=diagrama]').val(JSON.stringify(gridster ? gridster.serialize() : []))

        $('#edit-form').submit()
    })

    var timer = false
    $(window).resize(function() {
        if(timer)
            clearTimeout(timer)
        timer = setTimeout(function(){
            verticalFit()
            timer = false
        }, 300)
    })

    var xhr = false
    function loadGridData() {
        if(xhr)
            xhr.abort()

        xhr = $.getJSON('?', {
            act: 1
        }, populateGrid)
            .fail(function(e){
                console.log('ERROR: ' + e.statusText)
            })
            .always(function(){
                xhr = false
            })
    }

    function populateGrid(result) {

        /* SET CONTROL BOXES */
        $('#casilleros, #columnas').off('change', alterDiagram)
        $('#casilleros').val(result.grid ? result.grid.length : 0)
        $('#columnas').val(result.width ? result.width : 1)
        $('#casilleros, #columnas').trigger('change')
        $('#casilleros, #columnas').on('change', alterDiagram)

        if(result.grid) {

            if((!$('#edit-form').length || $('#edit-form').is(':hidden')) && JSON.stringify(result.grid) === grid)
                return

            grid = JSON.stringify(result.grid)

            if(gridster)
                gridster.destroy(true)

            $('.gridster').css('min-width', result.width * 50)
                .append('<ul>')
            gridster = $(".gridster ul").gridster({
                responsive_breakpoint: true,
                widget_base_dimensions: ['auto', 0],
                widget_margins: [8, 8],
                shift_widgets_up: false,
                min_cols: result.width,
                max_cols: result.width,
                helper: 'clone',
                resize: {
                    enabled: $('#edit-form').is(':visible'),
                    stop: function () {
                        $(window).trigger('resize')
                    }
                },
                draggable: {
                    ignore_dragging: function() {
                        return $('#edit-form').is(':hidden')
                    },
                    stop: function () {
                        $(window).trigger('resize')
                    }
                }
            }).data('gridster')

            verticalFit()

            /* ADD WIDGETS */
            var i = 0
            $.each(result.grid, function() {
                if(+this.id){
                    var card = (+this.cnt ? '<div class=\'badge badge-success float-right\'>' + this.cnt + ' items</div>' : '') + '<div><h6 class=\'name text-light\'><i class=\'fa fa-user\' style=\'margin-right:6px\'></i>' + this.name + '</h6></div>'
                    gridster.add_widget($('<li class="po-box bg-secondary" data-id="' + this.id + '" title="<div class=\'info-tip\'>' + card + '</div>"><div class="info-card"><div class="tag float-left">' + ++i + '</div>' + card + '</div></li>')
                            .on('mouseover', function(){
                                if($('#edit-form').is(':visible'))
                                    return

                                $(this).tipsy('show')
                            })
                            .on('mouseleave', function(){
                                $('.tipsy').remove()
                            })
                            .on('click', function(){
                                if($('#edit-form').is(':visible') || $('#edit-chk').is(':checked'))
                                    return

                                window.location = 'entrada.php?idc=' + $(this).data('id')

                            })
                            .tipsy({
                                gravity: (+this.col === 1 && +this.size_x === 1) ? 'sw' : (+this.col === +result.width ? 'se' : 's'),
                                html: true,
                                trigger: 'manual'
                            })
                            .on('mousedown', function(e) {
                                if(!$('#edit-chk').is(':checked'))
                                    return

                                startDrag(e)
                            })
                        ,
                        this.size_x, this.size_y, this.col, this.row)

                } else
                    gridster.add_widget($('<li class="po-box" data-id="0" data-box="' + ++i + '"><div class="info-card"><div class="tag">' + i + '</div></div></li>')
                            .on('click', function() {

                                if($('#edit-form').is(':visible') || $('#edit-chk').is(':checked'))
                                    return

                                $.fancybox.open({
                                    src: 'seleccion_cliente.php?box=' + $(this).data('box'),
                                    type: 'iframe',
                                    iframe: {
                                        css: {
                                            minHeight: '90%',
                                        }
                                    },
                                    afterShow: function () {
                                        $('.fancybox-iframe').contents().find("#q").focus();
                                    },
                                    afterClose: loadGridData
                                })

                            }),
                        this.size_x, this.size_y, this.col, this.row)

            })

            $(window).trigger('resize')

        } else  if(gridster) {
            gridster.destroy(true)
            gridster = false
        }


    }

    function startDrag(e) {

        var $el = $(e.currentTarget),
            top = $el.offset().top,
            left = $el.offset().left

        var $drag = $el.clone()
            .toggleClass('bg-secondary bg-primary')
            .css({
                position: 'absolute',
                display: 'block',
                cursor: 'move',
                boxShadow: '0 0 5px rgba(0, 0, 0, .5)',
                opacity: 0.75,
                zIndex: 1000
            })
            .appendTo('body')
            .offset({
                top: top,
                left: left
            })

        var diff_y = e.pageY - top,
            diff_x = e.pageX - left

        $drag.on('mouseout mouseup', function() {

            $('.po-box[data-id=0]').toggleClass('bg-primary', false)

            $(this).parent().off('mousemove')
            $(this).remove()

        })
            .on('mouseup', function(e){

                var y = e.pageY,
                    x = e.pageX

                var boxes = $('.po-box[data-id=0]')
                    .filter(function(idx, item){
                        var $el = $(item),
                            top = $el.offset().top,
                            left = $el.offset().left,
                            h = $el.outerHeight(),
                            w = $el.outerWidth()

                        return ((top < y  && y < (top + h)) && (left < x && x < (left + w)))

                    })

                if(boxes.length){
                    $.ajax('?', {
                        method: 'POST',
                        data: {
                            act: 5,
                            idc: $(this).data('id'),
                            box: boxes.eq(0).data('box')
                        }
                    }).done(function(data){
                        if(data.status) {
                            loadGridData()
                            flashAlert('El cliente HA SIDO MOVIDO!', 'primary')
                        } else
                            flashAlert('El casillero NO SE ENCUENTRA DISPONIBLE!')
                    })
                }

            })
            .parent()
            .on("mousemove", function(e) {

                var y = e.pageY,
                    x = e.pageX

                $drag.offset({
                    top: y - diff_y,
                    left: x - diff_x
                })

                $('.po-box[data-id=0]').each(function(idx, item){
                    var $el = $(item),
                        top = $el.offset().top,
                        left = $el.offset().left,
                        h = $el.outerHeight(),
                        w = $el.outerWidth()

                    var inside = ((top < y  && y < (top + h)) && (left < x && x < (left + w)))
                    $el.toggleClass('bg-primary', inside)

                })
            })

    }

    function alterDiagram(){
        $('input[name=act]').val($(this).data('action'))
        $('input[name=diagrama]').val(JSON.stringify(gridster ? gridster.serialize() : []))
        $.getJSON('?', $('#edit-form').serialize(), populateGrid)
            .fail(function(e){
                alert('ERROR: ' + e.statusText)
            })
    }

    function verticalFit() {
        gridster.resize_widget_dimensions({
            widget_base_dimensions: ['auto', Math.min((gridster.min_widget_width > 70 ? gridster.min_widget_width : 70), 100)]
        })

        $(JSON.parse(grid)).each(function(idx, item) {
            if(item.id)
                $(gridster.gridmap[item.col][item.row][0])
                    .toggleClass('bg-success', Boolean(+item.cnt && (gridster.min_widget_width * +item.size_x) <= 80))
                    .find('.name, .badge')
                    .toggle((gridster.min_widget_width * +item.size_x) > 80)
        })
    }

    setInterval(function(){
        if($('#edit-form').is(':visible'))
            return

        loadGridData()

    }, 5000)
    loadGridData()

})
