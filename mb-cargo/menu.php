<?php
if(!$user)
	exit();
?>

	<ul>
		<li><a href="main.php">INICIO</a></li>


        <?php if(+$user->id_nivel === -1): ?>
        <li><a>REPORTES</a>
            <ul>
                <li><a href="rep_entradas.php">Reporte de Entradas</a></li>
            </ul>
        </li>
        <?php endif ?>


        <?php if($user->id_nivel==2 || ($user->id_nivel==1 && $user->bod)): ?>
        <li><a>BODEGA</a>
            <ul>
                <li><a href="recepcion.php">Casilleros</a></li>
                <li><a href="bodega.php">Acciones de Bodega</a></li>
                <li><a href="manifiestos.php">Manifiestos</a></li>
                <li><a href="itinerarios.php">Agenda (USA)</a></li>
                <li><a href="rastreo.php">Rastreo de Envíos</a></li>
            </ul>
        </li>
        <?php endif ?>

        <?php if($user->id_nivel==2 || ($user->id_nivel==1 && $user->opr)): ?>
        <li><a>OPERACIONES</a>
            <ul>
                <li><a href="consultas.php">Consultas</a></li>
                <li><a href="clientes.php?act=1">Nueva Venta</a></li>
                <li><a href="clientes.php">Clientes</a></li>
            </ul>
        </li>
        <?php endif ?>

        <?php if($user->id_nivel==2): ?>
        <li><a>ADMINISTRACION</a>
            <ul>
                <li><a href="agentes.php">Representantes</a></li>
                <li><a><b>Reportes</b></a>
                    <ul>
                        <li><a href="rep_entradas.php">Reporte de Entradas</a></li>
                        <li><a href="rep_despacho.php">Reporte de Despacho</a></li>
                        <li><a href="rep_ventas.php">Ventas por Fecha</a></li>
                        <li><a href="rep_pagos.php">Pagos por Fecha</a></li>
                        <li><a href="graph_pagos.php">Estadisticas del Mes</a></li>
                    </ul>
                </li>
                <li><a><b>Configuración</b></a>
                    <ul>
                        <li><a href="tiendas.php">Tiendas</a></li>
                        <li><a href="transportistas.php">Transportistas</a></li>
                    </ul>
                </li>
                <li><a><b>Programación</b></a>
                    <ul>
                        <li><a href="programacion.php">Programar Salidas</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <?php endif ?>

        <?php if($user->id_nivel==3): ?>
        <li><a>CONFIGURACION</a>
            <ul>
                <li><a href="agencias.php">Agencias</a></li>
                <li><a><b>Usuarios</b></a>
                    <ul>
                        <li><a href="susuarios.php">Super Usuarios</a></li>
                        <li><a href="usuarios.php">Administradores / Representantes</a></li>
                    </ul>
                </li>
                <li><a href="carriers.php">Carriers</a></li>
                <li><a href="destinos.php">Destinos</a></li>
                <li><a href="clasificacion.php">Clasificación</a></li>
                <li><a href="productos.php">Productos</a></li>
                <li><a href="tarifas_pie.php">Tarifas por Pie</a></li>
                <li><a href="transportistas.php">Transportistas</a></li>
                <li><a href="rutas.php">Rutas (USA)</a></li>
            </ul>
        </li>

        <li><a>PROGRAMACION</a>
            <ul>
                <li><a href="programacion.php">Programar Salidas</a></li>
            </ul>
        </li>

        <li><a>REPORTES</a>
            <ul>
                <li><a href="rep_ventas_global.php">Ventas por Fecha</a></li>
                <li><a href="rep_pagos_global.php">Ingresos por Fecha</a></li>
                <li><a href="graph_pagos_global.php">Estadisticas del Mes</a></li>
            </ul>
        </li>


        <?php endif ?>


    </ul>