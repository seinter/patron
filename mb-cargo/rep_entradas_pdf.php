<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, -1);

$link = DB::connect();

$ida = $user->id_central ?: $user->id_agencia;

$pdf = new PDF("P");
$pdf->page_footer = $pdf_footer;

if($user->id_nivel > 1)
    $filter = $_GET['idc'] ? 'AND id = ' . $_GET['idc'] : '';
else
    $filter = "AND MD5(id) = '{$user->id}'";
$str = "SELECT id, casillero, CONCAT(nombre, ' ', apellido) cliente FROM clientes WHERE id_agencia = {$ida} AND casillero AND status {$filter} ORDER BY casillero";
$res = mysql_query($str, $link);
while($row = mysql_fetch_object($res)){
	$str = "SELECT entradas.id, entradas.cantidad, entradas.peso, entradas.piezas, entradas.observaciones, entradas.creado, usuarios.nombre usuario, tiendas.tienda FROM entradas INNER JOIN usuarios ON entradas.id_usuario = usuarios.id INNER JOIN tiendas ON entradas.id_tienda = tiendas.id WHERE id_cliente = {$row->id} AND entradas.creado BETWEEN '$f1 00:00:00' AND '$f2 23:59:59' AND entradas.status ORDER BY creado";
	$res2 = mysql_query($str, $link);
	if(mysql_num_rows($res2)){

		$pdf->title = array($title, '[ #' . $row->casillero . ' ] - '. formatClientCode($row->id) . ': ' . strtoupper($row->cliente));
		$pdf->AddPage();

        $pdf->SetFont('Verdana', 'B', 10);
        $pdf->Write('10',  "REPORTE DE ENTRADAS DEL '$f1' AL '$f2'");
        $pdf->Ln(15);

        $i = 0;
        unset($data, $totals);
        while($item = mysql_fetch_object($res2)){
            $data[] = [++$i, formatCode($item->id), date('Y-m-d h:i', strtotime($item->creado)), $item->usuario, $item->tienda, $item->cantidad, numFormat($item->peso), $item->piezas, substr($item->observaciones, 0, 30)];
            $totals['cantidad'] += $item->cantidad;
            $totals['peso'] += $item->peso;
            $totals['piezas'] += $item->piezas;
		}
		$header = array(" # ", "RECIBO", "FECHA", "USUARIO", "TIENDA", " CANTIDAD ", " PESO KG ", " PIEZAS ", "OBSERVACIONES");
		$footer = array("","","","","TOTALES:", $totals['cantidad'], numFormat($totals['peso']), $totals['piezas'], '');
		$pdf->ImprovedTable($header, $data, $footer);
	}
	
}

$pdf->Output();

?>