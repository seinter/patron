<?php
	session_start();
	require("config.php");
	include("classes/system.inc.php");
	include("classes/excel.inc.php");
	include("functions.php");

	$user = unserialize($_SESSION[$pfix."user"]);
	// User::authorize($user, 2);

	$link = DB::connect();

	$xls = new Excel("reporte_de_destintarios_con_whatsapp");

	$str = "select paises.pais, destinatarios.* from destinatarios left join paises on (destinatarios.id_pais = paises.id) where whatsapp or whatsapp2;";
	$res = mysql_query($str, $link);
	if (mysql_num_rows($res)){

        unset($data, $total, $pagado, $ventas);
        $xls->title(array($title, "REPORTE DE DESTINATARIOS CON WHATSAPP"));
        $xls->Ln();

		while($row = mysql_fetch_object($res)){
			$data[] = array(
				utf8_decode($row->nombre),
				($row->whatsapp ? '(W) ' : '') . $row->telefono,
				($row->whatsapp2 ? '(W) ' : '') . $row->telefono2,
				utf8_decode($row->direccion),
			);
		}
		$header = array("NOMBRE", utf8_decode("TELÉFONO"), utf8_decode("TELÉFONO 2"), utf8_decode("DIRECCIÓN"));
		$footer = array("","","","","");

        $xls->table($header, $data, $footer);

    }
	$xls->xlsEOF();
	exit();

?>