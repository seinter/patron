<?php
session_start();
require("../config.php");
include("../classes/system.inc.php");

$user = unserialize($_SESSION[$pfix . 'user']);
User::authorize($user);

$data = [
    'id' => $user->id,
    'name' => $user->nombre,
    'level' => $user->id_nivel
];

header('Content-Type: application/json');
print json_encode($data, JSON_NUMERIC_CHECK);
exit();

?>