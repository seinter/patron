// Javascript Document
// Requires custom fancybox 2.1

var RESOURCE_PATH = 'assets/js/payment/'

// IN PROGRESS?
var awaiting = false

// FORMAT MAIN TYPES
var cards = {
    names: ['Visa', 'MasterCard', 'American Express', 'Discover'],
    codes: ['visa', 'mastercard', 'amex', 'discover']
}


function openPaymentForm(options) {

    options = options || {}

    var callbacks = options.callbacks || {}
    var data = options.data || {}

    data.vault = data.vault || false

    $.fancybox2({
        content: getPaymentForm((options.type || 'direct'), data, callbacks),
        padding: 0,
        modal: true,
        wrapCSS: 'credit-card-form',
        helpers: {
            overlay: { css: overlayCSS || {'background': 'rgba(189, 189, 193, 0.75)'} }
        },
        afterShow: function(){
            data.vault || initPaymentForm()
        },
        beforeClose: function() {
            typeof callbacks.done === 'function' &&
                callbacks.done($('#cc-form').data())
        }
    })

}

function getPaymentForm(type, data, callbacks) {

    /* LAYOUT */
    var cc_form = $('<div>').attr('id', 'cc-form')
        .data('status', false)
    var amount

    switch (type) {
        case 'vault':
            $('<div>').attr('id', 'info-container')
                .append(
                    $('<div>').addClass('caption').html('<i class="fa fa-credit-card"></i> BOVEDA'),
                    $('<br>'),
                    $('<div>').addClass('info').html('ALMACENAR<br>NUEVO MEDIO DE PAGO')
                )
                .appendTo(cc_form)

            break
        default:
            amount = data.amount
            $('<div>').attr('id', 'info-container')
                .append(
                    $('<p>').html('No. REFERENCIA:'),
                    $('<input>').attr({
                        type: 'text',
                        name: 'ref',
                        id: 'ref',
                        class: 'ref',
                        value: data.ref || '- - - -'
                    }).prop('readonly', true),
                    $('<br>'),
                    $('<p>').html('MONTO:'),
                    $('<div>').addClass('amount').html(amount.currency + ' ' + (amount.value + amount.fee).toFixed(2))
                )
                .appendTo(cc_form)

            break
    }

    var card = data.card || {}
    var ccform = $('<div>').attr('id', 'form-container')
        .append(
            $('<input>').attr({
                type: 'hidden',
                name: 'cctype',
                id: 'cctype',
                value: card.type || ''
            }),

            $('<h1>').html(getCardName(card.type)),
            $('<img>').attr('src', getCardLogo(card.type)),
            $('<hr>'),

            $('<label>').html('Nombre'),
            $('<input>').attr({
                type: 'text',
                name: 'ccname',
                id: 'ccname',
                value: card.name
            }).prop('readonly', data.vault),

            $('<label>').html('No. de Tarjeta'),
            $('<input>').attr({
                type: 'text',
                name: 'ccnum',
                id: 'ccnum',
                placeholder: '---- ---- ---- ----',
                value: card.number || ''
            }).prop('readonly', data.vault)
        )
        .appendTo(cc_form)

    if(data.vault) {
        ccform.append(
            $('<input>').attr({
                type: 'hidden',
                name: 'code',
                id: 'code',
                value: card.code
            }),
            $('<input>').attr({
                type: 'hidden',
                name: 'customer_id',
                id: 'customer_id',
                value: card.customer_id
            })
        )
    } else {
        ccform
            .append(
                $('<div class="row d-flex">').append(
                    $('<div>').addClass('col').append(
                        $('<div>').append(
                            $('<label>').html('Expiración'),
                            $('<input>').attr({
                                type: 'tel',
                                name: 'expiry',
                                id: 'expiry',
                                placeholder: '-- / --'
                            })
                        )
                    ).css('flex', '0 0 130px'),
                    $('<div>').addClass('col').append(
                        $('<div>').append(
                            $('<label>').html('CVC'),
                            $('<input>').attr({
                                type: 'tel',
                                name: 'cvc',
                                id: 'cvc',
                                placeholder: '---'
                            })
                        )
                    ),
                    $('<div>').addClass('col').append(
                        $('<div>').append(
                            $('<label>').html('Origen'),
                            $('<input>').attr({
                                type: 'text',
                                name: 'country',
                                id: 'country',
                                placeholder: '---'
                            }).prop('readonly', true)
                        )
                    ).css('flex', '0 0 100px')
                ),
                $('<hr>')
            )
            .appendTo(cc_form)
    }


    $('<button>').attr('id', 'cc-cancel').html('<i class="fa fa-times"></i> CANCEL')
        .click(function() {
            $.fancybox2.close()
        }).appendTo(cc_form)


    $('<button>').attr('id', 'cc-submit').html((type === 'vault' ? 'STORE' : 'CHECKOUT') + ' <i class="fa fa-long-arrow-right">')
        .data('attempts', 0)
        .click(function() {


            $(this).data('attempts', Number($(this).data('attempts')) + 1)

            if(data.vault || validateCard()) {

                var form = $('#cc-form input').serializeArray()
                    .reduce(function(mix, item) {
                        mix[item.name] = item.value
                        return mix
                    }, {})

                $('#cc-form button, #cc-form input').prop('disabled', true)
                $.fancybox2.showLoading()


                var card
                if(data.vault)
                    card = {
                        code: form.code,
                        customer_id: form.customer_id
                    }
                else {
                    var name = NameParse.parse(form.ccname.capitalize())
                    var expiry = $.payform.parseCardExpiry(form.expiry)

                    card = {
                        number: form.ccnum.replace(/\s/g, ''),
                        type: form.cctype,
                        month: expiry.month.toString(),
                        year: expiry.year.toString(),
                        firstName: name.firstName + (name.initials !== '' ? ' ' + name.initials : ''),
                        lastName: name.lastName,
                        cvv: form.cvc,
                        countryCode: form.country
                    }
                }


                switch (type) {
                    case 'vault':

                        $.ajax(data.gateway['url'], {
                            method: 'POST',
                            contentType: 'application/json',
                            data: JSON.stringify({
                                customer_id: data.customer_id,
                                card: card
                            }),
                            beforeSend: function (jqXHR, settings) {
                                jqXHR.setRequestHeader('Authorization', 'Bearer ' +  data.gateway['token'])

                                awaiting = true
                                typeof callbacks.beforeSend === 'function' &&
                                callbacks.beforeSend(JSON.parse(settings.data))
                            }
                        })
                            .always(function(){ // data|jqXHR, textStatus, jqXHR|errorThrown
                                awaiting = false
                            })
                            .done(function(data){
                                if(data.status === "SUCCESSFUL") {
                                    $('#cc-form').data({
                                        status: true,
                                        code: data.code,
                                        validUntil: data.validUntil,
                                        card: card
                                    })
                                    $.fancybox2.close()

                                } else {
                                    flashAlert(data.message)
                                    restore()

                                }

                            })
                            .fail(function(jqXHR, textStatus, errorThrown){
                                flashAlert(textStatus.toUpperCase() + ': ' + errorThrown)
                                restore()
                            })


                        break
                    default:

                        var invoice = form.ref
                        var params  = {
                            invoice: invoice,
                            card: card,
                            items: data.items.map(function(item, idx) {

                                for(attr in item)
                                    item[attr] = item[attr].toString()

                                return item
                            }),
                            amount: amount.value.toString()
                        }

                        // LAST PARAMETER
                        if(data.gateway['url'].indexOf('Inclusive') > -1)
                            params.fee = amount.fee.toString()

                        $.ajax(data.gateway['url'], {
                            method: 'POST',
                            contentType: 'application/json',
                            data: JSON.stringify(params),
                            beforeSend: function (jqXHR, settings) {
                                jqXHR.setRequestHeader('Authorization', 'Bearer ' +  data.gateway['token'])

                                awaiting = true
                                typeof callbacks.beforeSend === 'function' &&
                                callbacks.beforeSend(JSON.parse(settings.data))
                            }
                        })
                            .always(function(data, textStatus, errorThrown){ // data|jqXHR, textStatus, jqXHR|errorThrown
                                awaiting = false
                                if(typeof callbacks.afterReceive === 'function')
                                    callbacks.afterReceive({
                                        invoice: invoice,
                                        status: data.status || 'ERROR',
                                        message: data.status ? data.message : errorThrown
                                    })

                                if(data.status !== 'SUCCESSFUL' && typeof callbacks.onFail === 'function')
                                    setTimeout(function() {
                                        callbacks.onFail({
                                            invoice: invoice,
                                            status: data.status || 'ERROR',
                                            message: data.status ? data.message : errorThrown
                                        })
                                    },500)


                            })
                            .done(function(data){
                                if(data.status === "SUCCESSFUL") {
                                    $('#cc-form').data({
                                        status: true,
                                        ref: invoice,
                                        fee: amount.fee
                                    })
                                    $.fancybox2.close()

                                } else {
                                    flashAlert('ERROR: ' + data.message)
                                    restore()
                                }

                            })
                            .fail(function(jqXHR, textStatus, errorThrown){
                                flashAlert(textStatus.toUpperCase() + ': ' + errorThrown)
                                restore()
                            })

                        break
                }

            }

        }).appendTo(cc_form)

    $('<img>').attr({
        id: 'seal',
        src: RESOURCE_PATH + 'trusted-site-seal.png',
        alt: 'Comodo Trusted Site Seal'
    }).appendTo(cc_form)

    function restore(){
        $.fancybox2.hideLoading()
        $('#cc-form button, #cc-form input').prop('disabled', false)
    }

    return cc_form

}

function initPaymentForm() {

    /* INIT */
    var ccname = $('#ccname'),
        ccnum = $('#ccnum'),
        expiry = $('#expiry'),
        cvc = $('#cvc'),
        country = $('#country')

    var closing = false

    ccnum.payform('formatCardNumber')
    expiry.payform('formatCardExpiry')
    cvc.payform('formatCardCVC')

    ;[ccname, ccnum, expiry, cvc, country].map(function(item, idx, arr) {
        $(item).on('input', validateCard)
            .on('keyup', function(e) {

                if(closing) {
                    closing = false
                    return
                }

                switch(e.keyCode) {
                    case 13:
                        if (idx < 3)
                            arr[idx + 1].focus()
                        else if (idx === 3)
                            arr[idx + 1].trigger('click')
                        else
                            $('#cc-submit').trigger('click')
                        break
                    case 9:
                        idx === 4 && item.trigger('click')
                        break
                }

            })
    })

    /* COUNTRY SELECTOR */
    var ccform = $('#cc-form')
    var sel = $('<select>')

    $.ajax(RESOURCE_PATH + 'countries.min.json').done(function(data){

        sel.select2({
            width: 340,
            dropdownParent: ccform,
            data: data.results.map(function(item){
                new Image().src = RESOURCE_PATH + 'flags/' +  item.code.toLowerCase() + '.png'
                return {
                    id: item.code,
                    text: item.country + ' (' + item.code + ')'
                }
            }),
            templateResult: function(item) {
                return item.id ?
                    $('<div class="item" style="background-image:url(' + RESOURCE_PATH + 'flags/' + item.id.toLowerCase() + '.png)">' + item.text + '</div>')
                    : item
            }
        })
            .on('select2:open', function() {
                setTimeout(function() {
                    $('#cc-form .select2-dropdown').removeClass('select2-dropdown--below')
                })
            })
            .on('select2:close', function() {
                closing = true
                country.focus()
            })
            .on('select2:select', function(e) {
                var code = e.params.data.id
                setCountry(code)
            })

        setCountry('US')

    })

    country.click(function(){
        sel.val($(this).val())
            .trigger('change').select2('open')
    }).on('mousedown', function(e){
        e.preventDefault()
    })

    ccform.on('click', function(e){
        if($(e.target).attr('id') === 'country' || $(e.target).closest('.select2-dropdown').length)
            return

        sel.select2('close')
    })

    function setCountry(code){
        country.val(code)
            .css('background-image', 'url(' + RESOURCE_PATH + 'flags/' + code.toLowerCase() + '.png)')
    }

    setTimeout(function() {
        ccname.focus().select()
    })

}


/* FUNCTIONS */
function getCardName(type){
    return cards.names[cards.codes.indexOf(type)] || (type ? type.toUpperCase() : false) || '&nbsp'
}

function getCardLogo(type) {
    return cards.codes.indexOf(type) !== -1 ? RESOURCE_PATH + 'cards/' + type + '.png' : ''
}

function validateCard() {

    var form = $('#cc-form #form-container')
    var ccname = $('#ccname'),
        ccnum = $('#ccnum'),
        expiry = $('#expiry'),
        cvc = $('#cvc'),
        country = $('#country')

    var valid = [],
        expiryObj = $.payform.parseCardExpiry(expiry.val()),
        cardType = $.payform.parseCardType(ccnum.val())

    form.find('#cctype').val(cardType)
    form.find('h1').html(getCardName(cardType))
    form.find('img').attr('src', getCardLogo(cardType))


    valid.push(fieldStatus(ccname, ccname.val()))
    valid.push(fieldStatus(ccnum, $.payform.validateCardNumber(ccnum.val())))
    valid.push(fieldStatus(expiry, $.payform.validateCardExpiry(expiryObj)))
    valid.push(fieldStatus(cvc, $.payform.validateCardCVC(cvc.val(), cardType)))
    valid.push(fieldStatus(country, country.val()))

    return valid.every(Boolean)

}

function fieldStatus(input, valid) {
    valid = Boolean(valid)

    if(input.attr('name') === 'ccname')
        valid = input.val().trim().indexOf(' ') !== -1

    Number($('#cc-submit').data('attempts')) && input.toggleClass('error', !valid)

    return valid
}

// PROTOTYPE
String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) {
        return a.toUpperCase()
    })
}
