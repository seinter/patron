// JavaScript Document

$(function(){

    $('.radio').iCheck({
        radioClass: 'iradio_flat-blue',
        increaseArea: '25%'
    }).on('ifChecked', function(event){
        this.form.submit()
    })

    $('#new-card').click(function(){

        self = $(this)
        self.prop('disabled', true)
        showLoading() // payment.js

        $.ajax('?', {
            method: 'POST',
            data: { dll: 10 }
        })
            .done(function(data) {
                if (data.status) {
                    openPaymentForm({
                        type: 'vault',
                        data: {
                            customer_id: data.customer_id,
                            gateway: data.gateway
                        },
                        callbacks: {
                            done: function (data) {
                                if(data.status){

                                    var form = getNewSubmitForm()

                                    createNewFormElement(form, 'dll', 1)
                                    createNewFormElement(form, 'code', data.code)
                                    createNewFormElement(form, 'validUntil', data.validUntil)
                                    ;['type', 'firstName', 'lastName', 'number'].map(function(item) {
                                        createNewFormElement(form, item, data.card[item])
                                    })
                                    form.submit()

                                } else
                                    self.prop('disabled', false)
                            }
                        }
                    })
                } else {
                    self.prop('disabled', false)
                    hideLoading()
                    flashAlert(data.message)
                }

            })

    })

    /*** PAYMENT ***/
    $('#btn-payment').click(function(){
        var self = $(this)

        showLoading() // payment.js

        self.attr('disabled', true)
        payWithCard(this.form, function() { // cancelCallback
            self.attr('disabled', false)
            hideLoading()
        })

    })


    function payWithCard(form, cancelCallback) {
        var id = $('input[name=id]').val()
        var total = Number($('#payment').text())

        $.ajax('?', {
            method: 'POST',
            data: {
                dll: 12,
                id: id,
                amount: total
            }
        })
            .done(function(data) {

                if(data.status) {

                    Cookies.set(buildCookieName('lock'), data.key)

                    var card = data.card
                    var fee = Number(data.fee)
                    var items = []

                    items.push({
                        name: 'Débito - VENTA No. ' + $('#titulo').text(),
                        quantity: 1,
                        price: total
                    })

                    openPaymentForm({
                        data: {
                            gateway: data.gateway,
                            ref: data.key,
                            vault: true,
                            card: card,
                            items: items,
                            amount: {
                                currency: 'USD',
                                value: total,
                                fee: fee
                            }
                        },
                        callbacks: {
                            beforeSend: function (data) {
                                $.ajax('?', {
                                    method: 'POST',
                                    data: {
                                        dll: 14,
                                        ref: data.invoice,
                                        card_id: card.id,
                                        amount: total + fee
                                    }
                                })
                            },
                            afterReceive: function (data) {
                                $.ajax('?', {
                                    method: 'POST',
                                    data: {
                                        dll: 14,
                                        ref: data.invoice,
                                        status: data.status,
                                        desc: data.message
                                    }
                                })
                            },
                            done: function (data) {
                                if (data.status) {

                                    flashAlert('Transacción efectuada con éxito!', 'success')

                                    // INVOICE
                                    $('<input>').attr({
                                        type: 'hidden',
                                        name: 'invoice',
                                        value: data.ref
                                    }).appendTo($(form))

                                    // FEE
                                    $('<input>').attr({
                                        type: 'hidden',
                                        name: 'fee',
                                        value: data.fee
                                    }).appendTo($(form))

                                    form.submit()

                                } else
                                    cleanUp(false, cancelCallback) // ASYNC CLEAN UP

                            }
                        }
                    })

                } else {
                    flashAlert('ERROR: ' + data.message)
                    cancelCallback()
                }

            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                alert(textStatus.toUpperCase() + ': ' + errorThrown)
                cancelCallback()
            })

    }


})

// FUNCTIONS
function confirmDelete(id, name){
    if(confirm("Esta a punto de eliminar la tarjeta '"+name+"',\ndesea continuar ?")){
        var get = http_get_vars()
        window.location = '?dll=3&id=' + id + (get['modal'] ? '&modal=true' : '')
    }
}

// CLEAN-UP
window.cleanUp = function (sync, callback) {

    sync = sync || false

    if(Cookies.get(buildCookieName('lock')))
        $.ajax('?', {
            method: 'POST',
            async: !sync,
            data: {
                dll: 13,
                key: Cookies.get(buildCookieName('lock'))
            },
            success: function(status) {
                status &&
                Cookies.remove(buildCookieName('lock'))
                typeof callback === 'function' &&
                callback()
            }
        })
    else
        typeof callback === 'function' &&
        callback()


}

window.onbeforeunload = function() {
    if(awaiting)
        return true
    else
        cleanUp(true) // SYNC CLEAN UP
}

window.onunload = function() {
    if(awaiting)
        cleanUp(true)

}

cleanUp() // ANY PREVIUOS TRACE