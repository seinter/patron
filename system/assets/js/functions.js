// JavaScript Document

var masks = {'1':'(000) 000-00009'};
var mask_defaults = {
    onKeyPress: function(cep, e, field, options){
        var id_pais = jQuery('select[name="codigo_de_area"]').val();
        field.mask(masks[id_pais], options)
    }
}

$(function(){
    /* PHONE MASK */
    try {
        $('.phone-input').mask(masks[0], mask_defaults)
    } catch(e){}

    $(document).on('keydown', '.select2', function (e) {
        if (e.originalEvent && e.which !== 9) {
            $(this).siblings('select').select2('open');
        }
    })

    $('#user-btn').click(function(e){
        e.stopPropagation()
        $('#user-menu').trigger('click')
    })

})

function validate(form){
    var y = 0

    resetErrors(form)
    for(x = 0; x< form.length; x++)
        if (form.elements[x].value === '' && !form.elements[x].disabled && ['button', 'submit', 'reset'].indexOf(form.elements[x].type) === -1 && form.elements[x].className.indexOf('optional-input') === -1 && form.elements[x].getAttribute("rel") != 'no-validate') {
            console.log(form.elements[x].name)
            stackError(form.elements[x])
            y++
        }

    if(y>0){
        alert("Debe llenar todos los campos requeridos!")
        return false
    }
    return true
}
            
function cargarImagenBanderaPais(pais) {
    if (!pais.id) {
        return pais.text;
    }
    var baseUrl = "assets/img/flags_pais";
    var pais_texto = $(
        '<span><img src="' + baseUrl + '/' + pais.element.value.toLowerCase() + '.png" class="img-flag" /> ' + pais.text + '</span>'
    );
    return pais_texto;
}
            
function cargarImagenBanderaCodigoDeArea(pais) {
    if (!pais.id) {
        return pais.text;
    }
    var baseUrl = "assets/img/flags_codigo_de_area";
    var pais_texto = $(
        '<span><img src="' + baseUrl + '/' + pais.element.value.toLowerCase() + '.png" class="img-flag" /> ' + pais.text + '</span>'
    );
    return pais_texto;
}
            
function cargarImagenBanderaCodigoDeAreaSinTexto(pais) {
    if (!pais.id) {
        return pais.text;
    }
    var baseUrl = "assets/img/flags_codigo_de_area";

    html = '<span><img src="' + baseUrl + '/' + pais.element.value.toLowerCase() + '.png" class="img-flag" /> </span>';

    return $(html);
}

/*** FANCYBOX 2 ***/
var overlayCSS = {'background': 'rgba(189, 189, 193, 0.75)'}

function showLoading() {
    $.fancybox2({})
    $.fancybox2.helpers.overlay.open({
        closeClick: false,
        css: overlayCSS
    })
    $.fancybox2.showLoading()
}

function hideLoading() {
    $.fancybox2.helpers.overlay.close()
    $.fancybox2.hideLoading()
}

/*** FANCYBOX 3 ***/
function authorize(id, callback){
    $.fancybox.open({
        src: 'authorize.php?id=' + id,
        type: 'iframe',
        modal: true,
        afterShow: function() {
            $('.fancybox-iframe').contents().find("#uname").focus()
        },
        beforeClose: function(){
            var idu = $('.fancybox-iframe').contents().find('#idu').val()

            if(+idu)
                callback(idu)
        }
    })
}

/* MESSAGES */
function flashAlert(text, type) {
    type = type === undefined ? 'danger' : type
    var color = ['light', 'warning'].indexOf(type) !== -1 ? 'text-dark' : 'text-light'
    var alert = $('<div class="alert bg-' + type + ' ' + color + ' text-center" style="opacity:.9; width:400px; position:fixed; top:50%; left:50%; margin-top:-30px; margin-left:-200px; z-index:9999; box-shadow: 0 0 10px rgba(0, 0, 0, .4)"><h5 style="margin:5px 0">' + text + '</h5></div>')
    $('body').append(alert)
    setTimeout(function(){
        alert.fadeOut(300, function(){
            $(this).remove()
        })
    }, 800)
}

/* ERROR HANDLING */
function resetErrors(form){
    $(form.elements).toggleClass('error', false)

    $(form).find('select').each(function(){
        $(this).data('select2').$container.toggleClass('error', false)
    })
}
function stackError(el){
    if(el.type === 'select-one')
        $(el).data('select2').$container.toggleClass('error', true)
    else
        $(el).toggleClass('error', true)
}

/* COOKIES */
function buildCookieName(pfix) {
    var path = window.location.pathname.split("/")
    var page = path.pop().split('.').slice(0, -1).toString()
    var dir = path.splice(1, path.length - 1).join('_')

    return (pfix || page || 'default') + '_' + (dir || 'root') + '_' + (window.location.port || window.location.protocol.replace(':', ''))
}


// LEGACY CODE
window.defaultStatus = 'Powered by WSNet';

var reDigits = /[\d+]/;
var reNoDigits = /[^\d]/gi;
var reNumbers = /[\d+.]/;

function setFocus(ctl,sel){
    var obj = document.getElementById(ctl);
    obj.focus();
    if(Boolean(sel))
        obj.select();
}

function showPhoto(id){
    $.fancybox.open({
        src: 'foto.php?id=' + id,
        type: 'iframe'
    })

}

function showZipCodes(id){
    createWindow('zips.php?idr='+id,'wndzips',600,520,'no','no');
}

function validateBinnacle(form){
    var y = 0;
    for(x = 0;x<form.length;x++){
        if(form.elements[x].value== "" && ['button', 'submit'].indexOf(form.elements[x].type) === -1){
            if(Number(form.entrega.value)){
                switch(form.elements[x].name){
                    case "nombre":
                    case "documento":
                    case "foto":
                        break;
                    default:
                        stackError(form.elements[x])
                        y++;
                        break;
                }
            } else {

                switch(form.elements[x].name){
                    case "foto":
                    case "id_carrier":
                    case "guia":
                        break;
                    default:
                        stackError(form.elements[x])
                        y++;
                        break;
                }
            }
        }
    }
    if(y>0){
        alert("Debe llenar todos los campos requeridos !!!");
        return false;
    }
    return true;
}

function validateAgency(form){
    var y = 0;
    for(x = 0;x<form.length;x++){
        if(form.elements[x].value==""){
            switch(form.elements[x].name){
                case "direccion":
                case "telefono":
                case "origen":
                    break;
                default:
                    y++;
                    break;
            }
        }
    }
    if(y>0){
        alert("Debe llenar todos los campos requeridos !!!");
        return false;
    }
    return true;
}

function validateUser(form){
    var y = 0;
    for(x = 0;x<form.length;x++){
        if(form.elements[x].value==""){
            switch(form.elements[x].name){
                case "password":
                    break;
                default:
                    y++;
                    break;
            }
        }
    }
    if(y>0){
        alert("You must fill the required fields !!!");
        return false;
    }
    return true;
}

function validateDestination(form){
    var y = 0;
    for(x = 0;x<form.length;x++){
        if(form.elements[x].value==""){
            switch(form.elements[x].name){
                case "info":
                    break;
                default:
                    y++;
                    break;
            }
        }
    }
    if(y>0){
        alert("Debe llenar todos los campos requeridos !!!");
        return false;
    }
    return true;
}

function validateClient(form){
    var y = 0;
    for(var x = 0;x<form.length;x++){
        if(form.elements[x].value=="" && !form.elements[x].disabled && ['button', 'submit'].indexOf(form.elements[x].type) === -1){

            switch(form.elements[x].name){
                case "telefono2":
                case "email":
                    break;
                case "zip":
                case "direccion_usa":
                case "direccion":
                case "id_depto":
                    if(Number(form.id_pais.value)==225 || Number(form.id_pais.value)==136){
                        switch(form.elements[x].name){
                            case "direccion":
                            case "id_depto":
                                break;
                            default:
                                stackError(form.elements[x])
                                y++;
                                break;
                        }
                    } else {
                        switch(form.elements[x].name){
                            case "zip":
                            case "direccion_usa":
                                break;
                            default:
                                stackError(form.elements[x])
                                y++;
                                break;
                        }
                    }
                    break;
                default:
                    stackError(form.elements[x])
                    y++;
                    break;
            }

        }
    }
    if(y>0){
        alert("Debe llenar todos los campos requeridos !!!");
        return false;
    }

    return true;
}

function validatePassword(form){
    if(validate(form)){
        if(form.npass1.value != form.npass2.value){
            alert("Los valores no coinciden !!!");
        } else
            return true;
    }
    return false;
}

function validateSearch(form){
    if(Number(form.dll.value)!=3)
        return validate(form);
    return;
}

function numFormat(number, def){
    var x;
    var y;
    if(def == undefined)
        def = 0;
    if(isNaN(number)||number==0)
        number = def;
    number = round(number, 2);
    number = number.toString();
    y = number.length - 1;
    for(x=0;x<number.length;x++)
        if(number.substr(x,1) == ".")
            y = x;
    switch((number.length-1) - y){
        case 1:
            number += "0";
            break;
        case 0:
            number += ".00";
            break;
    }
    return number;
}

function numFormatInt(number, def){
    var x;
    var y;

    if(def == undefined)
        def = 0;
    if(isNaN(number)||number==0)
        number = def;
    number = parseInt(number);
    return number;
}

function round(number,X) {
    X = (!X ? 2 : X);
    return Math.round(number*Math.pow(10,X))/Math.pow(10,X);
}

function createWindow(page, name, w, h, s, mb) {
    var winl = (screen.width-w)/2;
    var wint = (screen.height-h)/2;
    if (winl < 0) winl = 0;
    if (wint < 0) wint = 0;
    if(mb==undefined)
        mb = 'yes';
    var features =
        'width='        + w +
        ',height='      + h +
        ',top='         + wint +
        ',left='        + winl +
        ',menubar='     + mb +
        ',scrollbars='  + s +
        ',resizable='   + 'no';
    window.open (page, name, features);
}


var mask = "(###) ###-#####";

function doMask(textBox, event) {

    var keyCode = event.which ? event.which : event.keyCode;

    if(keyCode == 13 || keyCode == 8 || keyCode == 9 || keyCode == 46 || keyCode == 35 || keyCode == 36 || keyCode == 37 || keyCode == 39)
        return true;

    var keyCharacter = cleanKeyCode(keyCode);
    var val = textBox.value;

    if(reDigits.test(keyCharacter) == false)
        return false;

    val = val.replace(reNoDigits,'');
    val += keyCharacter;
    textBox.value = val.maskValue(mask);
    setCaretAtEnd(textBox);

    return false;
}

function onFocusMask(textBox) {

    var val = textBox.value;

    if(val.length == 0 || val == null) {
        var i = mask.indexOf('#');
        textBox.value = mask.substring(0,i);
    }

    setCaretAtEnd(textBox);
    textBox.maxlength = mask.length;

}

function onBlurMask(textBox) {

    var val = textBox.value;

    if(reDigits.test(val) == false)
        textBox.value = '';

}

String.prototype.maskValue = function(mask) {

    var retVal = mask;
    var val = this;

    for(var i=0;i<val.length;i++)
        retVal = retVal.replace(/#/i, val.charAt(i));

    retVal = retVal.replace(/#/gi, "");

    return retVal;

}

function cleanKeyCode(key) {

    switch(key) {
        case 96:
            return "0";
            break;
        case 97:
            return "1";
            break;
        case 98:
            return "2";
            break;
        case 99:
            return "3";
            break;
        case 100:
            return "4";
            break;
        case 101:
            return "5";
            break;
        case 102:
            return "6";
            break;
        case 103:
            return "7";
            break;
        case 104:
            return "8";
            break;
        case 105:
            return "9";
            break;
        case 110:
        case 190:
            return ".";
        default:
            return String.fromCharCode(key);
            break;
    }
}

function setCaretAtEnd (field) {

    if (field.createTextRange) {

        var r = field.createTextRange();

        r.moveStart('character', field.value.length);
        r.collapse();
        r.select();
    }

}


/* FORM FUNCTIONS */
function getNewSubmitForm(url){
    var url = url || "?";
    var submitForm = document.createElement("FORM");
    submitForm.method = "POST";
    submitForm.action = url;
    document.body.appendChild(submitForm);
    return submitForm;
}

function createNewFormElement(inputForm, elementName, elementValue){
    var newElement = document.createElement("INPUT");
    newElement.setAttribute("type", "hidden");
    newElement.setAttribute("name", elementName);
    newElement.setAttribute("value", elementValue);
    inputForm.appendChild(newElement);
    newElement.value = elementValue;
    return newElement;
}

function http_get_vars() {
    var $_GET = {}
    if (document.location.toString().indexOf('?') !== -1) {
        var query = document.location
            .toString()
            // get the query string
            .replace(/^.*?\?/, '')
            // and remove any existing hash string (thanks, @vrijdenker)
            .replace(/#.*$/, '')
            .split('&')

        for (var i = 0, l = query.length; i < l; i++) {
            var aux = decodeURIComponent(query[i]).split('=')
            $_GET[aux[0]] = aux[1]
        }
    }

    return $_GET
}