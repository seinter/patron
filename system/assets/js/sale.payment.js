// JavaScript Document


$(function(){

    /*** SUBMIT ***/
    $('#btn-submit').click(function() {

        var form = this.form

        if(validateSale(form)){

            if(+form.id_tipo.value)
                form.submit()
            else
                launchPayment(form)

        }
    })

    $('#btn-payment').click(function(){
        launchPayment(this.form)
    })

    function launchPayment(form) {

        if (!validatePayment(form))
            return

        showLoading() // payment.js
        var controls = $(this)
            .add($(form).find('input[name=tarjeta]'))
            .attr('disabled', true)

        if ($('#cash').is(':checked')) {

            $.ajax('?', {
                method: 'POST',
                data: { dll: 16 }
            })
                .done(function (data) {
                    if (data.status) {
                        if(data.card) {
                            form.id_card.value = data.card.id
                            payWithCard(form, function () { // cancelCallback
                                form.id_card.value = 0
                                controls.attr('disabled', false)
                                hideLoading()
                            })
                        } else {
                            if (form.id.value != '0') {
                                if (form.tot.value != form.payment.value) {
                                    form.dll.value = 5;
                                }
                            }
                            form.submit();
                        }
                    } else {
                        flashAlert('ALERTA: ' + data.message)
                        controls.attr('disabled', false)
                        hideLoading()

                        if(data.pending)
                            setTimeout(function() {
                                $.ajax('json/access.json.php')
                                    .done(function (data) {
                                        if (data.level > 1 && confirm('Desea acceder a \'MEDIOS DE PAGO\'?'))
                                            $.fancybox.open({
                                                src: 'cartera.php?modal=true',
                                                type: 'iframe'
                                            })

                                    })
                            }, 100)

                    }
                })

        } else {

            payWithCard(form, function () { // cancelCallback
                controls.attr('disabled', false)
                hideLoading()
            })

        }

    }


    /*** CREDIT CARD ***/
    $('input[name=tarjeta]').iCheck({
        radioClass: 'iradio_flat-blue',
        increaseArea: '20%'
    }).on('ifClicked', function(){
        if(+$('#id').val()){
            var val = Boolean(+this.value)
            var btn = $('#btn-payment')

            btn.toggleClass('btn-success', !val).toggleClass('btn-primary', val)
            btn.find('span').text(val ? 'Procesar Pago' : 'Aplicar Pago')
            btn.find('i').toggleClass('fa-money', !val).toggleClass('fa-credit-card', val)
        }
    })

    function payWithCard(form, cancelCallback) {
        var id = +$('#id').val()
        var cid = +$('#id_card').val()
        var total = id ? Number($('#pago').val()) : Number($('#payment').val())

        $.ajax('?', {
            method: 'POST',
            data: {
                dll: 12,
                amount: total,
                card_id: cid
            }
        })
            .done(function(data) {

                if(data.status) {

                    var fee = Number(data.fee)
                    var items = []
                    var desc = '[' + (data.vault ?  'DEBITO' : 'PAGO') + '] Envio: ' + $('#origen').select2('data')[0].text + ' - ' + $('#destino').select2('data')[0].text

                    if(id)
                        desc += ' (' + $('#fecha').text() + ')'
                    else {
                        var d = new Date()
                        desc += ' (' + [
                            d.getFullYear(),
                            ('0' + (d.getMonth() + 1)).slice(-2),
                            ('0' + d.getDate()).slice(-2)
                        ].join('-') + ')'
                    }

                    total = Number(data.amount)

                    items.push({
                        name: desc,
                        quantity: 1,
                        price: total
                    })

                    openPaymentForm({
                        data: {
                            gateway: data.gateway,
                            ref: data.key,
                            vault: data.vault,
                            card: data.vault ? data.card : {
                                name: document.getElementById('div_cliente').innerText
                            },
                            items: items,
                            amount: {
                                currency: 'USD',
                                value: total,
                                fee: fee
                            }
                        },
                        callbacks: {
                            beforeSend: function (data) {
                                $.ajax('?', {
                                    method: 'POST',
                                    data: {
                                        dll: 14,
                                        ref: data.invoice,
                                        card_id: cid,
                                        type: cid ? null : data.card['type'],
                                        name: cid ? null : data.card['firstName'] + ' ' + data.card['lastName'],
                                        number: cid ? null : Array(data.card['number'].replace(/[^\d]/g, "").length - 3).join("x") +
                                            data.card['number'].substr(-4),
                                        amount: total + fee
                                    }
                                })
                            },
                            afterReceive: function (data) {
                                $.ajax('?', {
                                    method: 'POST',
                                    data: {
                                        dll: 14,
                                        ref: data.invoice,
                                        status: data.status,
                                        desc: data.message
                                    }
                                })
                            },
                            done: function (data) {
                                if (data.status) {

                                    flashAlert('Transacción efectuada con éxito!', 'success')

                                    // ENABLE DISABLED CONTROLS
                                    $('input[name=tarjeta]')
                                        .attr('disabled', false)

                                    // INVOICE
                                    $('<input>').attr({
                                        type: 'hidden',
                                        name: 'invoice',
                                        value: data.ref
                                    }).appendTo($(form))

                                    // FEE
                                    $('<input>').attr({
                                        type: 'hidden',
                                        name: 'fee',
                                        value: data.fee
                                    }).appendTo($(form))

                                    id && (form.dll.value = 5)
                                    form.submit()

                                } else
                                    cancelCallback()

                            },
                            onFail: function(data) {
                                if(cid) {
                                    if (confirm('No se pudo debitar en este momento,\nDesea utilizar crédito y continuar?')){

                                        cancelCallback()

                                        // FEE
                                        $('<input>').attr({
                                            type: 'hidden',
                                            name: 'pending',
                                            value: 1
                                        }).appendTo($(form))

                                        form.submit()
                                    }
                                }
                            }
                        }
                    })


                } else {
                    alert('ERROR: ' + data.message)
                    cancelCallback()
                }

            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                alert(textStatus.toUpperCase() + ': ' + errorThrown)
                cancelCallback()
            })

    }

})


/*** VALIDATE ***/
function validatePayment(form){
    var payment = +$('#id').val() ? form.pago : form.payment

    resetErrors(form)
    if(+payment.value)
        return true

    alert('Debe especificar un monto!')
    stackError(payment)

    return false

}

window.onbeforeunload = function() {
    if(awaiting)
        return true
}