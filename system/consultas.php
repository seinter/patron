<?php
session_start();
require("config.php");

include("classes/system.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$str = "set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'";
mysql_query($str, $link);

if($act = +$_POST['act']) {
    $q = +$_POST['q'];
    $filter = $user->id_central ? "agencias.id = {$user->id_agencia}" : ($user->global ? 'TRUE' : "{$user->id_agencia} IN (agencias.id, agencias.id_central)");

    switch ($act) {
        case 1:
            $filter .= " AND ventas.id = {$q}";
            break;
        case 2:
            $str = "SELECT detalle_venta.id_venta FROM detalle_venta INNER JOIN bitacora ON detalle_venta.id = bitacora.id_detalle WHERE bitacora.id  = {$q}";
            $res = mysql_query($str, $link);
            $filter .= ' AND ventas.id = ' . (@mysql_result($res, 0) ?: 0);
            break;
        case 3:
            $filter .= " AND ventas.fecha = '{$_POST['fecha']}'";
            break;
    }

    $str = "SELECT ventas.id, ventas.fecha, ventas.descuento, ventas.cargos, ventas.seguro, pc_seguro, declarado, ventas.status, CONCAT(clientes.nombre,' ',clientes.apellido) cliente, local1.localidad origen, local2.localidad destino, SUM(detalle_venta.guias) guias, SUM(detalle_venta.tarifa * detalle_venta.cantidad) precio, pagos.pagado, pagos.costos FROM ventas INNER JOIN usuarios ON ventas.id_usuario = usuarios.id INNER JOIN agencias ON usuarios.id_agencia = agencias.id INNER JOIN clientes ON ventas.id_cliente = clientes.id INNER JOIN localidades local1 ON ventas.origen = local1.id INNER JOIN localidades local2 ON ventas.destino = local2.id LEFT JOIN detalle_venta ON ventas.id = detalle_venta.id_venta AND detalle_venta.status LEFT JOIN (SELECT id_venta, SUM(monto) pagado, SUM(costo) costos FROM pagos WHERE status GROUP BY id_venta) pagos ON ventas.id = pagos.id_venta WHERE $filter GROUP BY ventas.id ORDER BY ventas.id";
    $res = mysql_query($str, $link);

}


?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right caption">CONSULTAS</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">

                <table width="100%" border="0" cellspacing="10" cellpadding="0">
                    <tr>
                        <td style="padding:0">
                            <form id="add" name="add" method="post" action="?" onsubmit="return validateSearch(this)">

                            <div class="alert alert-primary big-box">

                                <div class="row">
                                    <div class="col-12 col-sm-3 text-sm-right" style="line-height:38px">
                                        <h4 class="text-truncate" style="margin:5px 0">BUSCAR POR</h4>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-3 big-combo" style="padding-right:0">
                                        <select name="act" id="act" style="width:100%" onchange="setSearch(this.value);">
                                            <option value="1">Número de Venta</option>
                                            <option value="2" <?php echo $act === 2 ? 'selected' : '' ?>>Número de Guía</option>
                                            <option value="3" <?php echo $act === 3 || !$act ? 'selected' : '' ?>>Fecha</option>
                                        </select>
                                    </div>
                                    <div class="col-6 col-sm-5 col-md-3" style="padding-left:5px">

                                        <div id="div_q" style="display:<?php echo $act === 3 || !$act ? 'none' : '' ?>">
                                            <input type="text" name="q" id="q" class="form-control" style="border-color:#AAA" value="<?php echo $act !== 3 ? $_POST['q'] : '' ?>" />
                                        </div>
                                        <div id="div_date" style="display:<?php echo $act && $act !== 3 ? 'none' : '' ?>">
                                            <div class="date-field">
                                                <input type="text" name="fecha" id="fecha" class="form-control" style="background-color:#FFF; border-color:#AAA" value="<?php echo $fecha ?: date('Y-m-d')?>" readonly="readonly" />
                                                <i class="fa fa-calendar" style="top:12px; right:14px;"></i>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-12 col-sm-9 offset-sm-3 d-md-none">
                                        <button class="btn btn-sm btn-primary w-100" style="margin-top:4px">BUSCAR <i class="fa fa-search" style="margin-left:5px"></i></button>
                                    </div>

                                    <div class="d-none d-md-block col-3" style="padding-right:19px; padding-left:0;">
                                        <button type="submit" class="btn btn-sm btn-primary w-100" style="margin-top:4px">BUSCAR <i class="fa fa-search" style="margin-left:5px"></i></button>
                                    </div>

                                </div>

                            </div>

                            </form>
                        </td>
                    </tr>
			<?php
			if($act){
				if($cnt = mysql_num_rows($res)) {
			?>
      		<tr>
      			<td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-grid">
				<caption>REGISTROS ENCONTRADOS: <?php echo $cnt ?></caption>
					<tr>
						<th>ID</th>
						<th>Fecha</th>
						<th>Cliente</th>
						<th class="d-none d-md-table-cell">Destino</th>
						<th>Guias</th>
						<th>Precio</th>
						<th>Pagado</th>
						<th>Saldo</th>
						<th width="75"></th>
						</tr>
			<?php
                    while($row = mysql_fetch_assoc($res)){
                        $seguro = $row["seguro"] ? ($row["declarado"] * $row["pc_seguro"]) / 100 : 0;
			?>

					<tr <?php echo !$row['status'] ? 'class="table-danger text-danger" style="text-decoration:line-through" ' : '' ?>">
						<td><?php echo formatCode($row["id"]) ?></td>
						<td><?php echo $row["fecha"] ?></td>
						<td><?php echo $row["cliente"] ?></td>
						<td class="d-none d-md-table-cell"><?php echo $row["origen"]." - ".$row["destino"] ?></td>
						<td class="text-right"><?php echo numFormatInt($row["guias"]) ?></td>
						<td class="text-right"><?php echo numFormat($precio = $row['precio'] - $row['descuento'] + $row['cargos'] + $row['costos'] + $seguro) ?></td>
						<td class="text-right"><?php echo numFormat($row["pagado"]) ?></td>
						<td class="text-right"><?php echo numFormat($precio - $row["pagado"]) ?></td>
                        <td>
                            <form action="venta.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $row["id"] ?>" />
                                <button type="submit" class="btn btn-sm btn-outline-dark w-100"><i class="fa fa-caret-right"></i> <i class="fa fa-caret-right"></i> <i class="fa fa-caret-right"></i></button>
                            </form>
                        </td>
                    </tr>
			<?php	} ?>
				</table></td>
      		</tr>
			<?php } else { ?>
      		<tr>
      			<td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FF0000">
					<tr class="forWCaption">
						<td><div align="center">NO SE ENCONTRO EL REGISTRO !!!</div></td>
					</tr>
				</table></td>
      		</tr>
			<?php
				}
			}
			?>
    </table>

            </div>
        </div>
    </div>

    <script type="text/javascript" language="JavaScript" src="popcalendar.js"></script>
    <script language="javascript" type="text/javascript">
        function setSearch(val){
            var q = document.getElementById('div_q');
            var date = document.getElementById('div_date');

            q.style.display = 'none'
            date.style.display = 'none'

            if(+val === 3)
                date.style.display = ''
            else {
                q.style.display = ''
                setTimeout(function(){
                    $('#q').focus()
                }, 100)
            }
        }

        function afterClose(ctl){
            ctl.form.submit()
        }

        <?php if($act !== 3): ?>
        setFocus('q', 1);
        <?php endif ?>

    </script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('select').select2()

            $('.date-field input').click(function() {
                showCalendar(this, this, 'yyyy-mm-dd','es',1)
            })
        })
    </script>

</div>
<?php include 'footer.php' ?>