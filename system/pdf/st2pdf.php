<?php
require('fpdf.php');

class PDF extends FPDF
{
    function __construct($orientation='P',$unit='pt',$format='Letter'){
        parent::__construct($orientation,$unit,$format);
        $this->SetMargins(18, 18, 18);
        $this->SetAutoPageBreak(1,18);

        $this->AddFont('Verdana','','verdana.php');
        $this->AddFont('Verdana','B','verdanab.php');
    }

    function Header()
    {
        $margins = 36;

        if($this->DefOrientation=="P"){
            $width = 612-$margins;
        } else {
            $width = 792-$margins;
        }
        $this->SetFont('Verdana','',12);

        $this->Cell($width,20,' '.$this->title[0],"B",1,'L');
        if($this->title[1]){
            $this->SetFont('Verdana','B',16);
            $this->Cell($width,26,$this->title[1],0,1,'L');
        }

        $this->SetTitle($this->title[0] ?: $this->page_footer);

    }

    function ImprovedTable($header,$data,$footer=array(),$color=0)
    {

        $margins = 36;
        $theight = 15;
        $height = 14;

        if($this->DefOrientation=="P"){
            $width = 612-$margins;
        } else {
            $width = 792-$margins;
        }

        foreach($header as $key => $value)
            $w[$key] = strlen($value);
        if(count($data)){
            foreach($data as $value){
                foreach($value[0] as $key02 => $value02)
                    $w[$key02] = (strlen($value02) > $w[$key02])?strlen($value02):$w[$key02];
            }
        }
        foreach($footer as $key => $value)
            $w[$key] = (strlen($value) > $w[$key])?strlen($value):$w[$key];

        $w_sum = array_sum($w);

        foreach($w as $key => $value)
            $w[$key] = ($value * $width)/$w_sum;

        //Colors, line width and bold font
        if($color){
            $this->SetFillColor(128,160,198);
            $this->SetDrawColor(128,160,198);
            $this->SetTextColor(255);
        } else {
            $this->SetFillColor(255);
            $this->SetDrawColor(0);
            $this->SetTextColor(0);
        }
        $this->SetLineWidth(1.5);
        $this->SetFont('Verdana','B',7);
        //Header
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],$theight,$header[$i],"B",0,'C',1);
        $this->Ln();
        //Color and font restoration
        if($color)
            $this->SetFillColor(217,227,238);
        else
            $this->SetFillColor(245);
        $this->SetTextColor(0);
        $this->SetFont('Verdana','',7);
        if(count($data)){
            //Data
            $fill=0;
            $this->SetLineWidth(.1);

            for($z=0; $z<(count($data));$z++){
                $box = "T";
                $wt = 0;
                foreach($data[$z][0] as $value){
                if(is_numeric($value))
                    $this->Cell($w[$wt++],$height,$value,$box,0,'C',$fill);
                else
                    $this->Cell($w[$wt++],$height,$value,$box,0,'C',$fill);
                }
                $this->Ln();
                $this->Cell(0,$height,$data[$z][1],"B",0,'C',$fill);
                $this->Ln();

                $fill=!$fill;
            }
        }
        if($color){
            $this->SetFillColor(128,160,198);
            $this->SetDrawColor(128,160,198);
            $this->SetTextColor(255);
        } else {
            $this->SetFillColor(255);
            $this->SetDrawColor(0);
            $this->SetTextColor(0);
        }
        $this->SetLineWidth(1);
        $this->SetFont('Verdana','B',7);
        for($i=0;$i<count($footer);$i++)
            $this->Cell($w[$i],$theight,$footer[$i],"TB",0,'C',1);

        $this->SetFillColor(246,246,246);
        $this->SetTextColor(0);
    }

    function Footer()
    {
        $this->SetY(-18);
        $this->SetTextColor(150);
        $this->SetFont('Arial','I',6);
        $this->Cell(0,18,'Page '.$this->PageNo().' - '.$this->page_footer,0,0,'C');
    }

    function AddPage($orientation='', $format='')
    {
        parent::AddPage($orientation, $format);

        /* SANDBOX WATERMARK */
        if(SANDBOX) {
            if ($this->DefOrientation === 'P') {
                $w = 612;
                $h = 792;
            } else {
                $w = 792;
                $h = 612;
            }

            $size = 612 - 200; // PORTRAIT WIDTH - 50mm

            $this->Image('images/watermark.jpg', ($w / 2) - ($size / 2), ($h / 2) - ($size / 2), $size, $size);
        }
        /* SANDBOX WATERMARK */

    }

}
?>