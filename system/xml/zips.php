<?php
	session_start();
	require("../config.php");
	include("../classes/system.inc.php");
	$user = unserialize($_SESSION[$pfix."user"]);
	User::authorize($user);

	$link = DB::connect();

	header("Content-type: text/xml");
	$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

	$id_pais = $idp ? $idp : $id_pais;

	$tbl_carriers  = $id_pais == 225 ? 'zips_carriers' : 'zipsmx_carriers';
	$tbl_zips      = $id_pais == 225 ? 'zips' : 'zipsmx';
	$tbl_estado    = $id_pais == 225 ? 'estados' : 'estadosmx';
	$tbl_munis     = $id_pais == 225 ? 'ciudades' : 'municipios';
	$field         = $id_pais == 225 ? 'id_ciudad' : 'id_muni';
	$title_field   = $id_pais == 225 ? '' : 'municipio';

	$val = $ide ? $ide : strtolower($val);
	$str = "select $title_field ciudad, estado, zip, comentarios, $tbl_zips.id from $tbl_zips inner join $tbl_estado on $tbl_zips.id_estado = $tbl_estado.id inner join $tbl_munis on $tbl_zips.$field = $tbl_munis.id and $tbl_estado.id = $tbl_munis.id_estado where " . ($ide ? ("$tbl_zips.id_estado = '$val' and $tbl_zips.$field = '$idm';") : ("zip = '$val';"));
	$res = mysql_query($str, $link);

	if (!$ide) {
		$str = "select count(*), $tbl_zips.zip from $tbl_carriers left join $tbl_zips on ($tbl_carriers.id_zip = $tbl_zips.id) where $tbl_zips.zip = $val group by $tbl_zips.zip";
		$res2 = mysql_query($str, $link);
		$zips_carriers = mysql_result($res2, 0, 0);
	}
	$xml .= "<records>";
	while($row = mysql_fetch_assoc($res)) {
		$xml .= "<record><id>".$row["id"]."</id><zip>".$row["zip"]."</zip><ciudad>".$row["ciudad"]."</ciudad><estado>".$row["estado"]."</estado><comentarios>".$row["comentarios"]."</comentarios><zips_carriers>$zips_carriers</zips_carriers></record>";
	}
	$xml .= "</records>";

	print $xml;

?>