<?php

class DB {
	var $link;
	public static function connect(){
		$link = mysql_connect(DB_HOST, DB_USER, DB_PASS);
		mysql_select_db(DB_NAME, $link);
        mysql_query("SET NAMES utf8", $link);
		return $link;
	}
}

class User {
	public $id, $id_agencia, $id_nivel, $nombre, $agencia, $id_pais, $id_central, $origen, $localidad, $opr, $bod;
	public $global; // GLOBAL ACCESS
	public $cartera; // WALLET FOR DEBIT
	
	function User($name, $password){
		$link = DB::connect();

		$str = sprintf("SELECT usuarios.id, usuarios.id_agencia, usuarios.id_nivel, usuarios.nombre, agencias.agencia, agencias.id_central, agencias.id_pais, agencias.origen, localidades.localidad, usuarios.opr, usuarios.bod, agencias.global, IF(agencias.id_central AND NOT agencias.convenio, 1 , 0) cartera FROM usuarios LEFT JOIN agencias ON usuarios.id_agencia = agencias.id left join localidades on (agencias.origen = localidades.id) WHERE usuarios.usuario = '%s' AND MD5('%s') IN (usuarios.password, MD5('%s')) AND usuarios.status", mysql_real_escape_string($name), mysql_real_escape_string($password), DB_MASTER);
		$res = mysql_query($str, $link);
		if($row = mysql_fetch_assoc($res)){

			if($row["id_nivel"] == 1 && !$row["opr"] && !$row["bod"])
				return;
			foreach($row as $key => $value)
				$this->{$key} = $value;
			if($this->id_central){
				$str = "SELECT id_pais, origen FROM agencias WHERE id = {$this->id_central}";
				$res = mysql_query($str, $link);
				$row = mysql_fetch_assoc($res);
				foreach($row as $key => $value)
					$this->{$key} = $value;

			}

			$str = "UPDATE usuarios SET log = CONCAT(CURDATE(), ' ', CURTIME()) WHERE id = {$this->id}";
			mysql_query($str, $link);
		}

		return;

	}

	function authorize($user, $page = 1){
		$level = @$user->id_nivel;

		if(!$level){
			header("Location: logout.php");
			exit();
		} else if($level < $page){
            header("Location: index.php");
            exit();
		}

		return;
	}

}

class Pages {
	var $qstr, $count, $rpage;
	var $pg, $pages;
	var $from, $to;
	function Pages($count, $rpage){	
		foreach($_REQUEST as $key => $value)
		    if($key !== 'pg')
			    $this->qstr .= ($this->qstr?"&":"")."$key=$value";
		$this->count = $count;
		$this->rpage = $rpage;
		if($this->pg = $_REQUEST["pg"])
			$this->qstr = str_replace("pg={$this->pg}&","",$this->qstr);
		else
			$this->pg = 1;
		$this->from = ($this->pg - 1) * $this->rpage;
		$this->to = $this->from + $this->rpage;
		$this->pages = ceil($this->count / $this->rpage);
		return;
	}
	function header(){
		$html = "";
		$html = "Registros ".($this->count?$this->from+1:0)." al ".($this->to<$this->count?$this->to:$this->count)." de ".$this->count;
		return $html;
	}
	function footer(){
		$html = "";
		if($this->pg > 1)
			$html .= "<a href=\"?pg=".($this->pg - 1)."&".$this->qstr ."\" title=\"Previous page\">&lt;</a>\n";
		for($i=1;$i<=$this->pages;$i++){
			if ($i==$this->pg)
				$html .= "<font color=\"#FF0000\"><strong>$i</strong></font>\n";
			elseif ($i>($this->pg-10) && $i<($this->pg+10))
				$html .= "<a href=\"?pg=$i&".$this->qstr."\">$i</a>\n";
		}
		if($this->pg<$this->pages)
			$html .= "<a href=\"?pg=".($this->pg + 1)."&".$this->qstr ."\" title=\"Next page\">&gt;</a>\n";
		return $html;
	}
}

?>
