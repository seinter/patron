<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>

    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>

    <script type="text/javascript" src="popcalendar.js"></script>
    <script type="text/javascript">

        function afterClose(obj){
            return;
        }

        function closeLightBox(){
            window.parent.$.fancybox.close();
        }

    </script>

    <script type="text/javascript">
        $(function(){
            $('select').select2()

            $('.date-field input').click(function() {
                showCalendar(this, this, 'yyyy-mm-dd','es',1)
            })

            $('.cancel').click(function(){
                this.form.reset()
                closeLightBox()
            })

            $('.save').click(closeLightBox)
        })
    </script>

    <style type="text/css">
        <!--
        table {
            border-collapse: initial;
        }
        table caption {
            caption-side: top;
        }

        .date {
            margin-bottom: 5px;
        }

        @media (min-width: 576px) {
            .date {
                margin-bottom: 0;
                padding-right: 0;
            }
            .hour {
                padding-left: 5px;
            }
        }
        -->
    </style>

</head>

<body class="modal-container">

<?php
/* SANDBOX WATERMARK */
if(SANDBOX && basename($_SERVER['PHP_SELF']) !== 'viewer.php'): ?>
    <script type="text/javascript">
        var watermark = document.createElement("img");
        watermark.setAttribute('src', 'images/watermark.png');
        watermark.setAttribute('class', 'watermark');
        document.body.appendChild(watermark);
    </script>
<?php endif ?>

<div class="container-fluid">
    <div class="row main-title">
        <div class="col text-right text-truncate caption">AGENDA PRE-VENTA</div>
    </div>
</div>

<table width="100%" border="0" cellpadding="0" cellspacing="10">

    <tr>
        <td>
            <form id="schedule" onsubmit="return false">

                <table class="data-table form-fields form-footer">
                    <caption>
                        <div class="row">
                            <div class="col-6">ENTREGA</div>
                            <div class="col-6">RECEPCION</div>
                        </div>
                    </caption>
                    <tr>
                        <th width="50%">Fecha / Hora:</th>
                        <th>Fecha / Hora:</th>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-12 col-sm-6 date">
                                    <div class="date-field">
                                        <input name="f1" type="text" id="f1" value="<?php echo $f1 ?>" readonly="readonly" />
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <div class="col-6 col-sm-3 hour" style="padding-right:0">
                                    <select name="hrs1" id="hrs1" style="width:100%">
                                        <?php for($i=0;$i<=23;$i++){ ?>
                                            <option value="<?php echo $i ?>" <?php echo $i === +$hrs1 ? 'selected' : '' ?> ><?php echo strlen($i)==1?"0$i":$i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-6 col-sm-3 minute" style="padding-left:5px">
                                    <select name="min1" id="min1" style="width:100%">
                                        <?php for($i=0;$i<60;$i+=15){ ?>
                                            <option value="<?php echo $i ?>" <?php echo $i === +$min1 ? 'selected' : '' ?> ><?php echo strlen($i)==1?"0$i":$i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-12 col-sm-6 date">
                                    <div class="date-field">
                                        <input name="f2" type="text" id="f2" value="<?php echo $f2 ?>" readonly="readonly" />
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <div class="col-6 col-sm-3 hour" style="padding-right:0">
                                    <select name="hrs2" id="hrs2" style="width:100%">
                                        <?php for($i=0;$i<=23;$i++){ ?>
                                            <option value="<?php echo $i ?>" <?php echo $i === +$hrs2 ? 'selected' : '' ?> ><?php echo strlen($i)==1?"0$i":$i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-6 col-sm-3 minute" style="padding-left:5px">
                                    <select name="min2" id="min2" style="width:100%">
                                        <?php for($i=0;$i<60;$i+=15){ ?>
                                            <option value="<?php echo $i ?>" <?php echo $i === +$min2 ? 'selected' : '' ?> ><?php echo strlen($i)==1?"0$i":$i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Obervaciones:</th>
                        <th>Obervaciones:</th>
                    </tr>
                    <tr>
                        <td>
                            <textarea name="observ1" id="observ1" style="width:100%; height:100px;"><?php echo $observ1 ?></textarea>
                        </td>
                        <td>
                            <textarea name="observ2" id="observ2" style="width:100%; height:100px;"><?php echo $observ2 ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center">
                            <div class="d-none d-sm-block">
                                <input name="button" type="button" class="btn btn-light cancel" value="&lt;&lt;&lt; Cancelar" />
                                <input name="button" type="button" class="btn btn-primary save" value="Aceptar &gt;&gt;&gt;" />
                            </div>
                            <div class="d-sm-none">
                                <input name="button" type="button" class="btn btn-light w-100 cancel" value="&lt;&lt;&lt; Cancelar" />
                                <input name="button" type="button" class="btn btn-primary w-100 save" value="Aceptar &gt;&gt;&gt;" style="margin-top:5px" />
                            </div>
                        </td>
                    </tr>
                </table>

            </form>
        </td>
    </tr>
</table>



<div class="container-fluid" style="position:fixed;bottom:0;">
    <div class="row main-footer">
        <div class="col copyright"><?php echo $copyright ?></div>
    </div>
</div>

</body>
</html>