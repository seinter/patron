<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require_once("pdf/fpdf.php");

include('classes/phpqrcode.php');
//include("barcode/barcode.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$pdf = new FPDF('P','in','Letter');
$pdf->AddPage();

$str = "select distinct ";
	if ($estado != "-1") {
		 $str .= "historial.id_accion, historial.id_estado, ";
	}
	$str .= "ventas.id_cliente, ventas.id_dest, ventas.id id_venta, bitacora.id no_guia, ventas.recepcion, concat(clientes.nombre, ' ', clientes.apellido) c_nombre, clientes.direccion c_dir, clientes.telefono c_tel, clientes.zip c_zip, destinatarios.nombre d_nombre, destinatarios.telefono d_tel, destinatarios.direccion d_dir, destinatarios.zip d_zip, historial.peso_v, clientes.id_pais c_id_pais, destinatarios.id_pais c_id_pais
		from ventas 
		left join detalle_venta on (ventas.id = detalle_venta.id_venta) 
		left join bitacora on (detalle_venta.id = bitacora.id_detalle) 
		left join clientes on (ventas.id_cliente = clientes.id)
		left join destinatarios on (ventas.id_dest = destinatarios.id)
		left join historial on (bitacora.id = historial.id_bitacora)
		WHERE historial.id_accion = $ida and historial.status and historial.activo";
$res_array = mysql_query($str, $link);

$x = .25;
$y = 0;

$pdf->SetFont('Helvetica','B',13);
$y += .25;
@$pdf->SetXY($x,$y);
if ($estado == '-1') {
	$estado_txt = 'Todos los estados';
} elseif ($estado == '1') {
	$estado_txt = 'No confirmados';
} else {
	$estado_txt = 'Confirmados';
}
$pdf->Write(.35, strtoupper('REPORTE DE ENTREGAS - ' . $estado_txt));
$y += .35;

$write_rows_count = 1;
if (mysql_num_rows($res_array)) {
	while ($fields = mysql_fetch_array($res_array, MYSQL_ASSOC)) {
		// echo '<pre>'; print_r($fields); echo '</pre>';
		if ($write_rows_count == 1) {
			$y = .85;
		} else {
			$y = ($write_rows_count * .20) + 0.8;
		}
		$y_top = $y;
		$y_top_2 = $y;
		$x = .25;

		$pdf->SetFont('Helvetica','B',8);
		$pdf->SetXY($x,$y);
		$pdf->Write(.35, 'Guia:');

		$pdf->SetFont('Helvetica','',8);
		$pdf->SetXY($x+.60,$y);
		$pdf->Write(.35, '#' . $fields['no_guia']);

		$write_rows_count++;

		if ($fields['peso_v']) {
			/*Peso*/
			$pdf->SetFont('Helvetica','B',8);
			$y += .20;
			$pdf->SetXY($x,$y);
			$pdf->Write(.35, 'Peso:');

	        $peso_v = explode(',', $fields['peso_v']);

			$pdf->SetFont('Helvetica','',8);
			$pdf->SetXY($x+.60,$y);
			$pdf->Write(.35, number_format($peso_v[3],2) . ' kl.');

			/*Medidas*/
			$pdf->SetFont('Helvetica','B',8);
			$y += .20;
			$pdf->SetXY($x,$y);
			$pdf->Write(.35, 'Medidas:');

			$pdf->SetFont('Helvetica','',8);
			$pdf->SetXY($x+.60,$y);
			$pdf->Write(.35, 'Lado: ' . number_format($peso_v[0],2) . ' cms.');
			$pdf->SetXY($x+.60,$y+.20);
			$pdf->Write(.35, 'Ancho: ' . number_format($peso_v[1],2) . ' cms.');
			$pdf->SetXY($x+.60,$y+.40);
			$pdf->Write(.35, 'Largo: ' . number_format($peso_v[2],2) . ' cms.');
		}

		/*Columna 2*/
			$x += 2;
			$pdf->SetFont('Helvetica','B',8);
			$pdf->SetXY($x,$y_top);
			$pdf->Write(.35, 'Remitente');
			$write_rows_count++;

			$y_top += .20;
			$pdf->SetFont('Helvetica','',8);
			$pdf->SetXY($x,$y_top);
			$pdf->Write(.35, $fields['c_nombre']);
			$write_rows_count++;

			$pdf->SetFont('Helvetica','',8);
			$c_dir = explode(' ', $fields['c_dir']);
			$c_dir_line = 1;
			$c_dir_count = 1;
			$c_dir_text = '';

			// echo '<pre>'; print_r($c_dir); echo '</pre>'; exit();
			$c_dir_txt = '';
			for ($key = 0; $key <= (count($c_dir)-1); $key++) {
				$c_dir_count++;
				if ($c_dir_count == 7) {
					$pdf->SetXY($x,$y_top+($c_dir_line * .20));
					$pdf->Write(.35, $c_dir_txt);
					$c_dir_txt = @$c_dir[$key] . ' ';
					$write_rows_count++;
					$c_dir_count = 1;
					$c_dir_line++;
				} else {
					$c_dir_txt .= @$c_dir[$key] . ' ';
				}
			}

			if ($c_dir_count < 7) {
				$pdf->SetXY($x,$y_top+($c_dir_line * .20));
				$pdf->Write(.35, $c_dir_txt);
				$write_rows_count++;
			}

			$zip = $fields['c_zip'];
			if($fields['c_id_pais']==225)
				$str = "select ciudad, estado from zips inner join ciudades on zips.id_ciudad = ciudades.id inner join estados on zips.id_estado = estados.id where zip = '$zip';";
			else
				$str = "select municipio ciudad, estado from zipsmx inner join estadosmx on zipsmx.id_estado = estadosmx.id inner join municipios on zipsmx.id_muni = municipios.id and estadosmx.id = municipios.id_estado where zip = '$zip';";
			$res = mysql_query($str, $link);
			$cliente = mysql_fetch_assoc($res);

			if ($cliente['ciudad'] && $cliente['ciudad']) {
				$pdf->SetFont('Helvetica','',8);
				$pdf->SetXY($x,$y_top);
				$pdf->Write(.35, 'CP ' . $fields['c_zip']);
				$write_rows_count++;

				$pdf->SetFont('Helvetica','',8);
				$pdf->SetXY($x,$y_top);
				$pdf->Write(.35, $cliente['ciudad'] . ', ' . $cliente['ciudad']);
			} else {
				$y_top += ($c_dir_line * .20) + .40;
				$write_rows_count++;
				$write_rows_count++;
			}

			$y_top += .20;
			$pdf->SetFont('Helvetica','',8);
			$pdf->SetXY($x,$y_top);
			$pdf->Write(.35, 'Tel.' . $fields['c_tel']);
			$write_rows_count++;
		/*Columna 2*/

		/*Columna 3*/
			$x += 3;
			$pdf->SetFont('Helvetica','B',8);
			$pdf->SetXY($x,$y_top_2);
			$pdf->Write(.35, 'Destinatario');

			$y_top_2 += .20;
			$pdf->SetFont('Helvetica','',8);
			$pdf->SetXY($x,$y_top_2);
			$pdf->Write(.35, $fields['d_nombre']);

			$pdf->SetFont('Helvetica','',8);
			$c_dir = explode(' ', $fields['d_dir']);
			$c_dir_line = 1;
			$c_dir_count = 1;
			$c_dir_text = '';

			// echo '<pre>'; print_r($c_dir); echo '</pre>'; exit();
			$c_dir_txt = '';
			for ($key = 0; $key <= (count($c_dir)-1); $key++) {
				$c_dir_count++;
				if ($c_dir_count == 7) {
					$pdf->SetXY($x,$y_top_2+($c_dir_line * .20));
					$pdf->Write(.35, $c_dir_txt);
					$c_dir_txt = @$c_dir[$key] . ' ';
					$c_dir_count = 1;
					$c_dir_line++;
				} else {
					$c_dir_txt .= @$c_dir[$key] . ' ';
				}
			}

			if ($c_dir_count < 7) {
				$pdf->SetXY($x,$y_top_2+($c_dir_line * .20));
				$pdf->Write(.35, $c_dir_txt);
			}

			$zip = $fields['d_zip'];
			if($fields['d_id_pais']==225)
				$str = "select ciudad, estado from zips inner join ciudades on zips.id_ciudad = ciudades.id inner join estados on zips.id_estado = estados.id where zip = '$zip';";
			else
				$str = "select municipio ciudad, estado from zipsmx inner join estadosmx on zipsmx.id_estado = estadosmx.id inner join municipios on zipsmx.id_muni = municipios.id and estadosmx.id = municipios.id_estado where zip = '$zip';";
			$res = mysql_query($str, $link);
			$cliente = mysql_fetch_assoc($res);

			if ($cliente['ciudad'] && $cliente['ciudad']) {
				$y_top_2 += ($c_dir_line * .20) + .20;
				$pdf->SetFont('Helvetica','',8);
				$pdf->SetXY($x,$y_top_2);
				$pdf->Write(.35, 'CP ' . $fields['d_zip']);

				$y_top_2 += .20;
				$pdf->SetFont('Helvetica','',8);
				$pdf->SetXY($x,$y_top_2);
				$pdf->Write(.35, $cliente['ciudad'] . ', ' . $cliente['ciudad']);
			} else {
				$y_top_2 += ($c_dir_line * .20) + .40;
			}

			$y_top_2 += .20;
			$pdf->SetFont('Helvetica','',8);
			$pdf->SetXY($x,$y_top_2);
			$pdf->Write(.35, 'Tel.' . $fields['d_tel']);
		/*Columna 3*/
	}
	 // exit();	
} else {
	$y += .35;
	$pdf->SetXY($x,$y);
	$pdf->Write(.35, strtoupper('No hay registros para mostrar.'));
}
// }

$pdf->Output();

?>