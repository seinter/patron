<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 2);

$link = DB::connect();

$pdf = new PDF("L");
$pdf->page_footer = $pdf_footer;

$str = "SELECT historial.*, acciones.accion, paises.pais, agencias.agencia, tipo_estados.tipo estado, tipo_salidas.tipo, programacion.descripcion FROM historial INNER JOIN acciones ON historial.id_accion = acciones.id INNER JOIN usuarios ON historial.id_usuario = usuarios.id INNER JOIN agencias ON usuarios.id_agencia = agencias.id INNER JOIN tipo_estados ON historial.id_estado = tipo_estados.id LEFT JOIN agencias central ON agencias.id_central = central.id LEFT JOIN paises ON IF(central.id IS NULL, agencias.id_pais, central.id_pais) = paises.id LEFT JOIN programacion ON historial.id_prog = programacion.id LEFT JOIN tipo_salidas ON programacion.id_tipo = tipo_salidas.id WHERE historial.id_bitacora = {$id_bitacora} AND historial.status ORDER BY historial.fecha, hora;";

$res2 = mysql_query($str, $link);
if(mysql_num_rows($res2)){
	$pdf->title = array("REPORTE DE HISTORIAL DE ACCIONES", 'No. Guia: ' . formatCode($id_bitacora));
	$pdf->AddPage();
	while($row = mysql_fetch_object($res2)){

		$detalle = '';
        switch($row->id_accion){
            case 3:
                $detalle = $row->id_prog ? formatCode($row->id_prog) . ": " . $row->descripcion : "";
                break;
            case 5:
                $str = "select entregas.id, id_carrier, carrier, carriers.website, guia, nombre, documento, if(foto is not null,1,0) ifoto from entregas left join carriers on entregas.id_carrier = carriers.id where id_bitacora = $idb and entregas.status;";
                $res2 = mysql_query($str, $link);
                $row2 = mysql_fetch_object($res2);
                $detalle = $row->id_carrier ? ($row2->carrier .  ': ' . $row2->guia ) : 'Destinatario: Nombre: ' . $row2->nombre . ' - ID: ' . $row2->documento;
                break;
            case 4:
                $peso_volumetrico = array('Lado', 'Ancho', 'Largo', 'Cálculo');
                $peso_v = explode(',', $peso_v);
                $peso_v_text      = '';
                foreach ($peso_volumetrico as $key => $value) {
                    if ($key < (count($peso_v) - 1)) {
                        $peso_v_text .= $value . ': ' . $peso_v[$key] . '" - ';
                    } else {
                        $peso_v_text .= $value . ': ' . $peso_v[$key];
                    }
                }
                $detalle = $row->id_estado ? $row->estado  : $row->estado . ' - ' . $motivo . ' - Peso volumetrico: ' . $peso_v_text . ' kl.';
                break;
            case 6:
                $detalle = 'Envío entregado.';
            break;
        }

		$data[] = array($row->fecha, $row->hora, $row->pais . ' / ' . $row->agencia, $row->accion, $detalle);
	}
	$header = array("FECHA", "HORA", "UBICACION", "ACTIVIDAD", "DETALLE");
	$pdf->ImprovedTable($header, $data, $footer, $c);
}

$pdf->Output();

?>