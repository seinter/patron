<?php
require_once("config.php");
include("classes/system.inc.php");
include("functions.php");
$link = DB::connect();

$send_email = false;
if (($_REQUEST['dll'] == 2) && ($_REQUEST['send'] == 1)) {
	$str = "select clientes.email, concat(clientes.nombre, ' ', clientes.apellido) from pagos left join ventas on (pagos.id_venta = ventas.id) left join clientes on (ventas.id_cliente = clientes.id)  where md5(pagos.id) = '" . $id ."';";
	$res = mysql_query($str, $link);
	$email  = mysql_result($res,0,0);
	$nombre = mysql_result($res,0,1);
    $from = 'ventas@patronenvios.net';
    $to = $email;
    $subject = 'Recibo de pago';
    $message = '<style type="text/css"> body { font-family: Helvetica, Verdana, Arial, sans-serif; } </style> <div style="height: 35px; background: #00205d;"></div> <div style="width: 700px; margin: 0 auto; text-align: center; color: #000; font-size: 14px; padding: 15px 0; border-bottom: 15px solid #00205d;"> <img src="https://patronenvios.net/demo/assets/img/logo.jpg" width="150" /> <br> <p>Es un gusto saludarte, te agradecemos por tu compra y preferencia.</p> <p>Para descargar el recibo de la venta, debes realizarlo hacienco click en el siguiente enlace: <p><a style="display: inline-block; width: 40%; padding: 15px; font-size: 20px; background: #00205d; color: #fff; text-decoration: none;" href="https://patronenvios.net/demo/recibo.php?id=' . $id . '" download target="_blank">DESCARGAR BOLETO</a></p> <h1 style="color: #00205d;">Gracias por tu compra</h1> </div>';

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: '.$from."\r\n".
        'X-Mailer: PHP/' . phpversion();

        // echo $message; exit();

    if (mail($to, $subject, $message, $headers)) {
    	$send_email = true;
    }
}

switch($_REQUEST['dll']){
	case 1:
		$src = "rutas_pdf.php?id=$id";
	break;
	case 2:
		$src = "recibo.php?id=$id";
	break;
	case 3:
	//REPORTE DE PAGOS
		$src = "rep_pagos_pdf.php?f1=$f1&f2=$f2&ida=$ida&c=$c";
	break;
	case 4:
	//REPORTE DE VENTAS
		$src = "rep_ventas_pdf.php?f1=$f1&f2=$f2&ida=$ida&c=$c";
	break;
	case 8:
		$src = "rep_upagos_pdf.php?id=$id&f1=$f1&c=$c";
	break;
	case 10:
	//REPORTE GOBAL DE PAGOS
		$src = "rep_pagos_global_pdf.php?f1=$f1&f2=$f2&ida=$ida&c=$c";
	break;
	case 11:
	//REPORTE GOBAL DE VENTAS
		$src = "rep_ventas_global_pdf.php?f1=$f1&f2=$f2&ida=$ida&c=$c";
	break;
	case 12:
	//ESTADO DE CUENTA DEL CLIENTE
		$src = "estado_de_cta.php?id=$id";
	break;
	case 13:
	//GUIA DE RECEPCION
		$src = "guia_recepcion.php?id=$id";
	break;
	case 14:
	//GUIA DE ENTREGA
		$src = "guia_entrega.php?id=$id";
	break;
	case 15:
	//ETIQUETAS
		$src = "etiquetas.php?id=$id";
	break;
	case 16:
	//MANIFIESTO
		$src = "manifiesto_pdf.php?id=$id";
	break;
	case 17:
	//GUIAS
		$src = "guias.php?idp=$id";
	break;
	case 18:
		$src = "comprobante.php?id=$id";
	break;
	case 19:
		$src = "itinerario.php?f1=$f1";
	break;
	case 20:
		//REPORTE SEMANAL DE COMISIONES GLOBAL
        	list($f1, $f2) = explode(' / ', $_POST['f']);
		$src = "rep_comisiones_global_pdf.php?ida=" . $_POST['ida'] . "&f1={$f1}&f2={$f2}";
	break;
	case 21:
		//REPORTE SEMANAL DE COMISIONES
        	list($f1, $f2) = explode(' / ', $_POST['f']);
		$src = "rep_comisiones_pdf.php?ida=" . $_POST['ida'] . "&f1={$f1}&f2={$f2}";
	break;
	case 22:
		//ETIQUETAS DE CONFIRMACIÓN
		$src = "etiqueta_de_confirmacion.php?id_dest=$id_dest&id_venta=$id_venta";
	break;
	case 23:
		$src = "rep_guias_por_accion_pdf.php?ida=$ida";
	break;
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">


    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/js/functions.js"></script>

    <style>
        html, body {
            height: 100%;
        }
        .flex-grow {
            flex: 1 0 auto;
        }
    </style>

</head>

<body>
<div class="d-flex flex-column h-100">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col text-center text-sm-left"><img src="assets/img/logo.jpg" width="90" style="margin:5px;"></div>
                    <div class="col d-none d-sm-block text-right">
                        <div style="margin:10px;"><span><?php echo $title ?></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($_REQUEST['dll'] == 2): ?>
    	<div class="row">
		    <div class="col-sm-center col-12 recibo-email-button">
				<button type="button" class="btn btn-sm btn-outline-danger" onclick="window.location='viewer.php?dll=2&id=<?php echo $id; ?>&send=1';"><i class="fa fa-file-pdf-o" style="margin-right:5px"></i> Enviar recibo por correo electrónico</button>
				<?php if ($send_email): ?>
					<h4>El recibo ha sido enviado</h4>
				<?php endif; ?>
		    </div>
		</div>
	<?php endif; ?>

    <div class="d-flex flex-column flex-grow">
        <iframe class="h-100 flex-grow" width="100%" frameborder="0" src="<?php echo $src ?>"></iframe>
    </div>

    <div class="container-fluid" style="position:fixed; bottom:0;">
        <div class="row main-footer" style="height:65px">
            <div class="col copyright"><?php echo $copyright ?></div>
        </div>
    </div>

</div>
</body>
</html>