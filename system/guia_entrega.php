<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require_once("pdf/fpdi.php");

include('classes/phpqrcode.php');
//include("barcode/barcode.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

class FPDI_EXT extends FPDI {
	function Footer() {
		$this->SetY(-20);
		$this->SetTextColor(150);
		$this->SetFont('Arial','I',6);
		$this->Cell(0,20,'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
	}
}

$pdf = new FPDI_EXT("P","mm","Letter");
$pdf->AliasNbPages();

$pdf->setSourceFile("pdf/entrega.pdf");  
$tplIdx = $pdf->importPage(1);

$str = "select bitacora.id, bitacora.codigo, id_venta, id_cliente, id_dest, local1.localidad origen, local2.localidad destino, agencia, agencias.direccion, agencias.telefono, clase, IF(productos.id, producto, detalle_venta.descripcion) producto, detalle_venta.cantidad from ventas inner join usuarios on ventas.id_usuario = usuarios.id inner join agencias on usuarios.id_agencia = agencias.id inner join detalle_venta on ventas.id = detalle_venta.id_venta inner join clasificacion on detalle_venta.id_clase = clasificacion.id inner join bitacora on detalle_venta.id = bitacora.id_detalle inner join localidades local1 on ventas.origen = local1.id inner join localidades local2 on ventas.destino = local2.id left join productos on detalle_venta.id_producto = productos.id where md5(bitacora.id) = '$id';";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$$key = $value;

$str = "select nombre, apellido, telefono, direccion, id_pais, id_depto, zip, paises.pais from clientes inner join paises on clientes.id_pais = paises.id where clientes.id = $id_cliente;";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$customer[$key] = $value;

switch($customer["id_pais"]){
case 225:
	$str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$customer[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$customer["direccion2"] = $row[0].", ".$row[1]." ".$customer["zip"];
break;
case 136:
	$str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$customer[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$customer["direccion2"] = $row[0].", ".$row[1] . ' ' . $customer['zip'];
break;
default:
	$str = "select departamento from departamentos where id = {$customer[id_depto]};";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$customer["direccion2"] = $row[0];
break;
}

$str = "select nombre, telefono, direccion, id_pais, id_depto, zip, paises.pais, comentarios from destinatarios inner join paises on destinatarios.id_pais = paises.id where destinatarios.id = $id_dest;";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$dest[$key] = $value;

switch($dest["id_pais"]){
case 225:
	$str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$dest[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0].", ".$row[1]." ".$dest["zip"];
break;
case 136:
	$str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$dest[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0].", ".$row[1] . ' ' . $dest['zip'];
break;
default:
	$str = "select departamento from departamentos where id = {$dest[id_depto]};";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0];
break;
}

$str = "select cantidad, descripcion, monto from contenidos where id_bitacora = $id and status order by id;";
$res = mysql_query($str, $link);

while($row = mysql_fetch_row($res))
	$conts[] = array("cnt"=>$row[0],"desc"=>$row[1],"mnt"=>$row[2]);

if($conts)
	$pages = array_chunk($conts,20,true);

do {
	
	//CONTEO DEL DETALLE Y CALCULO DE PAGINAS
	$pdf->AddPage();
	$pdf->useTemplate($tplIdx,0,0); 
	
	$pdf->SetFont('Arial','B',12);
	
	$x = 165;
	$y = 29;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,formatCode($id_venta));

	$x = 165;
	$y = 36;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,formatCode($id));


	$pdf->SetFont('Arial','',8);
	$x = 125;
	$y += 6;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,"Fecha: ".date("Y-m-d"));
	 
	$pdf->SetFont('Arial','B',9);
	
	$x = 53;
	$x2 = 133;
	$y = 51.2;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,formatClientCode($id_cliente));
	
	
	$pdf->SetFont('Arial','',8);
	
	$y += 5;
	$pdf->SetXY($x,$y);
	$pdf->Write(6, utf8_decode($customer["nombre"]." ".$customer["apellido"]));
	$pdf->SetXY($x2,$y);
	$pdf->Write(6, utf8_decode($dest["nombre"]));
	
	$y += 5;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,$customer["telefono"]);
	$pdf->SetXY($x2,$y);
	$pdf->Write(6,$dest["telefono"]);

	$pdf->SetFont('Arial','',7);
	
	$y += 6;
	$pdf->SetXY($x,$y);
	$pdf->MultiCell(60,3.1, utf8_decode(ucwords(strtolower($customer["direccion"]."\n".$customer["direccion2"]))),0,'L');
	$pdf->SetXY($x2,$y);
	$pdf->MultiCell(60,3.1, utf8_decode(ucwords(strtolower($dest["direccion"]."\n".$dest["direccion2"]))),0,'L');
	
	$pdf->SetFont('Arial','B',8);
	
	$x = 108;
	$y = 76.6;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,strtoupper($origen." - ".$destino));
	
	$pdf->SetFont('Arial','',8);
	
	$x = 30;
	$y = 94;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,strtoupper($clase." - ".$producto));
	$x = 173;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,$cantidad);
	
	
	$pdf->SetFont('Arial','I',8);
	
	$x = 27;
	$y = 239.5;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,$agencia);
	
	$y += 4;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,str_replace("\r\n",' ',$direccion));
	$y += 4;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,"Telefono: ".$telefono);
	
	
	$x = 132;
	$y = 184;

	/*
	 * OLD CODE39 BARCODE
	$barcode = formatBarCode($id);
	create_barcode($barcode);
	$pdf->Image('tmp/'.$barcode.'.png', $x, $y, 70, 15); 
	*/
    QRCode::png($codigo, 'tmp/'.$codigo.'.png', QR_ECLEVEL_H, 10, 2);
    $pdf->Image('tmp/'.$codigo.'.png', $x, $y, 25, 25);

    $pdf->SetFont('Helvetica','B',10);
    $pdf->SetXY($x + 1.5, $y + 24);
    $pdf->Write(5, $codigo);


    if(count($pages)){
		$detail = array_shift($pages);
		//====== DETALLE DE CONTENIDOS ======
		$pdf->SetFont('Arial','',7);
		
		$x = 34;
		$y = 102;
		$pdf->SetXY($x,$y);
		
		$h = 3.8;
		
		$pdf->SetDrawColor(100);
		$pdf->SetFillColor(240);
		$pdf->SetLineWidth(.05) ;
		$pdf->Cell(10,$h,"ITEM",1,0,'C',1);
		$pdf->Cell(15,$h,"CANT.",1,0,'C',1);
		$pdf->Cell(75,$h,"DESCRIPCION",1,0,'C',1);
		$pdf->Cell(20,$h,"MONTO USD",1,1,'C',1);
		
		$pdf->SetFillColor(250);

		foreach($detail as $key => $value){
			$pdf->SetX($x);
			$pdf->Cell(10,$h,($key + 1).".",1,0,'R',1);
			$pdf->Cell(15,$h,$value["cnt"],1,0,'R',0);
			$pdf->Cell(75,$h,$value["desc"],1,0,'L',0);
			$pdf->Cell(20,$h,numFormat($value["mnt"]),1,1,'R',0);
		}
		//====================================
	}
	
	$x = 34;
	$y = 188;
	$pdf->SetXY($x,$y);
	$pdf->SetFont('Arial','',7);

	$pdf->MultiCell(90,3,"OBSERVACIONES: ".substr($dest["comentarios"],0,300),0);

} while(count($pages));

$pdf->Output();
unlink('tmp/'.$codigo.'.png');

?>