<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 3);

$link = DB::connect();

switch($_REQUEST['dll']){
    case 1:
        $origen = $_POST['origen'] ?: 0;
        $global = $_POST['global'] ?: 0;
        if($_POST['id'])
            $str = "UPDATE agencias SET id_pais = " . $_POST['id_pais'] . ", agencia = '" . mysql_real_escape_string($_POST['agencia']) . "', codigo = '" . $_POST['codigo'] . "', direccion = '" . mysql_real_escape_string($_POST['direccion']) . "', telefono = '" . $_POST['telefono'] . "', comision = " . +$_POST['comision'] . ", origen = " . +$_POST['origen'] . ", global = " . +$_POST['global'] . ", codigo_de_area = '$codigo_de_area' WHERE id = " . $_POST['id'];
        else
            $str = "INSERT INTO agencias(id_pais, agencia, codigo, direccion, telefono, comision, origen, global, codigo_de_area) VALUES(" . $_POST['id_pais'] . ", '" . mysql_real_escape_string($_POST['agencia']) . "', '" . $_POST['codigo'] . "', '" . mysql_real_escape_string($_POST['direccion']) . "', '" . $_POST['telefono'] . "', " . +$_POST['comision'] . ", " . +$_POST['origen'] . ", " . +$_POST['global'] . ", '$codigo_de_area')";
        mysql_query($str, $link);
        header("Location: ?");
        exit();
        break;
    case 2:
        $str = 'SELECT * FROM agencias WHERE id = ' . $_POST['id'];
        $res = mysql_query($str,$link);
        $row = mysql_fetch_assoc($res);
        foreach($row as $key => $value)
            $$key = $value;     
        break;
    case 3:
        $str = "UPDATE agencias LEFT JOIN usuarios ON agencias.id = usuarios.id_agencia SET agencias.status = 0, usuarios.status = 0 WHERE agencias.id = " . $_GET['id'];
        mysql_query($str, $link);
        header("Location: ?");
        exit();
        break;
}

?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right caption">AGENCIAS</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">

                <table width="100%" cellspacing="10" cellpadding="0" border="0">
                    <tr>
                        <td>

                            <form id="add" name="add" method="post" action="?" onsubmit="return validate(this)">
                                <input type="hidden" name="dll" value="1"/>
                                <input type="hidden" name="id" value="<?php echo $id ?: 0 ?>"/>

                                <table cellpadding="0" cellspacing="0" class="data-form">
                                    <caption>AGREGAR / EDITAR AGENCIAS</caption>
                                    <tr>
                                        <th>Pais:</th>
                                        <td><select name="id_pais" id="id_pais" onchange="setOrigin(this.value)" style="width:100%">
                                                <?php
                                                $str = "select id, pais from paises where status order by pais;";
                                                $res = mysql_query($str, $link);
                                                while ($row = mysql_fetch_row($res)) {
                                                    $id_pais = $id_pais ? $id_pais : $row[0];
                                                ?>
                                                    <option value="<?php echo $row[0] ?>"<?php echo $row[0] == $id_pais ? " selected" : "" ?>><?php echo $row[1] ?></option>
                                                <?php } ?>
                                            </select></td>
                                        <th>Agencia:</th>
                                        <td><input name="agencia" type="text" id="agencia" value="<?php echo $agencia ?>"/></td>
                                    </tr>
                                    <tr>
                                        <th rowspan="2" valign="top">Dirección:</th>
                                        <td rowspan="2"><textarea name="direccion" id="direccion" class="optional-input" style="height:68px"><?php echo $direccion ?></textarea>
                                        </td>
                                        <th>Teléfono:</th>
                                        <td>
                                            <div class="codigo_de_area_div">
                                                <select name="codigo_de_area" id="codigo_de_area" style="width:100%" show-text="false">
                                                <?php
                                                    $str = "select id, pais, codigo_de_area from paises where status order by pais;";
                                                    $res = mysql_query($str, $link);
                                                    while($row = mysql_fetch_row($res)){
                                                ?>
                                                    <option value="<?php echo $row[2] ?>" <?php echo $row[2] == ($codigo_de_area ? $codigo_de_area : 1) ? " selected" : ""?>>
                                                        <?php echo $row[1]; ?> <span class="codigo_de_area">+<?php echo $row[2]; ?></span>
                                                    </option>
                                                <?php   } ?>
                                                </select>
                                            </div>
                                            <input name="telefono" type="text" id="telefono" value="<?php echo $telefono ?>" class="phone-input optional-input" placeholder="(000) 000-0000"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Código:</th>
                                        <td><input name="codigo" type="text" id="codigo" value="<?php echo $codigo ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Comisión %:</th>
                                        <td><input name="comision" type="text" class="forMoney" id="comision" value="<?php echo numFormat($comision) ?>" onblur="this.value = numFormat(this.value);"/></td>
                                        <th>Origen Predeterminado:</th>
                                        <td>
                                            <div id="div_origen">
                                                <select name="origen" id="origen" class="optional-input" style="width:100%">
                                                    <?php
                                                    $str = "select id, localidad from localidades where id_pais = '$id_pais' AND status order by localidad;";
                                                    $res = mysql_query($str, $link);
                                                    while ($row = mysql_fetch_row($res)) { ?>
                                                        <option value="<?php echo $row[0] ?>"<?php echo $row[0] == $origen ? " selected" : "" ?>><?php echo $row[1] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Acceso Global:</th>
                                        <td colspan="3">
                                            <label for="global" class="d-block" style="cursor:pointer">
                                                <input type="checkbox" name="global" id="global" value="1" <?php echo $global ? 'checked' : '' ?> >
                                            </label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4">
                                            <div align="center">
                                                <?php if ($id) { ?>
                                                    <input name="Button" type="button" class="btn btn-danger cancel" value="&lt;&lt;&lt; Cancelar" onclick="window.location = '?'"/>
                                                <?php } ?>
                                                <input name="Submit" type="submit" class="btn btn-primary save" value="Guardar &gt;&gt;&gt;"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </form>

                        </td>
                    </tr>

                    <tr>
                        <td>
                        <?php
                            $str = "select paises.id, pais, count(agencias.id), paises.codigo from paises inner join agencias on paises.id = agencias.id_pais where paises.status and agencias.status group by paises.id order by pais;";
                            $res = mysql_query($str, $link);
                            if (mysql_num_rows($res)) {
                        ?>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" class="data-grid">
                                <caption>AGENCIAS</caption>
                                <tr>
                                    <th width="25%">Pais</th>
                                    <th>Agencia</th>
                                    <th>Teléfono</th>
                                    <th class="d-none d-md-table-cell">Comisión %</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                    while ($row = mysql_fetch_array($res)) {
                                ?>
                                <tr>
                                    <td rowspan="<?php echo $row[2] ?>" bgcolor="#E9F0F8">
                                        <div align="center"><?php echo strtoupper($row[1]) ?> (<?php echo $row[3] ?>)</div>
                                    </td>
                                    <?php
                                        $str = "SELECT id, agencia, codigo, codigo_de_area, telefono, comision, global FROM agencias WHERE id_pais = {$row[0]} AND status ORDER BY agencia";
                                        $res1 = mysql_query($str, $link);
                                        $x = 0;
                                        while ($row1 = mysql_fetch_object($res1)) {
                                            if ($x)
                                                echo "<tr>";
                                            $x++;
                                    ?>
                                        <td <?php echo $row1->id === $id ? 'class="table-primary"' : '' ?>>
                                            <?php echo $row1->global ? '<i class="fa fa-globe fa-lg" style="color:#19356c; margin: 0 2px 0 3px"></i>' : '' ?>
                                            <?php echo $row1->agencia . ($row1->codigo ? " (" . $row1->codigo . ")" : "") ?>
                                        </td>
                                        <td <?php echo $row1->id === $id ? 'class="table-primary"' : '' ?>>
                                            <img src="assets/img/flags_codigo_de_area/<?php echo $row1->codigo_de_area; ?>.png" class="img-flag" />
                                            <?php echo $row1->telefono ?>
                                        </td>
                                        <td class="text-right d-none d-md-table-cell <?php echo $row1->id === $id ? 'table-primary' : '' ?>"><?php echo numFormat($row1->comision) ?></td>
                                        <td width="75" <?php echo $row1->id === $id ? 'class="table-primary"' : '' ?>>
                                            <form action="" method="post">
                                                <input type="hidden" name="dll" value="2"/>
                                                <input type="hidden" name="id" value="<?php echo $row1->id; ?>"/>

                                                <input name="Submit" type="submit" class="btn btn-sm btn-primary edit" value="- E -" title="Editar Registro"/>
                                            </form>
                                        </td>
                                        <td width="75" <?php echo $row1->id === $id ? 'class="table-primary"' : '' ?>>
                                            <div align="center">
                                                <input type="button" name="del" value="- X - " onclick="confirmDelete(<?php echo $row1->id ?>,'<?php echo $row1->agencia ?>')" class="btn btn-sm btn-danger delete" title="Eliminar Registro"/>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    }
                                ?>
                            </table>
                            <?php } ?>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">

    <script type="text/javascript" src="ajaxlib.js"></script>
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>
    <script type="text/javascript">
        <?php echo createCountryMaskJSArray(); ?>

        $(document).ready(function(){
            $('select').select2();

            $("#id_pais").select2({
                templateResult: cargarImagenBanderaPais,
                templateSelection: cargarImagenBanderaPais
            });

            var selectElement = null;
            $("#codigo_de_area, #codigo_de_area2").select2({
                templateResult: cargarImagenBanderaCodigoDeArea,
                templateSelection: cargarImagenBanderaCodigoDeAreaSinTexto
            }).on('select2:open', function (e) {
              $('.select2-dropdown').addClass('codigo_de_area_width_drop_down');
            }).on('select2:select', function (e) {
                selectElement = this;
                setTimeout( function() {
                    var id_pais = $(selectElement).val()
                    $('.phone-input', $(selectElement).parent().parent()).attr('placeholder', masks[id_pais]).focus();
                }, 100);
            })

            <?php if ($codigo_de_area): ?>
                $('.phone-input').mask(masks[<?php echo $codigo_de_area; ?>], mask_defaults)
            <?php endif; ?>
        });

        function confirmDelete(id, name){
            if(confirm("Esta a punto de eliminar la agencia '"+name+"' y todos sus agentes,\ndesea continuar?"))
                window.location = '?dll=3&id='+id;
        }

        function setOrigin(id){
            loadXMLDoc("xml/origenes.php?id="+id,decodeXMLData,false);
        }

        function decodeXMLData(){
            var records = resultXML.getElementsByTagName("record");
            var holder = document.getElementById("div_origen");
            var html = "<select name='origen' id='origen' style='width:100%'>";
            for(var index=0;index<records.length;index++)
                html += "<option value='"+getNodeValue(records[index],"id")+"'>"+getNodeValue(records[index],"text")+"</option>\n";

            html += "</select>";
            holder.innerHTML = html;
            $('#origen').select2()
        }
    </script>

</div>
<?php include 'footer.php' ?>
