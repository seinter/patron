<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$filter = $user->id_central ? "and agencias.id = {$user->id_agencia}" : ($user->global ? 'and TRUE' : "and {$user->id_agencia} IN (agencias.id, agencias.id_central)");

switch($dll){
    case 1:
        switch($qt){
            case 1:
                $str = "SELECT ventas.id from ventas left join usuarios on (ventas.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) where ventas.id = '$q' and ventas.status $filter;";
            break;
            case 2:
                $str = "SELECT ventas.id FROM ventas INNER JOIN detalle_venta ON ventas.id = detalle_venta.id_venta INNER JOIN bitacora ON detalle_venta.id = bitacora.id_detalle left join usuarios on (ventas.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) WHERE ventas.status AND detalle_venta.status AND bitacora.codigo = '$q' AND bitacora.status $filter";
            break;
            case 3:
                $str = "SELECT ventas.id FROM ventas INNER JOIN detalle_venta ON ventas.id = detalle_venta.id_venta INNER JOIN bitacora ON detalle_venta.id = bitacora.id_detalle left join usuarios on (ventas.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) WHERE ventas.status AND detalle_venta.status AND bitacora.id = '$q' AND bitacora.status $filter";
            break;
        }
        $res = mysql_query($str, $link);
        if($row = mysql_fetch_row($res))
            $idv = $row[0];
        else
            $fl = 1;
    break;
}

?>
<?php include 'header.php' ?>
<style>

    @media (min-width: 576px) {
        .text-cell {
            line-height: 30px;
        }
        .col-sm-left {
            padding-right: 5px;
        }
        .col-sm-right {
            padding-left: 5px;
        }
    }

</style>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right text-truncate caption">RASTREO DE ENVIOS</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">


            <table width="100%" border="0" cellspacing="10" cellpadding="0">
                <tr>
                    <td style="padding:0">
                        <form id="search" name="search" method="post" action="?" onsubmit="return validate(this)">
                            <input type="hidden" name="dll" id="dll" value="1" />

                            <div class="alert alert-success big-box">

                                <div class="row">
                                    <div class="col-4 text-right" style="line-height:38px">
                                        <h4 class="text-truncate" style="margin:5px 0; text-transform:uppercase;">BUSCAR POR</h4>
                                    </div>
                                    <div class="col-4 col-md-3 big-combo" style="padding-right:0">
                                        <select name="qt" id="qt" style="width:100%">
                                            <option value="2" <?php echo +$qt === 2 ? 'selected' : '' ?> >Codigo QR</option>
                                            <option value="3" <?php echo +$qt === 3 || !$qt ? 'selected' : '' ?> >Número de Guía</option>
                                            <option value="1" <?php echo +$qt === 1 ? 'selected' : '' ?>>Numero de Venta</option>
                                        </select>
                                    </div>
                                    <div class="col-4 col-md-2 big-combo" style="padding-left:5px">
                                        <input name="q" type="text" id="q" class="form-control" />
                                    </div>

                                    <div class="col-8 offset-4 d-md-none">
                                        <input type="submit" class="btn btn-sm btn-info w-100" style="margin-top:4px" value="BUSCAR &gt;&gt;&gt;" />
                                    </div>
                                    <div class="d-none d-md-block col-3" style="padding-right:19px; padding-left:0;">
                                        <input type="submit" class="btn btn-sm btn-info w-100" style="margin-top:4px" value="BUSCAR &gt;&gt;&gt;" />
                                    </div>

                                </div>

                            </div>

                        </form>
                    </td>
                </tr>

				<?php if($fl){ ?>
				<tr>
					<td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FF0000">
							<tr class="forWCaption">
								<td><div align="center">NO SE ENCONTRO EL REGISTRO !!!</div></td>
							</tr>
						</table></td>
				</tr>
				<?php
			} else if($idv) {
				$left_join = $user->id_nivel == 1 ? 'left join usuarios on (ventas.id_usuario = usuarios.id)' : '';
				$str = "select /* RIGHT(md5(ventas.id),14)*/ ventas.id codigo, ventas.entrega, ventas.fecha, clientes.nombre, clientes.apellido, id_dest, local1.localidad origen, local2.localidad destino, agencias.agencia, agencias.direccion adireccion, carriers.carrier, carriers.direccion cdireccion, destinatarios.nombre destinatario from ventas inner join clientes on ventas.id_cliente = clientes.id inner join destinatarios on ventas.id_dest = destinatarios.id inner join localidades local1 on ventas.origen = local1.id inner join localidades local2 on ventas.destino = local2.id left join agencias on ventas.id_agencia = agencias.id left join (select concat(carrier,' - ',agencia) carrier, agencias_carriers.id, agencias_carriers.direccion from carriers inner join agencias_carriers on carriers.id = agencias_carriers.id_carrier) carriers on ventas.id_carrier = carriers.id where ventas.id = '$idv' ";
				// $str .= $user->id_nivel == 1 ? " and usuarios.id_agencia = {$user->id_agencia};" : '';
				$res = mysql_query($str, $link);
				if (mysql_num_rows($res)) {
					$row = mysql_fetch_assoc($res);
					foreach($row as $key => $value)
						$$key = $value;
					
			?>
					<tr>
						<td><table class="data-table form-responsive">
								<caption>CODIGO ENCONTRADO</caption>
								<tr>
									<th>VENTA:</th>
									<td class="text-cell font-weight-bold"><?php echo formatCode($codigo) ?>: <?php echo $fecha ?></td>
								</tr>
								<tr>
									<th>CLIENTE:</th>
									<td class="text-cell"><?php echo $nombre." ".$apellido ?></td>
								</tr>
								<tr>
									<th>DESTINO:</th>
									<td class="text-cell"><?php echo strtoupper($origen." - ".$destino) ?></td>
								</tr>
								<tr>
									<th>DESTINATARIO:</th>
									<td class="text-cell"><?php echo $destinatario ?></td>
								</tr>
								<tr>
									<th>LUGAR DE ENTREGA:</th>
									<?php
										switch($entrega){
										case 1:
											$lugar = $agencia.": ".$adireccion;
										break;
										case 2:
											$lugar = $carrier.": ".$cdireccion;
										break;
										default:
											$lugar = "Direccion del Destinatario";
										break;
										}
									?>
									<td class="text-cell"><?php echo $lugar ?></td>
								</tr>
							</table>
			<?php
			$str = "SELECT bitacora.id, CONCAT(clasificacion.clase, ' - ', IF(productos.id, productos.producto, detalle_venta.descripcion)) producto, bitacora.codigo FROM detalle_venta INNER JOIN clasificacion ON detalle_venta.id_clase = clasificacion.id INNER JOIN bitacora ON detalle_venta.id = bitacora.id_detalle LEFT JOIN productos ON detalle_venta.id_producto = productos.id WHERE detalle_venta.id_venta = '$idv' AND detalle_venta.status AND bitacora.status ORDER BY bitacora.id;";
			$res0 = mysql_query($str, $link);
			while($row0 = mysql_fetch_row($res0)){
			?>
							<table class="data-grid">
							<caption>GUIA <?php echo formatCode($row0[0]).": ".strtoupper($row0[1]) ?><br>
                                <small class="text-sm"><?php echo $row0[2] ?></small>
                            </caption>
								<tr>
									<th width="15%">FECHA</th>
									<th width="10%">HORA</th>
									<th>UBICACION (PAIS / AGENCIA)</th>
									<th>ACTIVIDAD</th>
									<th>DETALLE</th>
								</tr>
			<?php
				$str = "select fecha, count(id) from historial where id_bitacora = {$row0[0]} and status group by fecha order by fecha;";
				$res = mysql_query($str, $link);
			
				$class = "";
				while ($row = mysql_fetch_row($res)){
?>
				<tr>
					<td width="120" rowspan="<?php echo $row[1] ?>" bgcolor="#E9F0F8"><div align="center"><?php echo $row[0] ?></div></td>
<?php
				$str = "select historial.*, acciones.accion, paises.pais, agencias.agencia, tipo_salidas.tipo, programacion.descripcion from historial inner join acciones on historial.id_accion = acciones.id inner join usuarios on historial.id_usuario = usuarios.id inner join (select agencias.id, agencias.agencia, if(agencias.id_central, central.id_pais, agencias.id_pais) id_pais from agencias left join agencias central on agencias.id_central = central.id) agencias on usuarios.id_agencia = agencias.id inner join paises on agencias.id_pais = paises.id left join programacion on historial.id_prog = programacion.id left join tipo_salidas on programacion.id_tipo = tipo_salidas.id where historial.id_bitacora = {$row0[0]} and historial.status and historial.fecha = '{$row[0]}' order by fecha, hora;";
				$res1 = mysql_query($str, $link);
				$x = 0;
				while($row1 = mysql_fetch_assoc($res1)){
					if($x){
						echo "<tr>";
					}
					$x++;
			?>
									<td><?php echo $row1["hora"] ?></td>
									<td><?php echo $row1["pais"]." / ".$row1["agencia"] ?></td>
									<td class="text-cell"><?php echo $row1["accion"] ?></td>
									<td><?php
									switch($row1["id_accion"]){
									case 3:
										echo $row1["id_prog"]?formatCode($row1["id_prog"]).": ".$row1["descripcion"]:"";
									break;
                                    case 4:
                                            $str = "select entregas.id, id_carrier, carrier, carriers.website, guia, nombre, documento, if(foto is not null,1,0) ifoto from entregas left join carriers on entregas.id_carrier = carriers.id where id_bitacora = {$row0[0]} and entregas.status;";
                                            $res2 = mysql_query($str, $link);
                                            $row2 = mysql_fetch_assoc($res2);
                                            echo $row2["id_carrier"] ? '<a class="btn btn-sm btn-link" href="' . $row2['website'] . '" target="_blank"><i class="fa fa-external-link" style="margin-right:5px"></i>  ' . $row2["carrier"] .  ': ' . $row2["guia"] . '</a>' : '<a class="btn btn-sm btn-link" rel="tipsy" title="<b>' . $row2['nombre'] . '</b><br>' . $row2['documento'] . '"><i class="fa fa-user" style="margin-right:5px"></i> Destinatario</a>';
                                            if($row2["ifoto"]){ ?>
                                                <div class="pull-right">
                                                    <button class="btn btn-sm btn-primary" onclick="showPhoto('<?php echo md5($row2["id"]) ?>')" rel="tipsy" title="Detalle"><i class="fa fa-camera" style="margin: 0 5px"></i></button>
                                                </div>
                                    <?php
                                            }
									break;
									}
									?>
                                    </td>
								</tr>
				<?php
					}
				}
				?>
							</table>
				<?php
				}
				?>
                        </td>
					</tr>
				<?php } else { ?>
					<tr>
						<td>
							<div class="alert alert-danger big-box">
                                <div class="row">
                                    <h5 style="margin:2px 0 2px 2px; text-align: center; display: block; width: 100%;">
                                    	NO EXISTEN REGISTROS PARA BUSQUEDA INDICADA
                                	</h5>
                                </div>
                            </div>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
		</table>

            </div>
        </div>
    </div>

    <script language="javascript" type="text/javascript">
        setFocus("q",1);
    </script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>

    <link rel="stylesheet" href="assets/js/tipsy/tipsy.css">
    <script type="text/javascript" src="assets/js/tipsy/jquery.tipsy.js"></script>

    <script type="text/javascript">
        $(function() {
            $('select').select2()

            $('div[rel=tipsy], button[rel=tipsy]').tipsy({gravity: 's'});
            $('a[rel=tipsy]').tipsy({
                gravity: 's',
                html: true
            })
        })
    </script>

</div>
<?php include 'footer.php' ?>