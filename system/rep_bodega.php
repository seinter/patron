<?php
	session_start();
	require("config.php");
	include("classes/system.inc.php");
	include("functions.php");

	$user = unserialize($_SESSION[$pfix."user"]);
	User::authorize($user);

	$link = DB::connect();
?>	
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right caption">REPORTE DE BODEGA</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">
                <table width="100%" border="0" cellspacing="10" cellpadding="0">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="data-form">
                                <caption>
                                    <ul class="nav nav-pills d-block d-sm-flex">
                                        <?php
                                            $suffix = $user->id_pais == 136 ? '_mx' : '_us';
                                            $str = "select id, accion, titulo$suffix from acciones where orden$suffix order by orden$suffix;";
                                            $res = mysql_query($str, $link);
                                            $row_start = mysql_result($res, 0, 0);

                                            $res = mysql_query($str, $link);
                                            while($row = mysql_fetch_row($res)):
                                        ?>
                                            <li class="nav-item">
                                                <a class="nav-link text-white <?php echo ($row_start == $row[0] ? 'active show' : ''); ?>" data-toggle="tab" href="#acciones-tab-<?php echo $row[0]; ?>" style="padding:5px 15px; text-transform: uppercase; font-size: 15px;">
                                                    <i class="fa fa-arrow-circle-down" style="margin-right:5px"></i>
                                                    <?php echo $row[2]; ?>
                                                </a>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                </caption>
                            </table>

                            <div class="tab-content">
                                <?php
                                    $suffix = $user->id_pais == 136 ? '_mx' : '_us';
                                    $str = "select id, accion, titulo$suffix, siguiente from acciones where orden$suffix order by orden$suffix;";
                                    $res = mysql_query($str, $link);
                                    $row_start = mysql_result($res, 0, 0);

                                    $res = mysql_query($str, $link);
                                    while($row = mysql_fetch_row($res)):
                                ?>
                                    <div id="acciones-tab-<?php echo $row[0]; ?>" class="tab-pane fade <?php echo ($row_start == $row[0] ? 'active show' : ''); ?>">
                                        <table class="data-grid">
                                            <tr>
                                                <th width="5%">VENTA</th>
                                                <th width="5%">GUÍA</th>
                                                <th width="5%">HORA</th>
                                                <th width="25%">REMITENTE</th>
                                                <th width="25%">DESTINATARIO</th>
                                                <th width="25%">DETALLE</th>
                                                <th width="5%"></th>
                                            </tr>
                                            <?php
                                                $str = "select distinct ventas.id, bitacora.id idb, concat(clientes.nombre, ' ', clientes.apellido) remitente, destinatarios.nombre destinatario, historial.hora, historial.id_estado, historial.peso_v, agencias.agencia, programacion.id id_programacion, local1.localidad prog_origen, local2.localidad prog_destino, historial2.id_estado, if (historial2.id_estado, 'Confirmado', 'No Confirmado') confirmado_texto, historial4.bodega_mx, historial5.peso_v bodega_usa, historial6.cantidad from ventas left join detalle_venta on (ventas.id = detalle_venta.id_venta) left join bitacora on (detalle_venta.id = bitacora.id_detalle) left join clientes on (ventas.id_cliente = clientes.id) left join destinatarios on (ventas.id_dest = destinatarios.id) left join historial on (bitacora.id = historial.id_bitacora) left join usuarios on (historial.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) left join programacion on (historial.id_prog = programacion.id) left join localidades local1 on (programacion.origen = local1.id) left join localidades local2 on (programacion.destino = local2.id) left join (select * from historial where id_accion = 5 and id_estado = 1) historial2 on (historial2.id_bitacora = historial.id_bitacora) left join ( select count(id) total, id_bitacora from historial where id_accion > 5 group by id_bitacora) historial3 on (historial3.id_bitacora = historial.id_bitacora) left join (select count(id_bitacora) bodega_mx, id_bitacora from historial left join usuarios on (historial.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) where id_accion = 2 and historial.status and usuarios.status and agencias.origen = 3 group by id_bitacora) historial4 on (historial4.id_bitacora = bitacora.id) left join (select historial.* from historial left join usuarios on (historial.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) where id_accion = 2 and historial.status and agencias.id_pais = 225) historial5 on (historial5.id_bitacora = historial.id_bitacora) left join (select count(*) cantidad, id_bitacora from historial where id_accion = 6 and status group by id_bitacora) historial6 on (historial6.id_bitacora = historial.id_bitacora) where " . ($row[0] == 1 ? ($user->id_pais == 136 ? "historial.id_accion in (1,2)" : "historial.id_accion = {$row[0]}") : "historial.id_accion = {$row[0]}") . " and historial.status and ventas.status and clientes.status and destinatarios.status and detalle_venta.status and bitacora.status and ventas.origen = 4";
                                                switch ($row[0]) {
                                                    case 1:
                                                        $str .= ' and historial.activo order by historial2.id_estado, historial.creado;';
                                                    break;
                                                    case 2:
                                                        $str .= ' and historial.activo and agencias.origen = ' . $user->origen . ' order by historial2.id_estado, historial.creado;';
                                                    break;
                                                    case 5:
                                                        $str .= ' and historial.id_estado and historial3.total is null order by bitacora.id;';
                                                    break;
                                                    case 4:
                                                        $str .= ' and historial4.bodega_mx is null order by historial2.id_estado, historial.creado;';
                                                    break;
                                                    default:
                                                        $str .= ' and historial.activo order by bitacora.id;';
                                                    break;
                                                }
                                                $res2 = mysql_query($str, $link);
                                                while($row2 = mysql_fetch_row($res2)):
                                                	$id_estado = 0;
                                            ?>
                                                <tr>
                                                    <td><?php echo formatCode($row2[0]); ?></td>
                                                    <td><?php echo formatCode($row2[1]); ?></td>
                                                    <td><?php echo $row2[4]; ?></td>
                                                    <td><?php echo $row2[2]; ?></td>
                                                    <td><?php echo $row2[3]; ?></td>
                                                    <td>
                                                        <?php 
                                                            switch ($row[0]) {
                                                                case 2:
                                                                    $row_peso_v = $row2[6];
                                                                    $peso_v_array   = explode(',', $row_peso_v);
                                                                    $medidas = $peso_v_array[0] ? (str_replace(',', 'x', substr($row_peso_v, 0,  strrpos($row_peso_v, ',')))) : 0;
                                                                    $peso    = substr($row_peso_v, strrpos($row_peso_v, ',') + 1);
                                                                    $peso_v_titulos = array('Lado', 'Ancho', 'Largo', 'Peso');
                                                                    $peso_v  = 1;
                                                                    foreach ($peso_v_titulos as $key => $value) {
                                                                        if ($peso_v_array[$key]) {
                                                                            $peso_v *= $key <= 2 ? $peso_v_array[$key] : 1;
                                                                        }
                                                                    }
                                                                    $peso_v = numFormat($peso_v / 5000);

                                                                    if ($user->id_pais == 136) {
                                                                        $row_peso_v_usa   = $row2[14];
                                                                        $peso_v_array_usa = explode(',', $row_peso_v_usa);
                                                                        $peso_usa         = substr($row_peso_v_usa, strrpos($row_peso_v_usa, ',') + 1);
                                                                    }

                                                                    echo '<span class="text-' . ($row2[12] == 'Confirmado' ? 'success' : 'danger') .' font-weight-bold">' . $row2[12] . '</span><br>' .
                                                                    	 ($medidas ? ('Medidas: ' . $medidas . '<br>') : '') . 
                                                                         'Peso' . ($user->id_pais == 225 ? ' USA: ' : ' MX: ') . $peso . 'kg.<br>' . 
                                                                         ($user->id_pais == 136 ? 'Peso USA: ' . ($peso_usa ? $peso_usa : 0) . 'kg.<br>' : '') . 
                                                                         ($medidas ? (' Peso volumétrico: ' . ($peso_v) . ' kgs.') : '');
                                                                break;
                                                                case 6:
                                                                    $str = "select entregas.id, entregas.id_carrier, carrier, carriers.website, entregas.id_agencia_carrier, agencias_carriers.agencia from entregas left join carriers on entregas.id_carrier = carriers.id left join agencias_carriers on (carriers.id = agencias_carriers.id_carrier) where id_bitacora = {$row2[1]} and entregas.status;";
                                                                    $res3 = mysql_query($str, $link);
                                                                    $row3 = mysql_fetch_assoc($res3);
                                                                    echo 'Paquetería: ' . $row3["carrier"] . '<br> Enviar a: ' . ($row3["id_agencia_carrier"] ? 'Ocurre (' . $row3['agencia'] . ')' : 'Dirección');
                                                                break;
                                                                case 7:
                                                                    $str = "select entregas.id, entregas.guia, carriers.website, carriers.carrier, carriers.parametro from entregas left join carriers on entregas.id_carrier = carriers.id where entregas.id_bitacora = {$row2[1]} and entregas.status;";
                                                                    $res3 = mysql_query($str, $link);
                                                                    $row3 = mysql_fetch_assoc($res3);
                                                                    echo '<a class="btn btn-sm btn-link" href="#" onclick="openCourierWeb(\'' . $row3['website'] . '\',\'' . ($row3['parametro'] ? $row3['parametro'] : '') .'\',\'' . $row3['guia'] . '\');"><i class="fa fa-external-link" style="margin-right:5px"></i>  ' . $row3["carrier"] .  ': ' . $row3["guia"] . '</a>'; 
                                                                break;
                                                                break;
                                                                case 8:
                                                                    echo '<div class="text-success font-weight-bold">Paquete entregado.</div>';
                                                                break;
                                                                default:
                                                                    echo '<span class="text-' . ($row2[12] == 'Confirmado' ? 'success' : 'danger') .' font-weight-bold">' . $row2[12] . '</span>';
                                                                break;
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>	
                                                        <form action="bodega.php?dll=1&qt=2&q=<?php echo $row2[1]; ?>&ida=<?php echo $row[0] < 6 ? ($row2[11] == 1 ? ($row2[13] ? 6 : ($row2[15] ? $row[3] : 6)) : 5) : $row[3]; ?>" method="post" target="_blank">
                                                            <button class="btn btn-sm btn-primary edit" <?php echo $item->cnt_empaque ? 'disabled' : '' ?>>- E -</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php endwhile; ?>

                                            <tr>
                                                <td class="text-right" colspan="7">
                                                    <form id="rep" name="rep" method="post" action="rep_paquetes_bodega_xls.php?dll=<?php echo $row[0]; ?>" target="_blank">
                                                        <button type="submit" class="btn btn-primary w-30 d-none d-md-inline-block">
                                                            <i class="fa fa-file" style="margin-left:5px"></i>
                                                            EXPORTAR A EXCEL
                                                        </button>
                                                        <button type="submit" class="btn btn-primary w-100 d-md-none">
                                                            <i class="fa fa-file" style="margin-left:5px"></i>
                                                            EXPORTAR A EXCEL
                                                        </button>
                                                    </form>

                                                    <?php if ($row[0] == 6): ?>
                                                        <form id="rep" name="rep" method="post" action="rep_paquetes_pre_asignados.php?dll=6" target="_blank">
                                                            <button type="submit" class="btn btn-primary w-30 d-none d-md-inline-block">
                                                                <i class="fa fa-file" style="margin-left:5px"></i>
                                                                EXPORTAR A EXCEL AGRUPADO
                                                            </button>
                                                            <button type="submit" class="btn btn-primary w-100 d-md-none">
                                                                <i class="fa fa-file" style="margin-left:5px"></i>
                                                                EXPORTAR A EXCEL AGRUPADO
                                                            </button>
                                                        </form>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<?php include 'footer.php' ?>


<script type="text/javascript">
    function openCourierWeb(courier_url, parametro, guia) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(guia).select();
        document.execCommand("copy");
        $temp.remove();

        var courier_url = courier_url + (parametro != '' ? (parametro + guia) : '');
        window.open(courier_url, '_blank');
    }
</script>