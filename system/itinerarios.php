<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
include("calendars.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$m = $m?$m:date("n");
$y = $y?$y:date("Y");
$d = $d?$d:date("d");

$pm = ($m == 1) ? 12 : $m - 1;
$py = ($m != 1) ? $y : $y - 1;
$nm = ($m == 12) ? 1 : $m + 1;
$ny = ($m != 12) ? $y : $y + 1;

//-----------------------------

$ops = array(1=>"Entrega","Recepcion");

$str = "select id from rutas where id_agencia  = {$user->id_agencia} and status;";
$res = mysql_query($str, $link);
if(mysql_num_rows($res)){
	$idr = mysql_result($res,0,0);

	$sqlstr = "select ventas.id, agenda.operacion, receptores.nombre, clientes.telefono, clientes.telefono2, receptores.direccion, receptores.id_pais, receptores.zip, receptores.id_depto, agenda.fecha, agenda.hora from ventas inner join clientes on ventas.id_cliente = clientes.id inner join receptores on ventas.id_recep = receptores.id inner join agenda on ventas.id = agenda.id_venta inner join zips on receptores.zip = zips.zip where zips.id_ruta = $idr and agenda.fecha = '$y-$m-#x#' and agenda.status and ventas.status order by agenda.hora;";
}
//-----------------------------

define("ADAY", (60*60*24));
$seldate = getdate(mktime (12, 0, 0, $m, $d, $y));
$start = mktime (12, 0, 0, $m, ($seldate["mday"]-$seldate["wday"]<1?1:$seldate["mday"]-$seldate["wday"]), $y);
$seldate = getdate($start);
for($x=$seldate["wday"], $i=0;$x<7;$x++, $i++){
	$dayarray = getdate($start);
	if($dayarray["mon"]!=$m)
		break;
	$selweek[$x] = $seldate["mday"]+$i;
	$start += ADAY;
}

$bg[1] = "#FFEFEC";
$bg[2] = "#FFF4EA";

$lbg[1] = "#FFB5AA";
$lbg[2] = "#FFD0A6";

?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right text-truncate caption">AGENDA DE SEMANAL (USA)</div>
    </div>
    <div class="row main-content">
        <div class="col">
            <div class="row">


            <table width="100%" border="0" cellspacing="10" cellpadding="0">

				<tr>
					<td><table class="data-table">
                            <caption>ITINERARIO SEMANAL</caption>

							<tr>
								<td style="padding:0">
                                <?php if($idr) { ?>
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td class="table-active" style="padding:0 20px">
                                                <div class="row">
                                                    <div class="col-12 col-md-4" style="padding:5px">
                                                        <div class="d-none d-md-block" style="height:15px"></div>
                                                        <?php drawCalendar($q, $pm, $py); ?>
                                                    </div>
                                                    <div class="col-12 col-md-4" style="padding:5px">
                                                        <?php drawCalendar($q, $m, $y, 1, $d, 1); ?>
                                                    </div>
                                                    <div class="col-12 col-md-4" style="padding:5px">
                                                        <div class="d-none d-md-block" style="height:15px"></div>
                                                        <?php drawCalendar($q, $nm, $ny); ?>
                                                    </div>
                                                </div>
                                            </td>
										</tr>
									</table>
                                <?php } else { ?>
									<div align="center" style="padding:50px 0 40px">No se ha configurado ruta para esta agencia</div>
                                <?php } ?>
                                </td>
							</tr>
						</table>
                    </td>
				</tr>
			</table>

            </div>
        </div>
    </div>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('select').select2()
        })
    </script>

</div>

<div class="container-fluid">
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">

                <table width="100%" cellpadding="0" cellspacing="10">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="3" class="data-table">
                                <caption>SEMANA SELECCIONADA</caption>
                                <tr bgcolor="#4B74A0" class="forWCaption">
                                    <td style="min-width:130px"><div align="center">DOMINGO <?php echo $selweek[0] ?></div></td>
                                    <td style="min-width:130px"><div align="center">LUNES <?php echo $selweek[1] ?></div></td>
                                    <td style="min-width:130px"><div align="center">MARTES <?php echo $selweek[2] ?></div></td>
                                    <td style="min-width:130px"><div align="center">MIERCOLES <?php echo $selweek[3] ?></div></td>
                                    <td style="min-width:130px"><div align="center">JUEVES <?php echo $selweek[4] ?></div></td>
                                    <td style="min-width:130px"><div align="center">VIERNES <?php echo $selweek[5] ?></div></td>
                                    <td style="min-width:130px"><div align="center">SABADO <?php echo $selweek[6] ?></div></td>
                                </tr>
                                <tr>
                                    <?php
                                    for($i=0;$i<7;$i++){
                                        ?>
                                        <td bgcolor="#F1F1F1" valign="top" style="height:100px"><?php
                                            if($x=$selweek[$i]){
                                                ?>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <?php
                                                    $str = str_replace("#x#",$x,$sqlstr);
                                                    $res = mysql_query($str, $link);
                                                    while($row = mysql_fetch_assoc($res)){
                                                        switch($row["id_pais"]){
                                                            case 225:
                                                                $str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$row[zip]}';";
                                                                $res1 = mysql_query($str, $link);
                                                                if($row1 = mysql_fetch_row($res1))
                                                                    $direccion2 = $row1[0].", ".$row1[1]." ".$row["zip"];
                                                                break;
                                                            case 136:
                                                                $str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$row[zip]}';";
                                                                $res1 = mysql_query($str, $link);
                                                                if($row1 = mysql_fetch_row($res1))
                                                                    $direccion2 = $row1[0].", ".$row1[1];
                                                                break;
                                                            default:
                                                                $str = "select departamento from departamentos where id = {$row[id_depto]};";
                                                                $res1 = mysql_query($str, $link);
                                                                if($row1 = mysql_fetch_row($res1))
                                                                    $direccion2 = $row1[0];
                                                                break;
                                                        }
                                                        ?>
                                                        <tr bgcolor="<?php echo $bg[$row["operacion"]]?>" ondblclick="window.location = 'venta.php?id=<?php echo $row["id"] ?>'" style="cursor:pointer;" title="header=[<?php echo strtoupper($ops[$row["operacion"]]) ?>] body=[<div style='font-size:9px; line-height:14px'><?php echo "FECHA: ".$row["fecha"]." | HORA: ".$row["hora"]."<br>NOMBRE: ".$row["nombre"]."<br>TELEFONO(S): ".$row["telefono"].($row["telefono2"]?" / ".$row["telefono2"]:"")."<br>DIRECCION: ".$row["direccion"]."<br>".$direccion2 ?></div>]">
                                                            <td bgcolor="<?php echo $lbg[$row["operacion"]] ?>" width="5%">&nbsp;</td>
                                                            <td><table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                                    <tr>
                                                                        <td style="line-height:14px"><?php echo $row["hora"] ?><br />
                                                                            <?php echo $ops[$row["operacion"]] ?><br />
                                                                            <?php echo $row["nombre"] ?></td>
                                                                    </tr>
                                                                </table></td>
                                                        </tr>
                                                        <tr><td colspan="2" height="2px"></td></tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </table>
                                                <?php
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <tr bgcolor="#D9E3EE">
                                    <?php for($i=0;$i<7;$i++){ ?>
                                        <td><div align="center">
                                                <button type="button" class="btn btn-sm btn-light" style="width:100%" onclick="window.open('viewer.php?dll=19&f1=<?php echo $y.'-'.$m.'-'.$selweek[$i] ?>')"><i class="fa fa-print" style="margin-right:5px"></i> Imprimir</button>
                                            </div></td>
                                    <?php } ?>
                                </tr>
                            </table>



                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>