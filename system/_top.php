<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <link rel="stylesheet" href="assets/js/menu/jquerycssmenu.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <script type="text/javascript" src="assets/js/menu/jquerycssmenu.js"></script>
    <script type="text/javascript" src="assets/js/functions.js"></script>

</head>

<body>

<?php
/* SANDBOX WATERMARK */
if(SANDBOX && basename($_SERVER['PHP_SELF']) !== 'viewer.php'): ?>
<script type="text/javascript">
    var watermark = document.createElement("img");
    watermark.setAttribute('src', 'images/watermark.png');
    watermark.setAttribute('class', 'watermark');
    document.body.appendChild(watermark);
</script>
<?php endif ?>