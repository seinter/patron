<?php
	session_start();
	if (!$output_pdf) {
		require("config.php");
		include("classes/system.inc.php");
		include("functions.php");
	} else {
		$id = $id_pago;
	}
	require_once("pdf/fpdi.php");

	$user = unserialize($_SESSION[$pfix."user"]);
	// User::authorize($user);

	$link = DB::connect();

	$pdf = new FPDI("P","mm","A4");
	$pdf->AddPage();
	$pdf->setSourceFile("pdf/recibo.pdf");  
	$tplIdx = $pdf->importPage(1);
	$pdf->useTemplate($tplIdx,0,0); 

	$str = "SELECT pagos.id, pagos.id_venta, pagos.fecha, monto pago, costo, agencia, agencias.telefono, agencias.direccion, usuarios.nombre usuario, bitacora.id id_guia FROM pagos INNER JOIN usuarios ON pagos.id_usuario = usuarios.id INNER JOIN agencias ON usuarios.id_agencia = agencias.id left join ventas on (pagos.id_venta = ventas.id) left join detalle_venta on (pagos.id_venta = detalle_venta.id_venta) left join bitacora on (detalle_venta.id = bitacora.id_detalle) WHERE md5(pagos.id) = '$id' AND pagos.status;";
	$res = mysql_query($str, $link);
	if (mysql_num_rows($res)) {
		while($row = mysql_fetch_assoc($res)) {
	    	extract($row);
		}
	} else {
	    exit('<hr>ERROR.');
	}

	// echo '<pre>'; print_r($row); echo '<pre>'; exit();

	$str = "SELECT if(SUM(monto) = 20, 0, SUM(monto)) pagado, SUM(costo) costos FROM pagos WHERE id_venta = {$id_venta} AND id < {$id} AND status;";
	$res = mysql_query($str, $link);
	extract(mysql_fetch_assoc($res));

	$str = "SELECT ventas.id_cliente, id_dest, cargos, descuento, seguro, facturas, pc_seguro, declarado, SUM(detalle_venta.tarifa * detalle_venta.cantidad) precio, local1.localidad origen, local2.localidad destino FROM ventas INNER JOIN detalle_venta ON ventas.id = detalle_venta.id_venta AND detalle_venta.status INNER JOIN localidades local1 ON ventas.origen = local1.id INNER JOIN localidades local2 ON ventas.destino = local2.id WHERE ventas.id = $id_venta and detalle_venta.id_clase != 7;";
	$res = mysql_query($str, $link);
	$row = mysql_fetch_assoc($res);
	foreach($row as $key => $value)
		$$key = $value;

	$str = "select nombre, apellido, telefono, direccion, id_pais, id_depto, zip, paises.pais from clientes inner join paises on clientes.id_pais = paises.id where clientes.id = $id_cliente;";
	$res = mysql_query($str, $link);
	$row = mysql_fetch_assoc($res);
	foreach($row as $key => $value)
		$customer[$key] = $value;

	$str = "select count(*) from detalle_venta where id_venta = $id_venta and status and id_clase = 7;";
	$res = mysql_query($str);
	$pre_guia = mysql_result($res, 0, 0);

	switch($customer["id_pais"]){
	case 225:
		$str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$customer[zip]}';";
		$res = mysql_query($str, $link);
		if($row = mysql_fetch_row($res))
			$customer["direccion2"] = $row[0].", ".$row[1]." ".$customer["zip"];
	break;
	case 136:
		$str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$customer[zip]}';";
		$res = mysql_query($str, $link);
		if($row = mysql_fetch_row($res))
			$customer["direccion2"] = $row[0].", ".$row[1];
	break;
	default:
		$str = "select departamento from departamentos where id = {$customer[id_depto]};";
		$res = mysql_query($str, $link);
		if($row = mysql_fetch_row($res))
			$customer["direccion2"] = $row[0];
	break;
	}

	$str = "select nombre, telefono, direccion, id_pais, id_depto, zip, paises.pais from destinatarios inner join paises on destinatarios.id_pais = paises.id where destinatarios.id = $id_dest;";
	$res = mysql_query($str, $link);
	$row = mysql_fetch_assoc($res);
	foreach($row as $key => $value)
		$dest[$key] = $value;

	switch($dest["id_pais"]){
	case 225:
		$str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$dest[zip]}';";
		$res = mysql_query($str, $link);
		if($row = mysql_fetch_row($res))
			$dest["direccion2"] = $row[0].", ".$row[1]." ".$dest["zip"];
	break;
	case 136:
		$str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$dest[zip]}';";
		$res = mysql_query($str, $link);
		if($row = mysql_fetch_row($res))
			$dest["direccion2"] = $row[0].", ".$row[1] . ' ' . $dest['zip'];
	break;
	default:
		$str = "select departamento from departamentos where id = {$dest[id_depto]};";
		$res = mysql_query($str, $link);
		if($row = mysql_fetch_row($res))
			$dest["direccion2"] = $row[0];
	break;
	}

	$pdf->SetFont('Arial','B',12);

	$x = 185;
	$y = 19;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,formatCode($id_venta));

	$x = 185;
	$y = 26;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,formatCode($id_guia));

	$pdf->SetFont('Arial','B',9);

	$x = 35;
	$x2 = 133;
	$y = 37;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,formatClientCode($id_cliente));


	$pdf->SetFont('Arial','',8);

	$y += 7;
	$pdf->SetXY($x+15,$y);
	$pdf->Write(6,$customer["nombre"]." ".$customer["apellido"]);
	$pdf->SetXY($x2+8,$y);
	$pdf->Write(6,$dest["nombre"]);

	$y += 6;
	$pdf->SetXY($x+15,$y);
	$pdf->Write(6,$customer["telefono"]);
	$pdf->SetXY($x2+8,$y);
	$pdf->Write(6,$dest["telefono"]);

	$pdf->SetFont('Arial','',7);

	$y += 7;
	$pdf->SetXY($x+15,$y);
	$pdf->MultiCell(60,3.1, utf8_decode(ucwords(strtolower($customer["direccion"].", ".$customer["direccion2"]))),0,'L');
	$pdf->SetXY($x2+8,$y);
	$pdf->MultiCell(60,3.1, utf8_decode(ucwords(strtolower($dest["direccion"].", ".$dest["direccion2"]))),0,'L');

	$pdf->SetFont('Arial','B',8);

	$x = 112;
	$y = 66;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,strtoupper($origen." - ".$destino));

	$y += 6.5;
	$pdf->SetXY($x,$y);
	$pdf->Write(6,formatTracking($id_venta));

	$seguro_si = $seguro;

	if($seguro)
		$seguro = ($declarado * $pc_seguro)/100;
	else
		$seguro = 0;

	$total = $precio + $cargos + $costos + $costo + $seguro - $descuento;
	$anterior = $total - $pagado;
	$saldo = $anterior - $pago;

	// echo '$total:' . $total . '<br>';
	// echo '$pagado:' . $pagado . '<br>';
	// echo '$anterior:' . $anterior . '<br>';
	// echo '$pago:' . $pago . '<br>';
	// exit();

	$pdf->SetFont('Arial','B',10);
	$x = 160;
	$y = 82;
	$pdf->SetXY($x,$y);

	if (($pre_guia) && ($pago == 20)) {
		$saldo = 0;
		$total = 20;
		$anterior = 0;
	} else if ($pre_guia) {
		$saldo = $saldo - 20;
		$anterior = $anterior - 20;
	}

	$pdf->cell(25,4.8,numFormat($total),0,2,'R');

	$pdf->SetXY($x,$y+7);
	$pdf->cell(25,4.8,numFormat($anterior),0,2,'R');

	$y = 96;
	$pdf->SetXY($x,$y);
	$pdf->cell(25,4.8,numFormat($pago),0,2,'R');

	$pdf->SetXY($x,$y+6);

	$pdf->cell(25,4.8,numFormat($saldo),0,2,'R');

	$pdf->SetFont('Arial','I',8);

	//====== DETALLE DE VENTA ======

	$pdf->SetDrawColor(100);
	$pdf->SetFillColor(240);
	$pdf->SetLineWidth(.05) ;
	$pdf->SetFont('Arial','B',6);

	$x = 16;
	$y = 82;
	$pdf->SetXY($x,$y);

	$pdf->Cell(90.5,3.5,"DETALLE DE LA VENTA",1,1,'C',1);

	$pdf->SetFillColor(250);
	$pdf->SetFont('Arial','B',5);

	$pdf->SetX($x);

	$pdf->Cell(68,3.5,"PRODUCTO",1,0,'L',1);
	$pdf->Cell(22.5,3.5,"CANT.",1,1,'C',1);

	$pdf->SetFont('Arial','',10);

	$y += 4.5;
	$pdf->SetXY(92.5,$y);
	$pdf->Write(25,"$" . number_format($declarado,2));

	$pdf->SetXY(92.5,$y+4.3);
	$prima = ($declarado * $pc_seguro)/100;
	$pdf->Write(25,"$" . number_format($prima,2));

	$x = $seguro_si ? 22.5 : 36;
	$y += 1.5;
	$pdf->SetXY($x,$y);
	$pdf->Write(40,"X");

	$x = $seguro_si ? 81.5 : 95;
	$pdf->SetXY($x,$y);
	$pdf->Write(40,"X");

	$pdf->SetFillColor(255);
	$pdf->SetFont('Arial','',5);

	$str = "select clase, IF(productos.id, producto, detalle_venta.descripcion) producto, cantidad from detalle_venta inner join clasificacion on detalle_venta.id_clase = clasificacion.id LEFT JOIN productos ON detalle_venta.id_producto = productos.id where detalle_venta.id_venta = $id_venta and detalle_venta.status limit 5;";
	$res = mysql_query($str, $link);
	$yRow = 86;
	while($row = mysql_fetch_assoc($res)){
		$yRow += 3;
		$pdf->SetXY(16, $yRow);
		$pdf->Cell(68,3,strtoupper($row["clase"].' - '.$row["producto"]),1,0,'L',0);
		$pdf->Cell(22.5,3,$row["cantidad"],1,1,'R',0);
	}
	//====================================


	$x = 15;
	$y = 262.5;
	$pdf->SetXY($x,$y);
	$pdf->Write(0,str_replace("\r\n",' ',$direccion));

	$pdf->SetXY($x,$y);
	$pdf->Write(6,"Telefono: ".$telefono);

	$pdf->SetFont('Arial','',7);

	$x = 146;
	$y = 263;
	$pdf->SetXY($x,$y);
	$pdf->Write(0,$usuario);

	$y += 3;
	$pdf->SetXY($x,$y);
	$pdf->Write(0,$agencia);

	$y += 3;
	$pdf->SetXY($x,$y);
	$pdf->Write(0,$fecha);

	if ($output_pdf) {
	    $file_name  = realpath('recibos') . '/' . $id_venta . '.pdf';
    	$pdf->Output($file_name, 'F');
	} else {
		$pdf->Output();
	}

?>