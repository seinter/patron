<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require_once("pdf/fpdf.php");

include('classes/phpqrcode.php');
//include("barcode/barcode.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$pdf = new FPDF('P','in','Letter');
$pdf->AddPage();

$str = "select recepcion from ventas where id = " . $_GET['id_venta'] . ";";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$dest[$key] = $value == 0 ? 'Agencia' : 'Dirección';

$str = "select nombre, telefono, direccion, pais, departamento, paises.id id_pais, zip from destinatarios inner join paises on destinatarios.id_pais = paises.id left join departamentos on destinatarios.id_depto = departamentos.id where destinatarios.id = " . $_GET['id_dest'] . ";";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$dest[$key] = $value;

$id_pais = $dest['id_pais'];
$zip     = $dest['zip'];
if($dest['id_pais']==225)
	$str = "select ciudad, estado from zips inner join ciudades on zips.id_ciudad = ciudades.id inner join estados on zips.id_estado = estados.id where zip = '$zip';";
else
	$str = "select municipio ciudad, estado from zipsmx inner join estadosmx on zipsmx.id_estado = estadosmx.id inner join municipios on zipsmx.id_muni = municipios.id and estadosmx.id = municipios.id_estado where zip = '$zip';";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);

$x = .25;
$y = 0;

for ($y = 0; $y<10; $y = $y + .5) {
	$pdf->SetFont('Helvetica','B',13);
	$y += .25;
	$pdf->SetXY($x,$y);
	$pdf->Write(.35, strtoupper($dest['nombre']));

	$y += .25;
	$pdf->SetXY($x,$y);
	$pdf->Write(.35, strtoupper($dest['direccion']));

	$y += .25;
	$pdf->SetXY($x,$y);
	$pdf->Write(.35, strtoupper('CP. ' . $dest['zip']));

	$y += .25;
	$pdf->SetXY($x,$y);
	$pdf->Write(.35, strtoupper('Cd, Edo: ' . $row['ciudad'] . ', ' . $row['estado']));

	$y += .25;
	$pdf->SetXY($x,$y);
	$pdf->Write(.35, strtoupper('Tel: ' . $dest['telefono']));

	$y += .25;
	$pdf->SetXY($x,$y);
	$pdf->Write(.35, strtoupper('Servicio: ' . $dest['recepcion']));
}

$pdf->Output();

?>