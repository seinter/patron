<?php
session_start();
require("config.php");
include("classes/system.inc.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

if($id = $_GET['id']){
    $str = "select operacion from operaciones where id = $id";
    $res = mysql_query($str, $link);
    $opt = mysql_result($res, 0);
} else
    exit();

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>

    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <script type="text/javascript" src="ajaxlib.js"></script>

    <script type="text/javascript">

        if(window.self === window.top)
            window.location = 'index.php'

        $(function(){
            $('#uname, #password').keyup(function(){
                $('#submit').prop('disabled', $('#uname').val() === '' || $('#password').val() === '')
            })

            $('#close').click(closeLightBox)
        })

        function authorize(){
            var uname = $('#uname')
            var pass = $('#password')
            var btn = $('#submit')

            uname.add(pass).add(btn).prop('disabled', true)
            btn.data('original-text', btn.html())
                .html(btn.data('loading-text'))

            loadXMLDoc("xml/auth.php?uname=" + uname.val() + "&pass=" + pass.val(), decodeXMLData, false);
        }

        function decodeXMLData(){
            var result = resultXML.getElementsByTagName("record");
            var icon = $('#icon')
            var btn = $('#submit')

            if(result.length){
                $('#idu').val(result[0].firstChild.nodeValue)
                icon.attr('class', 'fa fa-check-circle text-success blink')
                btn.addClass('btn-success').html('AUTORIZADO!')
                setTimeout(closeLightBox, 1000)
            } else {
                var uname = $('#uname')

                icon.attr('class', 'fa fa-times-circle text-danger blink')
                btn.addClass('btn-danger').html('DENEGADO!')
                setTimeout(function(){
                    btn.html(btn.data('original-text')).removeClass('btn-danger')
                    icon.attr('class', 'fa fa-shield')
                    uname.add('#password').add(btn).prop('disabled', false)
                    uname.focus().select()
                }, 1000)
            }
        }

        function getKey(event){
            if(event.keyCode === 13 && !$('#submit').prop('disabled'))
                authorize();
        }

        function closeLightBox(){
            window.parent.$.fancybox.close();
        }

    </script>

    <style type="text/css">
        <!--
        body {
            max-width: 400px;
            background-color: #ECECEC;
        }
        table {
            border-collapse: initial;
        }
        table caption {
            caption-side: top;
        }
        table tr:last-child td {
            border-top: 1px solid #CCC;
            padding-top: 5px;
        }
        @media (max-width: 239px) {
            table tr {
                display: block;
            }
            table th {
                text-align: left;
                display: block;
                padding: 3px 10px;
                width: 100%;
            }
            table td {
                display: block;
                width: 100% !important;
            }
            table tr:last-child td:first-child {
                display: none;
            }
        }
        -->
    </style>

</head>

<body>
<button id="close" class="btn btn-dark" style="position:absolute; z-index:1; border-radius:0;">
    <i class="fa fa-times text-secondary" style="font-size:30px"></i>
</button>
<input type="hidden" id="idu" value="0">
<div class="container-fluid">
    <div class="row main-title">
        <div class="col text-right text-truncate caption" style="padding-left:50px"><?php echo $opt ?></div>
    </div>
</div>

<div style="margin:0 5px 5px">
    <table width="100%" cellspacing="0" cellpadding="3">
        <caption class="table-dark" style="margin:0 -5px 5px">
            <div class="row" style="margin:0">
                <div class="col text-center text-truncate" style="width:0">SE REQUIERE AUTORIZACION DE UN ADMINISTRADOR</div>
            </div>
        </caption>
        <tr>
            <td rowspan="2" class="text-center" style="width:100px; vertical-align:middle;">
                <i id="icon" class="fa fa-shield" style="font-size:60px"></i>
            </td>
            <td>Usuario:</td>
            <td><input type="text" name="uname" id="uname" class="form-control form-control-sm"/></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password" id="password" class="form-control form-control-sm" onkeypress="getKey(event)" style="margin-bottom:2px"/></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>
                <button id="submit" class="btn btn-sm btn-primary btn-block" onclick="authorize()" data-loading-text="<i class='fa fa-circle-o-notch fa-spin' style='margin-right:5px'></i> Validando" disabled >Autorizar <i class="fa fa-arrow-right" style="margin-left:5px" ></i></button>
            </td>
        </tr>
    </table>
</div>

<div class="container-fluid" style="position:fixed;bottom:0;">
    <div class="row main-footer">
        <div class="col copyright"><?php echo $copyright ?></div>
    </div>
</div>

</body>
</html>
