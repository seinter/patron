<?php
session_start();
require("config.php");
include("classes/system.inc.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

if($dll){
	$str = "select password from usuarios where id = $user->id;";
	$res = mysql_query($str, $link);
	if(mysql_result($res,0,0) == md5($password)){
		$str = "update usuarios set password = md5('$new_pass1') where id = $user->id;";
		mysql_query($str, $link);
		header("Location: ?sc=1");
	} else
		header("Location: ?fl=1");
	exit();
}

?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right text-truncate caption">OPCIONES DE SEGURIDAD</div>
    </div>

    <div class="row main-content">
        <div class="col table-responsive">

            <div style="max-width:700px; margin:10px auto;">
                <form id="pass" name="pass" method="post" action="?" enctype="application/x-www-form-urlencoded" autocomplete="on" onsubmit="return validatePassword(this)">
                    <input type="hidden" name="dll" value="1" />

                    <table class="table">
                        <tr>
                            <td colspan="3" style="height:60px; border-top:0">
                                <?php if($sc){ ?>
                                    <table width="100%" bgcolor="#0000FF">
                                        <tr class="forWCaption">
                                            <td style="padding:5px 10px; border-top:0;"><div align="center">LA CONTRASEÑA HA SIDO CAMBIADA!</div></td>
                                        </tr>
                                    </table>
                                <?php } ?>
                                <?php if($fl){ ?>
                                    <table width="100%" bgcolor="#FF0000">
                                        <tr class="forWCaption">
                                            <td style="padding:5px 10px; border-top:0;"><div align="center">CONTRASEÑA INCORRECTA!</div></td>
                                        </tr>
                                    </table>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="row">
                                    <h4 class="col text-center text-truncate" style="width:0">CAMBIAR PASSWORD</h4>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="150" rowspan="3" style="text-align:center; vertical-align:middle;">
                                <i class="fa fa-lock" style="font-size:100px; color:lightgray;"></i>
                            </td>
                            <td colspan="2">
                                <div class="row">
                                    <div class="col-12 col-sm-5 font-weight-bold text-truncate">Contraseña Actual:</div>
                                    <div class="col-12 col-sm-7"><input type="password" name="password" id="password" autocomplete="current-password" /></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="row">
                                    <div class="col-12 col-sm-5 font-weight-bold text-truncate">Nueva Contraseña:</div>
                                    <div class="col-12 col-sm-7"><input type="password" name="new_pass1" id="new_pass1" autocomplete="new-password" /></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="row">
                                    <div class="col-12 col-sm-5 font-weight-bold text-truncate">Confirmarción:</div>
                                    <div class="col-12 col-sm-7"><input type="password" name="new_pass2" id="new_pass2" autocomplete="new-password" /></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div align="center">
                                    <button type="submit" class="btn btn-danger">CAMBIAR <i class="fa fa-arrow-right" style="margin-left:5px"></i></button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </table>
                </form>

            </div>

        </div>
    </div>

    <style>
        .table th {
            max-width: 150px;
        }
        .table td input, table td button {
            width:100%;
            min-width: 100px;
        }
    </style>

    <script type="text/javascript">
        setFocus("password");
    </script>

</div>
<?php include 'footer.php' ?>