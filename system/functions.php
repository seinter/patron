<?php



function numFormat($val){
	return number_format($val,2,'.','');
}

function numFormatInt($val){
	return number_format($val,0,'.','');
}

function maskPhone(){
	return " onFocus=\"return onFocusMask(this)\" onKeyDown=\"return doMask(this, event)\" onBlur=\"return onBlurMask(this)\"";
}

function formatCode($val, $len = 4){
    $num = $len - strlen($val);
    if($num > 0)
        $val = implode('', array_fill(0, $num, 0)) . $val . (SANDBOX ? 'S' : '');

    return $val;
}

function set_lenght_measures($user) {
	return (object) array(
		'abreviatura' => $user->id_pais == 225 ? 'inchs.' : 'cms.',
		'titulo' => $user->id_pais == 225 ? 'pulgadas' : 'centimetros',
		'calculo' => $user->id_pais == 225 ? 1 : 2.54
	);
}

function set_weight_measures($user) {
	return (object) array(
		'abreviatura' => $user->id_pais == 225 ? 'kgs.' : 'lbs.',
		'titulo' => $user->id_pais == 225 ? 'kilogramos' : 'libras',
		'calculo_peso_v' => $user->id_pais == 225 ? 305 : 5000,
		'calculo' => $user->id_pais == 225 ? 1 : 2.205
	);
}

function formatAgencyVaultCode($val) {
    return 'AGENCY-' . formatCode($val);
}

function formatClientCode($val){
	global $link;
	$str = "select codigo from agencias inner join clientes on agencias.id = clientes.id_agencia where clientes.id = $val;";
	$res = mysql_query($str, $link);
	return mysql_result($res,0,0).(strlen($val)<4?"0":"").(strlen($val)<3?"0":"").(strlen($val)<2?"0":"").$val;
}

function formatTracking($val){
    global $link;
    $str = "SELECT codigo from bitacora INNER JOIN detalle_venta ON bitacora.id_detalle = detalle_venta.id INNER JOIN ventas ON detalle_venta.id_venta = ventas.id WHERE ventas.id = {$val} ORDER BY bitacora.id LIMIT 1";
    $res = mysql_query($str, $link);
    return mysql_result($res, 0);

	//return strtoupper(substr(md5($val),18,14));
}

function formatBarCode($val){
	return strtoupper(substr(md5($val),0,14));
}

//FUNCTIONS
function getState($idp, $zip){
	global $link;
	if($zip){
		switch($idp){
		case 136:
			$str = "select estadosmx.estado from estadosmx inner join zipsmx on estadosmx.id = zipsmx.id_estado where zipsmx.zip = '$zip' group by estadosmx.id;";
			$res = mysql_query($str, $link);
			if(mysql_num_rows($res))
				return mysql_result($res,0,0);
		break;
		case 225:
			$str = "select estado from estados inner join zips on estados.id = zips.id_estado where zip = '$zip' group by estados.id;";
			$res = mysql_query($str, $link);
			if(mysql_num_rows($res))
				return mysql_result($res,0,0);
		break;
		}
	}
	return "&nbsp;- - - - -";
}

function getCity($idp, $zip){
	global $link;
	if($zip){
		switch($idp){
		case 136:
			$str = "select municipio from municipios inner join zipsmx on municipios.id = zipsmx.id_muni and zipsmx.id_estado = municipios.id_estado where zip = '$zip';";
			$res = mysql_query($str, $link);
			if(mysql_num_rows($res))
				return mysql_result($res,0,0);
		break;
		case 225:
			$str = "select ciudad from ciudades inner join zips on ciudades.id = zips.id_ciudad where zip = '$zip' group by ciudades.id;";
			$res = mysql_query($str, $link);
			if(mysql_num_rows($res))
				return mysql_result($res,0,0);
		break;
		}
	}
	return "&nbsp;- - - - -";
}

function checkAvailability($user, $id = 0){
	global $link;
	if($id)
		$str = "select count(*) from usuarios where usuario = '$user' and id != $id and status;";
	else
		$str = "select count(*) from usuarios where usuario = '$user' and status;";
	
	$res = mysql_query($str, $link);
	return !mysql_result($res,0,0);
}

function getUniqueCode($table){
    global $link;
    $i = 0;
    do {
        $res = mysql_query("SELECT code, COUNT({$table}.id) cnt FROM (SELECT UNIX_TIMESTAMP() + {$i} code) t1 LEFT JOIN {$table} ON t1.code = {$table}.codigo GROUP BY code", $link);
        $row = mysql_fetch_object($res);
        $i++;
    } while($row->cnt);

    return $row->code;

}

function createCountryMaskJSArray() {
    global $link;
	$str = "select id, codigo_de_area, mask from paises where status;";
	$res = mysql_query($str, $link);
	while($row = mysql_fetch_row($res)) {
		$mascaras_paises[$row[1]] = $row[2];
	}
	
	return "masks = " . json_encode($mascaras_paises) .";";

}

function specialCharactersToHTML($html) {
	$html = str_replace('á', '&aacute;', $html);
	$html = str_replace('é', '&eacute;', $html);
	$html = str_replace('í', '&iacute;', $html);
	$html = str_replace('ó', '&oacute;', $html);
	$html = str_replace('ú', '&uacute;', $html);
	$html = str_replace('ñ', '&ntilde;', $html);
	$html = str_replace('Á', '&Aacute;', $html);
	$html = str_replace('É', '&Eacute;', $html);
	$html = str_replace('Í', '&Iacute;', $html);
	$html = str_replace('Ó', '&Oacute;', $html);
	$html = str_replace('Ú', '&Uacute;', $html);
	$html = str_replace('Ñ', '&Ñacute;', $html);
	return $html;
}

?>
