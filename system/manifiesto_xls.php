<?php
	session_start();
	require("config.php");
	include("classes/system.inc.php");
	include("classes/excel.inc.php");
	include("functions.php");

	$user = unserialize($_SESSION[$pfix."user"]);

	// User::authorize($user, 2);
	// echo '<pre>'; print_r($user); echo '<pre>'; exit();

	$link = DB::connect();

	$xls = new Excel("manifiestos");

	$str = "select programacion.descripcion, programacion.fecha, local1.localidad origen, local2.localidad destino, tipo from programacion inner join localidades local1 on programacion.origen = local1.id inner join localidades local2 on programacion.destino = local2.id inner join tipo_salidas on programacion.id_tipo = tipo_salidas.id where programacion.id = $id;";
	$res = mysql_query($str, $link);
	$row = mysql_fetch_assoc($res);
	foreach($row as $key => $value)
		$$key = $value;

    $xls->title(array($title." - MANIFIESTO DE ".strtoupper($tipo), formatCode($id)." - ".	$fecha.": ".$origen." - ".$destino." | ".$descripcion));
    $xls->Ln();

	$str = "select distinct local3.localidad destino, historial4.peso_v, ventas.id, bitacora.id idb, historial.hora, concat(clientes.nombre, ' ', clientes.apellido) remitente, clientes.telefono cliente_tel, clientes.whatsapp clientes_whatsapp, destinatarios.nombre destinatario, destinatarios.colonia, destinatarios.direccion, destinatarios.telefono dest_tel, destinatarios.whatsapp dest_whatsapp, historial2.id_estado, if (historial2.id_estado, 'Confirmado', 'No Confirmado') confirmado_texto, departamentos.departamento, estados.estado, estadosmx.estado estadomx, ciudades.ciudad, municipios.municipio, destinatarios.zip, historial5.peso_v bodega_usa, historial6.fecha fecha_salida from ventas left join detalle_venta on (ventas.id = detalle_venta.id_venta) left join bitacora on (detalle_venta.id = bitacora.id_detalle) left join clientes on (ventas.id_cliente = clientes.id) left join destinatarios on (ventas.id_dest = destinatarios.id) left join historial on (bitacora.id = historial.id_bitacora) left join usuarios on (historial.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) left join programacion on (historial.id_prog = programacion.id) left join localidades local1 on (programacion.origen = local1.id) left join localidades local2 on (programacion.destino = local2.id) left join (select * from historial where id_accion = 5 and id_estado = 1) historial2 on (historial2.id_bitacora = historial.id_bitacora) left join ( select count(id) total, id_bitacora from historial where id_accion > 5 group by id_bitacora) historial3 on (historial3.id_bitacora = historial.id_bitacora) left join (select historial.* from historial left join usuarios on (historial.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) where historial.id_accion = 2 and agencias.origen = 3 and historial.status) historial4 on (historial4.id_bitacora = historial.id_bitacora) left join departamentos on (departamentos.id = destinatarios.id_depto) left join zips on (zips.zip = destinatarios.zip) left join zipsmx on (zipsmx.zip = destinatarios.zip) left join estados on (estados.id = zips.id_estado) left join estadosmx on (estadosmx.id = zipsmx.id_estado) left join ciudades on (ciudades.id = zips.id_ciudad) left join municipios on (municipios.id = zipsmx.id_muni) left join (select historial.* from historial left join usuarios on (historial.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) where id_accion = 2 and historial.status and agencias.id_pais = 225) historial5 on (historial5.id_bitacora = historial.id_bitacora) left join (select historial.* from historial left join usuarios on (historial.id_usuario = usuarios.id) left join agencias on (usuarios.id_agencia = agencias.id) where id_accion = 4 and historial.status and agencias.id_pais = 225) historial6 on (historial6.id_bitacora = historial.id_bitacora) left join localidades local3 on (ventas.destino = local3.id) where historial.id_prog = $id and historial.status and ventas.status and clientes.status and destinatarios.status and detalle_venta.status and bitacora.status and ventas.origen = 4";
	$res = mysql_query($str, $link);
	while($row = mysql_fetch_object($res))
		$records[$row->destino][] = $row;

	if($records) {
		foreach($records as $key => $value){
			unset($data);

			// echo '<pre>'; print_r($value); echo '</pre>'; exit();
			foreach($value as $row) {
	            $row_peso_v = $row->peso_v;
	            $peso_v_array   = explode(',', $row_peso_v);
	            $medidas = $peso_v_array[0] ? (str_replace(',', 'x', substr($row_peso_v, 0,  strrpos($row_peso_v, ',')))) : 0;
	            $peso    = substr($row_peso_v, strrpos($row_peso_v, ',') + 1);
	            $peso_v_titulos = array('Lado', 'Ancho', 'Largo', 'Peso');
	            $peso_v  = 1;
	            foreach ($peso_v_titulos as $key => $value) {
	                if ($peso_v_array[$key]) {
	                    $peso_v *= $key <= 2 ? $peso_v_array[$key] : 1;
	                }
	            }
	            $peso_v = numFormat($peso_v / 5000);
				$peso_mx  = explode(',', $row->peso_v);
				$peso_usa = explode(',', $row->bodega_usa);

				$data[] = array(
					formatCode($row->idb),
					$row->fecha_salida,
					$peso_usa[3],
					$peso_mx[3],
					$medidas,
					$peso_v,
					utf8_decode($row->destinatario),
					utf8_decode($row->direccion),
					utf8_decode($row->colonia),
					utf8_decode($user->id_pais == 136 ? $row->estadomx : $row->estado),
					utf8_decode($user->id_pais == 136 ? $row->municipio : $row->ciudad),
					$row->zip,
					$row->dest_tel,
					($row->dest_whatsapp ? 'WhatsApp' : 'NO')
				);
			}
		}

		$header = array(utf8_decode("No. GUÍA"), "FECHA SALIDA", "PESO USA", "PESO MX", "MEDIDAS", "PESO VOL.", "DESTINATARIO", utf8_decode("DIRECCIÓN"), "COLONIA", "ESTADO", "CIUDAD", utf8_decode("CÓDIGO POSTAL"), utf8_decode("TELÉFONO"), "WHATSAPP");

        $xls->table($header, $data, []);

    }
	$xls->xlsEOF();
	exit();

?>