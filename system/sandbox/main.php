<?php
session_start();
require("config.php");
include("classes/system.inc.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right text-truncate caption">INICIO</div>
    </div>
    <div class="row main-content">
        <div class="col">
            <div style="margin:15px 0">
                <h1>Bienvenido <?php echo strtoupper($user->nombre) ?></h1>

                <p class="text-justify">En el menu superior encontrará las opciones disponibles para su nivel de acceso en grupos lógicos, a continuación se muestra de forma detallada dichos grupos. Las opciones del usuario se encuentran en la parte inferior derecha.</p>
                <p>Puede regresar a esta pantalla por medio de la opcion Inicio.</p>

                <ul style="line-height:24px">

                <?php
                switch($user->id_nivel){
                    case 1:
                    case 2:
                        if(+$user->id_nivel === 2 || $user->bod):
                ?>
                        <li>BODEGA
                            <ul>
                                <li>Acciones de Bodega: <span class="text-muted">Ingresos a Bodega, Salidas de Bodega, Entregas a Destinatario.</span></li>
                                <li>Manifiestos <span class="text-muted">Detalle de Envíos por Salida.</span></li>
                                <li>Agenda: <span class="text-muted">Programación Semanal de Ruta (Distribución, Recolección de Cajas).</span></li>
                                <li>Rastreos: <span class="text-muted">Rastreo de Paquetes.</span></li>
                            </ul>
                        </li>
                <?php
                        endif;
                        if(+$user->id_nivel === 2 || $user->opr):
                ?>
                        <li>OPERACIONES
                            <ul>
                                <li>Consultas</li>
                                <li>Nueva Venta</li>
                                <li>Clientes: <span class="text-muted">Administrar, Actualizar Datos, Historial de la Cuenta.</span></li>
                            </ul>
                        </li>
                <?php
                        endif;
                        if($user->id_nivel > 1):
                ?>
                        <li>ADMINISTRACION
                            <ul>
                                <li>Representantes</li>
                                <li>Sub Agencias</li>
                                <li>Sub Agentes</li>
                                <li><b>Reportes</b>
                                    <ul>
                                        <li>Ventas por Fecha</li>
                                        <li>Ingresos por Fecha: <span class="text-muted">Pagos Recibidos.</span></li>
                                        <li>Estadisticas el Mes: <span class="text-muted">Gráficos de Barras.</span></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                <?php
                        endif;
                    break;
                    case 3:
                ?>
                <ul style="line-height:24px">
                    <li>CONFIGURACION
                        <ul>
                            <li>Agencias</li>
                            <li><b>Usuarios</b>
                                <ul>
                                    <li>Super Usuarios</li>
                                    <li>Administradores / Representantes</li>
                                </ul>
                            </li>
                            <li>PAQ-NAC: <span class="text-muted">Empresas de paquetería asociadas.</span></li>
                            <li>Destinos</li>
                            <li>Clasificación: <span class="text-muted">Subdivisión de Tipos (Peso, Volumen...).</span></li>
                            <li>Productos</li>
                            <li>Tarifas por Pie: <span class="text-muted">Volumenes NO Listados.</span></li>
                            <li>Rutas (USA): <span class="text-muted">Selección de Codigos Posatales por Ruta.</span></li>
                        </ul>
                    </li>
                    <li>PROGRAMACION
                        <ul>
                            <li>Programar Salidas</li>
                        </ul>
                    </li>
                    <li>REPORTES
                        <ul>
                            <li>Ventas por Fecha</li>
                            <li>Ingresos por Fecha: <span class="text-muted">Pagos Recibidos.</span></li>
                            <li>Estadisticas el Mes: <span class="text-muted">Gráficos de Barras.</span></li>
                        </ul>
                    </li>
                <?php
                    break;
                }
                ?>

                </ul>
            </div>
        </div>
    </div>

</div>
<?php include 'footer.php' ?>