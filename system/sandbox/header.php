<?php include '_top.php' ?>

<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col text-center text-sm-left"><img src="assets/img/logo.jpg" width="150" style="margin:5px;"></div>
                <div class="col d-none d-sm-block text-right">
                    <div style="margin:10px"><span><?php echo $title ?></span></div>
                    <?php if($user->id_agencia): ?>
                    <div style="position:absolute; bottom:0; right:15px; margin:10px 0">
                        <span class="badge <?php echo $user->id_central ? 'badge-warning' : 'badge-danger' ?>" style="font-size:13px; padding:5px 10px"><?php echo ($user->id_central ? 'SUB-':'') . 'AGENCIA ' . strtoupper($user->agencia) ?></span>
                    </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row main-menu position-relative">
        <div class="col">
            <div class="row">
                <div class="col d-md-none">
                    <div class="navbar-dark">
                        <a class="btn btn-primary navbar-toggler" data-toggle="collapse" aria-expanded="false" aria-controls="main-menu-collapse" role="button" href="#main-menu-collapse" style="margin:8px 0">
                            <span class="navbar-toggler-icon"></span>
                        </a>
                    </div>
                </div>
                <div class="col d-none d-md-block">
                    <div class="row">
                        <div id="top-menu" class="jquerycssmenu">
                            <?php include 'menu.php' ?>
                        </div>
                    </div>
                </div>
                <div class="col col-md-2 text-right today" style="padding-left:0"><?php echo date("l M j, Y") ?></div>
            </div>
        </div>
    </div>
    <div class="row d-md-none collapse main-menu-collapse" id="main-menu-collapse">
        <?php include 'menu.php' ?>
    </div>
</div>