<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$str = "select bitacora.id idb, bitacora.codigo, historial.fecha, historial.hora, entregas.nombre, entregas.documento from bitacora inner join historial ON bitacora.id = historial.id_bitacora INNER JOIN entregas on bitacora.id = entregas.id_bitacora WHERE bitacora.status AND historial.id_accion = 4 AND historial.status AND md5(entregas.id) = '$id' AND entregas.status;";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$$key = $value;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>

    <link rel="stylesheet" href="assets/js/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>

    <style type="text/css">
        <!--
        body {
            max-width:820px;
        }
        table {
            border-collapse: initial;
        }
        table caption {
            caption-side: top;
        }
        -->
    </style>

</head>

<body>
<div class="container-fluid">
    <div class="row main-title">
        <div class="col text-right caption">ENTREGA: <?php echo $codigo ?></div>
    </div>
</div>


            <table width="100%" border="0" cellpadding="0" cellspacing="10">
			<tr>
				<td><div align="center"><img class="img-fluid" src="file.php?dll=2&id=<?php echo $id ?>" /></div></td>
			</tr>
			<tr>
				<td>
                    <table class="data-table form-responsive table-orange">
                    <tr>
                        <th>FECHA / HORA</th>
                        <td><?php echo $fecha . ' '  . $hora ?></td>
                    </tr>
					<tr>
						<th>RECIBE:</td>
						<td><?php echo $nombre ?></td>
					</tr>
					<tr>
						<th>IDENTIFICACION:</th>
						<td><?php echo $documento ?></td>
					</tr>
				</table>
                </td>
			</tr>
		</table>


<div class="container-fluid" style="position:fixed;bottom:0;">
    <div class="row main-footer">
        <div class="col copyright"><?php echo $copyright ?></div>
    </div>
</div>

</body>
</html>