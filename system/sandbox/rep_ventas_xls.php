<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("classes/excel.inc.php");
include("functions.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 2);

$link = DB::connect();

$xls = new Excel("ventas_del_".$f1."_al_".$f2);

if($ida)
    $str = "SELECT id, agencia, id_central FROM agencias WHERE id = {$ida}";
else
    $str = "SELECT id, agencia, id_central FROM agencias WHERE (id = {$user->id_agencia} OR id_central = {$user->id_agencia}) AND status ORDER BY id;";
$res = mysql_query($str, $link);

$tipos = array("Normal", "Pago en Entrega", "Credito");
while($row = mysql_fetch_object($res)){
    $str = "SELECT ventas.*, carrier, clientes.nombre, clientes.apellido, usuarios.nombre agente, SUM(detalle_venta.tarifa * detalle_venta.cantidad) monto, pagos.pagos, pagos.costos FROM ventas INNER JOIN clientes ON ventas.id_cliente = clientes.id INNER JOIN usuarios ON ventas.id_usuario = usuarios.id INNER JOIN detalle_venta ON ventas.id = detalle_venta.id_venta LEFT JOIN (SELECT id_venta, SUM(monto) pagos, SUM(costo) costos FROM pagos WHERE status GROUP BY id_venta) pagos ON ventas.id = pagos.id_venta LEFT JOIN carriers ON id_credito = carriers.id WHERE ventas.fecha BETWEEN '$f1' AND '$f2' AND usuarios.id_agencia = {$row->id} AND ventas.status AND detalle_venta.status GROUP BY ventas.id ORDER BY ventas.fecha, ventas.id;";
	$res2 = mysql_query($str, $link);
	if(mysql_num_rows($res2)){

        unset($data, $total, $pagado, $ventas);
        $agencia = ($row->id_central ? 'SUB ' : '') . 'AGENCIA: ' . $row->agencia;
        $xls->title(array($title . ' - ' . $agencia, "REPORTE DE VENTAS DEL '{$f1}' AL '{$f2}'"));
        $xls->Ln();

		while($row2 = mysql_fetch_object($res2)){
			$data[] = array($row2->fecha, $row2->agente, formatCode($row2->id), $row2->nombre." ".$row2->apellido, $tipos[$row2->id_tipo], $row2->carrier, numFormat($row2->monto), numFormat($row2->cargos), numFormat($row2->descuento), numFormat($seguro = ($row2->declarado * $row2->pc_seguro) / 100), numFormat($precio = ($row2->monto + $row2->cargos + $row2->costos + $seguro - $row2->descuento)), numFormat($row2->pagos), numFormat($precio-$row2->pagos));
			$total += $precio;
			$pagado += $row2->pagos;
			$ventas++;
		}
		$resumen[] = array("agencia"=>$agencia,"ventas"=>$ventas,"total"=>$total,"tpagado"=>$pagado,"tsaldo"=>$total-$pagado);
		$header = array("FECHA", "AGENTE", "No. VENTA", "CLIENTE", "TIPO", "CARRIER", "MONTO", "CARGOS", "DESCUENTO", "SEGURO" ,"TOTAL", "PAGADO", "SALDO");
		$footer = array("","","","","","","","","","TOTALES USD:",numFormat($total),numFormat($pagado),numFormat($total-$pagado));

        $xls->table($header, $data, $footer);
        $xls->Ln(2);

    }
	
}

if($resumen){
    $xls->title(array($title,"RESUMEN"));
    $xls->Ln();
    unset($header, $data, $footer);

	$header = array("AGENCIA","VENTAS","MONTO","PAGADO","SALDO");
	foreach($resumen as $value){
		$data[] = array($value["agencia"], $value["ventas"], numFormat($value["total"]), numFormat($value["tpagado"]), numFormat($value["tsaldo"]));
		$totales["total"] += $value["total"];
		$totales["tpagado"] += $value["tpagado"];
		$totales["tsaldo"] += $value["tsaldo"];
	}
	$footer = array("","",numFormat($totales["total"]),numFormat($totales["tpagado"]),numFormat($totales["tsaldo"]));

    $xls->table($header, $data, $footer);
    $xls->Ln(2);
}

$xls->xlsEOF();
exit();

?>