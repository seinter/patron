<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");


// PAYMENT GATEWAY
include('classes/payment.inc.php');


$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();


// VALIDATE SCOPE
if($id = $_REQUEST['id']){
    $filter = $user->id_central ? "agencias.id = {$user->id_agencia}" : $user->global ? 'TRUE' : "{$user->id_agencia} IN (agencias.id, agencias.id_central)";
    $str = "SELECT COUNT(*) FROM ventas INNER JOIN usuarios ON ventas.id_usuario = usuarios.id INNER JOIN agencias ON usuarios.id_agencia = agencias.id WHERE {$filter} AND ventas.id = {$id}";
    $res = mysql_query($str, $link);
    if(!mysql_result($res, 0)){
        header('Location: index.php');
        exit();
    }
}


switch($_REQUEST['dll']){
    case 1:

    //--- GRABAR LA VENTA---------------------------------------------------------------------------------------

        //RECEPTOR
        if($recepcion){
            if($recep_pais==225 || $recep_pais==136){
                $recep_direccion = $recep_direccion_usa;
                $recep_depto = 0;
            } else
                $recep_zip = "";

            if($id_recep){
                $str = "update receptores set nombre = '" . mysql_real_escape_string($recep_nombre) . "', id_pais = $recep_pais, direccion = '" . mysql_real_escape_string($recep_direccion) . "', id_depto = $recep_depto, zip = '$recep_zip' where id = $id_recep;";
                mysql_query($str, $link);
            } else {
                $str = "insert into receptores(id_cliente, nombre, id_pais, direccion, id_depto, zip) values($idc, '" . mysql_real_escape_string($recep_nombre) . "', $recep_pais, '" . mysql_real_escape_string($recep_direccion) . "', $recep_depto, '$recep_zip');";
                mysql_query($str, $link);
                $id_recep = mysql_insert_id();
            }
        } else
            $id_recep = 0;

        //DESTINATARIO
        if($dest_pais==225 || $dest_pais==136){
            $dest_direccion = $dest_direccion_usa;
            $dest_depto = 0;
        } else
            $dest_zip = "";

        if($id_dest){
            $str = "update destinatarios set nombre = '" . mysql_real_escape_string($dest_nombre) . "', telefono = '$dest_telefono', telefono2 = '$dest_telefono2', id_pais = $dest_pais, direccion = '" . mysql_real_escape_string($dest_direccion) . "', id_depto = $dest_depto, zip = '$dest_zip', comentarios = '" . mysql_real_escape_string($comentarios) . "' where id = $id_dest;";
            mysql_query($str, $link);
        } else {
            $str = "insert into destinatarios(id_cliente, nombre, telefono, telefono2, id_pais, direccion, id_depto, zip, comentarios) values($idc, '" . mysql_real_escape_string($dest_nombre) . "', '$dest_telefono', '$dest_telefono2', $dest_pais, '" . mysql_real_escape_string($dest_direccion) . "', $dest_depto, '$dest_zip', '" . mysql_real_escape_string($comentarios) . "');";
            mysql_query($str, $link);
            $id_dest = mysql_insert_id();
        }

        //ENTREGA
        if(!$entrega){
            $id_agencia = 0;
            $id_carrier = 0;
        }
        $id_agencia = $id_agencia ?: 0;
        $id_carrier = $id_carrier ?: 0;

        if(!$seguro)
            $pc_seguro = 0;

        if($id){
            $scs = 0;
            $str = "update ventas set id_recep = $id_recep, id_dest = $id_dest, seguro = '$seguro', recepcion = '$recepcion', entrega = '$entrega', id_agencia = $id_agencia, id_carrier = $id_carrier, pc_seguro = '$pc_seguro', declarado = '$monto_declarado', comentarios = '" . mysql_real_escape_string($comentariosv) . "' where id = $id;";
            mysql_query($str, $link);

        } else {
            $scs = 1;
            $idu = $idu?$idu:$user->id;
            //CREDITO
            if($id_tipo != 2)
                $id_credito = 0;
            $str = "insert into ventas(id_usuario, id_cliente, id_tipo, id_credito, id_recep, id_dest, origen, destino, fecha, cargos, descuento, seguro, recepcion, entrega, id_agencia, id_carrier, pc_seguro, declarado,comentarios) values($idu, $idc, $id_tipo, '$id_credito', $id_recep, $id_dest, $origen, $destino, curdate(), '$cargos', '$descuento', '$seguro', $recepcion, $entrega, $id_agencia, $id_carrier, '$pc_seguro', '$monto_declarado', '" . mysql_real_escape_string($comentariosv) . "');";
            mysql_query($str, $link);
            $id = mysql_insert_id();

            //DETALLE
            foreach($_POST as $key => $value){
                if(substr($key,0,3)=="idd"){
                    $i = substr($key,3,strlen($key)-3);
                    $str = "insert into detalle_venta(id_venta, id_clase, id_producto, descripcion, tarifa, cantidad, guias) values($id, ".$_POST["cat".$i].", ".$_POST["prd".$i].", '".$_POST["desc".$i]."', '".$_POST["tar".$i]."', '".$_POST["cnt".$i]."', '".$_POST["blt".$i]."');";
                    mysql_query($str, $link);
                    $idd = mysql_insert_id();

                    for ($x = 0; $x < $_POST["blt" . $i]; $x++) {
                        $str = "insert into bitacora(id_detalle, codigo) values($idd, " . getUniqueCode('bitacora') . ");";
                        mysql_query($str, $link);
                        $idb = mysql_insert_id();
                        $str = "insert into historial(id_bitacora, id_accion, id_usuario, fecha, hora) values($idb, 1, $idu, curdate(), curtime());";
                        mysql_query($str, $link);
                    }
                }
            }

            if($payment > 0){
                $card = $_POST['tarjeta'] ?: 0;
                $pending = $_POST['pending'] ?: 0;
                $str = "INSERT INTO pagos(id_venta, id_tipo, id_usuario, fecha, monto, costo, pendiente, referencia) VALUES({$id}, {$card}, {$user->id}, CURDATE(), " . (+$_POST['payment'] + +$_POST['fee']). ", " . +$_POST['fee'] . ", {$pending}, NULLIF('" . $_POST['invoice'] . "', ''))";
                mysql_query($str, $link);
            }

            $str = "update clientes set origen = $origen, destino = $destino where id = $idc;";
            mysql_query($str, $link);
        }

        if($recepcion){
            for($x=1;$x<=2;$x++){
                $str = "select id from agenda where id_venta = $id and operacion = $x and status;";
                $res = mysql_query($str, $link);
                if($row = mysql_fetch_row($res))
                    $str = "update agenda set fecha = '".$_POST["f".$x]."', hora = '".$_POST["hrs".$x].":".$_POST["min".$x].":00', observaciones = '".$_POST["observ".$x]."' where id = {$row[0]};";
                else
                    $str = "insert into agenda(id_venta, operacion, fecha, hora, observaciones) values($id, $x, '".$_POST["f".$x]."', '".$_POST["hrs".$x].":".$_POST["min".$x].":00', '".$_POST["observ".$x]."');";
                mysql_query($str, $link);
            }
        } else {
            $str = "update agenda set status = 0 where id_venta = $id;";
            mysql_query($str, $link);
        }

        header("Location: ?id={$id}" . ($scs ? "&scs=true" : ''));
        exit();

    break;
    case 2:
        //ELIMINAR DETALLE
        $str = "UPDATE detalle_venta LEFT JOIN bitacora ON detalle_venta.id = bitacora.id_detalle LEFT JOIN historial ON bitacora.id = historial.id_bitacora SET detalle_venta.status = 0, detalle_venta.id_autoriza = $idu, bitacora.status = 0, bitacora.id_autoriza = $idu, historial.status = 0, historial.id_autoriza = $idu WHERE detalle_venta.id = $idr;";
        mysql_query($str, $link);
        header("Location: ?id=$id");
        exit();
    break;
    case 3:
        //ELIMINAR PAGO
        $str = "UPDATE pagos SET status = 0, id_autoriza = {$idu} WHERE id = {$idr}";
        mysql_query($str, $link);
        header("Location: ?id=$id");
        exit();
    break;
    case 4:
        //ANULAR VENTA
        $str = "update ventas inner join detalle_venta on ventas.id = detalle_venta.id_venta inner join bitacora on detalle_venta.id = bitacora.id_detalle inner join historial on bitacora.id = historial.id_bitacora left join pagos on ventas.id = pagos.id_venta left join agenda on ventas.id = agenda.id_venta set ventas.status = 0, ventas.id_autoriza = $idu, detalle_venta.status = 0, detalle_venta.id_autoriza = $idu, bitacora.status = 0, bitacora.id_autoriza = $idu, historial.status = 0, historial.id_autoriza = $idu, pagos.status = 0, pagos.id_autoriza = $idu, agenda.status = 0 where ventas.id = $id;";
        mysql_query($str, $link);
        header("Location: ?id=$id");
        exit();
    break;
    case 5:
        //APLICAR PAGO
        $card = $_POST['tarjeta'] ?: 0;
        $pending = $_POST['pending'] ?: 0;
        $str = "INSERT INTO pagos(id_venta, id_tipo, id_usuario, fecha, monto, costo, pendiente, referencia) VALUES(" . $_POST['id'] . ", {$card}, {$user->id}, CURDATE(), " . (+$_POST['pago'] + +$_POST['fee']). ", " . +$_POST['fee'] . ", {$pending}, NULLIF('" . $_POST['invoice'] . "', ''))";
        mysql_query($str, $link);
        header("Location: ?id={$id}" . ($card ? "&scs=true" : ''));
        exit();
    break;




    /*
     8888888b.                    8888888b.           888      .d8888b.           888
     888   Y88b                   888   Y88b          888     d88P  Y88b          888
     888    888                   888    888          888     888    888          888
     888   d88P  8888b.  888  888 888   d88P  8888b.  888     888         8888b.  888888  .d88b.  888  888  888  8888b.  888  888
     8888888P"      "88b 888  888 8888888P"      "88b 888     888  88888     "88b 888    d8P  Y8b 888  888  888     "88b 888  888
     888        .d888888 888  888 888        .d888888 888     888    888 .d888888 888    88888888 888  888  888 .d888888 888  888
     888        888  888 Y88b 888 888        888  888 888     Y88b  d88P 888  888 Y88b.  Y8b.     Y88b 888 d88P 888  888 Y88b 888
     888        "Y888888  "Y88888 888        "Y888888 888      "Y8888P88 "Y888888  "Y888  "Y8888   "Y8888888P"  "Y888888  "Y88888
                              888                                                                                             888
                         Y8b d88P                                                                                        Y8b d88P
                          "Y88P"                                                                                          "Y88P"
    */
    case 12:

        // GET TOKEN
        $data = ['status' => false];
        if($token = Payment::getGatewayToken($user)) {

            $key = Payment::generateKey();

            $amount = +$_POST['amount'];
            $fee = 0;
            $card = false;
            if(+$_POST['card_id']){
                $card = Payment::getActiveMethod($user);
                if($card && +$card->id === +$_POST['card_id'])
                    $card->customer_id = formatAgencyVaultCode($card->customer_id);
                else {
                    http_response_code(412); // Precondition Failed (COULDN'T FETCH THE CARD)
                    exit();
                }

                $res = mysql_query("SELECT comision FROM agencias WHERE id = {$user->id_agencia}", $link);
                $row = mysql_fetch_object($res);
                $commission = $amount * $row->comision / 100;

            } else
                switch(GATEWAY_FEE_TYPE){
                    case 0:
                        $fee = +GATEWAY_FEE_VALUE;
                    break;
                    case 1:
                        $fee = $amount * +GATEWAY_FEE_VALUE / 100;
                    break;
                    default: // -1
                        $fee = Payment::calculateFee($token, $amount);
                    break;
                }

            $data = [
                'status' => true,
                'key' => $key,
                'amount' => $card ? ($amount - $commission) : $amount,
                'fee' => $fee,
                'vault' => !!$card, // BOOL
                'card' => $card,
                'gateway' => [
                    'url' => GATEWAY_API_URL . 'services/gateway_PaymentService/directInclusivePayment',
                    'token' => $token
                ]
            ];

        } else
            $data['message'] = 'La procesadora de cobros se encuentra fuera de servicio.';

        header('Content-Type: application/json');
        print json_encode($data);
        exit();

    break;
    case 14:
        // LOG TRANSACTION
        header('Content-Type: application/json');
        print json_encode(Payment::logTransaction($user, $_POST));
        exit();
    break;
    case 16:
        // WALLET
        $data = ['status' => false];
        if($user->cartera) {
            if ($card = Payment::getActiveMethod($user)) {
                if (Payment::getPendingBalance($user))
                    $data = array_merge($data, [
                            'pending' => true,
                            'message' => 'Cobro en efectivo desactivado, débito pendiente.'
                        ]);
                else
                    $data = [
                        'status' => true,
                        'card' => ['id' => $card->id]
                    ];
            } else
                $data['message'] = 'Debe configurar un medio de pago válido.';
        } else
            $data = [
                'status' => true,
                'card' => false
            ];

        header('Content-Type: application/json');
        print json_encode($data);
        exit();
    break;

}




//DATOS GENERALES
$str = "select id, localidad from localidades where status order by localidad;";
$res = mysql_query($str, $link);
while($row = mysql_fetch_row($res))
	$localidades[$row[0]] = $row[1];

$str = "select clasificacion.id, clasificacion.id_tipo, clase, icono from clasificacion left join productos on clasificacion.id = productos.id_clase and productos.status where clasificacion.status and (productos.id IS NOT NULL OR clasificacion.otros) group by clasificacion.id order by posicion DESC;";
$res = mysql_query($str, $link);
while($row = mysql_fetch_assoc($res))
	$clasificacion[array_shift($row)] = $row;

$res = mysql_query('SELECT id, tipo FROM tipo_ventas', $link);
while($row = mysql_fetch_object($res))
    $tipos[$row->id] = $row->tipo;

if($id){

	//--- RECUPERAR DATOS PARA EDICION---------------------------------------------------------------------------------------

	//VALIDACION DE VENTA
	$str = "SELECT ventas.*, u1.nombre usuario, agencias.agencia, u2.nombre anula, ventas.modificado, SUM(IFNULL(pagos.costo, 0)) costos FROM ventas INNER JOIN usuarios u1 ON ventas.id_usuario = u1.id INNER JOIN agencias ON u1.id_agencia = agencias.id LEFT JOIN usuarios u2 ON ventas.id_autoriza = u2.id LEFT JOIN pagos ON ventas.id = pagos.id_venta AND pagos.id_tipo = 1 AND pagos.status WHERE ventas.id = $id";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_assoc($res)){
		foreach($row as $key => $value)
			$sale[$key] = $value;
	} else {
		header("Location: index.php");
		exit();
	}

	$idc = $sale["id_cliente"];
	$origen = $sale["origen"];
	$destino = $sale["destino"];

	//CLIENTE
	$str = "select clientes.*, paises.pais from clientes inner join paises on clientes.id_pais = paises.id where clientes.id = $idc;";
	$res = mysql_query($str, $link);
	$row = mysql_fetch_assoc($res);
	foreach($row as $key => $value)
		$customer[$key] = $value;

	//ORIGEN Y DESTINO
	$str = "select paises.id, pais from paises inner join localidades on paises.id = localidades.id_pais where localidades.id = $origen;";
	$res = mysql_query($str,$link);
	$recep["id_pais"] = mysql_result($res,0,0);
	$recep["pais"] = mysql_result($res,0,1);
	$str = "select paises.id, pais from paises inner join localidades on paises.id = localidades.id_pais where localidades.id = $destino;";
	$res = mysql_query($str,$link);
	$dest["id_pais"] = mysql_result($res,0,0);
	$dest["pais"] = mysql_result($res,0,1);

	//RECEPCION
	if($sale["recepcion"]){
		$str = "select receptores.*, paises.pais from receptores inner join paises on receptores.id_pais = paises.id where receptores.id = {$sale[id_recep]} and receptores.status;";
		$res = mysql_query($str, $link);
		$row = mysql_fetch_assoc($res);
		foreach($row as $key => $value)
			$recep[$key] = $value;
	}

	//DESTINATARIO
	$str = "select destinatarios.*, paises.pais from destinatarios inner join paises on destinatarios.id_pais = paises.id where destinatarios.id = {$sale[id_dest]} and destinatarios.status;";
	$res = mysql_query($str, $link);
	$row = mysql_fetch_assoc($res);
	foreach($row as $key => $value)
		$dest[$key] = $value;

	//DETALLE DE LA VENTA

	$str = "SELECT detalle_venta.id_clase, productos.id id_prod, IF(productos.id, productos.producto, detalle_venta.descripcion) producto, detalle_venta.id idd, tarifa, cantidad, guias from detalle_venta left join productos ON productos.id = detalle_venta.id_producto WHERE id_venta = $id AND detalle_venta.status;";
	$res = mysql_query($str, $link);
	$i = 0;
	while($row = mysql_fetch_assoc($res)){
		$i++;
		foreach($row as $key => $value){
			$cod[$i][$key] = htmlspecialchars($value);
		}
	}

	//AGENDA
	if($sale["recepcion"]){
		$str = "select operacion op, if(fecha,fecha,'') f, hour(hora) hrs, minute(hora) min, observaciones observ from agenda where id_venta = $id and status order by operacion;";
		$res = mysql_query($str, $link);
		while($row = mysql_fetch_assoc($res)){
			foreach($row as $key =>$value)
				$agenda[$key.$row["op"]] = $value;
		}
	}

	$str = "select sum(monto) from pagos where id_venta = {$sale[id]} and status;";
	$res = mysql_query($str, $link);
	$pagado = mysql_result($res, 0,0);

} else {

	//--- NUEVA VENTA

	//VALIDACION DEL CLIENTE

    if(!$idc){
        header("Location: index.php");
        exit();
    }

    //CLIENTE
    $str = "select clientes.*, paises.pais from clientes inner join paises on clientes.id_pais = paises.id where clientes.id = $idc;";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_assoc($res)){
		foreach($row as $key => $value)
			$customer[$key] = $value;
	} else {
		header("Location: index.php");
		exit();
	}


	//ORIGEN Y DESTINO
	$origen = $customer["origen"] ? $customer["origen"] : $origen;
	$destino = $customer["destino"] ? $customer["destino"] : $destino;

	$origen = $origen ? $origen : $user->origen;

	if(!$origen){
		$origen = key($localidades);
	}

	if(!$destino){
		do {
			$destino = key($localidades);
			next($localidades);
		} while($origen == $destino);
		reset($localidades);
	}

	$str = "select paises.id, pais from paises inner join localidades on paises.id = localidades.id_pais where localidades.id = $origen;";
	$res = mysql_query($str,$link);
	$recep["id_pais"] = mysql_result($res,0,0);
	$recep["pais"] = mysql_result($res,0,1);
	$str = "select paises.id, pais from paises inner join localidades on paises.id = localidades.id_pais where localidades.id = $destino;";
	$res = mysql_query($str,$link);
	$dest["id_pais"] = mysql_result($res,0,0);
	$dest["pais"] = mysql_result($res,0,1);

}

//DIRECCION DEL CLIENTE
switch($customer["id_pais"]){
    case 225:
        $str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$customer[zip]}';";
        $res = mysql_query($str, $link);
        if($row = mysql_fetch_row($res))
            $customer["direccion2"] = $row[0].", ".$row[1]." ".$customer["zip"];
    break;
    case 136:
        $str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$customer[zip]}';";
        $res = mysql_query($str, $link);
        if($row = mysql_fetch_row($res))
            $customer["direccion2"] = $row[0].", ".$row[1];
    break;
    default:
        $str = "select departamento from departamentos where id = {$customer[id_depto]};";
        $res = mysql_query($str, $link);
        if($row = mysql_fetch_row($res))
            $customer["direccion2"] = $row[0];
    break;
}

// SCHEDULE FIELDS
$schedule = ['f1', 'f2', 'hrs1', 'hrs2', 'min1', 'min2', 'observ1', 'observ2'];


/*

    888    888 888                  888
    888    888 888                  888
    888    888 888                  888
    8888888888 888888 88888b.d88b.  888
    888    888 888    888 "888 "88b 888
    888    888 888    888  888  888 888
    888    888 Y88b.  888  888  888 888
    888    888  "Y888 888  888  888 888

*/

?>
<?php include 'header.php' ?>
<style>
    @media (min-width: 768px) {
        .col-md-left {
            padding-right: 5px;
        }
        .col-md-right {
            padding-left: 5px;
        }

        .col-w-50-left {
            padding-right: 0;
            padding-left: 10px;
        }
        .col-w-50-right {
            padding-left: 0;
            padding-right: 10px;
        }

        #div_agencias {
            padding-top: 0 !important;
        }
    }

    @media (max-width: 767px) {
        .col-w-50-left, .col-w-50-right {
            padding: 0 10px;
        }
    }
    .row-col-w-50 {
        margin: 0 -10px;
    }

    @media (min-width: 576px) {
        .text-cell {
            line-height: 30px;
        }
        .col-sm-left {
            padding-right: 5px;
        }
        .col-sm-right {
            padding-left: 5px;
        }
    }

    .receptor th {
        background-color: #ff9a53;
    }
    .destinatario  th {
        background-color: #7bafb1;
    }
    .info th {
        background-color: rgb(214, 216, 217);
        color: rgb(108, 117, 125);
    }
    .resumen th {
        font-weight: bold;
    }

    .icon {
        font-size: 18px;
        color: #002252;
    }
    th .icon {
        color: #FFF;
    }
    .info, .resumen {
        font-size: 1.1em;
    }

</style>

<form id="sale_form" name="sale_form" method="post" action="?" onsubmit="return false">
    <input type="hidden" name="dll" value="1" />
    <input type="hidden" name="idc" id="idc" value="<?php echo $idc ?>" />
    <input type="hidden" name="id" id="id" value="<?php echo $id ?:0 ?>" />

<?php foreach($schedule as $value){ ?>
    <input type="hidden" id="<?php echo $value ?>" name="<?php echo $value ?>" value="<?php echo htmlspecialchars($agenda[$value]) ?>" />
<?php } ?>

    <input type="hidden" name="id_card" id="id_card" value="0" />

<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right caption"><?php echo $sale["id"] ? "VENTA No. <span id='code'>" . formatCode($sale[id]) . "</span>: <span id='fecha'>" . $sale["fecha"] . "</span>" : "NUEVA VENTA" ?></div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">

                <table width="100%" border="0" cellspacing="10" cellpadding="0">

                <?php if(!$sale["id"] || $sale["status"]){ ?>

                    <tr>
                        <td style="padding:0">

                            <div class="alert alert-primary big-box">

                                <div class="row">
                                    <div class="col-sm-left col-12 col-sm-6 big-combo">
                                        <h6>ORIGEN:</h6>

                                        <select name="origen" id="origen" style="width:100%;" onchange="setRecep();"<?php echo $sale["id"]?" disabled=\"disabled\"":"" ?>>
                                            <?php
                                            $str = "select paises.id, pais from paises inner join localidades on paises.id = localidades.id_pais where localidades.status group by paises.id order by pais;";
                                            $res = mysql_query($str, $link);
                                            while($row = mysql_fetch_row($res)){
                                            ?>
                                                <optgroup style="background-color:#7DAAEE; color:#FFFFFF" label="<?php echo $row[1] ?>">
                                                    <?php
                                                    $str = "select id, localidad from localidades where id_pais = {$row[0]} and status order by localidad";
                                                    $res2 = mysql_query($str, $link);
                                                    while($row2 = mysql_fetch_row($res2)){
                                                    ?>
                                                        <option value="<?php echo $row2[0] ?>"<?php echo $row2[0]==$origen?" selected":"" ?>><?php echo $row2[1] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </optgroup>
                                            <?php
                                            }
                                            ?>
                                        </select>

                                    </div>
                                    <div class="col-sm-right col-12 col-sm-6 big-combo">
                                        <div class="d-sm-none" style="height:10px"></div>
                                        <h6>DESTINO:</h6>

                                        <div id="div_dest">
                                            <select name="destino" id="destino" style="width:100%;" onchange="setDest();"<?php echo $sale["id"]?" disabled=\"disabled\"":"" ?>>
                                                <?php
                                                $str = "select paises.id, pais from paises inner join localidades on paises.id = localidades.id_pais where localidades.id != $origen and localidades.status group by paises.id order by pais;";
                                                $res = mysql_query($str, $link);
                                                while($row = mysql_fetch_row($res)){
                                                ?>
                                                    <optgroup style="background-color:#7DAAEE; color:#FFFFFF" label="<?php echo $row[1] ?>">
                                                        <?php
                                                        $str = "select id, localidad from localidades where id_pais = {$row[0]} and id != $origen and status order by localidad";
                                                        $res2 = mysql_query($str, $link);
                                                        $bg = "";
                                                        while($row2 = mysql_fetch_row($res2)){
                                                            $bg = $bg=="#FFFFFF"?"#F1F1F1":"#FFFFFF";
                                                            ?>
                                                            <option style="background-color:<?php echo $bg ?>; color:#333333" value="<?php echo $row2[0] ?>"<?php echo $row2[0]==$destino?" selected":"" ?>><?php echo $row2[1] ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </optgroup>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </td>
                    </tr>

                <?php
                    /*
                         .d8888b.  888 d8b                   888
                        d88P  Y88b 888 Y8P                   888
                        888    888 888                       888
                        888        888 888  .d88b.  88888b.  888888  .d88b.
                        888        888 888 d8P  Y8b 888 "88b 888    d8P  Y8b
                        888    888 888 888 88888888 888  888 888    88888888
                        Y88b  d88P 888 888 Y8b.     888  888 Y88b.  Y8b.
                         "Y8888P"  888 888  "Y8888  888  888  "Y888  "Y8888
                    */
                ?>

                    <tr>
                        <td>

                            <div id="customer-box" class="collapse <?php echo $sale['id'] ? 'show' : '' ?>" style="position:relative">
                                <button type="button" id="customer-maximize" class="box-control">
                                    <i class="fa fa-window-maximize"></i>
                                </button>

                                <table class="data-form" style="margin-bottom:-3px">
                                    <caption>CLIENTE / RECEPCION</caption>

                                    <tr>
                                        <th>Cliente:</th>
                                        <td class="text-cell" width="30%"><?php echo formatClientCode($customer["id"]) ?> - <span id="div_cliente"><?php echo $customer['nombre'] . ' ' . $customer['apellido'] ?></span></td>
                                        <th>Teléfono(s):</th>
                                        <td class="text-cell"><?php echo $customer["telefono"].($customer["telefono2"]?" / ".$customer["telefono2"]:"") ?></td>
                                    </tr>

                                    <tr class="receptor" <?php echo !$sale["recepcion"] ? 'style="display:none"' : '' ?> >
                                        <th>Contacto:</th>
                                        <td class="text-cell"><div id="div_recep_nombre"><?php echo $sale["recepcion"] ? $recep["nombre"] : '&nbsp;- - - - -' ?></div></td>
                                        <th>País:</th>
                                        <td class="text-cell"><div class="div_recep_pais"><?php echo $recep["pais"] ?></div></td>
                                    </tr>
                                    <tr class="receptor" <?php echo !$sale["recepcion"] ? 'style="display:none"' : '' ?> >
                                        <td colspan="4" class="text-center">
                                            <button type="button" class="btn btn-sm btn-light save" onclick="schedule()"><i class="fa fa-calendar-check-o" style="margin-right:5px"></i> AGENDA</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div id="customer" class="collapse <?php echo !$sale['id'] ? 'show' : '' ?>" style="position:relative">
                                <button type="button" id="customer-minimize" class="box-control">
                                    <i class="fa fa-window-minimize"></i>
                                </button>

                            <table class="data-form" style="margin-bottom:-3px">
                                <caption>CLIENTE / RECEPCION</caption>

                                <tr>
                                    <th>Cliente:</th>
                                    <td class="text-cell" width="30%"><?php echo formatClientCode($customer["id"]) ?> - <?php echo $customer["nombre"]." ".$customer["apellido"]; ?></td>
                                    <th>Teléfono(s):</th>
                                    <td class="text-cell"><?php echo $customer["telefono"].($customer["telefono2"]?" / ".$customer["telefono2"]:"") ?></td>
                                </tr>
                                <tr>
                                    <th>Nacionalidad:</th>
                                    <td class="text-cell"><?php echo $customer["pais"] ?></td>
                                    <th rowspan="2" valign="top">Dirección:</th>
                                    <td rowspan="2" valign="baseline" style="line-height:20px;">
                                        <div class="d-none d-md-block" style="height:5px"></div>
                                        <?php echo $customer["direccion"] . ($customer["direccion2"] ? '<br>' . $customer["direccion2"] : '') ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Recepción:</th>
                                    <td>

                                        <div class="row">
                                            <div class="col-5" style="white-space:nowrap">
                                                <input id="recepcion_ag" name="recepcion" type="radio" class="forOption" onclick="showRecep(0)" value="0"<?php echo !$sale["recepcion"]?" checked":""; ?> />
                                                <label for="recepcion_ag">Agencia</label>
                                            </div>
                                            <div class="col-6" style="white-space:nowrap">
                                                <input id="recepcion_dir" name="recepcion" type="radio" class="forOption" onclick="showRecep(1)" value="1"<?php echo $sale["recepcion"]?" checked":""; ?> />
                                                <label for="recepcion_dir">Direcci&oacute;n</label>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                <tr class="receptor" <?php echo !$sale["recepcion"] ? 'style="display:none"' : '' ?> >
                                    <th>Contacto:</th>
                                    <td>

                                        <div id="div_receps">
                                            <select name="id_recep" id="id_recep" style="width:100%" onchange="getRecep(this.value)">
                                                <option value="0">- - - - NUEVO CONTACTO - - - -</option>
                                                <?php
                                                $str = "select id, nombre from receptores where id_cliente = $idc and id_pais = {$recep[id_pais]} order by nombre;";
                                                $result = mysql_query($str, $link);
                                                while($row=mysql_fetch_row($result))
                                                    echo "<option value=\"" . $row[0] . "\"".($row[0]==$sale["id_recep"]?" selected":"").">" . htmlspecialchars($row[1]) . "</option>\n";
                                                ?>
                                            </select>
                                        </div>

                                    </td>
                                </tr>
                                <tr class="receptor" <?php echo !$sale["recepcion"] ? 'style="display:none"' : '' ?> >
                                    <th>Nombre:</th>
                                    <td>
                                        <input name="recep_nombre" type="text" id="recep_nombre" value="<?php echo htmlspecialchars($recep["nombre"]) ?>" maxlength="100" />
                                    </td>
                                    <th>País:</th>
                                    <td class="text-cell">
                                        <input type="hidden" name="recep_pais" id="recep_pais" value="<?php echo $recep["id_pais"] ?>" />
                                        <div class="div_recep_pais"><?php echo $recep["pais"] ?></div>
                                    </td>
                                </tr>

                                <tr class="receptor direcc_usa" <?php echo !$sale["recepcion"] || !in_array($recep["id_pais"], [225, 136]) ? 'style="display:none"' : '' ?> >
                                    <th>Zip Code:</th>
                                    <td>
                                        <input name="recep_zip" type="text" id="recep_zip" style="max-width:200px" onkeydown="loadRecepZipData(event, 1)" onkeyup="loadRecepZipData(event)" value="<?php echo $recep["zip"] ?>" maxlength="5" />
                                    </td>
                                    <th>Estado:</th>
                                    <td class="text-cell">
                                        <div id="div_recep_estado"><?php echo getState($recep["id_pais"],$recep["zip"]) ?></div>
                                    </td>
                                </tr>
                                <tr class="receptor direcc_usa" <?php echo !$sale["recepcion"] || !in_array($recep["id_pais"], [225, 136]) ? 'style="display:none"' : '' ?> valign="top">
                                    <th>Dirección:</th>
                                    <td>
                                        <textarea name="recep_direccion_usa" id="recep_direccion_usa" style="height:50px"><?php echo htmlspecialchars($recep["direccion"]) ?></textarea>
                                    </td>
                                    <th>Ciudad:</th>
                                    <td class="text-cell">
                                        <div id="div_recep_ciudad"><?php echo getCity($recep["id_pais"], $recep["zip"]) ?></div>
                                    </td>
                                </tr>

                                <tr class="receptor direcc" <?php echo !$sale["recepcion"] || in_array($recep["id_pais"], [225, 136]) ? 'style="display:none"' : '' ?> valign="top">
                                    <th>Dirección</th>
                                    <td>
                                        <textarea name="recep_direccion" id="recep_direccion" style="height:50px"><?php echo htmlspecialchars($recep["direccion"]) ?></textarea>
                                    </td>
                                    <th>Departamento:</th>
                                    <td>
                                        <div id="div_recep_depto">
                                            <select name="recep_depto" id="recep_depto" style="width:100%">
                                                <?php
                                                $str = "select id, departamento from departamentos where id_pais = ".$recep["id_pais"]." order by departamento;";
                                                $res = mysql_query($str, $link);
                                                while($row = mysql_fetch_row($res)){
                                                ?>
                                                    <option value="<?php echo $row[0] ?>"<?php echo $row[0]==$recep["id_depto"]?" selected":"" ?>><?php echo $row[1] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                </tr>

                                <tr class="receptor" <?php echo !$sale["recepcion"] ? 'style="display:none"' : '' ?> >
                                    <td colspan="4" class="text-center">
                                        <button type="button" class="btn btn-sm btn-light save" onclick="schedule()"><i class="fa fa-calendar-check-o" style="margin-right:5px"></i> AGENDA</button>
                                    </td>
                                </tr>
                            </table>

                            </div>

                        </td>
                    </tr>

                <?php
                    /*
                        8888888b.                    888    d8b                   888                     d8b
                        888  "Y88b                   888    Y8P                   888                     Y8P
                        888    888                   888                          888
                        888    888  .d88b.  .d8888b  888888 888 88888b.   8888b.  888888  8888b.  888d888 888  .d88b.
                        888    888 d8P  Y8b 88K      888    888 888 "88b     "88b 888        "88b 888P"   888 d88""88b
                        888    888 88888888 "Y8888b. 888    888 888  888 .d888888 888    .d888888 888     888 888  888
                        888  .d88P Y8b.          X88 Y88b.  888 888  888 888  888 Y88b.  888  888 888     888 Y88..88P
                        8888888P"   "Y8888   88888P'  "Y888 888 888  888 "Y888888  "Y888 "Y888888 888     888  "Y88P"
                    */
                ?>

                    <tr>
                        <td>
                            <div id="receiver-box" class="collapse <?php echo $sale['id'] ? 'show' : '' ?>" style="position:relative">
                                <button type="button" id="receiver-maximize" class="box-control">
                                    <i class="fa fa-window-maximize"></i>
                                </button>

                                <table class="data-grid form-responsive">
                                    <caption>DESTINATARIO</caption>
                                    <tr class="destinatario">
                                        <th>Nombre:</th>
                                        <td class="text-cell" width="30%"><div id="div_dest_nombre"><?php echo $sale["id_dest"] ? $dest["nombre"] : '&nbsp;- - - - -' ?></div></td>
                                        <th>Teléfono:</th>
                                        <td class="text-cell"><div id="div_dest_telefono"><?php echo $sale["id_dest"] ? $dest["telefono"] : '&nbsp;- - - - -' ?></div></td>
                                    </tr>
                                </table>

                            </div>
                            <div id="receiver" class="collapse <?php echo !$sale['id'] ? 'show' : '' ?>" style="position:relative">
                                <button type="button" id="receiver-minimize" class="box-control">
                                    <i class="fa fa-window-minimize"></i>
                                </button>

                                <table class="data-table form-responsive">
                                    <caption>DESTINATARIO</caption>

                                    <tr class="destinatario">
                                        <th>Destinatario:</th>
                                        <td width="30%">
                                            <div id="div_dests">
                                                <select name="id_dest" id="id_dest" onchange="getDest(this.value)" style="width:100%">
                                                    <option value="0">- - - - NUEVO DESTINATARIO - - - -</option>
                                                    <?php
                                                    $str = "select id, nombre from destinatarios where id_cliente = $idc and id_pais = {$dest[id_pais]} order by nombre;";
                                                    $result = mysql_query($str, $link);
                                                    while($row=mysql_fetch_row($result))
                                                        echo "<option value=\"" . $row[0] . "\"".($row[0]==$sale["id_dest"]?" selected":"").">" . htmlspecialchars($row[1]) . "</option>\n";
                                                    ?>
                                                </select>
                                            </div>
                                        </td>
                                        <th>País:</th>
                                        <td class="text-cell">
                                            <input name="dest_pais" id="dest_pais" type="hidden" value="<?php echo $dest["id_pais"] ?>" />
                                            <div id="div_dest_pais">&nbsp;<?php echo $dest["pais"] ?></div></td>
                                        </td>
                                    </tr>

                                    <tr class="destinatario">
                                        <th>Nombre:</th>
                                        <td>
                                            <input name="dest_nombre" type="text" id="dest_nombre" value="<?php echo htmlspecialchars($dest["nombre"]) ?>" maxlength="100" />
                                        </td>
                                        <th>Teléfono(s):</th>
                                        <td>
                                            <div class="row">
                                                <div class="col pr-0">
                                                    <input name="dest_telefono" type="text" id="dest_telefono" value="<?php echo $dest["telefono"]; ?>" class="phone-input" />
                                                </div>
                                                <div class="col pl-1">
                                                    <input name="dest_telefono2" type="text" id="dest_telefono2" value="<?php echo $dest["telefono2"]; ?>" class="phone-input" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="destinatario direcc_usa" <?php echo !in_array($dest["id_pais"], [225, 136]) ? 'style="display:none"' : '' ?> >
                                        <th>Zip Code:</th>
                                        <td>
                                            <input name="dest_zip" type="text" id="dest_zip" style="max-width:200px" onkeydown="loadDestZipData(event, 1)" onkeyup="loadDestZipData(event)" value="<?php echo $dest["zip"] ?>" maxlength="5" />
                                        </td>
                                        <th>Estado:</th>
                                        <td class="text-cell">
                                            <div id="div_dest_estado"><?php echo getState($dest["id_pais"],$dest["zip"]) ?></div>
                                        </td>
                                    </tr>

                                    <tr class="destinatario direcc_usa" <?php echo !in_array($dest["id_pais"], [225, 136]) ? 'style="display:none"' : '' ?> valign="top">
                                        <th>Dirección:</th>
                                        <td>
                                            <textarea name="dest_direccion_usa" rows="2" id="dest_direccion_usa" style="height:50px"><?php echo htmlspecialchars($dest["direccion"]) ?></textarea>
                                        </td>
                                        <th>Ciudad:</th>
                                        <td class="text-cell">
                                            <div id="div_dest_ciudad"><?php echo getCity($dest["id_pais"],$dest["zip"]) ?></div>
                                        </td>
                                    </tr>

                                    <tr class="destinatario direcc" <?php echo in_array($dest["id_pais"], [225, 136]) ? 'style="display:none"' : '' ?> valign="top">
                                        <th>Dirección:</th>
                                        <td>
                                            <textarea name="dest_direccion" id="dest_direccion" style="height:50px"><?php echo htmlspecialchars($dest["direccion"]) ?></textarea>
                                        </td>
                                        <th>Departamento:</th>
                                        <td>
                                            <div id="div_dest_depto">
                                                <?php if($dest["pais"]!=225&&$dest["pais"]!=136){ ?>
                                                    <select name="dest_depto" id="dest_depto" style="width:100%" onchange="setDeliver()">
                                                        <?php
                                                        $str = "select id, departamento from departamentos where id_pais = ".$dest["id_pais"]." order by departamento;";
                                                        $res = mysql_query($str, $link);
                                                        while($row = mysql_fetch_row($res)){
                                                            ?>
                                                            <option value="<?php echo $row[0] ?>"<?php echo $row[0]==$dest["id_depto"]?" selected":"" ?>><?php echo $row[1] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="destinatario" valign="top">
                                        <th>Comentarios:</th>
                                        <td colspan="3">
                                            <textarea name="comentarios" id="comentarios" style="height:50px"><?php echo htmlspecialchars($dest["comentarios"]) ?></textarea>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </td>
                    </tr>


                <?php
                /*
                    88888888888                        888      d8b
                        888                            888      Y8P
                        888                            888
                        888  888d888  8888b.   .d8888b 888  888 888 88888b.   .d88b.
                        888  888P"       "88b d88P"    888 .88P 888 888 "88b d88P"88b
                        888  888     .d888888 888      888888K  888 888  888 888  888
                        888  888     888  888 Y88b.    888 "88b 888 888  888 Y88b 888
                        888  888     "Y888888  "Y8888P 888  888 888 888  888  "Y88888
                                                                                  888
                                                                             Y8b d88P
                                                                              "Y88P"

                                    <?php if($sale["id"]){ ?>
										<table border="0" cellpadding="0" cellspacing="0" class="dataBlueTable" style="margin-top:3px">
											<tr class="forMWCaption">
												<td width="150" bgcolor="#538AC1"><div align="right">TRACKING No.:&nbsp;&nbsp;</div></td>
												<td bgcolor="#739FCC">&nbsp;<a class="forMWCaption" href="#" onclick="createWindow('rastreo_venta.php?dll=1&qt=1&q=<?php echo formatTracking($sale["id"]) ?>','wndTraking',700,400,'yes')"><?php echo formatTracking($sale["id"]) ?></a></td>
											</tr>
										</table>
                                    <?php } ?>
                */
                ?>

                <?php
                /*
                    8888888888          888
                    888                 888
                    888                 888
                    8888888    88888b.  888888 888d888  .d88b.   .d88b.   8888b.
                    888        888 "88b 888    888P"   d8P  Y8b d88P"88b     "88b
                    888        888  888 888    888     88888888 888  888 .d888888
                    888        888  888 Y88b.  888     Y8b.     Y88b 888 888  888
                    8888888888 888  888  "Y888 888      "Y8888   "Y88888 "Y888888
                                                                     888
                                                                Y8b d88P
                                                                 "Y88P"
                */
                ?>

                    <tr>
                        <td style="padding:0">

                            <div class="alert alert-primary big-box">

                                <div class="row">
                                    <div class="col-md-left col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-3 text-md-center"><h5 style="margin:2px 0 2px 2px">ENTREGA</h5></div>
                                            <div class="col-3" style="white-space:nowrap">
                                                <input id="entrega_dir" name="entrega" type="radio" onclick="setDeliver()" value="0"<?php echo !$sale["entrega"]?" checked":""; ?> />
                                                <label for="entrega_dir">Dirección</label>
                                            </div>
                                            <div class="col-3" style="white-space:nowrap">
                                                <input id="entrega_ag" name="entrega" type="radio" value="1"<?php echo $sale["entrega"]==1?" checked":""; ?> onclick="setDeliver()" />
                                                <label for="entrega_ag">Agencia</label>
                                            </div>
                                            <div class="col-3" style="white-space:nowrap">
                                                <input id="entrega_carr" name="entrega" type="radio" value="2"<?php echo $sale["entrega"]==2?" checked":""; ?> onclick="setDeliver()" />
                                                <label for="entrega_carr">PAQ-NAC</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-right col-12 col-md-6">

                                        <div id="div_agencias" style="padding-top:8px; <?php echo !$sale['entrega'] ? 'display:none;' : '' ?>" >
                                            <?php if($sale["entrega"]==1){ ?>
                                                <select name="id_agencia" id="id_agencia" style="width:100%">
                                                    <?php
                                                    $str = "select id, agencia from agencias where id_pais = {$dest[id_pais]} and status order by agencia;";
                                                    $res = mysql_query($str, $link);
                                                    while($row = mysql_fetch_row($res)){
                                                        $str = "select id, id_central, agencia from agencias where (id = {$row[0]} or id_central = {$row[0]}) and status order by id_central, agencia;";
                                                        $res1 = mysql_query($str, $link);
                                                        if(mysql_num_rows($res1)){
                                                            ?>
                                                            <optgroup style="background-color:#7DAAEE; color:#FFFFFF" label="<?php echo $row[1] ?>">
                                                                <?php
                                                                while($row1 = mysql_fetch_row($res1)){
                                                                    $bg = $bg=="#FFFFFF"?"#F1F1F1":"#FFFFFF";
                                                                    ?>
                                                                    <option style="background-color:<?php echo $bg ?>; color:#333333" value="<?php echo $row1[0] ?>"<?php echo $row1[0]==$sale["id_agencia"]?" selected":""?>><?php echo ($row1[1]?" - - - > ":"").$row1[2] ?></option>
                                                                <?php		} ?>
                                                            </optgroup>
                                                            <?php
                                                        }

                                                    }
                                                    ?>
                                                </select>
                                            <?php } elseif($sale["entrega"]==2){ ?>
                                                <select name="id_carrier" id="id_carrier" style="width:100%">
                                                    <?php
                                                    $ids = 0;
                                                    switch($dest["id_pais"]){
                                                        case 225:
                                                            $str = "select id_estado from zips where zip = '$dest[zip]';";
                                                            $res1 = mysql_query($str, $link);
                                                            if($row1 = mysql_fetch_row($res1))
                                                                $ids = $row1[0];
                                                            break;
                                                        case 136:
                                                            $str = "select id_estado from zipsmx where zip = '$dest[zip]';";
                                                            $res1 = mysql_query($str, $link);
                                                            if($row1 = mysql_fetch_row($res1))
                                                                $ids = $row1[0];
                                                            break;
                                                        default:
                                                            $ids = $dest["depto"];
                                                            break;
                                                    }
                                                    $str = "select id, carrier from carriers where id_pais = {$dest[id_pais]} and status;";
                                                    $res = mysql_query($str, $link);
                                                    while($row = mysql_fetch_row($res)){
                                                        $str = "select id, agencia from agencias_carriers where id_carrier = {$row[0]} and id_estado = $ids and status order by agencia;";
                                                        $res1 = mysql_query($str, $link);
                                                        if(mysql_num_rows($res1)){
                                                            ?>
                                                            <optgroup style="background-color:#7DAAEE; color:#FFFFFF" label="<?php echo $row[1] ?>">
                                                                <?php
                                                                while($row1 = mysql_fetch_row($res1)){
                                                                    $bg = $bg=="#FFFFFF"?"#F1F1F1":"#FFFFFF";
                                                                    ?>
                                                                    <option style="background-color:<?php echo $bg ?>; color:#333333" value="<?php echo $row1[0] ?>"<?php echo $row1[0]==$sale["id_carrier"]?" selected":""?>><?php echo $row1[1] ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </optgroup>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>
                                <div id="agencias_direcc" class="row" <?php echo !$sale["entrega"]? 'style="display:none"' : '' ?> >
                                    <div class="col-12">
                                        <hr style="margin:8px 0">
                                        <div id="div_agencias_direcc" class="bg-light" style="padding:5px">
                                            <?php
                                            if($sale["id"]){
                                                if($sale["entrega"]==1)
                                                    $str = "select direccion, telefono from agencias where id = {$sale[id_agencia]};";
                                                else
                                                    $str = "select direccion, telefono from agencias_carriers where id = {$sale[id_carrier]};";
                                                $res = mysql_query($str, $link);
                                                if($row = mysql_fetch_row($res))
                                                    print "Dirección: ".$row[0]." / Teléfono: ".$row[1];
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>



                        </td>
                    </tr>



                <?php
                /*
                     .d8888b.           888                                       d8b
                    d88P  Y88b          888                                       Y8P
                    888    888          888
                    888         8888b.  888888  .d88b.   .d88b.   .d88b.  888d888 888  8888b.  .d8888b
                    888            "88b 888    d8P  Y8b d88P"88b d88""88b 888P"   888     "88b 88K
                    888    888 .d888888 888    88888888 888  888 888  888 888     888 .d888888 "Y8888b.
                    Y88b  d88P 888  888 Y88b.  Y8b.     Y88b 888 Y88..88P 888     888 888  888      X88
                     "Y8888P"  "Y888888  "Y888  "Y8888   "Y88888  "Y88P"  888     888 "Y888888  88888P'
                                                             888
                                                        Y8b d88P
                                                         "Y88P"
                */
                ?>

                <?php if(!$sale["id"]){ ?>

                    <tr>
                        <td>

                            <div class="alert alert-dark big-box">
                                <div class="row">
                                    <div class="col clearfix">
                                        <div class="text-secondary pull-left"><h4>CLASIFICACION</h4></div>
                                        <div class="pull-right" style="margin:-3px">
                                        <?php foreach($clasificacion as $key => $value) { ?>
                                            <div class="icon-box pull-right" style="margin:3px" onclick="addTableNode(<?php echo $key ?>, <?php echo $value['id_tipo'] ?>)" rel="tipsy" title="<?php echo $value['clase'] ?>">
                                                <i class="fa fa-<?php echo $value['icono']; ?>"></i>
                                            </div>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>

                <?php } ?>


                <?php
                /*
                    8888888b.           888             888 888
                    888  "Y88b          888             888 888
                    888    888          888             888 888
                    888    888  .d88b.  888888  8888b.  888 888  .d88b.
                    888    888 d8P  Y8b 888        "88b 888 888 d8P  Y8b
                    888    888 88888888 888    .d888888 888 888 88888888
                    888  .d88P Y8b.     Y88b.  888  888 888 888 Y8b.
                    8888888P"   "Y8888   "Y888 "Y888888 888 888  "Y8888
                */
                ?>

                    <tr>
                        <td>

                            <table class="data-grid form-fields">
                                <caption>DETALLE</caption>
                                <tbody id="sTable">
                                <tr>
                                <?php if($sale['id']): ?>
                                    <th width="40"></th>
                                <?php endif ?>
                                    <th width="40"></th>
                                    <th>PRODUCTO</th>
                                    <th width="80">TARIFA USD</th>
                                    <th width="80">CANTIDAD</th>
                                    <th width="80">GUIAS</th>
                                    <th width="80">SUB-TOTAL</th>
                                    <th width="50"></th>
                                </tr>
                                <?php

                                if($cod){
                                    foreach($cod as $key => $value){
                                        $x++;
                                        ?>
                                        <tr id="tr<?php echo $x ?>">
                                            <th width="40" class="text-center">
                                            <?php if(+$value["guias"]): ?>
                                            <div  style="cursor:pointer" onclick="showPrints(<?php echo $x ?>)">
                                                <i class="fa fa-th-list icon" title="GUIAS"></i>
                                            </div>
                                            <?php else: ?>
                                                <i class="fa fa-ellipsis-h"></i>
                                            <?php endif ?>
                                            </th>
                                            <td width="40" class="text-center">
                                                <i class="fa fa-<?php echo $clasificacion[$value["id_clase"]]['icono'] ?> icon" title="<?php echo $clasificacion[$value['id_clase']]['clase'] ?>"></i>
                                            </td>
                                            <td>
                                                <input name="idd<?php echo $x; ?>" type="hidden" id="idd<?php echo $x; ?>" value="<?php echo $value["idd"] ?>" />
                                                <?php echo $value["producto"] ?>
                                            </td>
                                            <td width="80"><input name="tar<?php echo $x; ?>" id="tar<?php echo $x; ?>" type="text" class="text-right" value="<?php echo numFormat($value["tarifa"]); ?>" readonly /></td>
                                            <td width="80"><input name="cnt<?php echo $x; ?>" id="cnt<?php echo $x; ?>" type="text" class="text-right" value="<?php echo $value["cantidad"] ?>" onchange="numInt(this,1); calculate(<?php echo $x ?>)" readonly /></td>
                                            <td width="80"><input name="blt<?php echo $x; ?>" id="blt<?php echo $x; ?>" type="text" class="text-right" value="<?php echo $value["guias"]; ?>" onchange="numInt(this,1)" readonly /></td>
                                            <td width="80"><input name="sub<?php echo $x; ?>" id="sub<?php echo $x; ?>" type="text" class="text-right" value="<?php echo number_format($sub = ($value["cantidad"] * $value["tarifa"]),2,'.',''); ?>" readonly /></td>
                                            <td width="50"><input type="button" class="btn btn-sm btn-danger delete" style="width:50px" value="- X -" onclick="confirmDeleteDetail(<?php echo $value["idd"] ?>,'<?php echo $value["producto"] ?>')" <?php echo count($cod) === 1 ? 'disabled' : '' ?> /></td>
                                        </tr>

                                        <?php if($sale["id"]){ ?>
                                            <tr id="prn_<?php echo $x ?>" style="display:none">
                                                <td colspan="8" style="padding:3px">
                                                    <table class="data-grid grid-compact table-orange">
                                                        <tr>
                                                            <th class="d-none d-md-table-cell">No. DE GUIA</th>
                                                            <th>CODIGO</th>
                                                            <th width="90"></th>
                                                            <th width="100"></th>
                                                            <th colspan="2" width="220">GUIAS</th>
                                                        </tr>
                                                        <?php
                                                        $str = "select id, codigo from bitacora where id_detalle = {$value[idd]} and status;";
                                                        $res1 = mysql_query($str, $link);
                                                        while($row1 = mysql_fetch_assoc($res1)){
                                                            ?>
                                                            <tr>
                                                                <td class="d-none d-md-table-cell"><?php echo formatCode($row1['id']) ?></td>
                                                                <td>
                                                                    <a class="btn-link" href="rastreo.php?dll=1&qt=2&q=<?php echo $row1['codigo'] ?>"><?php echo $row1['codigo'] ?></a>
                                                                <td width="90">
                                                                    <button type="button" class="btn btn-sm btn-outline-success" style="width:90px" rel="tipsy" title="Detalle de Contenido" onclick="setDetail(<?php echo $row1['id'] ?>)"><i class="fa fa-list-alt" style="margin-right:5px"></i>Detalle</button>
                                                                </td>
                                                                <td width="100">
                                                                    <button type="button" class="btn btn-sm btn-outline-dark" style="width:100px" rel="tipsy" title="Etiquetas" onclick="window.open('viewer.php?dll=15&id=<?php echo md5($row1['id']) ?>')"><i class="fa fa-sticky-note-o" style="margin-right:5px"></i>Etiquetas</button>
                                                                <td width="110">
                                                                    <button type="button" class="btn btn-sm btn-outline-dark" style="width:110px" rel="tipsy" title="Guía de Recepción" onclick="window.open('viewer.php?dll=13&id=<?php echo md5($row1['id']) ?>')"><i class="fa fa-file-text-o" style="margin-right:5px"></i>Recepción</button>
                                                                </td>
                                                                <td width="110">
                                                                    <button type="button" class="btn btn-sm btn-outline-dark" style="width:110px" rel="tipsy" title="Guía de Entrega" onclick="window.open('viewer.php?dll=14&id=<?php echo md5($row1['id']) ?>')"><i class="fa fa-file-text-o" style="margin-right:5px"></i>Entrega</button>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </table></td>
                                            </tr>
                                        <?php }

                                        $total += $sub;
                                    }

                                }
                                ?>
                                </tbody>
                            </table>

                        </td>
                    </tr>




                    <?php
                    /*

                         .d8888b.
                        d88P  Y88b
                        Y88b.
                         "Y888b.    .d88b.   .d88b.  888  888 888d888  .d88b.
                            "Y88b. d8P  Y8b d88P"88b 888  888 888P"   d88""88b
                              "888 88888888 888  888 888  888 888     888  888
                        Y88b  d88P Y8b.     Y88b 888 Y88b 888 888     Y88..88P
                         "Y8888P"   "Y8888   "Y88888  "Y88888 888      "Y88P"
                                                 888
                                            Y8b d88P
                                             "Y88P"
                    */
                    ?>

                    <tr>
                        <td style="padding:0">
                            <div class="alert alert-success big-box">
                                <div class="row">

                                    <div class=" col-md-left col-12 col-md-6">
                                        <div class="row">

                                            <div class="col-6 text-md-center" style="white-space:nowrap"><h5 style="margin:2px 0 2px 2px">MONTO DECLARADO</h5></div>
                                            <div class="col-5 offset-1 pl-0 form-fields">
                                                <input name="monto_declarado" type="text" class="text-right" id="monto_declarado" value="<?php echo numFormat($sale["declarado"]) ?>" onchange="this.value = numFormat(this.value); total()" <?php echo $user->id_central && $sale['id'] ? 'readonly' : '' ?> />
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-right col-12 col-md-6">
                                        <hr class="d-md-none" style="margin:8px 0">
                                        <div class="row">

                                            <div class="col-3 text-md-center"><h5 style="margin:2px 0 3px 2px">SEGURO</h5></div>
                                            <div class="col-2" style="white-space:nowrap">
                                                <input id="seguro_no" name="seguro" type="radio" value="0" onclick="<?php echo $user->id_central && $sale['id'] ? 'return false' : 'setEnsurance(0)' ?>" <?php echo !$sale["seguro"]?" checked":"" ?> />
                                                <label for="seguro_no">No</label>
                                            </div>
                                            <div class="col-2" style="white-space:nowrap">
                                                <input id="seguro_si" name="seguro" type="radio" value="1" onclick="<?php echo $user->id_central && $sale['id'] ? 'return false' : 'setEnsurance(1)' ?>" <?php echo $sale["seguro"]?" checked":"" ?> />
                                                <label for="seguro_si">Si</label>
                                            </div>
                                            <div id="div_seguro" class="col-5 pl-0 form-fields" <?php echo !$sale["seguro"] ? 'style="display:none"' : '' ?> >
                                                <div class="row">
                                                    <div class="col pr-1 position-relative">
                                                        <i class="fa fa-percent position-absolute mt-2 ml-2"></i>

                                                        <input name="pc_seguro" type="text" class="text-right" id="pc_seguro" value="<?php echo numFormat($sale["seguro"] ? $sale["pc_seguro"] : $_seguro) ?>" onchange="this.value = numFormat(this.value); total()" style="margin-bottom:-1px" <?php echo $user->id_central && $sale['id'] ? 'readonly' : '' ?> />
                                                    </div>
                                                    <div class="col pl-0 position-relative">
                                                        <i class="fa fa-angle-double-right position-absolute mt-2 ml-2"></i>
                                                        <input name="monto_seguro" type="text" class="text-right" id="monto_seguro" value="<?php echo numFormat($seguro = $sale["seguro"]?(($sale["declarado"]*$sale["pc_seguro"])/100):0) ?>" style="margin-bottom:-1px" readonly />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </td>
                    </tr>


                    <?php
                    /*
                        88888888888  .d88888b. 88888888888     d8888 888      8888888888  .d8888b.
                            888     d88P" "Y88b    888        d88888 888      888        d88P  Y88b
                            888     888     888    888       d88P888 888      888        Y88b.
                            888     888     888    888      d88P 888 888      8888888     "Y888b.
                            888     888     888    888     d88P  888 888      888            "Y88b.
                            888     888     888    888    d88P   888 888      888              "888
                            888     Y88b. .d88P    888   d8888888888 888      888        Y88b  d88P
                            888      "Y88888P"     888  d88P     888 88888888 8888888888  "Y8888P"
                    */
                    ?>

                    <tr>
                        <td>
                            <div class="row row-col-w-50">

                                <div class="col-12 col-md-6 col-w-50-left">
                                    <table class="data-table form-responsive-w-50">
                                        <caption>INFO</caption>
                                        <tr class="info">
                                            <th>NOTA:</th>
                                            <td>
                                                <input type="text" name="comentariosv" id="comentariosv" value="<?php echo htmlspecialchars($sale["comentarios"]) ?>" >
                                            </td>
                                        </tr>
                                        <tr class="info">
                                            <th>PEDIDO POR:</th>
                                            <td>
                                                <?php
                                                if($sale["id"]) {
                                                    echo $sale["agencia"]." / ".$sale["usuario"];
                                                } elseif (!$user->id_central) {
                                                    ?>
                                                    <select name="idu" id="idu" style="width:100%">
                                                        <option value="0">- - - - -</option>
                                                        <?php
                                                        $str = "select id, nombre from usuarios where id_agencia = {$user->id_agencia} and id != {$user->id} and status;";
                                                        $res = mysql_query($str, $link);
                                                        while($row = mysql_fetch_row($res)){
                                                            ?>
                                                            <option value="<?php echo $row[0] ?>"> &#8226; <?php echo $row[1] ?></option>
                                                            <?php
                                                        }

                                                        $str = "select agencias.id, agencia from agencias inner join usuarios on agencias.id = usuarios.id_agencia where id_central = {$user->id_agencia} and agencias.status and usuarios.status group by agencias.id order by agencia";
                                                        $res = mysql_query($str, $link);
                                                        while($row = mysql_fetch_row($res)){
                                                            ?>
                                                            <optgroup style="background-color:#7DAAEE; color:#FFFFFF" label="<?php echo $row[1] ?>">
                                                                <?php
                                                                $str = "select id, nombre from usuarios where id_agencia = {$row[0]} and status order by nombre;";
                                                                $ares = mysql_query($str, $link);
                                                                while($arow = mysql_fetch_row($ares)) {
                                                                    ?>
                                                                    <option style="background-color:<?php echo $bg ?>; color:#333333" value="<?php echo $arow[0] ?>"<?php echo $arow[0]==$idu?" selected":"" ?>><?php echo $arow[1] ?></option>
                                                                <?php } ?>
                                                            </optgroup>
                                                        <?php } ?>
                                                    </select>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <tr class="info">
                                            <th>TIPO DE VENTA:</th>
                                            <td>
                                                <select name="id_tipo" id="id_tipo" style="width:100%" onchange="setPayment(this.value)"<?php echo $sale["id"]?" disabled":"" ?>>
                                                    <?php
                                                    foreach($tipos as $key => $value)
                                                        echo "<option value=\"$key\"".($sale["id_tipo"]==$key?" selected":"").">$value</option>\n";
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr id="creditTable" <?php echo $sale["id_tipo"] !=2 ? 'style="display:none"' : '' ?>>
                                            <th style="background-color:#4F7DB0">A LA CUENTA DE:</th>
                                            <td>
                                                <select name="id_credito" id="id_credito" style="width:100%"<?php echo $sale["id"]?" disabled":"" ?>>
                                                    <?php
                                                    $str = "select id, carrier from carriers where credito and status order by carrier;";
                                                    $res = mysql_query($str, $link);
                                                    while($row = mysql_fetch_row($res)){
                                                        ?>
                                                        <option value="<?php echo $row[0] ?>" <?php echo $row[0]==$id_credito ? 'selected' : '' ?> ><?php echo $row[1] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>

                                    </table>
                                </div>

                                <?php
                                if($sale['id']) {
                                    // RESUME
                                    $total += $sale["cargos"] + $sale['costos'] + $seguro - $sale["descuento"];
                                    $saldo = $total - $pagado;
                                }
                                ?>
                                <div class="col-12 col-md-6 col-w-50-right">
                                    <table class="data-table form-responsive-w-50">
                                        <caption>RESUMEN</caption>
                                        <tr class="resumen">
                                            <th>OTROS CARGOS USD:</th>
                                            <td>
                                                <input name="cargos" type="text" class="text-right" id="cargos" value="<?php echo numFormat($sale['cargos'] + $sale['costos']); ?>" onchange="sub_cargos_desc(this.form)" <?php echo $sale['id'] ? 'readonly' : '' ?> />
                                            </td>
                                        </tr>
                                        <tr class="resumen">
                                            <th>DESCUENTO USD:</th>
                                            <td>
                                                <input name="descuento" type="text" class="text-right" id="descuento" value="<?php echo numFormat($sale["descuento"]); ?>" onchange="sub_cargos_desc(this.form)" <?php echo $sale['id'] ? 'readonly' : '' ?> />
                                            </td>
                                        </tr>
                                        <tr class="resumen">
                                            <th>TOTAL USD:</th>
                                            <td><input name="tot" type="text" class="text-right" id="tot" value="<?php echo numFormat($total); ?>" readonly /></td>
                                        </tr>
                                        <tr class="resumen">
                                            <th>PAGADO USD:</th>
                                            <td>
                                                <input name="payment" type="text" class="text-right" id="payment" value="<?php echo numFormat($pagado); ?>" onblur="this.value = numFormat(this.value); checkPay(this.form);" readonly />
                                            </td>
                                        </tr>

                                    <?php if($sale["id"]){ ?>
                                        <tr class="resumen">
                                            <th>SALDO USD:</th>
                                            <td class="text-cell text-right" style="font-weight:bold; color:#FFF; background-color:<?php echo $saldo ? '#FF0000' : '#3380E0' ?>; padding-right:10px;" ><?php echo numFormat($saldo) ?></td>
                                        </tr>
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="2" class="font-weight-bold bg-white" style="color:#006EBD; border:1px solid #7586A6">
                                                <div class="row pt-2 pb-2">
                                                    <div class="offset-2 col-3 text-nowrap text-right pr-0">
                                                        <input type="radio" name="tarjeta" id="cash" value="0" checked >
                                                        <label for="cash" class="d-inline-block m-0 ml-2 mr-2" style="cursor:pointer;">CASH</label>
                                                    </div>
                                                    <div class="col-7">
                                                        <input type="radio" name="tarjeta" id="card" value="1">
                                                        <label for="card" class="d-inline-block m-0 ml-2" style="cursor:pointer;">TARJETA DE CREDITO</label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                    </table>
                                </div>

                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="text-center">
                            <div class="text-center" style="background-color:#b4b4b4; padding:8px">
                            <?php if($sale["id"]){ ?>
                                <button type="button" class="btn btn-danger" style="width:250px" onclick="confirmDeleteSale()"><i class="fa fa-trash" style="margin-right:5px"></i> Anular Venta</button>
                                <button type="button" class="btn btn-primary" style="width:250px" onclick="validateModify(this.form)"><i class="fa fa-save" style="margin-right:5px"></i> Guardar Cambios</button>
                            <?php } else { ?>
                                <button type="button" id="btn-submit" class="btn btn-primary" style="width:250px"><i class="fa fa-save" style="margin-right:5px"></i> Guardar Venta</button>
                            <?php } ?>
                            </div>
                        </td>
                    </tr>


                <?php
                /*
                    8888888b.                                                    888
                    888   Y88b                                                   888
                    888    888                                                   888
                    888   d88P  8888b.  888  888 88888b.d88b.   .d88b.  88888b.  888888
                    8888888P"      "88b 888  888 888 "888 "88b d8P  Y8b 888 "88b 888
                    888        .d888888 888  888 888  888  888 88888888 888  888 888
                    888        888  888 Y88b 888 888  888  888 Y8b.     888  888 Y88b.
                    888        "Y888888  "Y88888 888  888  888  "Y8888  888  888  "Y888
                                             888
                                        Y8b d88P
                                         "Y88P"
                */
                ?>

                <?php if($sale["id"] && $sale["id_tipo"]!=2) { ?>

                    <tr>
                        <td>

                        <?php if(+$saldo > 0){ ?>

                            <table class="data-form">
                                <caption>APLICAR PAGO</caption>
                                <tr class="destinatario">
                                    <th>Monto USD:</th>
                                    <td width="30%">
                                        <input name="pago" type="text" class="text-right" id="pago" value="<?php echo numFormat($saldo) ?>" maxlength="50" readonly />
                                    </td>
                                    <th class="invisible d-none d-sm-inline-block d-md-none"></th>
                                    <td class="font-weight-bold bg-white" style="color:#006EBD">
                                        <div class="row pt-1 pb-1 pt-sm-2 pb-sm-2 p-md-0">
                                            <div class="offset-2 col-3 text-nowrap text-right pr-0">
                                                <input type="radio" name="tarjeta" id="cash" value="0" checked >
                                                <label for="cash" class="d-inline-block m-0 ml-2 mr-2" style="cursor:pointer;">CASH</label>
                                            </div>
                                            <div class="col-7">
                                                <input type="radio" name="tarjeta" id="card" value="1">
                                                <label for="card" class="d-inline-block m-0 ml-2" style="cursor:pointer;">TARJETA DE CREDITO</label>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">
                                        <button type="button" id="btn-payment" class="btn btn-success" style="width:250px"><i class="fa fa-money mr-1"></i> <span>Aplicar Pago</span></button>
                                    </td>
                                </tr>
                            </table>
                        <?php } else { ?>
                            <div class="alert text-white text-center m-0" style="background-color:#3380E0; margin-right:5px">
                                <h6 class="m-0">EL MONTO DE ESTA FACTURA HA SIDO PAGADO!</h6>
                            </div>
                        <?php } ?>

                        </td>
                    </tr>

                <?php } ?>

                <?php if($sale["id"]){ ?>

                    <tr>
                        <td>

                            <table id="payments" class="data-grid">
                                <caption>PAGOS REALIZADOS</caption>
                                <?php
                                $str = 'SELECT pagos.*, tipo_pagos.tipo, usuarios.nombre usuario, agencias.agencia FROM pagos INNER JOIN tipo_pagos ON pagos.id_tipo = tipo_pagos.id INNER JOIN usuarios ON pagos.id_usuario = usuarios.id INNER JOIN agencias ON usuarios.id_agencia = agencias.id WHERE id_venta = ' . $sale['id'] . ' AND pagos.status ORDER BY pagos.fecha, pagos.id;';
                                $res = mysql_query($str, $link);
                                if(mysql_num_rows($res)){
                                    ?>
                                    <tr>
                                        <th width="100">Fecha</th>
                                        <th class="d-none d-md-table-cell">Agencia</th>
                                        <th class="d-none d-sm-table-cell">Recibe</th>
                                        <th class="d-none d-lg-table-cell">Tipo</th>
                                        <th>Monto USD</th>
                                        <th>Costo USD</th>
                                        <th class="d-none d-lg-table-cell">Referencia</th>
                                        <th width="100">&nbsp;</th>
                                        <th width="75">&nbsp;</th>
                                    </tr>
                                    <?php
                                    while($row = mysql_fetch_assoc($res)){
                                        ?>
                                        <tr>
                                            <td width="100"><?php echo $row["fecha"] ?></td>
                                            <td class="d-none d-md-table-cell"><?php echo $row['agencia'] ?></td>
                                            <td class="d-none d-sm-table-cell"><?php echo $row['usuario'] ?></td>
                                            <td class="d-none d-lg-table-cell"><?php echo $row['tipo'] ?></td>
                                            <td class="text-right"><?php echo numFormat($row['monto']) ?></td>
                                            <td class="text-right"><?php echo numFormat($row['costo']) ?></td>
                                            <td class="d-none d-lg-table-cell text-primary"><?php echo $row['referencia'] ?></td>
                                            <td width="100"><button type="button" class="btn btn-sm btn-outline-danger" style="width:100px" onclick="window.open('viewer.php?dll=2&id=<?php echo md5($row["id"]) ?>')"><i class="fa fa-file-pdf-o" style="margin-right:5px"></i> Recibo</button></td>
                                            <td width="75"><input name="button" type="button" class="btn btn-sm btn-danger" style="width:75px" value="- X -" onclick="confirmDeletePayment(<?php echo $row["id"] ?>, '<?php echo $row["fecha"].": USD ".numFormat($row["monto"]) ?>')" /></td>
                                        </tr>
                                    <?php 	} ?>
                                <?php } else { ?>
                                    <tr>
                                        <th class="text-center">NO HAY PAGOS REALIZADOS!</td>
                                        <td width="200">
                                            <button type="button" class="btn btn-sm btn-outline-danger" style="width:200px" onclick="window.open('viewer.php?dll=18&id=<?php echo md5($sale["id"]) ?>')"><i class="fa fa-file-pdf-o" style="margin-right:5px"></i> Comprobante</button>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </table>

                        </td>
                    </tr>

                <?php } ?>

                <?php } else { ?>
                    <tr>
                        <td style="padding:0">
                            <div class="alert alert-danger text-center font-weight-bold p-4">
                                Esta Reservacion a sido anulada por: <?php echo $sale["anula"] ?><br>
                                Fecha: <?php echo $sale['modificado'] ?>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </table>

            </div>
        </div>
    </div>

</div>

</form>

<?php
/*

      888888                                                      d8b          888
        "88b                                                      Y8P          888
         888                                                                   888
         888  8888b.  888  888  8888b.  .d8888b   .d8888b 888d888 888 88888b.  888888
         888     "88b 888  888     "88b 88K      d88P"    888P"   888 888 "88b 888
         888 .d888888 Y88  88P .d888888 "Y8888b. 888      888     888 888  888 888
         88P 888  888  Y8bd8P  888  888      X88 Y88b.    888     888 888 d88P Y88b.
         888 "Y888888   Y88P   "Y888888  88888P'  "Y8888P 888     888 88888P"   "Y888
       .d88P                                                          888
     .d88P"                                                           888
    888P"                                                             888

*/
?>

<script type="text/javaScript">
var nodeCount = <?php echo count($cod) ?>
</script>

<link rel="stylesheet" href="assets/js/tipsy/tipsy.css">
<script type="text/javascript" src="assets/js/tipsy/jquery.tipsy.js"></script>

<script type="text/javascript" src="ajaxlib.js"></script>
<script type="text/javascript" src="ext-core.js"></script>

<script type="text/javascript" src="assets/js/sale.js"></script>

<link href="assets/js/icheck/skins/flat/blue.css" rel="stylesheet">
<link rel="stylesheet" href="assets/js/payment/payment.css">

<script src="assets/js/icheck/icheck.min.js"></script>

<script type="text/javascript" src="assets/js/payment/jquery.payform.js"></script>
<script type="text/javascript" src="assets/js/payment/payment.js"></script>
<script type="text/javascript" src="assets/js/payment/parse-names.js"></script>

<script type="text/javascript" src="assets/js/sale.payment.js"></script>


<link rel="stylesheet" href="assets/js/fancybox2/jquery.fancybox.css" />
<script type="text/javascript" src="assets/js/fancybox2/jquery.fancybox.js"></script>

<link rel="stylesheet" href="assets/js/fancybox/jquery.fancybox.min.css" />
<script src="assets/js/fancybox/jquery.fancybox.min.js"></script>

<link rel="stylesheet" href="assets/js/select2/select2.min.css">
<script type="text/javascript" src="assets/js/select2/select2.min.js"></script>

<script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>


<?php include 'footer.php' ?>