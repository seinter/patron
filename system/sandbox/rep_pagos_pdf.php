<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 2);

$link = DB::connect();

$pdf = new PDF("L");
$pdf->page_footer = $pdf_footer;

if($ida)
	$str = "SELECT agencias.id, agencias.agencia, agencias.id_central, if(agencias.id_central, central.comision, agencias.comision) comision1, if(agencias.id_central, agencias.comision, 0) comision2 FROM agencias LEFT JOIN agencias central ON agencias.id_central = central.id WHERE agencias.id = {$ida}";
else
	$str = "SELECT agencias.id, agencias.agencia, agencias.id_central, if(agencias.id_central, central.comision, agencias.comision) comision1, if(agencias.id_central, agencias.comision, 0) comision2 FROM agencias LEFT JOIN agencias central ON agencias.id_central = central.id WHERE (agencias.id = {$user->id_agencia} OR agencias.id_central = {$user->id_agencia}) AND agencias.status ORDER BY id";
$res = mysql_query($str, $link);
while($row = mysql_fetch_object($res)){
	$str = "SELECT pagos.*, usuarios.nombre, IFNULL(SUM(pagado.monto), 0) pagado, IFNULL(SUM(pagado.costo), 0) + pagos.costo costos FROM pagos INNER JOIN usuarios ON pagos.id_usuario = usuarios.id LEFT JOIN (SELECT id, id_venta, fecha, monto, costo FROM pagos WHERE status) pagado ON pagos.id_venta = pagado.id_venta AND ((pagado.fecha < pagos.fecha) OR (pagado.fecha = pagos.fecha && pagado.id < pagos.id)) WHERE pagos.fecha BETWEEN '{$f1}' AND '{$f2}' AND usuarios.id_agencia = {$row->id} AND pagos.status GROUP BY pagos.id ORDER BY fecha, pagos.id";
	$res2 = mysql_query($str, $link);
	if(mysql_num_rows($res2)){
		unset($data, $total, $tcomision1, $tcomision2, $tneto);
		$agencia = ($row->id_central ? 'SUB ' : '') . 'AGENCIA: ' . $row->agencia;
		$pdf->title = array($title." - ".$agencia,"REPORTE DE PAGOS DEL '$f1' AL '$f2'");

		$pdf->AddPage();
		while($row2 = mysql_fetch_object($res2)){
			$str = "SELECT CONCAT(clientes.nombre, ' ', clientes.apellido) cliente, cargos, descuento, pc_seguro, declarado, SUM(detalle_venta.tarifa * detalle_venta.cantidad) monto FROM ventas INNER JOIN clientes ON ventas.id_cliente = clientes.id INNER JOIN detalle_venta ON ventas.id = detalle_venta.id_venta WHERE detalle_venta.status AND ventas.id = {$row2->id_venta}";
			$res3 = mysql_query($str, $link);
			$row3 = mysql_fetch_object($res3);
			$seguro = ($row3->declarado*$row3->pc_seguro)/100;

			$comision2 = (($row2->monto - $row2->costo) * ($row->comision2 > $row->comision1 ? $row->comision1 : $row->comision2)) / 100;
			$comision1 = ((($row2->monto - $row2->costo) * $row->comision1) / 100) - $comision2;
			$data[] = array($row2->fecha, $row2->nombre, formatCode($row2->id_venta), $row3->cliente, numFormat($row3->monto), numFormat($row3->cargos + $row2->costos), numFormat($row2->descuentos), numFormat($seguro), numFormat($precio = $row3->monto + $row3->cargos + $row2->costos + $seguro - $row3->descuento), numFormat($row2->pagado), numFormat($row2->monto), numFormat($precio - $row2->pagado - $row2->monto), numFormat($comision1), numFormat($comision2), numFormat($neto = $row2->monto - $comision1 - $comision2));
			$total += $row2->monto;
			$tcomision1 += $comision1;
			$tcomision2 += $comision2;
			$tsaldo += $saldo;
			$tneto += $neto;
		}
		$resumen[] = array("agencia"=>$agencia,"total"=>$total,"comision1"=>$tcomision,"neto"=>$tneto);
		$header = array("FECHA", "RECIBE", "No. VENTA", "CLIENTE", "MONTO", "CARGOS", "DESCUENTO", "SEGURO", "TOTAL", "PAGADO", "PAGO", "SALDO", "COMISION", "COMISION SUB", "NETO");
		$footer = array("","","","","","","","","","TOTAL USD:",numFormat($total),"",numFormat($tcomision1),numFormat($tcomision2),numFormat($tneto));
		$pdf->ImprovedTable($header, $data, $footer, $c);
	}
	
}

if($resumen){
	$pdf->title = array($title,"RESUMEN");
	$pdf->AddPage();
	unset($header, $data, $footer);
	$header = array("AGENCIA","MONTO","COMISION","COMISION SUB","NETO");
	foreach($resumen as $value){
		$data[] = array($value["agencia"], numFormat($value["total"]), numFormat($value["comision1"]), numFormat($value["comision2"]), numFormat($value["neto"]));
		$totales["total"] += $value["total"];
		$totales["comision1"] += $value["comision1"];
		$totales["comision2"] += $value["comision2"];
		$totales["neto"] += $value["neto"];
	}
	$footer = array("",numFormat($totales["total"]),numFormat($totales["comision1"]),numFormat($totales["comision2"]),numFormat($totales["neto"]));
	
	$pdf->ImprovedTable($header,$data, $footer, $c);
}

$pdf->Output();

?>