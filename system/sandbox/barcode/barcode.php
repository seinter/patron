<?php

 define (__TRACE_ENABLED__, false);
 define (__DEBUG_ENABLED__, false);
 			   
 require("gd_calls.php");		   
 require("c39object.php");

function create_barcode($barcode){

	if (!isset($output))  $output   = "png";
	if (!isset($type))    $type     = "C39";
	if (!isset($width))   $width    = "540";
	if (!isset($height))  $height   = "100";
	if (!isset($xres))    $xres     = "2.6";
	if (!isset($font))    $font     = "5";
	
	$output  = "png";
	$border  = "off";
	$drawtext = "on";
	$stretchtext = "on";
	$negative = "off";
										
	if (isset($barcode) && strlen($barcode)>0) {    
	  $style  = BCS_ALIGN_LEFT;					       
	  $style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0; 
	  $style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0; 
	  $style |= ($border  == "on"  ) ? BCS_BORDER 	  : 0; 
	  $style |= ($drawtext == "on"  ) ? BCS_DRAW_TEXT  : 0; 
	  $style |= ($stretchtext == "on" ) ? BCS_STRETCH_TEXT  : 0; 
	  $style |= ($negative == "on"  ) ? BCS_REVERSE_COLOR  : 0; 
	  
	  $obj = new C39Object($width, $height, $style, $barcode);
	
	  if ($obj) {
		 if ($obj->DrawObject($xres)) {
		 
		  if (!isset($style))  $style   = BCD_DEFAULT_STYLE;
		  if (!isset($width))  $width   = BCD_DEFAULT_WIDTH;
		  if (!isset($height)) $height  = BCD_DEFAULT_HEIGHT;
		  if (!isset($xres))   $xres    = BCD_DEFAULT_XRES;
		  if (!isset($font))   $font    = BCD_DEFAULT_FONT;
						
		  $obj = new C39Object($width, $height, $style, $barcode);
		   
		  if ($obj) {
			  $obj->SetFont($font);   
			  $obj->DrawObject($xres);
			  $obj->FlushObject($barcode);
			  //$obj->DestroyObject($barcode);
			  unset($obj);  /* clean */
	  }
			 return true;
		 } else return false;
	  }
	}
}

?>