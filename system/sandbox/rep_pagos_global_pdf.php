<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 3);

$link = DB::connect();

$pdf = new PDF("L");
$pdf->page_footer = $pdf_footer;

if($ida)
	$str = "SELECT id, agencia, codigo, comision FROM agencias WHERE id = {$ida}";
else
	$str = "SELECT id, agencia, codigo, comision FROM agencias WHERE NOT id_central AND status ORDER BY id";
$res = mysql_query($str, $link);
while($row = mysql_fetch_object($res)){
	$str = "SELECT pagos.*, agencias.agencia, usuarios.nombre, comision, IFNULL(SUM(pagado.monto), 0) pagado, IFNULL(SUM(pagado.costo), 0) + pagos.costo costos FROM pagos INNER JOIN usuarios ON pagos.id_usuario = usuarios.id INNER JOIN agencias ON usuarios.id_agencia = agencias.id LEFT JOIN (SELECT id, id_venta, fecha, monto, costo FROM pagos WHERE status) pagado ON pagos.id_venta = pagado.id_venta AND ((pagado.fecha < pagos.fecha) OR (pagado.fecha = pagos.fecha && pagado.id < pagos.id)) WHERE pagos.fecha BETWEEN '{$f1}' AND '{$f2}' AND (agencias.id = {$row->id} OR agencias.id_central = {$row->id}) AND pagos.status GROUP BY pagos.id ORDER BY fecha, pagos.id";

	$res2 = mysql_query($str, $link);
	if(mysql_num_rows($res2)){
		unset($data, $total, $tcomision, $tneto);
		$agencia = "AGENCIA: {$row->agencia} ({$row->codigo})";
		$pdf->title = array($title." - ".$agencia,"REPORTE DE PAGOS DEL '{$f1}' AL '{$f2}'");
		$pdf->AddPage();

		while($row2 = mysql_fetch_object($res2)){
			$str = "SELECT CONCAT(clientes.nombre, ' ', clientes.apellido) cliente, cargos, descuento, pc_seguro, declarado, SUM(detalle_venta.tarifa * detalle_venta.cantidad) monto FROM ventas INNER JOIN clientes ON ventas.id_cliente = clientes.id INNER JOIN detalle_venta ON ventas.id = detalle_venta.id_venta WHERE detalle_venta.status AND ventas.id = {$row2->id_venta}";
			$res3 = mysql_query($str, $link);
			$row3 = mysql_fetch_object($res3);
			$seguro = ($row3->declarado * $row3->pc_seguro) / 100;

			$data[] = array($row2->fecha, $row2->agencia, $row2->nombre, formatCode($row2->id_venta), $row3->cliente, numFormat($precio = $row3->monto + $row3->cargos + $row2->costos + $seguro - $row3->descuento),  numFormat($row2->pagado), numFormat($row2->monto), numFormat($precio - $row2->pagado - $row2->monto), numFormat($comision = (($row2->monto - $row2->costo) * $row->comision) / 100), numFormat($neto = $row2->monto - $comision));
			$total += $row2->monto;
			$tcomision += $comision;
			$tneto += $neto;
		}
		$resumen[] = array("agencia"=>$agencia,"total"=>$total);
		$header = array("FECHA", "RECIBE", "USUARIO", "No. VENTA", "CLIENTE", "TOTAL VENTA", "PAGADO", "PAGO", "SALDO", "COMISION", "NETO");
		$footer = array("","","","","","","TOTAL USD:",numFormat($total),"",numFormat($tcomision),numFormat($tneto));
		$pdf->ImprovedTable($header, $data, $footer, $c);
	}
	
}

if($resumen){
	$pdf->title = array($title,"RESUMEN");
	$pdf->AddPage();
	unset($header, $data, $footer);
	$header = array("AGENCIA","MONTO");
	foreach($resumen as $value){
		$data[] = array($value["agencia"], numFormat($value["total"]));
		$totales["total"] += $value["total"];
	}
	$footer = array("",numFormat($totales["total"]));
	
	$pdf->ImprovedTable($header,$data, $footer, $c);
}

$pdf->Output();

?>