// JavaScript Document

var dh = Ext.DomHelper
var fields = ['f1', 'f2', 'hrs1', 'hrs2', 'min1', 'min2', 'observ1', 'observ2']


$(function(){
    // CONTROL SETTINGS
    
    $('select').select2()

    $('#destino').on('select2:selecting', confirmChangeDest)

    $('div[rel=tipsy], button[rel=tipsy], .icon').tipsy({ gravity: 's' })

    $('#customer-minimize').click(function(){
        $('#customer').collapse('hide')
        setTimeout(function(){
            $('#customer-box').collapse('show')
        }, 500)
    })

    $('#customer-maximize').click(function(){
        $('#customer-box').collapse('hide')
        setTimeout(function(){
            $('#customer').collapse('show')
        }, 500)
    })

    $('#receiver-minimize').click(function(){
        $('#receiver').collapse('hide')
        setTimeout(function(){
            $('#receiver-box').collapse('show')
        }, 500)
    })

    $('#receiver-maximize').click(function(){
        $('#receiver-box').collapse('hide')
        setTimeout(function(){
            $('#receiver').collapse('show')
        }, 500)
    })

    $('#recep_nombre').keyup(function(){
        $('#div_recep_nombre').html($(this).val())
    })

    $('#dest_nombre').keyup(function(){
        $('#div_dest_nombre').html($(this).val())
    })
    $('#dest_telefono').keyup(function(){
        $('#div_dest_telefono').html($(this).val())
    })

})


//-------VALIDACION---------

function validateSale(form){
    var y = 0
    var rec = document.getElementsByName("recepcion")
    var ent = document.getElementsByName("entrega")
    var seg = document.getElementsByName("seguro")
    var type = Number(document.getElementById("id_tipo").value)

    // RESET
    resetErrors(form)

    for(var x = 0;x<form.length;x++){
        if(form.elements[x].value=="" && !form.elements[x].disabled && ['button', 'submit'].indexOf(form.elements[x].type) === -1){
            switch(form.elements[x].name){
                //RECEPCION
                case "recep_nombre":
                case "recep_zip":
                case "recep_direccion_usa":
                case "recep_direccion":
                case "recep_depto":
                    if(rec[1].checked){
                        if(document.getElementById("recep_pais").value == 225 || document.getElementById("recep_pais").value == 136){
                            switch(form.elements[x].name){
                                case "recep_direccion":
                                case "recep_depto":
                                    break
                                default:
                                    stackError(form.elements[x])
                                    y++
                                    break
                            }
                        } else {
                            switch(form.elements[x].name){
                                case "recep_zip":
                                case "recep_direccion_usa":
                                    break
                                default:
                                    stackError(form.elements[x])
                                    y++
                                    break
                            }
                        }
                    }
                    break
                //DESTINATARIO
                case "dest_zip":
                case "dest_direccion_usa":
                case "dest_direccion":
                case "dest_depto":
                    if(document.getElementById("dest_pais").value == 225 || document.getElementById("dest_pais").value == 136){
                        switch(form.elements[x].name){
                            case "dest_direccion":
                            case "dest_depto":
                                break
                            default:
                                stackError(form.elements[x])
                                y++
                                break
                        }
                    } else {
                        switch(form.elements[x].name){
                            case "dest_zip":
                            case "dest_direccion_usa":
                                break
                            default:
                                stackError(form.elements[x])
                                y++
                                break
                        }
                    }

                    break
                //ENTREGA
                case "id_agencia":
                case "id_carrier":
                    if(!ent[0].checked){
                        if(ent[1].checked) {
                            switch(form.elements[x].name){
                                case "id_carrier":
                                    break
                                default:
                                    stackError(form.elements[x])
                                    y++
                                    break
                            }
                        } else {
                            switch(form.elements[x].name){
                                case "id_agencia":
                                    break
                                default:
                                    stackError(form.elements[x])
                                    y++
                                    break
                            }
                        }
                    }
                    break
                //TIPO DE VENTA
                case "id_credito":
                    if(type==2) {
                        stackError(form.elements[x])
                        y++
                    }
                    break
                case "dest_telefono2":
                case "comentarios":
                case "comentariosv":
                    break;
                default:
                    if(fields.indexOf(form.elements[x].name) === -1){
                        stackError(form.elements[x])
                        y++
                    }
                    break
            }

        }
    }
    //CAMPOS
    if(y>0){
        alert("Debe llenar correctamente todas las casillas!")
        return false
    }
    //SEGURO
    if(seg[1].checked && Number(document.getElementById("monto_declarado").value)<=0){
        alert("Debe declarar un monto para el seguro!")
        return false
    }
    //PRODUCTOS
    y = 0;
    for(var x = 0; x < form.length; x++){
        if(form.elements[x].name.substr(0,3) == "prd")
            y++
    }
    if(!y){
        alert("Debe seleccionar algun producto!")
        return false;
    }
    //TARIFAS
    y = 0
    for(var x = 0; x < form.length; x++)
        if(form.elements[x].name.substr(0,3) == "sub" && Number(form.elements[x].value) <= 0){
            stackError(form.elements[x])
            y++
        }

    if(y){
        alert("Debe seleccionar tarifas validas!")
        return false
    }

    return true

}

function validateModify(form){
    var y = 0;
    var rec = document.getElementsByName("recepcion");
    var ent = document.getElementsByName("entrega");
    var seg = document.getElementsByName("seguro");

    for(var x = 0;x<form.length;x++){
        if(form.elements[x].value=="" && !form.elements[x].disabled){
            switch(form.elements[x].name){
                //RECEPCION
                case "recep_nombre":
                case "recep_zip":
                case "recep_direccion_usa":
                case "recep_direccion":
                case "recep_depto":
                    if(rec[1].checked){
                        if(document.getElementById("recep_pais").value==225 || document.getElementById("recep_pais").value==136){
                            switch(form.elements[x].name){
                                case "recep_direccion":
                                case "recep_depto":
                                    break;
                                default:
                                    stackError(form.elements[x])
                                    y++;
                                    break;
                            }
                        } else {
                            switch(form.elements[x].name){
                                case "recep_zip":
                                case "recep_direccion_usa":
                                    break;
                                default:
                                    stackError(form.elements[x])
                                    y++;
                                    break;
                            }
                        }
                    }
                    break;
                //DESTINATARIO
                case "dest_zip":
                case "dest_direccion_usa":
                case "dest_direccion":
                case "dest_depto":
                    if(document.getElementById("dest_pais").value==225 || document.getElementById("dest_pais").value==136){
                        switch(form.elements[x].name){
                            case "dest_direccion":
                            case "dest_depto":
                                break;
                            default:
                                stackError(form.elements[x])
                                y++;
                                break;
                        }
                    } else {
                        switch(form.elements[x].name){
                            case "dest_zip":
                            case "dest_direccion_usa":
                                break;
                            default:
                                stackError(form.elements[x])
                                y++;
                                break;
                        }
                    }

                    break;
                //ENTREGA
                case "id_agencia":
                case "id_carrier":
                    if(!ent[0].checked){
                        if(ent[1].checked) {
                            switch(form.elements[x].name){
                                case "id_carrier":
                                    break;
                                default:
                                    stackError(form.elements[x])
                                    y++;
                                    break;
                            }
                        } else {
                            switch(form.elements[x].name){
                                case "id_agencia":
                                    break;
                                default:
                                    stackError(form.elements[x])
                                    y++;
                                    break;
                            }
                        }
                    }
                    break;
            }

        }
    }
    if(y>0){
        alert("Debe llenar todos los campos requeridos!");
        return false;
    }
    //SEGURO
    if(seg[1].checked && Number(document.getElementById("monto_declarado").value)<=0){
        alert("Debe declarar un monto para el seguro!");
        return false;
    }

    if(!confirm("Desea guardar los cambios?"))
        return false;

    form.submit();
}


// CONFIRM DIALOGS
function confirmDeleteDetail(id, name){
    if(confirm("Esta a punto de eliminar el producto \n'" + name + "', desea continuar?"))
        authorize(3, function(idu) {
            window.location = 'venta.php?dll=2&id=' + $('#id').val() + '&idr='+id+"&idu="+idu;
        })
}

function confirmDeletePayment(id, name){
    if(confirm("Esta a punto de anular el pago '" + name + "',\ndesea continuar?"))
        authorize(2, function(idu) {
            window.location = 'venta.php?dll=3&id=' + $('#id').val() + '&idr=' + id + '&idu=' + idu
        })
}

function confirmDeleteSale(){
    if(confirm("Esta a punto de anular la venta No. '" + $('#code').text() + "',\ndesea continuar?"))
    authorize(1, function(idu) {
        window.location = 'venta.php?dll=4&id=' + $('#id').val() + '&idu=' + idu
    })
}

function confirmChangeDest() {
    if($('input[name^=sub]').length) {
        if (!confirm("Los volumenes calculados manualmente seran removidos,\nDesea continuar?")){
            $(this).select2('close')
            return false
        }
    }

    return true
}


//-------VALIDACION---------



//-------RECEPTOR---------

function getRecep(id){
    if(Number(id))
        loadXMLDoc("xml/receptor.php?id="+id, decodeRecepDataXml, false);
    else{
        document.getElementById("recep_nombre").value = "";
        $('#div_recep_nombre').html('&nbsp;- - - - -')
        if(Number(document.getElementById("recep_pais").value)==225 || Number(document.getElementById("recep_pais").value)==136){
            document.getElementById("recep_direccion_usa").value = "";
            document.getElementById("recep_zip").value = "";
            $('#recep_zip').keyup()
        } else {
            document.getElementById("recep_direccion").value = "";
            document.getElementById("recep_depto").selectedIndex = 0;
        }
    }
}

function decodeRecepDataXml(){
    var record = resultXML.getElementsByTagName("data");
    if(record.length){
        document.getElementById("recep_nombre").value = getNodeValue(record[0],"nombre");
        $('#div_recep_nombre').html(getNodeValue(record[0],"nombre"))

        if(Number(getNodeValue(record[0],"pais"))==225 || Number(getNodeValue(record[0],"pais"))==136 ){
            document.getElementById("recep_direccion_usa").value = getNodeValue(record[0],"direcc");
            document.getElementById("recep_zip").value = getNodeValue(record[0],"zip");
            $('#recep_zip').keyup()
        } else {
            document.getElementById("recep_direccion").value = getNodeValue(record[0],"direcc");
            document.getElementById("recep_depto").value = getNodeValue(record[0],"depto");
        }

    }
}

function showRecep(val){
    $('.receptor').hide()
    if(+val){
        var usa = [136, 225].indexOf(+$('#recep_pais').val()) > -1
        $('.receptor:not(.' + (usa ? 'direcc' : 'direcc_usa') + ')').show()
    }
}

function setRecep(){
    var id = document.getElementById("origen").value;
    var idc = document.getElementById("idc").value;
    loadXMLDoc("xml/direcciones.php?tipo=0&id="+id+"&idc="+idc, decodeRecepXml, false);
}

function decodeRecepXml(){

    var pais = resultXML.getElementsByTagName("pais");
    var recep_pais = document.getElementById("recep_pais");
    var obj, loopIndex, str;

    if(Number(getNodeValue(pais[0],"id")) != Number(recep_pais.value)){

        recep_pais.value = getNodeValue(pais[0],"id");
        $('.div_recep_pais').html(getNodeValue(pais[0],"desc"))

        if([136, 225].indexOf(+recep_pais.value) === -1) {
            obj = document.getElementById("div_recep_depto");
            var deptos = resultXML.getElementsByTagName("depto");

            str = "<select name=\"recep_depto\" id=\"recep_depto\" style=\"width:100%\">\n";
            for(loopIndex=0;loopIndex<deptos.length;loopIndex++)
                str += "<option value=\""+getNodeValue(deptos[loopIndex],"id")+"\">"+getNodeValue(deptos[loopIndex],"desc")+"</option>\n";
            str += "</select>";
            dh.overwrite(obj, str);
            $('#recep_depto').select2()
        }

        obj = document.getElementById("div_receps");
        var dests = resultXML.getElementsByTagName("direcc");
        str = "<select name=\"id_recep\" id=\"id_recep\" onChange=\"getRecep(this.value)\" style=\"width:100%\">\n<option value=\"0\"> - - - - NUEVO CONTACTO - - - - </option>\n";
        for(loopIndex=0;loopIndex<dests.length;loopIndex++)
            str += "<option value=\""+getNodeValue(dests[loopIndex],"id")+"\">"+getNodeValue(dests[loopIndex],"nombre")+"</option>\n";
        str += "</select>";
        dh.overwrite(obj, str);

        $('#id_recep').select2()

        getRecep(0)
        showRecep($('input[name=recepcion]:checked').val())

    }

    var id = document.getElementById("origen").value;
    changeDestination(id);
}

function loadRecepZipData(event, sp){
    var cntr = document.getElementById("recep_pais").value;
    var val = document.getElementById("recep_zip").value;
    var state = document.getElementById("div_recep_estado");
    var city = document.getElementById("div_recep_ciudad");


    if(sp==undefined)
        sp = 0;
    if(Number(sp)){
        var keyCode = event.which ? event.which : event.keyCode;
        if(keyCode==8)
            val = val.substr(0,val.length-1);
    }

    if(val.length!=5){
        state.innerHTML = "&nbsp;- - - - -";
        city.innerHTML = "&nbsp;- - - - -";
    } else
        loadXMLDoc("xml/zips.php?idp="+cntr+"&val="+val,decodeRecepZipXml, false);

}

function decodeRecepZipXml(){
    var records = resultXML.getElementsByTagName("record");
    var state = document.getElementById("div_recep_estado");
    var city = document.getElementById("div_recep_ciudad");

    if(records.length){
        state.innerHTML = getNodeValue(records[0],"estado");
        city.innerHTML = getNodeValue(records[0],"ciudad");
    } else {
        state.innerHTML = "&nbsp;- - - - -";
        city.innerHTML = "&nbsp;- - - - -";
    }
}

//-------RECEPTOR---------



//-------DESTINATARIO---------

function setDest(){
    var id = document.getElementById("destino").value;
    var idc = document.getElementById("idc").value;
    loadXMLDoc("xml/direcciones.php?tipo=1&id="+id+"&idc="+idc, decodeDestXml, false);

    // UPDATE PRODUCTS
    $('select[name^=prd]').trigger('change')
    $('input[name^=prd]').each(function() {
        deleteTableNode($(this).data('idx'))
    })

}

function decodeDestXml(){
    var pais = resultXML.getElementsByTagName("pais");
    var dest_pais = document.getElementById("dest_pais");
    var obj, loopIndex, str;

    if(Number(getNodeValue(pais[0],"id")) != Number(dest_pais.value)){

        $('.destinatario.direcc_usa, .destinatario.direcc').hide()

        dest_pais.value = getNodeValue(pais[0],"id");
        document.getElementById("div_dest_pais").innerHTML = "&nbsp;"+getNodeValue(pais[0],"desc");

        var usa = [136, 225].indexOf(+$('#dest_pais').val()) > -1

        if(!usa) {
            obj = document.getElementById("div_dest_depto");
            var deptos = resultXML.getElementsByTagName("depto");

            str = "<select name=\"dest_depto\" id=\"dest_depto\" style=\"width:100%\" onchange=\"setDeliver()\">\n";
            for(loopIndex=0;loopIndex<deptos.length;loopIndex++)
                str += "<option value=\""+getNodeValue(deptos[loopIndex],"id")+"\">"+getNodeValue(deptos[loopIndex],"desc")+"</option>\n";
            str += "</select>";
            dh.overwrite(obj, str);
            $('#dest_depto').select2()
        }

        obj = document.getElementById("div_dests");
        var dests = resultXML.getElementsByTagName("direcc");
        str = "<select name=\"id_dest\" id=\"id_dest\" onChange=\"getDest(this.value)\" style=\"width:100%\">\n<option value=\"0\"> - - - - NUEVO DESTINATARIO - - - - </option>\n";
        for(loopIndex=0;loopIndex<dests.length;loopIndex++)
            str += "<option value=\""+getNodeValue(dests[loopIndex],"id")+"\">"+getNodeValue(dests[loopIndex],"nombre")+"</option>\n";
        str += "</select>";
        dh.overwrite(obj, str);

        $('#id_dest').select2()

        getDest(0);
        $('.destinatario:not(.' + (usa ? 'direcc' : 'direcc_usa') + ')').show()

    }

}

function loadDestZipData(event, sp){
    var cntr = document.getElementById("dest_pais").value;
    var val = document.getElementById("dest_zip").value;
    var state = document.getElementById("div_dest_estado");
    var city = document.getElementById("div_dest_ciudad");


    if(sp==undefined)
        sp = 0;
    if(Number(sp)){
        var keyCode = event.which ? event.which : event.keyCode;
        if(keyCode==8)
            val = val.substr(0,val.length-1);
    }

    if(val.length!=5){
        state.innerHTML = "&nbsp;- - - - -";
        city.innerHTML = "&nbsp;- - - - -";
        try{
            if(Number(document.getElementById('carrier').value))
                setDeliver();
        } catch(e){};
    } else
        loadXMLDoc("xml/zips.php?idp="+cntr+"&val="+val,decodeDestZipXml, false);

}

function decodeDestZipXml(){
    var records = resultXML.getElementsByTagName("record");
    var state = document.getElementById("div_dest_estado");
    var city = document.getElementById("div_dest_ciudad");

    if(records.length){
        state.innerHTML = getNodeValue(records[0],"estado");
        city.innerHTML = getNodeValue(records[0],"ciudad");
    } else {
        state.innerHTML = "&nbsp;- - - - -";
        city.innerHTML = "&nbsp;- - - - -";
    }
    setDeliver();

}

function getDest(id){
    if(Number(id))
        loadXMLDoc("xml/destinatario.php?id="+id,decodeDestDataXml,false);
    else{
        document.getElementById("dest_telefono").value = "";
        document.getElementById("dest_nombre").value = "";
        document.getElementById("comentarios").value = "";

        $('#div_dest_nombre, #div_dest_telefono').html('&nbsp;- - - - -')

        if(Number(document.getElementById("dest_pais").value)==225 || Number(document.getElementById("dest_pais").value)==136){
            document.getElementById("dest_direccion_usa").value = "";
            document.getElementById("dest_zip").value = "";
            $('#dest_zip').keyup()
        } else {
            document.getElementById("dest_direccion").value = "";
            document.getElementById("dest_depto").selectedIndex = 0;
        }
        setDeliver();
    }
}

function decodeDestDataXml(){
    var record = resultXML.getElementsByTagName("data");
    if(record.length){
        document.getElementById("dest_telefono").value = getNodeValue(record[0],"tel");
        document.getElementById("dest_telefono2").value = getNodeValue(record[0],"tel2");
        document.getElementById("dest_nombre").value = getNodeValue(record[0],"nombre");
        document.getElementById("comentarios").value = getNodeValue(record[0],"comentarios");

        $('#div_dest_nombre').html(getNodeValue(record[0],"nombre"))
        $('#div_dest_telefono').html(getNodeValue(record[0],"tel"))

        if(Number(getNodeValue(record[0],"pais"))==225 || Number(getNodeValue(record[0],"pais"))==136 ){
            document.getElementById("dest_direccion_usa").value = getNodeValue(record[0],"direcc");
            document.getElementById("dest_zip").value = getNodeValue(record[0],"zip");
            $('#dest_zip').keyup()
        } else {
            document.getElementById("dest_direccion").value = getNodeValue(record[0],"direcc");
            document.getElementById("dest_depto").value = getNodeValue(record[0],"depto");
        }
    }
    setDeliver();
}
//-------DESTINATARIO---------



//-------ENTREGA---------
function setDeliver(){
    var div = document.getElementById("div_agencias");
    var addr = document.getElementById("agencias_direcc");

    var type = +$('input[name=entrega]:checked').val()

    if(type){
        div.style.display = "";
        var idp = document.getElementById("dest_pais").value;
        if(Number(idp)==225||Number(idp)==136)
            var ids = document.getElementById("dest_zip").value;
        else
            var ids = document.getElementById("dest_depto").value;
        loadXMLDoc("xml/agencias.php?type="+type+"&idp="+idp+"&ids="+ids,decodeAgenciesXml, false);
    } else {
        div.style.display = "none";
        addr.style.display = "none";
    }
}

function decodeAgenciesXml(){
    obj = document.getElementById("div_agencias");
    addr = document.getElementById("agencias_direcc");

    var groups = resultXML.getElementsByTagName("grupo");
    var val = document.getElementsByName("entrega");

    if(val[1].checked)
        str = "<select name=\"id_agencia\" id=\"id_agencia\" style=\"width:100%\" onchange=\"getAgencyAddress()\">\n";
    else
        str = "<select name=\"id_carrier\" id=\"id_carrier\" style=\"width:100%\" onchange=\"getAgencyAddress()\">\n";

    for(loopIndex=0;loopIndex<groups.length;loopIndex++){
        str += "<optgroup style=\"background-color:#7DAAEE; color:#FFFFFF\" label=\""+getNodeValue(groups[loopIndex],"nombre")+"\">\n";
        var bg = "";
        var agencies = groups[loopIndex].getElementsByTagName("agencia");
        for(loopSubIndex=0;loopSubIndex<agencies.length;loopSubIndex++){
            bg = bg=="#FFFFFF"?"#F1F1F1":"#FFFFFF";
            str += "<option style='background-color:"+bg+"; color:#333333' value=\""+getNodeValue(agencies[loopSubIndex],"id")+"\">"+getNodeValue(agencies[loopSubIndex],"nombre")+"</option>\n";
        }
    }
    str += "</select>";
    dh.overwrite(obj, str);

    $(val[1].checked ? '#id_agencia' : '#id_carrier').select2()

    if(groups.length){
        addr.style.display = "";
        getAgencyAddress();
    } else
        addr.style.display = "none";

}

function getAgencyAddress(){

    var val = document.getElementsByName("entrega");
    if(val[1].checked){
        type = 1;
        var ida = document.getElementById("id_agencia").value;
    } else {
        type = 2;
        var ida = document.getElementById("id_carrier").value;
    }
    loadXMLDoc("xml/agencia.php?type="+type+"&ida="+ida,decodeAgencyAddressXml, false);
}

function decodeAgencyAddressXml(){
    var agencies = resultXML.getElementsByTagName("agency");
    addr = document.getElementById("div_agencias_direcc");
    addr.innerHTML = "Direcci&oacute;n:&nbsp;"+getNodeValue(agencies[0],"addr")+" / Tel&eacute;fono:&nbsp;"+getNodeValue(agencies[0],"tel");
}
//-------ENTREGA---------


//-------DESTINO---------
function changeDestination(id){
    loadXMLDoc("xml/destinos.php?id="+id, decodeDestinationXml, false);
}

function decodeDestinationXml(){
    var records = resultXML.getElementsByTagName("record");
    var destino = document.getElementById("destino").value;
    var holder = document.getElementById("div_dest");

    var html = "<select name='destino' id='destino' style='width:100%;' onChange=\"setDest();\">\n";
    for(var index=0;index<records.length;index++){
        html += "<optgroup label='"+getNodeValue(records[index],"text")+"'>\n";
        var subrecords = records[index].getElementsByTagName("subrecord");
        for(var subindex=0;subindex<subrecords.length;subindex++){
            html += "<option value='"+getNodeValue(subrecords[subindex],"id")+"'"+(Number(destino)==Number(getNodeValue(subrecords[subindex],"id"))?" selected":"")+">"+getNodeValue(subrecords[subindex],"text")+"</option>\n";
        }
        html += "</optgroup>\n";
    }
    html += "</select>";
    dh.overwrite(holder, html);

    $('#destino').select2()
        .on('select2:selecting', confirmChangeDest)

    setDest();

}
//-------DESTINO---------



//-------SEGURO---------
function setEnsurance(val){
    var obj = document.getElementById("div_seguro");

    if(Number(val))
        obj.style.display = "";
    else
        obj.style.display = "none";
    total();
}

function getEnsurance(){
    var val = document.getElementsByName("seguro");
    var seg = document.getElementById("monto_seguro");
    if(val[1].checked){
        seg.value = (Number(document.getElementById("monto_declarado").value)*Number(document.getElementById("pc_seguro").value))/100;
        seg.value = numFormat(seg.value);
    } else
        seg.value = "0.00";
    return Number(seg.value);
}
//-------SEGURO---------


//-------PRODUCTOS---------
function addTableNode(val, type){
    try {

        nodeCount++

        var myTable = document.getElementById("sTable")
        var newTR = document.createElement("TR")
        newTR.setAttribute("id","tr"+nodeCount)

        //COL0
        newTD = document.createElement("TD")
        dh.overwrite(newTD, "<input type=\"hidden\" name=\"cat" + nodeCount + "\" id=\"cat" + nodeCount + "\" value=\"0\"><div id=\"div_cat"+nodeCount+"\" class=\"text-center\"></div>")
        newTR.appendChild(newTD)

        //COL1
        newTD = document.createElement("TD");
        dh.overwrite(newTD, "<input name=\"idd"+nodeCount+"\" type=\"hidden\" id=\"idd"+nodeCount+"\" value=\"0\" /><div id=\"div_prd"+nodeCount+"\"></div>");
        newTR.appendChild(newTD);

        //COL2
        newTD = document.createElement("TD");
        newTD.setAttribute("width","80");
        dh.overwrite(newTD, "<input name=\"tar"+nodeCount+"\" id=\"tar"+nodeCount+"\" type=\"text\" class=\"text-right\" value=\"0.00\" readonly=\"readonly\" />");
        newTR.appendChild(newTD);

        //COL3
        newTD = document.createElement("TD");
        newTD.setAttribute("width","80");
        dh.overwrite(newTD, "<input name=\"cnt"+nodeCount+"\" id=\"cnt"+nodeCount+"\" type=\"text\" class=\"text-right\" value=\"1\" onchange=\"numInt(this, 1); calculate("+nodeCount+");\" />");
        newTR.appendChild(newTD);

        //COL4
        newTD = document.createElement("TD");
        newTD.setAttribute("width","80");
        dh.overwrite(newTD, "<input name=\"blt"+nodeCount+"\" id=\"blt"+nodeCount+"\" type=\"text\" class=\"text-right\" value=\"" + (+type === 4 ? 0 : 1) + "\" " + (+type === 4 ? 'readonly="readonly"' : '')  + " onchange=\"numInt(this, 0)\" />");
        newTR.appendChild(newTD);

        //COL5
        newTD = document.createElement("TD");
        newTD.setAttribute("width","80");
        dh.overwrite(newTD, "<input name=\"sub"+nodeCount+"\" id=\"sub"+nodeCount+"\" type=\"text\" class=\"text-right\" value=\"0.00\" readonly=\"readonly\" />");
        newTR.appendChild(newTD);

        //COL6
        newTD = document.createElement("TD");
        newTD.setAttribute("width","50");
        dh.overwrite(newTD, "<input type=\"button\" class=\"btn btn-sm btn-danger\" style=\"width:50px\" value=\"X\" onClick=\"deleteTableNode("+nodeCount+")\">");
        newTR.appendChild(newTD);

        myTable.appendChild(newTR);

        loadXMLDoc("xml/productos.php?id="+val, decodeProductXml, false);

    } catch(e) {
        alert(e.message);
    }
}

function deleteTableNode(val){
    try{

        var myTable = document.getElementById("sTable");
        var killNode = document.getElementById("tr"+val);
        myTable.removeChild(killNode);
        total();

    } catch(e){
        alert(e.message);
    }
}

function decodeProductXml(){
    var cat = resultXML.getElementsByTagName("clase")[0];
    var prods = resultXML.getElementsByTagName("prod");
    var loopIndex;
    document.getElementById("cat"+nodeCount).value = getNodeValue(cat, 'id');
    document.getElementById("div_cat"+nodeCount).innerHTML = '<i class="fa fa-' + getNodeValue(cat, 'icon') + ' icon" title="' + getNodeValue(cat, 'desc') + '"></i>';
    $('.icon').tipsy({gravity: 's'});

    var html = "<select name=\"prd"+nodeCount+"\" id=\"prd"+nodeCount+"\" style=\"width:100%\" data-idx=\"" + nodeCount + "\" data-idc=\"" + getNodeValue(cat, 'id') + "\">";
    if(prods.length){
        for(loopIndex=0;loopIndex<prods.length;loopIndex++)
            html += "<option value=\""+getNodeValue(prods[loopIndex],"id")+"\">"+getNodeValue(prods[loopIndex],"desc")+"</option>";
    }
    html += "</select>";
    dh.overwrite(document.getElementById("div_prd"+nodeCount), html);
    $('#prd' + nodeCount).select2({
        templateResult: function(item) {
            if(+item.id)
                return item.text
            else
                return $('<span><i class="fa fa-external-link-square" style="margin-right:3px"></i> ' + item.text + '</span>')

        }
    })
        .on('change', function(){
            var idx = $(this).data('idx')

            if(+this.value)
                $.ajax('xml/tarifas.php', {
                    data: {
                        id: this.value,
                        org: document.getElementById("origen").value,
                        dest: document.getElementById("destino").value
                    }
                }).done(function(data) {

                    try {
                        var records = data.getElementsByTagName("record");

                        document.getElementById("tar" + idx).value = numFormat(getNodeValue(records[0],"tar"));
                        calculate(idx);

                    } catch(e) {
                        alert(e.message);
                    }

                })
            else
                customProduct(idx, $(this).data('idc'))

        })
        .trigger('change')

}

function customProduct(idx, idc){

    $.fancybox.open({
        src: 'producto.php?id=' + idc + '&org=' + $('#origen').val() + '&dest=' + $('#destino').val(),
        type: 'iframe',
        modal: true,
        afterShow: function() {
            $('.fancybox-iframe').contents().find('input[type=text]:first').focus()
        },
        beforeClose: function(){
            var frame = $('.fancybox-iframe').contents()
            var act = frame.find('#act').val()

            if(+act) {
                $('#prd' + idx).select2('destroy')
                $('#prd' + idx).replaceWith('<input type="hidden" id="prd' + idx + '" name="prd' + idx + '" value="0"><input type="text" id="desc' + idx + '" name="desc' + idx + '" data-idx="' + idx + '" value="' + frame.find('#desc').val() + '" readonly >')
                $('#tar' + idx).val(frame.find('#total').val())
                calculate(idx);
            } else
                deleteTableNode(idx)
        }
    })

}

function showPrints(val){
    var tr = document.getElementById("prn_"+val);
    tr.style.display = tr.style.display==""?"none":"";
}

//-------PRODUCTOS---------


//-------CALCULATIONS---------
function calculate(item){
    var tar = document.getElementById("tar"+item);
    var cnt = document.getElementById("cnt"+item);
    var stot = document.getElementById("sub"+item);

    stot.value = Number(tar.value) * Number(cnt.value);
    stot.value = numFormat(stot.value);

    total();
}

function total(){
    var form = document.sale_form
    var y = 0;

    for(x=0;x<form.length;x++)
        if(form.elements[x].name.substr(0,3) == "sub")
            y += parseFloat(form.elements[x].value);

    form.tot.value = y + getEnsurance() + parseFloat(form.cargos.value);

    if(parseFloat(form.descuento.value)>parseFloat(form.tot.value))
        form.descuento.value = numFormat(form.tot.value)

    form.tot.value = parseFloat(form.tot.value) - parseFloat(form.descuento.value);
    form.tot.value = numFormat(form.tot.value);
    checkPay(form);
}

function sub_cargos_desc(form){
    form.cargos.value = numFormat(form.cargos.value);
    form.descuento.value = numFormat(form.descuento.value);
    total();
}
//-------CALCULATIONS---------


//---------PAGO---------
function setPayment(val){
    var credit = document.getElementById('creditTable')
    var form = document.getElementById('sale_form')

    credit.style.display = +val === 2 ? '' : 'none'

    checkPay(form)
}

function checkPay(form){
    form.payment.value = +form.id_tipo.value ? '0.00' : form.tot.value
}
//---------PAGO---------


//-------AGENDA---------
function schedule(){

    var args = fields.reduce(function(mix, item) {
        mix[item] = $('#' + item).val()
        return mix
    }, {})

    $.fancybox.open({
        src: 'agenda.php?' + $.param(args),
        type: 'iframe',
        modal: true,
        beforeClose: function(){
            var frame = $('.fancybox-iframe').contents()
            $.each(fields, function(idx, item){
                $('#' + item).val(frame.find('#' + item).val())
            })
        }
    })

}

function setDetail(id){
    $.fancybox.open({
        src: 'contenidos.php?id=' + id,
        type: 'iframe',
        modal: true
    })
}
//-------AGENDA---------


//-------GLOBAL---------
function numInt(obj, val){
    var num = numFormatInt(obj.value)
    obj.value = Number(num) < Number(val) ? parseInt(val) : num

    return obj
}

function numFloat(obj, val){
    var num = numFormat(obj)
    obj.value = Number(num) < Number(val) ? numFormat(val) : num
    return obj
}
//-------GLOBAL---------