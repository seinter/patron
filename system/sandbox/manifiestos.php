<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$lang["months"]	= array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$m = $m?$m:date("n");
$y = $y?$y:date("Y");

$nextyear = ($m != 12) ? $y : $y + 1;
$prevyear = ($m != 1) ? $y : $y - 1;
$prevmonth = ($m == 1) ? 12 : $m - 1;
$nextmonth = ($m == 12) ? 1 : $m + 1;

function monthPullDown($month, $montharray) {
    echo "\n<select name=\"m\" style=\"width:100%\">\n";

    for($i=0;$i < 12; $i++){
        if ($i != ($month - 1))
            echo "	<option value=\"" . ($i + 1) . "\">$montharray[$i]</option>\n";
        else
            echo "	<option value=\"" . ($i + 1) . "\" selected>$montharray[$i]</option>\n";
    }

    echo "</select>\n\n";
}

function yearPullDown($year){
    echo "<select name=\"y\" style=\"width:100%\">\n";

    $z = 3;
    for($i=1;$i < 8; $i++) {
        if ($z == 0)
            echo "	<option value=\"" . ($year - $z) . "\" selected>" . ($year - $z) . "</option>\n";
        else
            echo "	<option value=\"" . ($year - $z) . "\">" . ($year - $z) . "</option>\n";

        $z--;
    }

    echo "</select>\n\n";
}

?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right text-truncate caption">MANIFIESTOS</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">


    <table width="100%" border="0" cellspacing="10" cellpadding="0">
        <tr>
            <td style="padding:0">

                <div class="alert alert-success big-box">
                    <form action="?" method="post">

                        <div class="row">
                            <div class="col-4 col-md-3 text-right" style="line-height:38px">
                                <h4 class="text-truncate" style="margin:5px 0; text-transform:uppercase;"><?php echo $lang["months"][$m-1] . ' ' . $y ?></h4>
                            </div>
                            <div class="col-4 col-md-3 big-combo" style="padding-right:0">
                                <?php monthPullDown($m, $lang["months"]) ?>
                            </div>
                            <div class="col-4 col-md-2 big-combo" style="padding-left:5px">
                                <?php echo yearPullDown($y) ?>
                            </div>

                            <div class="col-4 offset-4 d-md-none" style="padding-right:0">
                                <input name="prev" type="button" class="btn btn-sm btn-info w-100" id="prev" onclick="window.location = '?m=<?php echo $prevmonth ?>&y=<?php echo $prevyear ?>'" style="margin-top:4px" value="&lt;&lt;&lt; Anterior" />
                            </div>
                            <div class="col-4 d-md-none" style="padding-left:5px">
                                <input name="next" type="button" class="btn btn-sm btn-info w-100" id="next" onclick="window.location = '?m=<?php echo $nextmonth ?>&y=<?php echo $nextyear ?>'" style="margin-top:4px" value="Siguiente &gt;&gt;&gt;" />
                            </div>
                            <div class="d-none d-md-block col-2" style="padding-right:0; padding-left:0;">
                                <input name="prev" type="button" class="btn btn-sm btn-info w-100" id="prev" onclick="window.location = '?m=<?php echo $prevmonth ?>&y=<?php echo $prevyear ?>'" style="margin-top:4px" value="&lt;&lt;&lt; Anterior" />
                            </div>
                            <div class="d-none d-md-block col-2" style="padding-right:19px; padding-left:5px;">
                                <input name="next" type="button" class="btn btn-sm btn-info w-100" id="next" onclick="window.location = '?m=<?php echo $nextmonth ?>&y=<?php echo $nextyear ?>'" style="margin-top:4px" value="Siguiente &gt;&gt;&gt;" />
                            </div>

                        </div>

                    </form>
                </div>

            </td>
        </tr>
<?php
$str = "select programacion.*, local1.localidad origen, local2.localidad destino, tipo, count(historial.id) cnt from programacion inner join localidades local1 on programacion.origen = local1.id inner join localidades local2 on programacion.destino = local2.id inner join tipo_salidas on programacion.id_tipo = tipo_salidas.id left join historial on programacion.id = historial.id_prog and historial.status where year(programacion.fecha) = $y and month(programacion.fecha) = $m and programacion.status group by programacion.id order by programacion.fecha;";
$res = mysql_query($str, $link);
if($count = mysql_num_rows($res)){
?>
	<tr>
		<td><table cellpadding="0" cellspacing="0" class="data-grid">
			<caption>
				SALIDAS PROGRAMADAS DEL MES | (<?php echo $count ?>)
				</caption>
			<tr>
				<th>No.</th>
				<th>Fecha</th>
				<th>Destino</th>
				<th class="d-none d-lg-table-cell">Descripcion</th>
				<th>Tipo</th>
				<th>Guias</th>
				<th width="110"></th>
				<th width="75"></th>
            </tr>
<?php while($row = mysql_fetch_assoc($res)){ ?>
            <tr>
                <td><?php echo formatCode($row["id"]) ?></td>
                <td><?php echo $row["fecha"]; ?></td>
                <td><?php echo $row["origen"]." - ".$row["destino"]; ?></td>
                <td class="d-none d-lg-table-cell"><?php echo $row["descripcion"]; ?></td>
                <td><?php echo $row["tipo"] ?></td>
                <td><div align="right"><?php echo $row["cnt"] ?></div></td>
                <td width="110"><button type="button" class="btn btn-sm btn-outline-danger" onclick="window.open('viewer.php?dll=16&id=<?php echo $row["id"] ?>')" style="width:110px"><i class="fa fa-file-pdf-o" style="margin-right:5px"></i>Manifiesto</button></td>
                <td width="100"><button type="button" class="btn btn-sm btn-outline-dark" style="width:100px" onclick="window.open('viewer.php?dll=17&id=<?php echo $row["id"] ?>')"><i class="fa fa-files-o" style="margin-right:5px"></i>Guias</button></td>
            </tr>
    <?php } ?>

		</table></td>
	</tr>
<?php } ?>
</table>

            </div>
        </div>
    </div>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('select').select2()

        })
    </script>

</div>
<?php include 'footer.php' ?>