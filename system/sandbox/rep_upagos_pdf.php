<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$pdf = new PDF("P");
$pdf->page_footer = $pdf_footer;


$str = "select pagos.*, usuarios.nombre, sum(pagado) pagado from pagos inner join usuarios on pagos.id_usuario = usuarios.id left join (select id, id_venta, fecha, monto pagado from pagos where status) pagado on pagos.id_venta = pagado.id_venta and ((pagado.fecha < pagos.fecha) or (pagado.fecha = pagos.fecha && pagado.id < pagos.id)) where pagos.fecha = '$f1' and usuarios.id = {$user->id} and pagos.status group by pagos.id order by fecha, pagos.id;";
	$res2 = mysql_query($str, $link);
	if(mysql_num_rows($res2)){
		unset($data, $total);
		$pdf->title = array($title." - USUARIO:".strtoupper($user->nombre),"REPORTE DE PAGOS DEL '$f1'");
		$pdf->AddPage();
		while($row2 = mysql_fetch_assoc($res2)){
			$str = "select pc_seguro, declarado, cantidad, cargos, descuento, sum(detalle_venta.tarifa*detalle_venta.cantidad) monto from ventas inner join detalle_venta on ventas.id = detalle_venta.id_venta where ventas.id = {$row2[id_venta]} and detalle_venta.status;";
			$res3 = mysql_query($str, $link);
			$row3 = mysql_fetch_assoc($res3);
			$seguro = ($row3["declarado"]*$row3["pc_seguro"])/100;
			$data[] = array($row2["fecha"], $row2["nombre"], formatCode($row2["id_venta"]), numFormat($precio = ($row3["monto"]+$row3["cargos"]+$seguro-$row3["descuento"])), numFormat($row2["pagado"]), numFormat($row2["monto"]),numFormat($precio-$row2["pagado"]-$row2["monto"]));
			$total += $row2["monto"];
		}
		$header = array("FECHA", "RECIBE", "No. VENTA", "TOTAL VENTA USD", "PAGADO USD", "PAGO USD", "SALDO USD");
		$footer = array("","","","","TOTAL USD:",numFormat($total),"");
		$pdf->ImprovedTable($header, $data, $footer, $c);
	}
	

$pdf->Output();

?>