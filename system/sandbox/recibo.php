<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require_once("pdf/fpdi.php");

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$pdf = new FPDI("P","mm","Letter");
$pdf->AddPage();
$pdf->setSourceFile("pdf/recibo.pdf");  
$tplIdx = $pdf->importPage(1);
$pdf->useTemplate($tplIdx,0,0); 

$str = "SELECT pagos.id, pagos.id_venta, fecha, monto pago, costo, agencia, agencias.telefono, agencias.direccion, usuarios.nombre usuario FROM pagos INNER JOIN usuarios ON pagos.id_usuario = usuarios.id INNER JOIN agencias ON usuarios.id_agencia = agencias.id WHERE md5(pagos.id) = '$id' AND pagos.status;";
$res = mysql_query($str, $link);
if($row = mysql_fetch_assoc($res))
    extract($row);
else
    exit('<hr>ERROR.');

$str = "SELECT SUM(monto) pagado, SUM(costo) costos FROM pagos WHERE id_venta = {$id_venta} AND id < {$id} AND status;";
$res = mysql_query($str, $link);
extract(mysql_fetch_assoc($res));

$str = "SELECT ventas.id_cliente, id_dest, cargos, descuento, seguro, pc_seguro, declarado, SUM(detalle_venta.tarifa * detalle_venta.cantidad) precio, local1.localidad origen, local2.localidad destino FROM ventas INNER JOIN detalle_venta ON ventas.id = detalle_venta.id_venta AND detalle_venta.status INNER JOIN localidades local1 ON ventas.origen = local1.id INNER JOIN localidades local2 ON ventas.destino = local2.id WHERE ventas.id = $id_venta;";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$$key = $value;

$str = "select nombre, apellido, telefono, direccion, id_pais, id_depto, zip, paises.pais from clientes inner join paises on clientes.id_pais = paises.id where clientes.id = $id_cliente;";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$customer[$key] = $value;

switch($customer["id_pais"]){
case 225:
	$str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$customer[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$customer["direccion2"] = $row[0].", ".$row[1]." ".$customer["zip"];
break;
case 136:
	$str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$customer[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$customer["direccion2"] = $row[0].", ".$row[1];
break;
default:
	$str = "select departamento from departamentos where id = {$customer[id_depto]};";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$customer["direccion2"] = $row[0];
break;
}

$str = "select nombre, telefono, direccion, id_pais, id_depto, zip, paises.pais from destinatarios inner join paises on destinatarios.id_pais = paises.id where destinatarios.id = $id_dest;";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$dest[$key] = $value;

switch($dest["id_pais"]){
case 225:
	$str = "select ciudad, abbr from estados inner join ciudades on estados.id = ciudades.id_estado inner join zips on ciudades.id = zips.id_ciudad where zips.zip = '{$dest[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0].", ".$row[1]." ".$dest["zip"];
break;
case 136:
	$str = "select municipio, estado from estadosmx inner join municipios on estadosmx.id = municipios.id_estado inner join zipsmx on municipios.id = zipsmx.id_muni and estadosmx.id = zipsmx.id_estado where zipsmx.zip = '{$dest[zip]}';";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0].", ".$row[1] . ' ' . $dest['zip'];
break;
default:
	$str = "select departamento from departamentos where id = {$dest[id_depto]};";
	$res = mysql_query($str, $link);
	if($row = mysql_fetch_row($res))
		$dest["direccion2"] = $row[0];
break;
}

$pdf->SetFont('Arial','B',12);

$x = 145;
$y = 38;
$pdf->SetXY($x,$y);
$pdf->Write(6,formatCode($id_venta)." / ".formatCode($id));

$pdf->SetFont('Arial','B',9);

$x = 53;
$x2 = 133;
$y = 51.2;
$pdf->SetXY($x,$y);
$pdf->Write(6,formatClientCode($id_cliente));


$pdf->SetFont('Arial','',8);

$y += 5;
$pdf->SetXY($x,$y);
$pdf->Write(6,$customer["nombre"]." ".$customer["apellido"]);
$pdf->SetXY($x2,$y);
$pdf->Write(6,$dest["nombre"]);

$y += 5;
$pdf->SetXY($x,$y);
$pdf->Write(6,$customer["telefono"]);
$pdf->SetXY($x2,$y);
$pdf->Write(6,$dest["telefono"]);

$pdf->SetFont('Arial','',7);

$y += 6;
$pdf->SetXY($x,$y);
$pdf->MultiCell(60,3.1, utf8_decode(ucwords(strtolower($customer["direccion"]."\n".$customer["direccion2"]))),0,'L');
$pdf->SetXY($x2,$y);
$pdf->MultiCell(60,3.1, utf8_decode(ucwords(strtolower($dest["direccion"]."\n".$dest["direccion2"]))),0,'L');

$pdf->SetFont('Arial','B',8);

$x = 108;
$y = 76.6;
$pdf->SetXY($x,$y);
$pdf->Write(6,strtoupper($origen." - ".$destino));

$y += 5;
$pdf->SetXY($x,$y);
$pdf->Write(6,formatTracking($id_venta));



if($seguro)
	$seguro = ($declarado * $pc_seguro)/100;
else
	$seguro = 0;

$total = $precio + $cargos + $costos + $costo + $seguro - $descuento;
$anterior = $total - $pagado;
$saldo = $anterior - $pago;

$x = 160;
$y = 89;
$pdf->SetXY($x,$y);

$pdf->cell(25,4.8,numFormat($total),0,2,'R');
$pdf->cell(25,4.8,numFormat($anterior),0,2,'R');

$y = 101.5;
$pdf->SetXY($x,$y);
$pdf->cell(25,4.8,numFormat($pago),0,2,'R');
$pdf->cell(25,4.8,numFormat($saldo),0,2,'R');

$pdf->SetFont('Arial','I',8);

$x = 27;
$y = 239.5;
$pdf->SetXY($x,$y);
$pdf->Write(6,$agencia);

$y += 4;
$pdf->SetXY($x,$y);
$pdf->Write(6,str_replace("\r\n",' ',$direccion));
$y += 4;
$pdf->SetXY($x,$y);
$pdf->Write(6,"Telefono: ".$telefono);


$x = 146;
$y = 240.9;
$pdf->SetXY($x,$y);
$pdf->Write(6,$usuario);

$y += 5.1;
$pdf->SetXY($x,$y);
$pdf->Write(6,$fecha);

//====== DETALLE DE VENTA ======

$pdf->SetDrawColor(100);
$pdf->SetFillColor(240);
$pdf->SetLineWidth(.05) ;
$pdf->SetFont('Arial','B',6);

$x = 27.9;
$y = 89;
$pdf->SetXY($x,$y);

$pdf->Cell(78,3.5,"DETALLE DE LA VENTA",1,1,'C',1);

$pdf->SetFillColor(250);
$pdf->SetFont('Arial','B',5);

$pdf->SetX($x);

$pdf->Cell(68,3.5,"PRODUCTO",1,0,'L',1);
$pdf->Cell(10,3.5,"CANT.",1,1,'C',1);

$pdf->SetFillColor(255);
$pdf->SetFont('Arial','',5);

$str = "select clase, IF(productos.id, producto, detalle_venta.descripcion) producto, cantidad from detalle_venta inner join clasificacion on detalle_venta.id_clase = clasificacion.id LEFT JOIN productos ON detalle_venta.id_producto = productos.id where detalle_venta.id_venta = $id_venta and detalle_venta.status limit 5;";
$res = mysql_query($str, $link);
while($row = mysql_fetch_assoc($res)){
	$pdf->SetX($x);
	$pdf->Cell(68,3,strtoupper($row["clase"].' - '.$row["producto"]),1,0,'L',0);
	$pdf->Cell(10,3,$row["cantidad"],1,1,'R',0);
}
//====================================

$pdf->Output();

?>