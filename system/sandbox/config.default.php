<?php

date_default_timezone_set('America/Los_Angeles');

// SANDOBOX MODE
define('SANDBOX', true);


/* WSNET PAYPAL GATEWAY: 6080:LIVE | 8080:SANDBOX */
// LOCAL NGROK: define('GATEWAY_API_URL', 'https://44432928.ngrok.io/app/rest/v2/');
define('GATEWAY_API_URL', 'https://seinter.com:' . (SANDBOX ? '8080' : '6080') . '/app/rest/v2/');
define('GATEWAY_API_KEY', 'cmVzdC1hcGk6WDNCLVE0bXdUMkVIV1Vj');

define('GATEWAY_CEXPRESS_USER', 'cexpress-user');
define('GATEWAY_CEXPRESS_PASS', '');

define('GATEWAY_FEE_TYPE', -1); // [-1: TRANSACTION COST (DEFAULT), 0: AMOUNT, 1: PERCENTAGE]
define('GATEWAY_FEE_VALUE', 0);


/* PLIVO */
define('PLIVO_NAME', 'USA CARGO EXPRESS');
define('PLIVO_PHONE_NUMBER', '');
define('PLIVO_AUTH_ID', '');
define('PLIVO_AUTH_TOKEN', '');


// MYSQL DB
define('DB_HOST', 'localhost');
define('DB_NAME', 'courier');
define('DB_USER', '');
define('DB_PASS', '');

define('DB_MASTER', '');


// VARIABLES
$title = "Sistema Courier USA Cargo Express - CExpress_v4.0";
$copyright = "&copy; " . date('Y') . " Copyright USA Cargo Express.<br>All rights reserved.";
$pdf_footer = "Powered by WSNet";

$_seguro = 10;

$pfix = "pfix_"; // INSTANCE PREFIX
$company = "USA CARGO EXPRESS";
$contact = "support@seinter.com";
$records_page = 30;


// DEPRECATED
if($_POST)
	foreach($_POST as $key => $value)
		$$key = $value;
if($_GET)
	foreach($_GET as $key => $value)
		$$key = $value;

?>