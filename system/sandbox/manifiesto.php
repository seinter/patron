<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user);

$link = DB::connect();

$pdf = new PDF("L");
$pdf->page_footer = $pdf_footer;

$str = "select programacion.descripcion, programacion.fecha, local1.localidad origen, local2.localidad destino, tipo from programacion inner join localidades local1 on programacion.origen = local1.id inner join localidades local2 on programacion.destino = local2.id inner join tipo_salidas on programacion.id_tipo = tipo_salidas.id where programacion.id = $id;";
$res = mysql_query($str, $link);
$row = mysql_fetch_assoc($res);
foreach($row as $key => $value)
	$$key = $value;

$pdf->title = array($title." - MANIFIESTO DE ".strtoupper($tipo), formatCode($id)." - ".	$fecha.": ".$origen." - ".$destino." | ".$descripcion);
$pdf->AddPage();

$str = "select bitacora.id, bitacora.codigo, ventas.id id_venta, local1.localidad origen, local2.localidad destino, concat(clientes.nombre,' ',clientes.apellido) nombre, clientes.telefono, destinatarios.nombre dest_nombre, destinatarios.telefono dest_telefono, clasificacion.clase, IF(productos.id, productos.producto, detalle_venta.descripcion) producto from historial inner join bitacora on historial.id_bitacora = bitacora.id inner join detalle_venta on bitacora.id_detalle = detalle_venta.id INNER JOIN clasificacion ON detalle_venta.id_clase = clasificacion.id inner join ventas on detalle_venta.id_venta = ventas.id inner join clientes on ventas.id_cliente = clientes.id inner join destinatarios on ventas.id_dest = destinatarios.id inner join localidades local1 on ventas.origen = local1.id inner join localidades local2 on ventas.destino = local2.id LEFT JOIN productos ON detalle_venta.id_producto = productos.id where historial.status and historial.id_prog = $id order by bitacora.id;";
$res = mysql_query($str, $link);
while($row = mysql_fetch_assoc($res))
	$records[$row["destino"]][] = $row;

if($records)
	foreach($records as $key => $value){
		unset($data);
		foreach($value as $row)
			$data[] = array(formatCode($row["id"]), formatCode($row["id_venta"]), "| ".$row["origen"], "| ".$row["nombre"], "| ".$row["telefono"], "| ".$row["destino"], "| ".$row["dest_nombre"], "| ".$row["dest_telefono"], "| ".$row["clase"]." - ".$row["producto"]);
		$pdf->SetFont('Verdana','B',10);
		$pdf->Cell(0,20,"DESTINO: ".$key,0,1);
		$header = array("No. GUIA", "No. VENTA", "ORIGEN", "REMITENTE", "TELEFONO", "DESTINO", "DESTINATARIO", "TELEFONO", "PRODUCTO");
		$footer = array('','','','','','','','','');
		$pdf->ImprovedTable($header, $data, $footer);
		$pdf->Ln(20);
	}

$pdf->Output();

?>