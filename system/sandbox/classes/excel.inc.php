<?php

class Excel
{

	var $y = 0;
	
	function Excel($name){
	
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$name.".xls");
		header("Content-Transfer-Encoding: binary");
		
		$this->xlsBOF();
		
	}
	
	function title($title){
		$this->xlsWriteLabel($this->y++, 0, $title[0]);
		$this->xlsWriteLabel($this->y++, 0, $title[1]);
	}
	
	function table($header, $data, $footer = array()){
	
		array_unshift($data, $header);
		if($footer)
			array_push($data, $footer);
	
		for($y = 0; $y < count($data); $y++){
			for($x = 0; $x < count($data[$y]); $x++){
				if(is_numeric($data[$y][$x]))
					$this->xlsWriteNumber($this->y, $x, $data[$y][$x]);
				else
					$this->xlsWriteLabel($this->y, $x, $data[$y][$x]);
			
			}
			$this->y++;
		}
		
	}
	
	function Ln($val = 1){
		$this->y += $val;
	}
	
	function xlsBOF() {
	    echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
	    return;
	}
	
	function xlsEOF() {
	    echo pack("ss", 0x0A, 0x00);
	    return;
	}
	
	function xlsWriteNumber($Row, $Col, $Value) {
	    echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
	    echo pack("d", $Value);
	    return;
	}
	
	function xlsWriteLabel($Row, $Col, $Value ) {
	    $L = strlen($Value);
	    echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
	    echo $Value;
	    return;
	}
}

?>