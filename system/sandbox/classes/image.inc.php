<?php

class Image {

	public static function resampImage($forcedwidth, $forcedheight, $sourcefile, $imgcomp){
		$g_imgcomp=100-$imgcomp;
		$g_srcfile=$sourcefile;
		$g_fw=$forcedwidth;
		$g_fh=$forcedheight;
		if(file_exists($g_srcfile)){
			$g_is=getimagesize($g_srcfile);
			if(($g_is[0]-$g_fw)>=($g_is[1]-$g_fh)){
				$g_iw=$g_fw;
				$g_ih=($g_fw/$g_is[0])*$g_is[1];
			} else {
				$g_ih=$g_fh;
				$g_iw=($g_ih/$g_is[1])*$g_is[0]; 
			}
			$img_src=imagecreatefromjpeg($g_srcfile);
			$img_dst=imagecreatetruecolor($g_iw,$g_ih);
			imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, $g_iw, $g_ih, $g_is[0], $g_is[1]);
			ob_start();
			imagejpeg($img_dst, null, $g_imgcomp);
			$image = ob_get_contents();
			ob_end_clean();
			imagedestroy($img_dst);
			return $image;
		} else
			return false;
	}

}
	
?>