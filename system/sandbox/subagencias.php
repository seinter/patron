<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 2);

if(+$user->id_nivel !== 2){
    header('Location: index.php');
    exit();
}


$link = DB::connect();

$res = mysql_query("SELECT comision max FROM agencias WHERE id = {$user->id_agencia}", $link);
extract(mysql_fetch_assoc($res));

switch($_REQUEST['dll']){
	case 1:
		$comision = $comision > $max ? $max : $comision;
		if($_POST['id'])
                $str="UPDATE agencias SET agencia = '" . mysql_real_escape_string($_POST['agencia']) . "', direccion = '" . mysql_real_escape_string($_POST['direccion']) . "', telefono = '" . $_POST['telefono'] . "', comision = " . +$_POST['comision'] . " WHERE id = " . $_POST['id'];
		else
			$str = "INSERT INTO agencias(id_central, agencia, direccion, telefono, comision) VALUES({$user->id_agencia}, '" . mysql_real_escape_string($_POST['agencia']) . "', '" . mysql_real_escape_string($_POST['direccion']) . "', '" . $_POST['telefono'] . "', " . +$_POST['comision'] . ")";
		mysql_query($str, $link);
		header("Location: ?");
		exit();
		break;
	case 2:
		$str = 'SELECT * FROM agencias WHERE id = ' . $_POST['id'];
		$res = mysql_query($str,$link);
		$row = mysql_fetch_assoc($res);
		foreach($row as $key => $value)
			$$key = $value;
		break;
	case 3:
		$str = "UPDATE agencias LEFT JOIN usuarios ON agencias.id = usuarios.id_agencia SET agencias.status = 0, usuarios.status = 0 WHERE agencias.id = " . $_GET['id'];
		mysql_query($str, $link);
		header("Location: ?");
		exit();
		break;
}

?>
<?php include 'header.php' ?>
<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right caption">SUB-AGENCIAS</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">

                <table width="100%" border="0" cellspacing="10" cellpadding="0">
                    <tr>
                        <td>
                            <form id="add" name="add" method="post" action="?" onsubmit="return validateAgency(this)">
                                <input type="hidden" name="dll" value="1"/>
                                <input type="hidden" name="id" value="<?php echo $id ?: 0 ?>"/>
                                <table cellpadding="0" cellspacing="0" class="data-form">
                                    <caption>AGREGAR / EDITAR SUB AGENCIAS</caption>
                                    <tr>
                                        <th>Sub Agencia:</th>
                                        <td><input name="agencia" type="text" id="agencia" value="<?php echo $agencia ?>"/></td>
                                        <th>Teléfono:</th>
                                        <td><input name="telefono" type="text" id="telefono" value="<?php echo $telefono ?>" class="phone-input"/></td>
                                    </tr>
                                    <tr>
                                        <th rowspan="2" valign="top">Dirección:</th>
                                        <td rowspan="2">
                                            <textarea name="direccion" id="direccion" style="height:68px"><?php echo $direccion ?></textarea>
                                        </td>
                                        <th>Comisión <strong>(MAX <?php echo $max ?>%)</strong>:</th>
                                        <td><input name="comision" type="text" class="forMoney" id="comision" value="<?php echo numFormat($comision) ?>" onblur="this.value = numFormat(+this.value > <?php echo $max ?> ? <?php echo $max ?> : this.value)"/>
                                        </td>
                                    </tr>

                                    <tr></tr>

                                    <tr>
                                        <th colspan="4" class="text-center">
                                        <?php if ($id) { ?>
                                            <input name="Button" type="button" class="btn btn-danger cancel" value="&lt;&lt;&lt; Cancelar" onclick="window.location = '?'"/>
                                        <?php } ?>
                                            <input name="Submit" type="submit" class="btn btn-primary save" value="Guardar &gt;&gt;&gt;"/>
                                        </th>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <?php
                    //------------------
                    $str = "SELECT count(*) FROM agencias WHERE id_central = {$user->id_agencia} AND status;";
                    $res = mysql_query($str, $link);
                    $row = mysql_fetch_row($res);
                    $counter = $row[0];
                    $pages = new Pages($counter, $records_page);
                    //------------------
                    ?>

                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="data-grid">
                                <caption>
                                    SUB AGENCIAS | <?php echo $pages->header() ?>
                                </caption>
                                <tr>
                                    <th>Sub Agencia</th>
                                    <th>Telefono</th>
                                    <th class="d-none d-md-table-cell">Comisión %</th>
                                    <th width="75">&nbsp;</th>
                                    <th width="75">&nbsp;</th>
                                </tr>
                                <?php
                                $str = "SELECT * FROM agencias WHERE id_central = {$user->id_agencia} AND status ORDER BY agencia LIMIT {$pages->from}, {$pages->rpage};";
                                $res = mysql_query($str, $link);
                                while ($row = mysql_fetch_assoc($res)) {
                                    ?>
                                    <tr class="<?php echo $row["id"] === $id ? "table-primary" : '' ?>">
                                        <td><?php echo $row["agencia"] ?></td>
                                        <td><?php echo $row["telefono"] ?></td>
                                        <td class="text-right d-none d-md-table-cell"><?php echo numFormat($row['comision']) ?></td>
                                        <td width="75">
                                            <form action="?" method="post" name="edit" id="edit">
                                                <input type="hidden" name="dll" value="2"/>
                                                <input type="hidden" name="id" value="<?php echo $row["id"] ?>"/>
                                                <input name="Submit" type="submit" class="btn btn-sm btn-primary edit" value="- E -"/>
                                            </form>
                                        </td>
                                        <td width="75"><input type="button" class="btn btn-sm btn-danger delete" value="- X -" onclick="confirmDelete(<?php echo $row["id"] ?>,'<?php echo $row["agencia"] ?>')"/>
                                        </td>
                                    </tr>
                                    <?php
                                }

                                //----------------------------------
                                if ($pages->pages > 1) {
                                    ?>
                                    <tr bgcolor="#FFFFFF">
                                        <td colspan="5">
                                            <div align="right"><?php echo $pages->footer() ?></div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                //---------------------------
                                ?>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <script language="javascript" type="text/javascript">
        function confirmDelete(id, name){
            if(confirm("Esta a punto de eliminar la Sub-Agencia '" + name + "',\ndesea continuar?"))
                window.location = '?dll=3&id=' + id;
        }
    </script>

    <script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>

</div>
<?php include 'footer.php' ?>