<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

require 'vendor/autoload.php';
// use PHPExcel;


/*
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);
*/

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 3);

$link = DB::connect();

$workbook = new PHPExcel();

list($f1, $f2) = explode(' / ', $_POST['f']);
$filter = ($ida = $_POST['ida']) ? " AND id = {$ida} " : '';
$str = "SELECT id, agencia, codigo, comision FROM agencias WHERE NOT id_central AND status {$filter} ORDER BY id";
$res = mysql_query($str, $link);
while($row = mysql_fetch_object($res)){

    $str = "SELECT
        ventas.id, IFNULL(pagos.modificado, pagos.creado) fecha, tipo_ventas.tipo,
        agencias.id id_agencia, CONCAT(IF(agencias.id_central, ' -> ', ''), agencias.agencia) agencia,
        CONCAT(clientes.nombre, ' ', clientes.apellido) cliente,
        
        @wallet := agencias.id_central AND NOT agencias.convenio,
        
        @amount := pagos.monto - pagos.costo monto,
        @cash := IF(pagos.id_tipo, 0, pagos.monto - IF(@wallet AND agencias.id = reciben.id_agencia, (pagos.monto * agencias.comision) / 100, 0)) cash,
        @card := IF(pagos.id_tipo, @amount, 0) card,
        @central := @amount * IF(agencias.id_central, centrales.comision - agencias.comision, agencias.comision) / 100 comision_central,
        @sub := IF(agencias.id_central, IF(pagos.id_tipo OR agencias.id != reciben.id_agencia OR NOT (pagos.id_tipo OR @wallet), @amount * agencias.comision / 100, 0), 0) comision_subagencia,
        IF(pagos.id_tipo, @card, @cash) - @central - @sub neto,
        
        log_transacciones.referencia
        
    FROM ventas
        INNER JOIN tipo_ventas ON ventas.id_tipo = tipo_ventas.id
        INNER JOIN usuarios ON ventas.id_usuario = usuarios.id
        INNER JOIN agencias ON usuarios.id_agencia = agencias.id
        INNER JOIN clientes ON ventas.id_cliente = clientes.id
        INNER JOIN pagos ON ventas.id = pagos.id_venta
        INNER JOIN usuarios reciben ON pagos.id_usuario = reciben.id
        
        LEFT JOIN agencias centrales ON agencias.id_central = centrales.id
        
        LEFT JOIN log_transacciones ON pagos.referencia = log_transacciones.referencia
        
    WHERE
        {$row->id} IN (agencias.id, agencias.id_central)
        AND DATE(IFNULL(pagos.modificado, pagos.creado)) BETWEEN '{$f1}' AND '{$f2}' AND NOT pagos.pendiente AND pagos.status
        AND ventas.status";

	$res2 = mysql_query($str, $link);
	if(mysql_num_rows($res2)) {

		unset($data);

        $sub = "REPORTE SEMANAL DE COMISIONES: [{$f1} / {$f2}]";
        $agencia = "AGENCIA: {$row->agencia} ({$row->codigo}) | " . numFormat($row->comision) . '%';


        $sheet = $sheet ? $workbook->createSheet() : $workbook->getActiveSheet();
        $sheet->setTitle("{$row->agencia} ({$row->codigo})");
        $sheet->setCellValue('A1', $title);
        $sheet->setCellValue('A2', $sub);
        $sheet->getStyle('A2')->getFont()->setSize(20);

        for($x = 0; $x < 12; $x++)
            $sheet->getColumnDimensionByColumn($x)->setWidth(in_array($x, [1, 3, 4]) ? 25 : 15);


        /* HEADER */
        $i = 4;
        $style1 = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                ]
            ],
            'font' => [
                'bold' => true
            ]
        ];
        $sheet->setCellValue("A{$i}", $agencia);
        $sheet->mergeCells($caption = "A{$i}:L{$i}");
        $sheet->getStyle($caption)->applyFromArray($style1);
        $sheet->getStyle($caption)->getFont()->setSize(12);

        $style2 = array_merge($style1,
            [
                'borders' => [
                    'allborders' => [
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    ]
                ],
                'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => '66799d']
                ],
                'font' => [
                    'color' => ['rgb' => 'FFFFFF']
                ]
            ]);
        $sheet->fromArray(
            ['No.', 'FECHA / HORA', 'TIPO', 'AGENCIA', 'CLIENTE', 'MONTO', 'EFECTIVO', 'TARJETA', 'COMISION CENTRAL', 'COMISION SUBAG', 'NETO', 'REFERENCIA'],
            [NULL], 'A' . ++$i
        );
        $sheet->getStyle("A{$i}:L{$i}")->applyFromArray($style2);


		while($item = mysql_fetch_object($res2)){

			$data[] = [formatCode($item->id), $item->fecha, $item->tipo, utf8_decode($item->agencia), utf8_decode($item->cliente), numFormat($item->monto),  numFormat($item->cash), numFormat($item->card), numFormat($item->comision_central), numFormat($item->comision_subagencia), numFormat($item->neto), $item->referencia];

			$agencias[$row->id][$item->id_agencia] = $item->agencia;

			$total[$row->id][$item->id_agencia] += $item->monto;
			$tcash[$row->id][$item->id_agencia] += $item->cash;
			$tcard[$row->id][$item->id_agencia] += $item->card;
			$tcentral[$row->id][$item->id_agencia] += $item->comision_central;
			$tsub[$row->id][$item->id_agencia] += $item->comision_subagencia;
			$tneto[$row->id][$item->id_agencia] += $item->neto;

		}


        $h = $i + count($data);
        $style3 = [
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                ]
            ]
        ];
        $sheet->fromArray($data, [NULL], 'A' . ++$i);
        $sheet->getStyle("A{$i}:L{$h}")->applyFromArray($style3);


        $style4 = array_merge($style3,
            [
                'borders' => [
                    'allborders' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                    ]
                ],
                'font' => [
                    'bold' => true
                ]
            ]);

        $right = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
            ],
        ];

        $sheet->fromArray(
            ['TOTALES USD: ', numFormat(array_sum($total[$row->id])), numFormat(array_sum($tcash[$row->id])), numFormat(array_sum($tcard[$row->id])), numFormat(array_sum($tcentral[$row->id])), numFormat(array_sum($tsub[$row->id])), numFormat(array_sum($tneto[$row->id]))]
            , [NULL], 'E' . ++$h);
        $sheet->getStyle("E{$h}:K{$h}")->applyFromArray($style4);
        $sheet->getStyle("E{$h}")->applyFromArray($right);
        $currency = ['code' => '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)'];
        $sheet->getStyle("E{$i}:K{$h}")->getNumberFormat()->applyFromArray($currency);



        /* RESUME */
        $i = $h + 3;
        $sheet->setCellValue("A{$i}", 'RESUMEN');
        $sheet->mergeCells($caption = "A{$i}:D{$i}");
        $sheet->getStyle($caption)->applyFromArray($style1);
        $sheet->getStyle($caption)->getFont()->setSize(12);

        $i++;
        $sheet->setCellValue("A{$i}", 'AGENCIA');
        $sheet->mergeCells($caption = "A{$i}:C{$i}");
        $sheet->getStyle($caption)->applyFromArray($style2);
        $sheet->setCellValue("D{$i}", 'COMISION');
        $sheet->getStyle("D{$i}")->applyFromArray($style2);


        $resumen[$row->id][] = [$row->agencia, numFormat(array_sum($tcentral[$row->id]))];
        foreach($agencias[$row->id] as $key => $value)
            if(+$row->id !== $key)
                $resumen[$row->id][] = [$value, numFormat($tsub[$row->id][$key])];

        foreach($resumen[$row->id] as $key => $value) {

            $i++;
            $sheet->setCellValue("A{$i}", $value[0]);
            $sheet->mergeCells($caption = "A{$i}:C{$i}");
            $sheet->getStyle($caption)->applyFromArray($style3);
            $sheet->setCellValue("D{$i}", $value[1]);
            $sheet->getStyle("D{$i}")->applyFromArray($style3);
            $sheet->getStyle("D{$i}")->getNumberFormat()->applyFromArray($currency);

        }

        $i++;
        $sheet->setCellValue("A{$i}", 'TOTAL USD: ');
        $sheet->mergeCells($caption = "A{$i}:C{$i}");
        $sheet->getStyle($caption)->applyFromArray($style4);
        $sheet->getStyle($caption)->applyFromArray($right);
        $sheet->setCellValue("D{$i}", numFormat(array_sum($tcentral[$row->id]) + array_sum($tsub[$row->id])));
        $sheet->getStyle("D{$i}")->applyFromArray($style4);
        $sheet->getStyle("D{$i}")->getNumberFormat()->applyFromArray($currency);

    }

}


$workbook->setActiveSheetIndex(0);
$writer = new PHPExcel_Writer_Excel2007($workbook);
$filename = "Reporte de Comisiones {$f1} - {$f2}.xlsx";

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename .'"');
header('Cache-Control: max-age=0');
$writer->save('php://output');


?>
