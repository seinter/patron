<?php
if(!$user)
	exit();
?>

	<ul>
		<li><a href="main.php">INICIO</a></li>

    <?php if($user->id_nivel==2 || ($user->id_nivel==1 && $user->bod)): ?>
        <li><a>BODEGA</a>
            <ul>
                <li><a href="bodega.php">Acciones de Bodega</a></li>
                <li><a href="manifiestos.php">Manifiestos</a></li>
                <li><a href="itinerarios.php">Agenda (USA)</a></li>
                <li><a href="rastreo.php">Rastreo de Envíos</a></li>
            </ul>
        </li>
    <?php endif ?>

    <?php if($user->id_nivel==2 || ($user->id_nivel==1 && $user->opr)): ?>
        <li><a>OPERACIONES</a>
            <ul>
                <li><a href="consultas.php">Consultas</a></li>
                <li><a href="clientes.php?act=true">Nueva Venta</a></li>
                <li><a href="clientes.php">Clientes</a></li>
            </ul>
        </li>
    <?php endif ?>

    <?php if($user->id_nivel==2): ?>
        <li><a>ADMINISTRACION</a>
            <ul>
                <li><a href="agentes.php">Representantes</a></li>
            <?php if(!$user->id_central): ?>
                <li><a href="subagencias.php">Sub Agencias</a></li>
                <li><a href="subagentes.php">Sub Agentes</a></li>
            <?php endif ?>
                <li><a><b>Reportes</b></a>
                    <ul>
                        <li><a href="rep_ventas.php">Ventas por Fecha</a></li>
                        <li><a href="rep_pagos.php">Pagos por Fecha</a></li>
                        <li><a href="rep_comisiones.php">Comisiones Semanales</a></li>
                        <li><a href="graph_pagos.php">Estadisticas del Mes</a></li>
                    </ul>
                </li>
            <?php if($user->cartera): ?>
                <li><a href="cartera.php">Medios de Pago</a></li>
            <?php endif ?>
            </ul>
        </li>
    <?php endif ?>

    <?php if($user->id_nivel==3): ?>
        <li><a>CONFIGURACION</a>
            <ul>
                <li><a href="agencias.php">Agencias</a></li>
                <li><a><b>Usuarios</b></a>
                    <ul>
                        <li><a href="susuarios.php" style="min-width:210px">Super Usuarios</a></li>
                        <li><a href="usuarios.php" style="min-width:210px">Administradores / Representantes</a></li>
                    </ul>
                </li>
                <li><a href="carriers.php">PAQ-NAC</a></li>
                <li><a href="destinos.php">Destinos</a></li>
                <li><a href="clasificacion.php">Clasificación</a></li>
                <li><a href="productos.php">Productos</a></li>
                <li><a href="tarifas_pie.php">Tarifas por Pie</a></li>
                <li><a href="rutas.php">Rutas (USA)</a></li>
            </ul>
        </li>

        <li><a>PROGRAMACION</a>
            <ul>
                <li><a href="programacion.php">Programar Salidas</a></li>
            </ul>
        </li>

        <li><a>REPORTES</a>
            <ul>
                <li><a href="rep_ventas_global.php">Ventas por Fecha</a></li>
                <li><a href="rep_pagos_global.php">Ingresos por Fecha</a></li>
                <li><a href="rep_comisiones_global.php">Comisiones Semanales</a></li>
                <li><a href="graph_pagos_global.php">Estadisticas del Mes</a></li>
            </ul>
        </li>
    <?php endif ?>


    </ul>