<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");

// PAYMENT GATEWAY
include('classes/payment.inc.php');

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 2);


$link = DB::connect();

switch($_REQUEST['dll']){

    case 1:
        mysql_query("UPDATE cartera INNER JOIN usuarios ON cartera.id_usuario = usuarios.id SET cartera.activo = 0 WHERE usuarios.id_agencia = {$user->id_agencia}", $link);

        $cc_number = implode('', array_fill(0, strlen($_POST['number']) - 5, 'x')) . substr($_POST['number'], -4);
        $cc_name = strtoupper($_POST['firstName'] . ' ' . $_POST['lastName']);

        $str = "INSERT INTO cartera(id_usuario, codigo, valido_hasta, tipo, nombre, numero) VALUES({$user->id}, '" . $_POST['code'] . "', '" . date("Y-m-d H:i:s", strtotime($_POST['validUntil'])) . "', '" . $_POST['type'] . "', '$cc_name', '$cc_number')";
        mysql_query($str, $link);
        header("Location: ?" . ($_REQUEST['modal'] ? 'modal=true' : ''));
        exit();
        break;
    case 2:
        mysql_query("UPDATE cartera INNER JOIN usuarios ON cartera.id_usuario = usuarios.id SET cartera.activo = 0 WHERE usuarios.id_agencia = {$user->id_agencia}", $link);
        $str = "UPDATE cartera SET activo = 1 WHERE id = " . $_REQUEST['id'];
        mysql_query($str, $link);
        header("Location: ?" . ($_REQUEST['modal'] ? 'modal=true' : ''));
        exit();
        break;
    case 3:
        /*** DELETE FROM VAULT ***/
        if($token = Payment::getGatewayToken($user)) {

            $res = mysql_query("SELECT codigo FROM cartera WHERE id = {$id}", $link);
            extract(mysql_fetch_assoc($res));
            $data = Payment::deleteCard($token, formatAgencyVaultCode($user->id_agencia), $codigo);

            if($data->status === 'SUCCESSFUL'){
                mysql_query("UPDATE cartera SET status = 0 WHERE id = {$id}", $link);
                header("Location: ?");
                exit();
            } else
                $err = $data->message;

        } else
            $err = 'La procesadora de tarjetas se encuentra fuera de servicio!';

        break;
    case 4:
        $str = "UPDATE pagos SET pendiente = 0, monto = monto + " . +$_POST['fee'] . ", costo = " . +$_POST['fee'] . ", referencia = '" . $_POST['invoice'] . "' WHERE id = " . $_POST['id'];
        mysql_query($str, $link);
        header("Location: ?" . ($_REQUEST['modal'] ? 'modal=true' : ''));
        exit();
        break;



    /*
     8888888b.                    8888888b.           888      .d8888b.           888
     888   Y88b                   888   Y88b          888     d88P  Y88b          888
     888    888                   888    888          888     888    888          888
     888   d88P  8888b.  888  888 888   d88P  8888b.  888     888         8888b.  888888  .d88b.  888  888  888  8888b.  888  888
     8888888P"      "88b 888  888 8888888P"      "88b 888     888  88888     "88b 888    d8P  Y8b 888  888  888     "88b 888  888
     888        .d888888 888  888 888        .d888888 888     888    888 .d888888 888    88888888 888  888  888 .d888888 888  888
     888        888  888 Y88b 888 888        888  888 888     Y88b  d88P 888  888 Y88b.  Y8b.     Y88b 888 d88P 888  888 Y88b 888
     888        "Y888888  "Y88888 888        "Y888888 888      "Y8888P88 "Y888888  "Y888  "Y8888   "Y8888888P"  "Y888888  "Y88888
                              888                                                                                             888
                         Y8b d88P                                                                                        Y8b d88P
                          "Y88P"                                                                                          "Y88P"
    */
    case 10:
        /*** VAULT ***/
        // GET TOKEN
        $data = ['status' => false];
        if($token = Payment::getGatewayToken($user)) {

            $data = [
                'status' => true,
                'customer_id' => formatAgencyVaultCode($user->id_agencia),
                'gateway' => [
                    'url' => GATEWAY_API_URL . 'services/gateway_PaymentService/storeCard',
                    'token' => $token,
                ]
            ];

        } else
            $data['message'] = 'La procesadora de tarjetas se encuentra fuera de servicio!';

        header('Content-Type: application/json');
        print json_encode($data);
        exit();

        break;


    case 12:
        /*** DIRECT INCLUSIVE PAYMENT ***/

        // IF ALREADY PAID
        if(!$pending = Payment::getPendingBalance($user, $id = $_POST['id'])) {
            http_response_code(412); // Precondition Failed (IS NO LONGER PENDING)
            exit();
        }
        if($pending->referencia) {
            http_response_code(423); // Locked
            exit();
        }

        // GET TOKEN AND LOCK
        $data = ['status' => false];
        if($token = Payment::getGatewayToken($user)) {

            $key = Payment::generateKey();

            // FETCH VAULTED CARD

            if($card = Payment::getActiveMethod($user)) {
                $card->customer_id = formatAgencyVaultCode($card->customer_id);

                // DO THE LOCKING
                if(!Payment::lockPending($id, $key)){
                    http_response_code(412); // Precondition Failed (COULDN'T DO THE LOCKING)
                    exit();
                }

                $data = [
                    'status' => true,
                    'key' => $key,
                    'fee' => 0,
                    'card' => $card,
                    'gateway' => [
                        'url' => GATEWAY_API_URL . 'services/gateway_PaymentService/directInclusivePayment',
                        'token' => $token
                    ]
                ];

            } else
                $data['message'] = 'Debe configurar un medio de pago válido.';

        } else
            $data['message'] = 'La procesadora de cobros se encuentra fuera de servicio.';

        header('Content-Type: application/json');
        print json_encode($data);
        exit();

        break;
    case 13:
        // RELEASE PENDING
        header('Content-Type: application/json');
        print json_encode(Payment::releasePending($_POST['key']));
        exit();
        break;
    case 14:
        // LOG TRANSACTION
        header('Content-Type: application/json');
        print json_encode(Payment::logTransaction($user, $_POST));
        exit();
        break;


}

$wallet = [];
$str = "SELECT cartera.* FROM cartera INNER JOIN usuarios ON cartera.id_usuario = usuarios.id WHERE usuarios.id_agencia = {$user->id_agencia} AND cartera.status";
$res = mysql_query($str, $link);
while($row = mysql_fetch_assoc($res))
    $wallet[] = $row;

$pending = Payment::getPendingBalance($user);


?>
<?php include ($_REQUEST['modal'] ? '_top.php' : 'header.php') ?>

<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right text-truncate caption">MEDIOS DE PAGO</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">

                <table width="100%" border="0" cellspacing="10" cellpadding="0">
                    <tr>
                        <td>

                            <div class="alert bg-warning panel-orange text-right p-2 mb-2">
                                <button type="button" id="new-card" class="btn btn-primary"><i class="fa fa-credit-card" style="margin-right:5px"></i> NUEVO MEDIO DE PAGO</button>
                            </div>

                        <?php if ($pending) { ?>

                            <div class="alert alert-danger p-2 mb-2">

                                <form action="?" method="post" onsubmit="return false">
                                    <input type="hidden" name="dll" value="4">
                                    <input type="hidden" name="modal" value="<?php echo $_REQUEST['modal'] ?: 0 ?>">
                                    <input type="hidden" name="id" value="<?php echo $pending->id_pago ?>">

                                    <h6 class="bg-danger text-white m-0 p-2">
                                        <i class="fa fa-warning ml-1"></i> <strong>ALERTA:</strong> Cobro en efectivo desactivado, débito pendiente.
                                    </h6>
                                    <table class="table table-sm table-light table-hover mb-2" style="margin-bottom:9px">
                                        <thead>
                                        <tr>
                                            <th width="25%">Venta No.</th>
                                            <th class="d-none d-md-table-cell">Cliente</th>
                                            <th width="15%">Monto USD</th>
                                            <th width="15%">Comision USD</th>
                                            <th width="15%">Saldo USD</th>
                                        </tr>
                                        </thead>
                                        <tr>
                                            <td><div id="titulo"><strong><?php echo formatCode($pending->id) ?>:</strong> <?php echo $pending->fecha ?></div></td>
                                            <td class="d-none d-md-table-cell"><?php echo $pending->cliente ?></td>
                                            <td class="text-right"><?php echo numFormat($pending->monto) ?></td>
                                            <td class="text-right"><?php echo numFormat($comision = ($pending->monto * $pending->comision / 100)) ?></td>
                                            <td class="text-right"><?php echo numFormat($balance = $pending->monto - $comision) ?></td>
                                        </tr>
                                        <tfoot>
                                        <tr class="text-right">
                                            <th class="d-none d-md-table-cell"></th>
                                            <th colspan="3">PENDIENTE DE DEBITAR <i class="fa fa-arrow-circle-right ml-1"></i></th>
                                            <th><div id="payment"><?php echo numFormat($balance) ?></div></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <div class="text-center">
                                        <button type="button" id="btn-payment" class="btn btn-danger pr-4 pl-4"><i class="fa fa-credit-card" style="margin-right:5px"></i> PROCESAR PAGO</button>
                                    </div>

                                </form>
                            </div>

                        <?php } ?>

                            <form action="?" method="post">
                                <input type="hidden" name="dll" value="2">
                                <input type="hidden" name="modal" value="<?php echo $_REQUEST['modal'] ?: 0 ?>">

                                <table class="data-grid">
                                    <caption>MEDIOS DE PAGO</caption>
                                    <tr>
                                        <th width="50">Activo</th>
                                        <th width="75">Tipo</th>
                                        <th>Nombre</th>
                                        <th>Numero</th>
                                        <th class="d-none d-md-table-cell">Creado</th>
                                        <th class="d-none d-sm-table-cell">Valido Hasta</th>
                                        <th width="75"></th>
                                    </tr>
                                    <?php foreach($wallet as $row): ?>
                                        <tr>
                                            <td class="text-center">
                                                <input class="radio" type="radio" name="id" value="<?php echo $row['id'] ?>" <?php echo $row['activo'] ? 'checked' : '' ?>>
                                            </td>
                                            <td  class="text-center text-light" style="background-color:#2A5980; padding:0;">
                                                <?php if(in_array($row['tipo'], ['amex', 'discover', 'mastercard', 'visa'])){ ?>
                                                    <img src="assets/js/payment/cards/<?php echo $row['tipo'] ?>.png" width="75">
                                                <?php } else
                                                    echo strtoupper($row['tipo'])
                                                ?>
                                            </td>
                                            <td style="padding-left:6px"><samp><?php echo $row["nombre"] ?></samp></td>
                                            <td><samp class="text-muted"><?php echo $row["numero"] ?></samp></td>
                                            <td class="d-none d-md-table-cell"><?php echo $row["creado"] ?></td>
                                            <td class="d-none d-sm-table-cell <?php echo strtotime($row['valido_hasta']) < time() ? 'text-danger' : 'text-success' ?>"><?php echo $row["valido_hasta"] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-danger w-100" onclick="confirmDelete(<?php echo $row["id"] ?>,'<?php echo strtoupper($row['tipo']) . ': '. $row['numero'] ?>')">- X -</button>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </table>
                            </form>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>


    <link rel="stylesheet" href="assets/js/fancybox2/jquery.fancybox.css" />
    <script type="text/javascript" src="assets/js/fancybox2/jquery.fancybox.js"></script>

    <link href="assets/js/icheck/skins/flat/blue.css" rel="stylesheet">
    <script type="text/javascript" src="assets/js/icheck/icheck.min.js"></script>

    <script type="text/javascript" src="assets/js/js.cookie.js"></script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.full.min.js"></script>

    <link rel="stylesheet" href="assets/js/payment/payment.css">
    <script type="text/javascript" src="assets/js/payment/jquery.payform.js"></script>
    <script type="text/javascript" src="assets/js/payment/payment.js"></script>
    <script type="text/javascript" src="assets/js/payment/parse-names.js"></script>

<?php if($err): ?>
    <script language="javascript" type="text/javascript">
    $(function(){
        flashAlert('<?php echo $err ?>')
    })
    </script>
<?php endif ?>

    <script type="text/javascript" src="assets/js/wallet.js"></script>

</div>

<?php include ($_REQUEST['modal'] ? '_bottom.php' : 'footer.php') ?>