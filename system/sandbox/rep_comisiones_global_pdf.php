<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
require('pdf/stpdf.php');

/*
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);
*/

$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 3);

$link = DB::connect();

$pdf = new PDF("L");
$pdf->page_footer = $pdf_footer;


$filter = ($ida = $_GET['ida']) ? " AND id = {$ida} " : '';
$str = "SELECT id, agencia, codigo, comision FROM agencias WHERE NOT id_central AND status {$filter} ORDER BY id";
$res = mysql_query($str, $link);
while($row = mysql_fetch_object($res)){


    $str = "SELECT
        ventas.id, IFNULL(pagos.modificado, pagos.creado) fecha, tipo_ventas.tipo,
        agencias.id id_agencia, CONCAT(IF(agencias.id_central, ' -> ', ''), agencias.agencia) agencia,
        CONCAT(clientes.nombre, ' ', clientes.apellido) cliente,
        
        @wallet := agencias.id_central AND NOT agencias.convenio,
        
        @amount := pagos.monto - pagos.costo monto,
        @cash := IF(pagos.id_tipo, 0, pagos.monto - IF(@wallet AND agencias.id = reciben.id_agencia, (pagos.monto * agencias.comision) / 100, 0)) cash,
        @card := IF(pagos.id_tipo, @amount, 0) card,
        @central := @amount * IF(agencias.id_central, centrales.comision - agencias.comision, agencias.comision) / 100 comision_central,
        @sub := IF(agencias.id_central, IF(pagos.id_tipo OR agencias.id != reciben.id_agencia OR NOT (pagos.id_tipo OR @wallet), @amount * agencias.comision / 100, 0), 0) comision_subagencia,
        IF(pagos.id_tipo, @card, @cash) - @central - @sub neto,
        
        log_transacciones.referencia
        
    FROM ventas
        INNER JOIN tipo_ventas ON ventas.id_tipo = tipo_ventas.id
        INNER JOIN usuarios ON ventas.id_usuario = usuarios.id
        INNER JOIN agencias ON usuarios.id_agencia = agencias.id
        INNER JOIN clientes ON ventas.id_cliente = clientes.id
        INNER JOIN pagos ON ventas.id = pagos.id_venta
        INNER JOIN usuarios reciben ON pagos.id_usuario = reciben.id
        
        LEFT JOIN agencias centrales ON agencias.id_central = centrales.id
        
        LEFT JOIN log_transacciones ON pagos.referencia = log_transacciones.referencia
        
    WHERE
        {$row->id} IN (agencias.id, agencias.id_central)
        AND DATE(IFNULL(pagos.modificado, pagos.creado)) BETWEEN '" . $_GET['f1'] . "' AND '" . $_GET['f2'] . "' AND NOT pagos.pendiente AND pagos.status
        AND ventas.status";

	$res2 = mysql_query($str, $link);
	if(mysql_num_rows($res2)) {

		unset($data);

		$agencia = "AGENCIA: {$row->agencia} ({$row->codigo}) | " . numFormat($row->comision) . '%';
		$pdf->title = [$title, "REPORTE SEMANAL DE COMISIONES: [{$f1} / {$f2}]"];
		$pdf->AddPage();

		$pdf->SetFillColor(220);
		$pdf->SetFontSize(10);
		$pdf->Cell(756, 25, ' ' . utf8_decode($agencia), 0, 1, 'L', 1);

		while($item = mysql_fetch_object($res2)){

			$data[] = [formatCode($item->id), $item->fecha, $item->tipo, utf8_decode($item->agencia), utf8_decode($item->cliente), numFormat($item->monto),  numFormat($item->cash), numFormat($item->card), numFormat($item->comision_central), numFormat($item->comision_subagencia), numFormat($item->neto)];

			$agencias[$row->id][$item->id_agencia] = $item->agencia;

			$total[$row->id][$item->id_agencia] += $item->monto;
			$tcash[$row->id][$item->id_agencia] += $item->cash;
			$tcard[$row->id][$item->id_agencia] += $item->card;
			$tcentral[$row->id][$item->id_agencia] += $item->comision_central;
			$tsub[$row->id][$item->id_agencia] += $item->comision_subagencia;
			$tneto[$row->id][$item->id_agencia] += $item->neto;

		}

		$header = ['  No.  ', 'FECHA / HORA', 'TIPO', 'AGENCIA', '  CLIENTE  ', '  MONTO  ', 'EFECTIVO', 'TARJETA', 'COMISION CENTRAL', 'COMISION SUBAG', '   NETO   '];
		$footer = ['', '', '', 'TOTALES USD:', '', numFormat(array_sum($total[$row->id])), numFormat(array_sum($tcash[$row->id])), numFormat(array_sum($tcard[$row->id])), numFormat(array_sum($tcentral[$row->id])), numFormat(array_sum($tsub[$row->id])), numFormat(array_sum($tneto[$row->id]))];
		$pdf->ImprovedTable($header, $data, $footer);



		$pdf->Ln(30);

        $pdf->SetFillColor(220);
        $pdf->SetFontSize(12);
        $pdf->Cell(500, 25, ' RESUMEN', 0, 1, 'C', 1);

        $pdf->SetFontSize(9);
        $pdf->SetLineWidth(2);
        $pdf->Cell(350, 20, 'AGENCIA', 'B', 0, 'C');
        $pdf->Cell(150, 20, 'COMISION', 'B', 1, 'C');

        $resumen[$row->id][] = [$row->agencia, numFormat(array_sum($tcentral[$row->id]))];
        foreach($agencias[$row->id] as $key => $value)
            if(+$row->id !== $key)
                $resumen[$row->id][] = [$value, numFormat($tsub[$row->id][$key])];

        $pdf->SetLineWidth(.1);
        foreach($resumen[$row->id] as $key => $value) {
            $pdf->Cell(350, 20, $value[0], ($key ? 'T' : 0), 0, 'L');
            $pdf->Cell(150, 20, $value[1], ($key ? 'T' : 0), 1, 'R');
        }

        $pdf->SetLineWidth(1);
        $pdf->Cell(350, 20, 'TOTAL USD: ', 'TB', 0, 'R', 1);
        $pdf->Cell(150, 20, numFormat(array_sum($tcentral[$row->id]) + array_sum($tsub[$row->id])), 'TB', 1, 'R', 1);


    }

}

$pdf->Output();

?>