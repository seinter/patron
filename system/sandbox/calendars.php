<?php

function monthName($m){
	$marr = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	return $marr[$m];
}

function monthPullDown($month){
	echo "\n<select name=\"m\" onChange=\"this.form.submit()\" style='width:100px;'>\n";
	for($i=0;$i < 12; $i++) {
		if ($i != ($month - 1))
			echo "	<option value=\"".($i+1)."\">".monthName($i)."</option>\n";
		else
			echo "	<option value=\"".($i+1)."\" selected>".monthName($i)."</option>\n";
	}
	echo "</select>\n\n";
}

function yearPullDown($year){
	echo "<select name=\"y\" onChange=\"this.form.submit()\" style='width:80px;'>\n";
	$z = 3;
	for($i=1;$i < 8; $i++) {
		if ($z == 0)
			echo "	<option value=\"" . ($year - $z) . "\" selected>" . ($year - $z) . "</option>\n";
		else
			echo "	<option value=\"" . ($year - $z) . "\">" . ($year - $z) . "</option>\n";
		$z--;
	}
	echo "</select>\n\n";
}

function drawCalendar($q="", $m, $y, $type=0, $d=0, $w=0) {

	define("ADAY", (60*60*24));
	if($type){
		$cbg1 = "#FFFFFF";
		$cbg2 = "#F5CBB1";
		$cbg3 = "#FAE2D3";
	} else {
		$cbg1 = "#FFFFFF";
		$cbg2 = "#CCCCCC";
		$cbg3 = "#F1F1F1";
	}
	
	$start = mktime (12, 0, 0, $m, 1, $y);
	$firstDayArray = getdate($start);
	$curday = ($m==date("n")&&$y==date("Y"))?date("d"):0;

	$seldate = getdate(mktime (12, 0, 0, $m, ($d?$d:1), $y));
	$seldate = getdate(mktime (12, 0, 0, $m, ($seldate["mday"]-$seldate["wday"]<1?1:$seldate["mday"]-$seldate["wday"]), $y));

	$days = Array("D", "L", "M", "M", "J", "V", "S");
	echo "<table cellpadding=5 width=\"100%\"><tr><td>\n";
	echo "<table cellspacing=1 cellpadding=0 bgcolor=\"".($type?$cbg2:$cbg1)."\" width=\"100%\" align=center>";
	if($type){
		echo "<tr><td colspan=7>";
		echo "<table width=\"100%\">\n";
		echo "<form action=\"?\" method=\"post\"><tr><td align=center>\n";
		echo "<input type=\"hidden\" name=\"d\" value=\"1\">\n";
		monthPullDown($m);
		yearPullDown($y);
		echo "</td></tr></form></table>\n";
		echo "</td></tr>";
	} else {
		echo "<tr class=\"forText\"><td bgcolor=\"".$cbg3."\" colspan=7 align=center>".monthName($m-1)."&nbsp;".$y."</td></tr>";
	}
	echo "<tr class=\"forText\">\n";
	foreach ($days as $day) {
		echo "<td align=center><strong>$day</strong></td>\n";
	}
	for ($count=0; $count < (6*7); $count++) {
		$dayArray = getdate($start);
		if (($count % 7) == 0) {
			if($selweek)
				$selweek = false;
			if ($dayArray['mon'] != $m)
				break;
			else
				echo "</tr><tr class=\"forText\">\n";
		}
		
		if(!$selweek && ($count-$firstDayArray["wday"]+1)==$seldate["mday"])
			$selweek = true;
		
		if($count < $firstDayArray['wday'] || $dayArray['mon'] != $m)
			echo "<td bgcolor=\"".$cbg3."\">&nbsp;</td>\n";
		else {
			if($w)
				echo "<td bgcolor=\"".($dayArray['mday']==$curday?$cbg2:($selweek?$cbg3:$cbg1))."\" align=center class=\"setPointer\"><a href=\"?".$q."m=$m&y=$y&d=".$dayArray['mday']."\">".$dayArray['mday']."</a></td>\n";
			else
				echo "<td bgcolor=\"".($dayArray['mday']==$curday?$cbg2:($dayArray['mday']==$d?$cbg3:$cbg1))."\" align=center class=\"setPointer\"><a href=\"?".$q."m=$m&y=$y&d=".$dayArray['mday']."\">".$dayArray['mday']."</a></td>\n";
			$start += ADAY;
		}
	}
	echo "</tr></table>";
	echo "</td></tr></table>";
}

?>