<?php
session_start();
require("config.php");
include("classes/system.inc.php");
include("functions.php");
$user = unserialize($_SESSION[$pfix."user"]);
User::authorize($user, 3);

$link = DB::connect();

?>
<?php include 'header.php' ?>

<style type="text/css">
    @media (max-width: 576px) {
        .text-cell {
            line-height: 30px;
        }
    }
</style>
<form id="rep" name="rep" method="post" action="viewer.php" target="_blank" onsubmit="return false">
    <input type="hidden" name="dll" value="20" />

<div class="container-fluid">

    <div class="row main-title">
        <div class="col text-right text-truncate caption">REPORTE SEMANAL DE COMISIONES</div>
    </div>
    <div class="row main-content">
        <div class="col table-responsive">
            <div class="row">

                <table width="100%" border="0" cellspacing="10" cellpadding="0">
                    <tr>

                        <td style="padding:0">
                            <div class="alert alert-dark big-box">

                                <div class="row">
                                    <div class="col-4 text-right" style="line-height:38px">
                                        <h4 style="margin:5px 0">AGENCIA</h4>
                                    </div>
                                    <div class="col-8 big-combo">
                                        <select name="ida" id="ida" style="width:100%">
                                            <option value="0">&nbsp;- - - TODAS - - - </option>
                                            <?php
                                            $str = "select paises.id, pais from paises inner join agencias on paises.id = agencias.id_pais where agencias.status group by paises.id order by pais;";
                                            $res = mysql_query($str, $link);
                                            while($row = mysql_fetch_row($res)){
                                                ?>
                                                <optgroup label="<?php echo $row[1] ?>">
                                                    <?php
                                                    $str = "select id, agencia from agencias where id_pais = {$row[0]} and status order by agencia";
                                                    $res2 = mysql_query($str, $link);
                                                    while($row2 = mysql_fetch_row($res2)){
                                                        ?>
                                                        <option value="<?php echo $row2[0] ?>"><?php echo $row2[1] ?></option>
                                                    <?php } ?>
                                                </optgroup>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="data-form">

                                <caption>SELECCIONAR SEMANA</caption>

                                <tr>
                                    <th class="text-right text-cell" style="min-width:30%">Semana (Lunes / Domingo):</th>
                                    <td width="30%">
                                        <input name="f" class="form-control form-control-sm" data-weekpicker="weekpicker" data-months="1" data-week_start="1" style="cursor:pointer" readonly/>
                                    </td>
                                </tr>

                                <tr>
                                    <th colspan="2" class="text-center">
                                        <div class="row d-sm-none">
                                            <div class="col-12">
                                                <button type="button" class="btn btn-success print w-100 excel"><i class="fa fa-file-excel-o" style="margin-right:5px"></i> Generar</button>
                                            </div>
                                            <div class="col-12" style="margin-top:5px">
                                                <button type="button" class="btn btn-danger print w-100 pdf"><i class="fa fa-file-pdf-o" style="margin-right:5px"></i> Generar</button>
                                            </div>
                                        </div>
                                        <div class="d-none d-sm-block">
                                            <button type="button" class="btn btn-success print excel"><i class="fa fa-file-excel-o" style="margin-right:5px"></i> Generar</button>
                                            <button type="button" class="btn btn-danger print pdf"><i class="fa fa-file-pdf-o" style="margin-right:5px"></i> Generar</button>
                                        </div>
                                    </th>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>


    <link rel="stylesheet" href="assets/js/week-picker/week-picker-view.css">
    <script type="text/javascript" src="assets/js/week-picker/week-picker.js"></script>

    <link rel="stylesheet" href="assets/js/select2/select2.min.css">
    <script type="text/javascript" src="assets/js/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('select').select2()

            $('.date-field input').click(function() {
                showCalendar(this, this, 'yyyy-mm-dd','es',1)
            })

            $('.excel').click(function(){
                if(validate(this.form)) {
                    this.form.action = 'rep_comisiones_global_xls.php'
                    this.form.submit()
                }
            })

            $('.pdf').click(function(){
                if(validate(this.form)) {
                    this.form.action = 'viewer.php'
                    this.form.submit()
                }
            })
        })
    </script>

</div>
</form>

<?php include 'footer.php' ?>